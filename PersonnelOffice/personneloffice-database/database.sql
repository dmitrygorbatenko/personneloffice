-- MySQL dump 10.13  Distrib 5.5.15, for Win32 (x86)
--
-- Host: localhost    Database: person_office
-- ------------------------------------------------------
-- Server version	5.5.15

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authority`
--

DROP TABLE IF EXISTS `authority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authority` (
  `role` tinytext,
  `auth_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`auth_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authority`
--

LOCK TABLES `authority` WRITE;
/*!40000 ALTER TABLE `authority` DISABLE KEYS */;
INSERT INTO `authority` VALUES ('ROLE_ADMIN',1),('ROLE_USER',2);
/*!40000 ALTER TABLE `authority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cards_orders_xref`
--

DROP TABLE IF EXISTS `cards_orders_xref`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cards_orders_xref` (
  `person_card_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cards_orders_xref`
--

LOCK TABLES `cards_orders_xref` WRITE;
/*!40000 ALTER TABLE `cards_orders_xref` DISABLE KEYS */;
/*!40000 ALTER TABLE `cards_orders_xref` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person_cards`
--

DROP TABLE IF EXISTS `person_cards`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person_cards` (
  `person_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `patronymic` varchar(25) DEFAULT NULL,
  `birthplace_country` varchar(25) DEFAULT NULL,
  `birthplace_province` varchar(25) DEFAULT NULL,
  `birthplace_region` varchar(25) DEFAULT NULL,
  `birthplace_locality` varchar(25) DEFAULT NULL,
  `education` varchar(25) DEFAULT NULL,
  `academic_rank` varchar(25) DEFAULT NULL,
  `learning_form` varchar(25) DEFAULT NULL,
  `family_status` varchar(25) DEFAULT NULL,
  `position` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`person_card_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person_cards`
--

LOCK TABLES `person_cards` WRITE;
/*!40000 ALTER TABLE `person_cards` DISABLE KEYS */;
INSERT INTO `person_cards` VALUES (1,'ваня','пупкин','MALE','6757567','1984-06-11',NULL,NULL,'','bvtg','dbhrtdshb','rthrth','rthrthrt','HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(2,'Анна','кукин','FEMALE','4677','1988-06-03','2014-05-16','2015-01-14','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(3,'Александр','пупкин','MALE','6756756','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(4,'Дмитрий','пупкин','MALE','123','1983-06-09','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(5,'Сергей','пупкин','MALE','56756','1986-06-13','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(6,'Олег','пупкин','MALE','896784','1984-10-23','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(7,'Владислав','пупкин','MALE','345563','1982-05-31',NULL,NULL,'',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(8,'Юрий','пупкин','MALE','13534','1984-10-14','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(9,'Ольга','пупкин','FEMALE','845613','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(10,'Елена','пупкин','FEMALE','53634','1984-06-11','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(11,'Екатерина','пупкин','FEMALE','874423','1987-06-13','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(12,'Наталия','пупкин','FEMALE','295478','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(13,'Виктория','пупкин','FEMALE','2698414','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(14,'Дмитрий','кин','MALE','662594','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(15,'Юлия','пупкин','FEMALE','362147','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(16,'Евгений','пупкин','MALE','7985146','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(17,'Федор','пупкин','MALE','6632588','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(18,'ваня','пупкин','MALE','798621','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(19,'ваня','пупкин','MALE','59646','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER'),(20,'ваня','пупкин','MALE','98846','1984-06-20','2012-08-02','2014-12-26','',NULL,NULL,NULL,NULL,'HIGHER','PhD','FULL_TIME','SINGLE','DEVELOPER');
/*!40000 ALTER TABLE `person_cards` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  `auth_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','admin','Вася','Пупкин','',1);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-07-01 19:35:02
