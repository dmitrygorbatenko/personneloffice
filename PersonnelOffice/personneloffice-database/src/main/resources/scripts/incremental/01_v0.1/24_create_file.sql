CREATE TABLE `file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `extension` varchar(10) NOT NULL,
  `content` LONGTEXT NOT NULL,
  PRIMARY KEY (`file_id`)
);

CREATE TABLE `person_card_file` (
  `file_id` int(11) NOT NULL,
  `person_card_id` int(11) NOT NULL,
  PRIMARY KEY (`file_id`, `person_card_id`)
); 

ALTER TABLE `person_card_file`
  ADD CONSTRAINT `fk_person_card_file__file__person_card_id`  FOREIGN KEY (`file_id` )  REFERENCES `file` (`file_id` )    ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_person_card_file__file__person_card_id` (`file_id` ASC);

ALTER TABLE `person_card_file`
  ADD CONSTRAINT `fk_person_card_file__person_card__person_card_id`  FOREIGN KEY (`person_card_id`)  REFERENCES `person_card` (`person_card_id` )    ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_person_card_file__person_card__person_card_id` (`person_card_id` ASC);