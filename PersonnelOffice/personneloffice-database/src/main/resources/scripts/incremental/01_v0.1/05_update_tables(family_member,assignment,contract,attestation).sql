ALTER TABLE family_member   CHANGE family_member_id   family_member_id INT NOT NULL AUTO_INCREMENT;
ALTER TABLE contract        CHANGE contract_id        contract_id INT NOT NULL AUTO_INCREMENT;
ALTER TABLE attestation     CHANGE attestation_id     attestation_id INT NOT NULL AUTO_INCREMENT;
ALTER TABLE assignment      CHANGE assignment_id      assignment_id INT NOT NULL AUTO_INCREMENT;