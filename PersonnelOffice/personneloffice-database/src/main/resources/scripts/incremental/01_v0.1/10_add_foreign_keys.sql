ALTER TABLE `person_card`
  ADD CONSTRAINT `fk_person_card_address_registration_address_id`  FOREIGN KEY (`registration_address_id` )  REFERENCES `address` (`address_id` )  ON DELETE RESTRICT  ON UPDATE RESTRICT, 
  ADD CONSTRAINT `fk_person_card_address_residential_address_id`  FOREIGN KEY (`residential_address_id` )  REFERENCES `address` (`address_id` )  ON DELETE RESTRICT  ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_person_card_military_military_id`  FOREIGN KEY (`military_id` )  REFERENCES `military` (`military_id` )  ON DELETE RESTRICT  ON UPDATE RESTRICT,
  ADD INDEX `fk_person_card_military_military_id` (`military_id` ASC),
  ADD INDEX `fk_person_card_address_registration_address_id` (`registration_address_id` ASC),
  ADD INDEX `fk_person_card_address_residential_address_id` (`residential_address_id` ASC);
  
DELETE FROM  `assignment` WHERE `person_card_id` NOT IN(SELECT `person_card_id` from `person_card`);
ALTER TABLE `assignment` 
  ADD CONSTRAINT `fk_assignment_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )    ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_assignment_person_card_person_card_id` (`person_card_id` ASC);

DELETE FROM  `attestation` WHERE `person_card_id` NOT IN(SELECT `person_card_id` from `person_card`);
ALTER TABLE `attestation` 
  ADD CONSTRAINT `fk_attestation_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )  ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_attestation_person_card_person_card_id` (`person_card_id` ASC);

DELETE FROM  `family_member` WHERE `person_card_id` NOT IN(SELECT `person_card_id` from `person_card`);
ALTER TABLE `family_member` 
  ADD CONSTRAINT `fk_family_member_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )  ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_family_member_person_card_person_card_id` (`person_card_id` ASC);

DELETE FROM  `contract` WHERE `person_card_id` NOT IN(SELECT `person_card_id` from `person_card`);
ALTER TABLE `contract` 
  ADD CONSTRAINT `fk_contract_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )  ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_contract_person_card_person_card_id` (`person_card_id` ASC);

DELETE FROM  `card_order_xref` WHERE `order_id` NOT IN(SELECT `order_id` from `orders`) or `person_card_id` NOT IN(SELECT `person_card_id` from `person_card`);
ALTER TABLE `card_order_xref` 
  ADD CONSTRAINT `fk_card_order_xref_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )  ON DELETE CASCADE  ON UPDATE RESTRICT,
  ADD CONSTRAINT `fk_card_order_xref_orders_order_id`  FOREIGN KEY (`order_id` )  REFERENCES `orders` (`order_id` )  ON DELETE CASCADE  ON UPDATE RESTRICT,
  ADD INDEX `fk_card_order_xref_person_card_person_card_id` (`person_card_id` ASC),
  ADD INDEX `fk_card_order_xref_orders_order_id` (`order_id` ASC);
  
DELETE FROM `user_role_xref` WHERE `user_id` NOT IN(SELECT `user_id` from `user`) or `role_id` NOT IN(SELECT `role_id` from `role`);
ALTER TABLE `user_role_xref` 
  ADD CONSTRAINT `fk_user_role_xref_user_user_id`  FOREIGN KEY (`user_id` )  REFERENCES `user` (`user_id` )  ON DELETE CASCADE  ON UPDATE RESTRICT, 
  ADD CONSTRAINT `fk_user_role_xref_role_role_id`  FOREIGN KEY (`role_id` )  REFERENCES `role` (`role_id` )  ON DELETE CASCADE  ON UPDATE RESTRICT,
  ADD INDEX `fk_user_role_xref_user_user_id` (`user_id` ASC),
  ADD INDEX `fk_user_role_xref_role_role_id` (`role_id` ASC);

DELETE FROM  `permission` WHERE `role_id` NOT IN(SELECT `role_id` from `role`);
ALTER TABLE `permission` 
  ADD CONSTRAINT `fk_permission_role_role_id`  FOREIGN KEY (`role_id` )  REFERENCES `role` (`role_id` )  ON DELETE CASCADE  ON UPDATE RESTRICT,
  ADD INDEX `fk_permission_role_role_id` (`role_id` ASC);
