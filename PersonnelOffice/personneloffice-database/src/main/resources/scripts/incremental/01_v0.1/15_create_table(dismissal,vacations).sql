CREATE  TABLE `dismissal` (
  `dismissal_id` INT NOT NULL ,
  `person_card_id` INT NOT NULL ,
  `order_date` DATE NULL ,
  `order_number` VARCHAR(45) NULL ,
  `reason`  VARCHAR(45)  NULL ,
  `dismissal_date` DATE NULL ,
  `compensation_days` INT NULL ,

  PRIMARY KEY (`dismissal_id`) );

CREATE  TABLE `vacation` (
  `vacation_id` INT NOT NULL ,
  `person_card_id` INT NOT NULL ,
  `period_from` DATE NULL ,
  `period_to` DATE NULL ,
  `vacation_type` VARCHAR(45) NULL ,
  `vacation_from` DATE NULL ,
  `vacation_to` DATE NULL ,
  `duration` INT NULL ,
  `order_number` VARCHAR(45) NULL ,
  `order_date` DATE NULL ,

  PRIMARY KEY (`vacation_id`) );

ALTER TABLE `dismissal`
  ADD CONSTRAINT `fk_dismissal_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )    ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_dismissal_person_card_person_card_id` (`person_card_id` ASC);

ALTER TABLE `vacation`
  ADD CONSTRAINT `fk_vacation_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )    ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_vacation_person_card_person_card_id` (`person_card_id` ASC);
