
-- --------------
-- - TRIGGERS ---
-- --------------
CREATE TRIGGER tr__change_card_status_on_xref_insert BEFORE INSERT ON card_document_xref
FOR EACH ROW
  BEGIN
    DECLARE v_species varchar(50);
    SET v_species = (select species
                     from document
                     where document_id = NEW.document_id);
    UPDATE person_card pc
    SET pc.status = CASE v_species
                    WHEN "WORKER" THEN "WORKER"
                    WHEN "DISMISSAL" THEN "DISMISSED" END
    WHERE pc.person_card_id = NEW.person_card_id and
          v_species in ("WORKER", "DISMISSAL");
  END;
/

CREATE TRIGGER tr__on_doc_delete_with_cascade_xref_delete BEFORE DELETE ON card_document_xref
FOR EACH ROW
  BEGIN
    DECLARE max_doc_dt date; -- max doc date whithin person's docs
    DECLARE max_doc_id int; -- latest doc_id whithin person's (if there are several docs with max dt) -- currently date is only stored, but not both date and time
    DECLARE v_species varchar(50);
    SET max_doc_dt = (select max(d.paper_date)
                      from card_document_xref cd
                        join document d on cd.document_id = d.document_id
                      where cd.person_card_id = OLD.person_card_id and
                            cd.document_id <> OLD.document_id
                      group by cd.person_card_id);

    SET max_doc_id = (select max(d.document_id)
                      from document d
                        join card_document_xref cd on d.document_id = cd.document_id
                      where cd.person_card_id = OLD.person_card_id and
                            cd.document_id <> OLD.document_id and
                            d.paper_date = max_doc_dt);

    SET v_species = (select species
                     from document
                     where document_id = max_doc_id);

    UPDATE person_card pc
    SET pc.status = CASE v_species
                    WHEN "WORKER" THEN "WORKER"
                    WHEN "DISMISSAL" THEN "DISMISSED"
                    ELSE "CANDIDATE" END
    WHERE pc.person_card_id = OLD.person_card_id;
  END;
/

CREATE TRIGGER tr_delete_doc_related_xrefs BEFORE DELETE ON document -- CASCADE DELETE DO NOT TRIGGER THE DELETE EVENT in MySQL, SO WE HAVE TO DELETE MANUALLY BEFORE TO TRIGGER THE EVENTS
FOR EACH ROW
  BEGIN
    DELETE FROM card_document_xref WHERE document_id = OLD.document_id;
  END;
/

-- ----------------
-- - STATEMENTS ---
-- ----------------

UPDATE document
SET species = "WORKER";

UPDATE person_card
SET status = "WORKER";

update person_card
set status = 'WORKER';

update person_card
set gender = 'MALE'
where person_card_id in (2, 11, 12, 13, 15);

update document
set species = 'WORKER',
  paper_date = '2012-01-01';


-- ----------------
-- - PROCEDURES ---
-- ----------------
-- SP №1
-- CALL get_report_active_card_count_up_to_date(@count, 2015, 1);
-- CALL get_report_active_card_count_up_to_date(@count, 2014, 1);
-- CALL get_report_active_card_count_up_to_date(@count, 2013, 1);
-- SELECT @count;
-- --------------------------------------------------------------------------------
-- Routine DDL. EXEC: CALL get_report_active_card_count_up_to_date(@count, 2015, 1);
-- Note: FOR EACH PERSON SELECT MAX DOC DT, JOIN DOC FIELDS, COUNT WORKERS UP TO SOME DATE
-- --------------------------------------------------------------------------------
CREATE PROCEDURE `get_report_active_card_count_up_to_date`(OUT count INT, year INT, month INT)
  BEGIN
    SET count = (select count(distinct cd.person_card_id) -- cd.person_card_id, d.document_id, d.species, d.paper_date
                 from document d
                   join card_document_xref cd on d.document_id = cd.document_id
                   join  (select cd.person_card_id, max(d.paper_date) as max_doc_dt
                          from document d
                            join card_document_xref cd on d.document_id = cd.document_id
                          where d.paper_date < CONCAT(year, '-', LPAD(month, 2, '00'), '-01')
                          group by cd.person_card_id) as x
                     on x.person_card_id = cd.person_card_id and
                        d.paper_date = x.max_doc_dt
                 where d.species = "WORKER" and
                       cd.person_card_id not in (select pc.person_card_id -- , d.document_id, d.species, d.paper_date
                                                 from person_card pc
                                                   join card_document_xref cd on pc.person_card_id = cd.person_card_id
                                                   join document d on d.document_id = cd.document_id
                                                 where d.species = "DISMISSAL" and
                                                       d.paper_date < CONCAT(year, '-', LPAD(month, 2, '00'), '-01')));
  END;
/


-- PROC #2
-- SP №2
-- CALL get_report_active_card_count_up_to_date_and_by_gender(@count, "MALE", 2015, 1); -- 6
-- CALL get_report_active_card_count_up_to_date_and_by_gender(@count, "FEMALE", 2015, 1); -- 3
-- SELECT @count;
-- --------------------------------------------------------------------------------
-- Routine DDL. EXEC: CALL get_report_active_card_count_up_to_date_and_by_gender(@count, "MALE", 2015, 1);
-- Note: MOSTLY DUPLICATES get_report_active_card_count_up_to_date
-- --------------------------------------------------------------------------------
CREATE PROCEDURE `get_report_active_card_count_up_to_date_and_by_gender`(OUT count INT, gender VARCHAR(10), year INT, month INT)
  BEGIN
    SET count = (select count(distinct cd.person_card_id) -- cd.person_card_id, d.document_id, d.species, d.paper_date
                 from document d
                   join card_document_xref cd on d.document_id = cd.document_id
                   join person_card pc on pc.person_card_id = cd.person_card_id
                   join  (select cd.person_card_id, max(d.paper_date) as max_doc_dt
                          from document d
                            join card_document_xref cd on d.document_id = cd.document_id
                          where d.paper_date < CONCAT(year, '-', LPAD(month, 2, '00'), '-01')
                          group by cd.person_card_id) as x
                     on x.person_card_id = cd.person_card_id and
                        d.paper_date = x.max_doc_dt
                 where d.species = "WORKER" and
                       pc.gender = gender and
                       cd.person_card_id not in (select pc.person_card_id -- , d.document_id, d.species, d.paper_date
                                                 from person_card pc
                                                   join card_document_xref cd on pc.person_card_id = cd.person_card_id
                                                   join document d on d.document_id = cd.document_id
                                                 where d.species = "DISMISSAL" and
                                                       d.paper_date < CONCAT(year, '-', LPAD(month, 2, '00'), '-01')));
  END;
/


-- PROC #3
-- SP №3
-- CALL get_active_count_for_specific_gender_year_month(@count, "MALE", 2014, 8); -- 6
-- CALL get_active_count_for_specific_gender_year_month(@count, "MALE", 2014, 9); -- 0
-- SELECT @count;
-- --------------------------------------------------------------------------------
-- Routine DDL. EXEC: CALL get_active_count_for_specific_gender_year_month(@count, "MALE", 2014, 8);
-- IMPORTANT! DOUBLE WORK MUST BE DISABLED FOR EACH PERSON - TRIGGER IS REQUIRED;
-- Note: Get active (workers) for specific months;
-- --------------------------------------------------------------------------------
CREATE PROCEDURE `get_active_count_for_specific_gender_year_month`(OUT count INT, gender VARCHAR(10), year INT, month INT)
  BEGIN
    SET count = (select count(distinct cd.person_card_id)
                 from document d
                   join card_document_xref cd on d.document_id = cd.document_id
                   join person_card pc on pc.person_card_id = cd.person_card_id
                   join   (select cd.person_card_id, max(d.paper_date) as max_doc_dt
                           from document d
                             join card_document_xref cd on d.document_id = cd.document_id
                           where YEAR(d.paper_date) = year AND
                                 MONTH(d.paper_date) = month
                           group by cd.person_card_id) as x
                     on x.person_card_id = cd.person_card_id and
                        d.paper_date = x.max_doc_dt
                 where d.species = "WORKER" and
                       pc.gender = gender and
                       cd.person_card_id not in (select pc.person_card_id
                                                 from person_card pc
                                                   join card_document_xref cd on pc.person_card_id = cd.person_card_id
                                                   join document d on d.document_id = cd.document_id
                                                 where d.species = "DISMISSAL" and
                                                       YEAR(d.paper_date) = year AND
                                                       MONTH(d.paper_date) = month));
  END;
/

-- 4
-- --------------------------------------------------------------------------------
-- Routine DDL. EXEC: CALL CALL get_dismissed_count_for_specific_gender_year_month(@count, "MALE", 2014, 8);
-- IMPORTANT! DOUBLE DISMISSED MUST BE DISABLED FOR EACH PERSON - TRIGGER IS REQUIRED;
-- Note: Get dismissed for specific months;
-- --------------------------------------------------------------------------------
CREATE PROCEDURE `get_dismissed_count_for_specific_gender_year_month`(OUT count INT, gender VARCHAR(10), year INT, month INT)
  BEGIN
    SET count = (select count(distinct cd.person_card_id)
                 from document d
                   join card_document_xref cd on d.document_id = cd.document_id
                   join person_card pc on pc.person_card_id = cd.person_card_id
                   join   (select cd.person_card_id, max(d.paper_date) as max_doc_dt
                           from document d
                             join card_document_xref cd on d.document_id = cd.document_id
                           where YEAR(d.paper_date) = year AND
                                 MONTH(d.paper_date) = month
                           group by cd.person_card_id) as x
                     on x.person_card_id = cd.person_card_id and
                        d.paper_date = x.max_doc_dt
                 where pc.gender = gender and
                       cd.person_card_id in (select pc.person_card_id
                                             from person_card pc
                                               join card_document_xref cd on pc.person_card_id = cd.person_card_id
                                               join document d on d.document_id = cd.document_id
                                             where d.species = "DISMISSAL" and
                                                   YEAR(d.paper_date) = year AND
                                                   MONTH(d.paper_date) = month));
  END;
/