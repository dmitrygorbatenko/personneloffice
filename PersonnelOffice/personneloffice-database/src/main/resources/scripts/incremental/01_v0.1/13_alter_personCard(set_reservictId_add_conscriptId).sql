ALTER TABLE person_card ADD conscript_id INT DEFAULT NULL;
ALTER TABLE person_card CHANGE military_id reservist_id INT DEFAULT NULL;

ALTER TABLE `person_card`
  ADD CONSTRAINT `fk_person_card_military_conscript_id`  FOREIGN KEY (`conscript_id` )  REFERENCES `military` (`military_id` )
  ON DELETE RESTRICT  ON UPDATE RESTRICT,
  ADD INDEX `fk_person_card_military_conscript_id` (`conscript_id` ASC);
