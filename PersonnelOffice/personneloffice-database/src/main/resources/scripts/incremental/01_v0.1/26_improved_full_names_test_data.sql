UPDATE person_card pc
JOIN (
	SELECT  1 as person_card_id,
			'Антон' as first_name,
			'Чехов' as last_name,
			'Павлович' as patronymic
	UNION
	SELECT  2, 'Сухой', 'Павел', 'Осипович' UNION
	SELECT  3, 'Александр', 'Пушкин', 'Сергеевич' UNION
	SELECT  4, 'Андрей', 'Болконский', 'Александрович' UNION
	SELECT  5, 'Пьер', 'Безухов', 'Сергеевич' UNION
	SELECT  6, 'Аркадий', 'Шипунов', 'Георгиевич' UNION
	SELECT  7, 'Владислав', 'Иванов', 'Иванович' UNION
	SELECT  8, 'Юрий', 'Гагарин', 'Алексеевич' UNION
	SELECT  9, 'Наталья', 'Ростова', 'Юрьевна' UNION
	SELECT 10, 'Елена', 'Сидорова', 'Яновна' UNION
	SELECT 11, 'Королёв', 'Сергей', 'Павлович' UNION
	SELECT 12, 'Леонид', 'Гаев', 'Андреевич' UNION
	SELECT 13, 'Ермолай', 'Лопахин', 'Алексеевич' UNION
	SELECT 14, 'Нина', 'Заречная', 'Михайловна' UNION
	SELECT 15, 'Илья', 'Шамраев', 'Афанасьевич' UNION
	SELECT 16, 'Евгений', 'Евтушенко', 'Александрович' UNION
	SELECT 17, 'Фёдор', 'Емельяненко', 'Владимирович' UNION
	SELECT 18, 'Константин', 'Левин', 'Дмитриевич' UNION
	SELECT 19, 'Алексей', 'Пешков', 'Максимович' UNION
	SELECT 20, 'Михаил', 'Лермонтов', 'Юрьевич'
) x
ON x.person_card_id = pc.person_card_id
SET pc.first_name = x.first_name,
    pc.last_name = x.last_name,
    pc.patronymic = x.patronymic;

UPDATE assignment
SET position = 'SENIOR_SOFTWARE_ENGINEER';
