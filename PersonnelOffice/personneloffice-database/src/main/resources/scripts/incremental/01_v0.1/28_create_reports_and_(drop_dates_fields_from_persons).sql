create table `report` (
  `report_id` INT NOT NULL AUTO_INCREMENT,
  `report_type` VARCHAR(45) NULL,
  `last_using_date` DATE NULL,
  `selected_year` VARCHAR(4),
  `selected_month` VARCHAR(45) NULL,

  PRIMARY KEY (`report_id`)
);

alter table person_card drop `start_date`, drop `finish_date`;

insert into report (
  `report_type`,
  `last_using_date`,
  `selected_year`,
  `selected_month`)
values
  ('Месячный', '2014-10-12', 2014, 'SEPTEMBER'),
  ('T6', '2014-08-16', 2014, 'AUGUST');