CREATE  TABLE `family_member` (
  `family_member_id` INT NOT NULL ,
  `person_card_id` INT NOT NULL ,
  `name` VARCHAR(100) NULL ,
  `birthday` DATE NULL ,
  `kinship` VARCHAR(45) NULL ,
  `invalid` BIT NULL DEFAULT 0 ,
  PRIMARY KEY (`family_member_id`) );

  
CREATE  TABLE `assignment` (
  `assignment_id` INT NOT NULL ,
  `person_card_id` INT NOT NULL ,
  `date` DATE NULL ,
  `department` VARCHAR(45) NULL ,
  `position` VARCHAR(45) NULL ,
  `rate`  VARCHAR(45)  NULL ,
  `rank`  VARCHAR(45)  NULL ,
  `order_number` VARCHAR(45) NULL ,
  `order_date` DATE NULL ,
  `material_liability` BIT DEFAULT 0,	
  PRIMARY KEY (`assignment_id`) );
  
  
CREATE  TABLE `contract` (
  `contract_id` INT NOT NULL ,
  `person_card_id` INT NOT NULL ,
  `type` VARCHAR(45) NULL ,
  `document_type` VARCHAR(45) NULL ,
  `document_number` VARCHAR(45) NULL ,
  `start_date` DATE NULL ,
  `duration` INT NULL ,
  `probation` INT NULL ,
  `reason` VARCHAR(45) NULL ,
  PRIMARY KEY (`contract_id`) );
  
 CREATE  TABLE `attestation` (
  `attestation_id` INT NOT NULL ,
  `person_card_id` INT NOT NULL ,
  `date` DATE NULL ,
  `decision` VARCHAR(45) NULL ,
  `document_type` VARCHAR(45) NULL ,
  `document_number` VARCHAR(45) NULL ,
  `document_date` DATE NULL ,
  `next_attestation` DATE NULL ,
  `reason` VARCHAR(45) NULL ,
  PRIMARY KEY (`attestation_id`) );

 
 
  
ALTER TABLE `person_card` DROP COLUMN `position`, ADD COLUMN `allocation` VARCHAR(45) NULL  AFTER `birthday` ;
  
  
INSERT INTO `assignment`	(`assignment_id`,	`person_card_id`,	`date`,			`department`,	`position`,		`rate`,	`order_number`,	`order_date`)
                            VALUES 		(1,					1,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (2,					2,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (3,					3,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (4,					4,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (5,					5,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (6,					6,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (7,					7,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (8,					8,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (9,					9,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (10,				10,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (11,				11,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (12,				12,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (13,				13,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (14,				14,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (15,				15,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (16,				16,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (17,				17,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (18,				18,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (19,				19,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26'),
                                      (20,				20,					'2014-12-26',	'118',			'DEVELOPER',	'1.5',	'14/26',		'2014-12-26');
				
ALTER TABLE `assignment` AUTO_INCREMENT = 21;						


INSERT INTO `contract`		(`contract_id`,	`person_card_id`,	`type`,	`document_type`,	`document_number`,	`start_date`,	`duration`,	`probation`,	`reason`)
				VALUES		( 1, 			1,					'',		'',					1,					'2014-12-26',	2,			2,				''),
							( 2, 			2,					'',		'',					2,					'2014-12-26',	2,			2,				''),
							( 3, 			3,					'',		'',					3,					'2014-12-26',	2,			2,				''),
							( 4, 			4,					'',		'',					4,					'2014-12-26',	2,			2,				''),
							( 5, 			5,					'',		'',					5,					'2014-12-26',	2,			2,				''),
							( 6, 			6,					'',		'',					6,					'2014-12-26',	2,			2,				''),
							( 7, 			7,					'',		'',					7,					'2014-12-26',	2,			2,				''),
							( 8, 			8,					'',		'',					8,					'2014-12-26',	2,			2,				''),
							( 9, 			9,					'',		'',					9,					'2014-12-26',	2,			2,				''),
							( 10, 			10,					'',		'',					10,					'2014-12-26',	2,			2,				''),
							( 11, 			11,					'',		'',					11,					'2014-12-26',	2,			2,				''),
							( 12, 			12,					'',		'',					12,					'2014-12-26',	2,			2,				''),
							( 13, 			13,					'',		'',					13,					'2014-12-26',	2,			2,				''),
							( 14, 			14,					'',		'',					14,					'2014-12-26',	2,			2,				''),
							( 15, 			15,					'',		'',					15,					'2014-12-26',	2,			2,				''),
							( 16, 			16,					'',		'',					16,					'2014-12-26',	2,			2,				''),
							( 17, 			17,					'',		'',					17,					'2014-12-26',	2,			2,				''),
							( 18, 			18,					'',		'',					18,					'2014-12-26',	2,			2,				''),
							( 19, 			19,					'',		'',					19,					'2014-12-26',	2,			2,				''),
							( 20, 			20,					'',		'',					20,					'2014-12-26',	2,			2,				'');
ALTER TABLE `contract` AUTO_INCREMENT = 21;				

INSERT INTO `attestation` 	(`attestation_id`, `person_card_id`,	`date`,			`decision`,	`document_type`,	`document_number`,	`document_date`,	`next_attestation`,	`reason`)
			VALUES			(1,					1,					'2014-04-21',	'ok',		'',					1,					'2014-05-12',		'2015-02-15',		''),
							(2,					2,					'2014-04-21',	'ok',		'',					2,					'2014-05-12',		'2015-02-15',		''),
							(3,					3,					'2014-04-21',	'ok',		'',					3,					'2014-05-12',		'2015-02-15',		''),
							(4,					4,					'2014-04-21',	'ok',		'',					4,					'2014-05-12',		'2015-02-15',		''),
							(5,					5,					'2014-04-21',	'ok',		'',					5,					'2014-05-12',		'2015-02-15',		''),
							(6,					6,					'2014-04-21',	'ok',		'',					6,					'2014-05-12',		'2015-02-15',		''),
							(7,					7,					'2014-04-21',	'ok',		'',					7,					'2014-05-12',		'2015-02-15',		''),
							(8,					8,					'2014-04-21',	'ok',		'',					8,					'2014-05-12',		'2015-02-15',		''),
							(9,					9,					'2014-04-21',	'ok',		'',					9,					'2014-05-12',		'2015-02-15',		''),
							(10,				10,					'2014-04-21',	'ok',		'',					10,					'2014-05-12',		'2015-02-15',		''),
							(11,				11,					'2014-04-21',	'ok',		'',					11,					'2014-05-12',		'2015-02-15',		''),
							(12,				12,					'2014-04-21',	'ok',		'',					12,					'2014-05-12',		'2015-02-15',		''),
							(13,				13,					'2014-04-21',	'ok',		'',					13,					'2014-05-12',		'2015-02-15',		''),
							(14,				14,					'2014-04-21',	'ok',		'',					14,					'2014-05-12',		'2015-02-15',		''),
							(15,				15,					'2014-04-21',	'ok',		'',					15,					'2014-05-12',		'2015-02-15',		''),
							(16,				16,					'2014-04-21',	'ok',		'',					16,					'2014-05-12',		'2015-02-15',		''),
							(17,				17,					'2014-04-21',	'ok',		'',					17,					'2014-05-12',		'2015-02-15',		''),
							(18,				18,					'2014-04-21',	'ok',		'',					18,					'2014-05-12',		'2015-02-15',		''),
							(19,				19,					'2014-04-21',	'ok',		'',					19,					'2014-05-12',		'2015-02-15',		''),
							(20,				20,					'2014-04-21',	'ok',		'',					20,					'2014-05-12',		'2015-02-15',		'');
							
ALTER TABLE `attestation` AUTO_INCREMENT = 21;								
