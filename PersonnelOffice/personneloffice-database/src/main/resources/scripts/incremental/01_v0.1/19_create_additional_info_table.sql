CREATE TABLE `additional_info` (
  `additional_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `person_card_id` int NOT NULL,
  `event_name` varchar(100) NULL,
  `event_dt` date NULL,
  `notification_dt` date NULL,
  PRIMARY KEY (`additional_info_id`)
);

ALTER TABLE `additional_info`
  ADD CONSTRAINT `fk_additional_info_person_card_person_card_id`  FOREIGN KEY (`person_card_id` )  REFERENCES `person_card` (`person_card_id` )  ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_additional_info_person_card_person_card_id` (`person_card_id` ASC);

-- -----------------------------
-- ADDITIONAL_INFO TEST DATA ---
-- -----------------------------
INSERT INTO `additional_info` (`person_card_id`, `event_name`,	`event_dt`,	`notification_dt`)
SELECT person_card_id,
       CONCAT('Event - Project Started: ', last_name, ' ', first_name) as event_name,
       start_date as event_dt,
       DATE_ADD(start_date, INTERVAL -7 DAY) as notification_dt
FROM `person_card`
UNION
SELECT person_card_id,
       CONCAT('Event - Birthday: ', first_name) as event_name,
       birthday as event_dt,
       DATE_ADD(start_date, INTERVAL -7 DAY) as notification_dt
FROM `person_card`;

-- ----------------------------
-- FAMILY_MEMBERS TEST DATA ---
-- ----------------------------
INSERT INTO `family_member` (`person_card_id`, `name`, `birthday`, `kinship`, `invalid`)
SELECT person_card_id,
       'test_relative' as name,
       CURDATE() as birthday,
       'BROTHER' as kinship,
	   0 invalid
FROM `person_card`;