UPDATE `person_card` set `status` = 'CONTRACTOR' where `person_card_id` < 5;
UPDATE `person_card` set `status` = 'DISMISSED' where `person_card_id` between 5 and 7;
UPDATE `person_card` set `status` = 'CANDIDATE' where `person_card_id` between 8 and 10;
UPDATE `person_card` set `status` = 'WORKER' where `person_card_id` between 10 and 20;