--
-- Table structure for table `cards_orders_xref`
--

DROP TABLE IF EXISTS `card_order_xref`;
CREATE TABLE `card_order_xref` (
  `person_card_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(20) DEFAULT NULL,
  `date` DATE DEFAULT NULL,
  `kind` VARCHAR(45) DEFAULT NULL, 
  `reason` VARCHAR(45) DEFAULT NULL,	
  PRIMARY KEY (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `permission`
--

DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission` (
  `permission_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `value` int(11) DEFAULT NULL,
  PRIMARY KEY (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `person_cards`
--

DROP TABLE IF EXISTS `person_card`;
CREATE TABLE `person_card` (
  `person_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `registration_address_id` INT NULL, 
  `residential_address_id` INT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `patronymic` varchar(25) DEFAULT NULL,  
  `card_number` VARCHAR(25) DEFAULT NULL,
  `tab_number` VARCHAR(25) DEFAULT NULL,
  `status` VARCHAR(25) DEFAULT NULL,
  `trade_unionist` BIT default 0 , 
  `gender` varchar(45) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `finish_date` date DEFAULT NULL,
  `birthplace_country` varchar(25) DEFAULT NULL,
  `birthplace_province` varchar(25) DEFAULT NULL,
  `birthplace_region` varchar(25) DEFAULT NULL,
  `birthplace_locality` varchar(25) DEFAULT NULL,
  `education` varchar(25) DEFAULT NULL,
  `academic_rank` varchar(25) DEFAULT NULL,
  `learning_form` varchar(25) DEFAULT NULL,
  `family_status` varchar(25) DEFAULT NULL,
  `position` varchar(25) DEFAULT NULL,  
  `nationality` VARCHAR(45) DEFAULT NULL, 
  `document` VARCHAR(45) DEFAULT NULL, 
  `document_series` VARCHAR(10) DEFAULT NULL, 
  `document_serial_number` VARCHAR(45) DEFAULT NULL, 
  `document_individual_number` VARCHAR(45) DEFAULT NULL, 
  `document_date_issuance` DATE, 
  `document_validity_period` DATE, 
  `document_issued_by` VARCHAR(100) DEFAULT NULL, 
  `insurance_certificate_number` VARCHAR(45)DEFAULT NULL,
  PRIMARY KEY (`person_card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` tinytext,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `first_name` varchar(25) DEFAULT NULL,
  `last_name` varchar(25) DEFAULT NULL,
  `enabled` bit(1) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `login_UNIQUE` (`login`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


--
-- Table structure for table `user_role_xref`
--

DROP TABLE IF EXISTS `user_role_xref`;
CREATE TABLE `user_role_xref` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `address`;
CREATE  TABLE `address` (
  `address_id` INT NOT NULL AUTO_INCREMENT ,
  `province` VARCHAR(45) NULL ,
  `region` VARCHAR(45) NULL ,
  `locality` VARCHAR(45) NULL ,
  `rural_committee` VARCHAR(45) NULL ,
  `postcode` VARCHAR(45) NULL ,
  `street` VARCHAR(45) NULL ,
  `house` SMALLINT NULL ,
  `building` VARCHAR(5) NULL ,
  `apartment` SMALLINT NULL ,
  `phone` VARCHAR(15) NULL ,
  `work_phone` VARCHAR(15) NULL ,
  `mobile_phone` VARCHAR(15) NULL ,
  PRIMARY KEY (`address_id`) );


