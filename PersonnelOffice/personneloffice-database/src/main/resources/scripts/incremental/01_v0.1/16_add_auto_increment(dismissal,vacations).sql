ALTER TABLE dismissal   CHANGE dismissal_id   dismissal_id INT NOT NULL AUTO_INCREMENT;
ALTER TABLE vacation        CHANGE vacation_id        vacation_id INT NOT NULL AUTO_INCREMENT;
ALTER TABLE vacation        CHANGE duration        duration VARCHAR(45);

insert into dismissal (person_card_id, order_date, reason, order_number, dismissal_date, compensation_days)
values
(1, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(2, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(3, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(4, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(5, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(6, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(7, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(8, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(9, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5),
(10, '2014-07-01', 'AGREED_BY_THE_PARTIES', '189/K', '2014-08-20', 5);


insert into vacation (person_card_id, period_from, period_to, vacation_type, vacation_from, vacation_to, duration, order_number, order_date)
values
(1, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15'),
(2, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15'),
(3, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15'),
(4, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15'),
(5, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15'),
(6, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15'),
(7, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15'),
(8, '2013-10-02', '2014-10-02', 'LABOR_COMPENSATION', '2013-07-05', '2013-07-21', '21/5', '154', '2013-06-15');