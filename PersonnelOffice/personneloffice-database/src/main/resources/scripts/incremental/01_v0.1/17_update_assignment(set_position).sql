ALTER TABLE assignment DROP `date`, DROP `rank`, DROP `material_liability`;

ALTER TABLE assignment ADD (
  form VARCHAR(1) DEFAULT NULL,
  contract_number VARCHAR(10) DEFAULT NULL,
  contract_date DATE DEFAULT NULL,
  contract_start DATE DEFAULT NULL,
  contract_end DATE DEFAULT NULL
  );

UPDATE assignment
set
position="CLEANER_FACILITIES",
form="Д", contract_number="189/К",
order_number="2000/К",
department="Отдел маркетинга",
contract_date='2014-07-01',
contract_start='2014-07-01',
contract_end='2015-03-20',
order_date='2013-03-20';

