ALTER TABLE `document`
  ADD COLUMN `content` LONGTEXT NULL;

CREATE TABLE `card_document_xref` (
  `person_card_id` int(11) NOT NULL,
  `document_id` int(11) NOT NULL,
  PRIMARY KEY (`person_card_id`, `document_id`)
);

ALTER TABLE `card_document_xref`
  ADD CONSTRAINT `fk_card_document_xref__document__person_card_id`
  FOREIGN KEY (`document_id`)  REFERENCES `document` (`document_id`)  ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_card_document_xref__document__person_card_id` (`document_id` ASC);

ALTER TABLE `card_document_xref`
  ADD CONSTRAINT `fk_card_document_xref__person_card__person_card_id`
  FOREIGN KEY (`person_card_id`)  REFERENCES `person_card` (`person_card_id`)  ON DELETE CASCADE ON UPDATE RESTRICT,
  ADD INDEX `fk_card_document_xref__person_card__person_card_id` (`person_card_id` ASC);

INSERT INTO `card_document_xref` (`person_card_id`, `document_id`)
  SELECT `person_card_id`, `document_id`
  FROM `document`
  ORDER BY `person_card_id`, `document_id`;

ALTER TABLE `document`
  DROP COLUMN `person_card_id`;

DROP TABLE `person_card_file`;
DROP TABLE `file`;