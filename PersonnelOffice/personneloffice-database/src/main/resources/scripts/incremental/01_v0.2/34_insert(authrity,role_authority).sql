INSERT INTO authority (authority_id, authority)
VALUES
  (1, 'ADMIN'),
  (2, 'USER');

INSERT INTO role_authority (role_id, authority_id)
VALUES
  (1, 1),
  (1, 2),
  (2, 2);