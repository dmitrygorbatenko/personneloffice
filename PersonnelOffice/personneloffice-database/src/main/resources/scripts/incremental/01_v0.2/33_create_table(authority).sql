DROP TABLE IF EXISTS `authority`;
CREATE TABLE authority (
  authority_id       INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
  authority VARCHAR(30)            NOT NULL UNIQUE
);

DROP TABLE IF EXISTS `role_authority`;
CREATE TABLE role_authority (
  role_id      INTEGER NOT NULL,
  authority_id INTEGER NOT NULL,
  CONSTRAINT userRole_authority_UNIQUE UNIQUE (role_id, authority_id)
);