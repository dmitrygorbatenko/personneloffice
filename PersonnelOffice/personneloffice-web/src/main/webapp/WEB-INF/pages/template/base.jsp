<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="${pageContext.request.contextPath}/resources/images/personnel_office_ico.png" type="image/png" rel="icon">
    <link href="${pageContext.request.contextPath}/resources/images/personnel_office_ico.png" type="image/png" rel="shortcut icon">
    <tiles:insertAttribute name="styles"/>
    <title><tiles:insertAttribute name="title"/></title>
</head>
<body>
    <tiles:insertAttribute name="header"/>
    <tiles:insertAttribute name="body"/>
    <tiles:insertAttribute name="footer"/>

    <script type="text/javascript">
        var contextPath = "${pageContext.request.contextPath}";
    </script>
    <tiles:insertAttribute name="scripts"/>
</body>
</html>

