<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<div class="top-center-menu">
    <div class="container">
        <div class="welcome-message">
            <spring:message code="welcome"/>, <span id="username-settings">${user.firstName}</span>
        </div>
        <div id="welcomeDivContent">
            <div class="menu-links">
                <a href="<c:url value="/logout"/>"><spring:message code="sign_out"/></a>
            </div>
        </div>
    </div>
</div>


<div class="top-header-div">
    <div class="container">
        <div style="position: absolute; left: 0; top: 0;">
            <img src="${pageContext.request.contextPath}/resources/images/personnel_office_logo_big.png"/>
        </div>
        <div style="position: absolute; right: 0; top:0;">
            <img src="${pageContext.request.contextPath}/resources/images/coherent_logo.png"/>
            <img src="${pageContext.request.contextPath}/resources/images/issoft_logo.png"/>
        </div>
    </div>
</div>


<div class="top-center-div-tabs">
    <div class="container">
        <div class="tabs-menu">


                <div class="menu-left-float <c:if test="${activeTab eq 'cards'}">tab-active</c:if> ">
                    <a href="${pageContext.request.contextPath}/persons/list" id="personCards"> Карточки</a>
                    <span onclick="location.href='#'"><span class="count"></span></span>
                </div>


                <div class="menu-left-float <c:if test="${activeTab eq 'documents'}">tab-active</c:if> ">
                    <a href="${pageContext.request.contextPath}/documents/list" id="documents">Документы</a>
                    <span onclick="location.href='#'"><span class="count"></span></span>
                </div>



                <div class="menu-left-float <c:if test="${activeTab eq 'reports'}">tab-active</c:if> ">
                    <a href="${pageContext.request.contextPath}/report/list" id="reports">Отчеты</a>
                    <span onclick="location.href='#'"><span class="count"></span></span>
                </div>



                <div class="menu-left-float <c:if test="${activeTab eq 'settings'}">tab-active</c:if> ">
                    <a href="#" id="settings">Настройки</a>
                    <span onclick="location.href='#'"><span class="count"></span></span>
                </div>


            <sec:authorize access="hasAuthority('ADMIN')">
                <div class="menu-left-float <c:if test="${activeTab eq 'administration'}">tab-active</c:if> ">
                    <a href="#" id="administration">Администрирование</a>
                    <span onclick="location.href='#'"><span class="count"></span></span>
                </div>
            </sec:authorize>
        </div>
    </div>
</div>

