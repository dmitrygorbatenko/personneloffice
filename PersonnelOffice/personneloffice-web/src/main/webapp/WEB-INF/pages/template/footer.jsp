<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<footer>
    <div class="horizontal-buttonset">
        <a class="resource-link" href="http://www.coherentsolutions.com/" target="_blank"><div class="resource-link-container coherent-solutions">Coherent Solutions</div></a>
        <a class="resource-link" href="http://www.issoft.by/" target="_blank"><div class="resource-link-container issoft">ISsoft</div></a>
        <a class="resource-link" href="https://time.coherentsolutions.com" target="_blank"><div class="resource-link-container bigtime">BigTime</div></a>
        <a class="resource-link" href="https://helpdesk.issoft.by" target="_blank"><div class="resource-link-container helpdesk">HelpDesk</div></a>
        <a class="resource-link" href="http://minsk-dev-rmsys:8080/lunchvoting" target="_blank">
            <div class="resource-link-container lunchvoting">Lunch Voting</div>
        </a>
    </div>
</footer>