<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<tiles:insertDefinition name="baseTemplate" >

    <tiles:putAttribute name="title" value="Login"/>
    <tiles:putAttribute name="header" value=""/>
    <tiles:putAttribute name="styles">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/login.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="scripts">
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.validate.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.placeholder.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/login.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="body">
        <div class="login">
            <form class="loginForm" method="post" action="${pageContext.request.contextPath}/j_spring_security_check" >
                <h1><img id="logo" src="${pageContext.request.contextPath}/resources/images/personnel_office_logo.png" alt="Login to Personnel Office"></h1>
                <div class="separatorLine"></div>
                <div id="cookieDisabledMessage" class="cookie-error-important-icon">
                    Для работы с приложением необходимо включить поддержку cookie в Вашем браузере
                </div>
                <div id="loginForm">
                    <div id="error_message_username" class="requiredError">

                    </div>
                    <div id="usernameContainer" class="formInput">
                        <div id="domain">BY-MINSK</div>
                        <div class="username">
                            <input id="login" name="j_username" value="" placeholder="Имя пользователя" type="text">
                        </div>
                    </div>
                    <div id="error_message_password" class="requiredError"></div>
                    <div id="passwordContainer" class="formInput">
                        <input id="password" name="j_password" value="" placeholder="Пароль" type="password">
                    </div>
                    <div id="remember-me-wrapper">
                        <div id="remember_me_container">
                            <input id="remember_me" name="remember_me" value="true" type="hidden">
                            <span  id="spanCheck"></span>
                            <label id="rememberLabel" for="remember_me">Запомнить</label>
                        </div>
                    </div>
                    <button type="submit" class="login_button">Войти</button>
                </div>
            </form>
        </div>
        <div class="footer">Personnel Office 0.0.1 </div>
    </tiles:putAttribute>
    <tiles:putAttribute name="footer" value=""/>
</tiles:insertDefinition>