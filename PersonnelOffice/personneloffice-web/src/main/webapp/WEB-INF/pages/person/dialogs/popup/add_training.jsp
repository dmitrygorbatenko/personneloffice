<%@ page import="com.coherent.api.dao.enums.education.DocumentTypeEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% pageContext.setAttribute("documentTypes", DocumentTypeEnum.values()); %>

<span class="popup-title"
      data-add="Создать повышение квалификации"
      data-edit="Редактировать повышение квалификации"
      data-delete="Удалить повышение квалификации">
</span>

<hr>

<form:form id="training_form" action="${pageContext.request.contextPath}/persons/training/save" commandName="training">
    <form:input path="personCardId" type="hidden"  value="${ personCard.id }"/>
    <form:input path="id" type="hidden" value="${ training.id }" />

    <div class="divTable add-education">
        <div class="divRow">
            <div>Дата начала</div>
            <div><form:input path="startDate" class="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>Дата окончания</div>
            <div><form:input path="endDate" class="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>Учебное заведение<i class="red">*</i></div>
            <div><form:textarea path="institution"/></div>
        </div>
        <div class="divRow">
            <div>Тематика обучения</div>
            <div><form:textarea path="studyingTopic"/></div>
        </div>
        <div class="divRow">
            <div>Тип документа</div>
            <div>
                <div class="select_div popup-select">
                    <form:select path="documentType">
                        <form:option value=""/>
                        <c:forEach items="${documentTypes}" var="documentType">
                            <form:option value="${documentType}"><spring:message code="${documentType.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </div>
        <div class="divRow">
            <div>Дата документа</div>
            <div><form:input path="documentDate" cssClass="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>№ документа</div>
            <div><form:input path="documentNumber" cssClass="document-number"/></div>
        </div>
    </div>
</form:form>