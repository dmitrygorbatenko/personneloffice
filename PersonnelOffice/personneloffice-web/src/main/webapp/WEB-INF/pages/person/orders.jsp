<%@ page import="com.coherent.api.dao.enums.documents.PaperTypeEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% pageContext.setAttribute("documentTypes", PaperTypeEnum.values()); %>

<tiles:insertDefinition name="baseTemplate">
    <tiles:putAttribute name="styles">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery.dataTables.min.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery.resizableColumns.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery-ui.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/customDataTable.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/Master.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/GlobalSearch.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dialogs.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="scripts">
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/date.format.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/Master.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/init-main-table.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/orders.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/ckeditor/ckeditor.js"></script>

        <!-- Document Dialog -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/autocomplete.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/intellisence.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/ckeditor.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/name.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/template-functions.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/template-generator.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/doc_dialog.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="title" value="Persons"/>
    <tiles:putAttribute name="body">

        <div class="toolbar-border-top">
            <div class="container">

                <div class="container-search">
                    <div>
                        <spring:message var="placholder" code="search"/>
                        <div class="global-search-container">
                            <input id="search" onblur="if(this.value=='') this.value='<spring:message code="search"/>'"
                                   onfocus="if (this.value=='<spring:message code="search"/>') this.value = ''"
                                   class="global-search" name="SearchQuery"
                                   value="<spring:message code="search"/>" type="text"/>
                            <button id="clearSearch" type="button" class="clear-button"
                                    title="<spring:message code="search"/>">
                            </button>
                        </div>
                    </div>

                    <div class="container-search">
                        <div>
                            <label style="color: #0975A6;"><spring:message code="general.documentType"/>: </label>
                        </div>
                        <div class="filter-selector">
                            <div class="select_div popup-select">
                                <select id="documentTypeFilter">
                                    <option value=""></option>
                                    <c:forEach var="documentType" items="${documentTypes}">
                                        <spring:message var="documentLabel" code="${documentType.value}"/>
                                        <option value="${documentLabel}">${documentLabel}</option>
                                    </c:forEach>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="new-item-buttons">
                        <div class="new-item-btn new-single-btn">
                            <button id="newOrder" data-url="document" data-person-id="0">Создать документ</button>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div id="scroll_container">
                <table id="ordersTable">
                    <thead>
                    <tr>
                        <th class="column-number">
                            <div class="fixed_header"><span><spring:message code="number"/></span></div>
                        </th>
                        <th class="column-type">
                            <div class="fixed_header"><span>Тип</span></div>
                        </th>
                        <th class="column-date">
                            <div class="fixed_header"><span><spring:message code="date"/></span></div>
                        </th>
                        <th class="column-full_name">
                            <div class="fixed_header"><span><spring:message code="full_name"/></span></div>
                        </th>
                        <th class="column-kind">
                            <div class="fixed_header"><span><spring:message code="kind"/></span></div>
                        </th>
                        <th class="column-kind">
                            <div class="fixed_header"><span>Подвид</span></div>
                        </th>
                        <th class="column-reason">
                            <div class="fixed_header"><span><spring:message code="reason"/></span></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${documents}" var="documents" varStatus="iter">
                        <tr>
                            <td class="column-number">${documents.paperNumber} /
                            <c:if test="${documents.category != null }">
                                <spring:message code="${documents.category.value}"/>
                            </c:if></td>
                            <td class="column-type">
                                <c:if test="${documents.paperType != null }">
                                    <spring:message code="${documents.paperType.value}"/>
                                </c:if>
                            </td>
                            <td class="column-date"><spring:eval expression="documents.paperDate"/></td>
                            <td class="column-full_name">
                                <c:forEach var="personCards" items="${documents.personCards}" varStatus="i">
                                    <c:if test="${ i.index < 5 }">
                                        <a href="#">${personCards.lastName } ${personCards.firstName.substring(0, 1)}. ${personCards.patronymic.substring(0, 1)}.</a><c:if test="${ not i.last && i.index < 4 }">,</c:if>
                                        <c:if test="${ i.count > 4 }"> <a href="#">След. >></a></c:if>
                                    </c:if>
                                </c:forEach>
                            </td>
                            <td class="column-kind">
                                <c:if test="${documents.species != null }">
                                    <spring:message code="${documents.species.value}"/>
                                </c:if>
                            </td>
                            <td class="column-kind">
                                <c:if test="${documents.subspecies != null }">
                                    <spring:message code="${documents.subspecies.value}"/>
                                </c:if>
                            </td>
                            <td class="column-reason">${documents.reason}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <div class="table-right-border"></div>
            </div>
        </div>

        <div class="curtain">
            <div id="editDialog" class="reveal-modal xlarge"></div>
        </div>

    </tiles:putAttribute>
</tiles:insertDefinition>