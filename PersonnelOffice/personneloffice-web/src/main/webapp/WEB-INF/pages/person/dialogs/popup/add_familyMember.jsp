<%@ page import="com.coherent.api.dao.enums.family.KinshipEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% pageContext.setAttribute("kinshipValues", KinshipEnum.values()); %>

<span class="popup-title"
      data-add="Создать члена семьи"
      data-edit="Редактировать члена семьи"
      data-delete="Удалить члена семьи">
</span>

<hr>
<form:form id="familyMember_form" action="${pageContext.request.contextPath}/persons/family_member/save" commandName="familyMember">
    <form:input path="personCardId" type="hidden"  value="${ personCard.id }"/>
    <form:input path="id" type="hidden" value="${ familyMember.id }" />

    <div class="divTable add-family-member">

        <div class="divRow">
            <div>ФИО<i class="red">*</i></div>
            <div><form:input path="name" class="required" /></div>
        </div>
        <div class="divRow">
            <div>Родство<i class="red">*</i></div>
            <div>
                <div class="select_div popup-select">
                    <form:select path="kinship" cssClass="required">
                        <form:option value=""/>
                        <c:forEach items="${kinshipValues}" var="kinship">
                            <form:option value="${kinship}"><spring:message code="${kinship.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </div>
        <div class="divRow">
            <div>Дата рождения<i class="red">*</i></div>
            <div>
                <form:input path="birthday" class="date-picker required" />
            </div>
        </div>
        <div class="divRow">
            <div><label for="invalid">Инвалид</label></div>
            <div><form:checkbox path="invalid" id="invalid" /></div>
        </div>
    </div>
</form:form>
<br>