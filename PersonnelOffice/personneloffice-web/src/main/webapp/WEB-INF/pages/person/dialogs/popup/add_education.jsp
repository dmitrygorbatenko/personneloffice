<%@ page import="com.coherent.api.dao.enums.education.DocumentTypeEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% pageContext.setAttribute("documentTypes", DocumentTypeEnum.values()); %>

<span class="popup-title"
      data-add="Создать образование"
      data-edit="Редактировать образование"
      data-delete="Удалить образование">
</span>

<hr>

<form:form id="education_form" action="${pageContext.request.contextPath}/persons/education/save" commandName="education">
    <form:input path="personCardId" type="hidden"  value="${ personCard.id }"/>
    <form:input path="id" type="hidden" value="${ education.id }" />
    <div class="divTable add-education">

        <div class="divRow">
            <div>Учебное заведение<i class="red">*</i></div>
            <div><form:textarea path="institution" cssClass="required"/></div>
        </div>
        <div class="divRow">
            <div>Факультет</div>
            <div><form:textarea path="faculty"/></div>
        </div>
        <div class="divRow">
            <div>Квалификация</div>
            <div><form:textarea path="qualification"/></div>
        </div>
        <div class="divRow">
            <div>Специальность</div>
            <div><form:textarea path="specialty"/></div>
        </div>
        <div class="divRow">
            <div>Бюджет</div>
            <div><form:checkbox path="budget"/></div>
        </div>
        <div class="divRow">
            <div>Дата окончания</div>
            <div><form:input path="educationEndDate" cssClass="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>Тип документа</div>
            <div>
                <div class="select_div popup-select">
                    <form:select path="documentType">
                        <form:option value=""/>
                        <c:forEach items="${documentTypes}" var="documentType">
                            <form:option value="${documentType}"><spring:message code="${documentType.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </div>
        <div class="divRow">
            <div>Дата документа</div>
            <div><form:input path="diplomaDate" cssClass="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>№ документа</div>
            <div><form:input path="diplomaNumber"  cssClass="document-number"/></div>
        </div>

    </div>
</form:form>