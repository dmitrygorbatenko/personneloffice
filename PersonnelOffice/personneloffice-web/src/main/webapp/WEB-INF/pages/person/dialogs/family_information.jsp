<%@ page import="com.coherent.api.dao.enums.family.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% pageContext.setAttribute("familyMember", KinshipEnum.values()); %>


<div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }">
    <a class="add-row crud" href="#" data-url="family_member" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
    <a class="edit-row crud" href="#"></a>
    <a class="delete-row crud" href="#"></a>
</div>
<table class="multi-child">
    <tr>
        <td class="blue-label">Многодетная семья:</td>
        <td><label for="large_family">№ удостоверения</label></td>
        <td><form:input id="large_family" path="largeFamily" /></td>
    </tr>
</table>

<hr>
<div class="info-tables-wrapper">

    <table id="family_table" class="info-tables">
        <thead data-entity-attributes="data-id:id">
            <tr>
                <td data-entity-property="kinship">Родство</td>
                <td data-entity-property="name">ФИО</td>
                <td data-entity-property="birthday">Дата Рождения</td>
                <td data-entity-property="invalid">Инвалид</td>
            </tr>
        </thead>
        <tbody>
        <c:forEach items="${personCard.familyMembers}" var="familyMember">
            <tr data-url="family_member" data-person-id="${personCard.id}" data-id="${familyMember.id}">
                <td>
                    <c:if test="${familyMember.kinship != null}">
                        <spring:message code="${familyMember.kinship.value}"/>
                    </c:if>
                </td>
                <td>${familyMember.name}</td>
                <td><spring:eval expression="familyMember.birthday"/></td>
                <td><c:if test="${familyMember.invalid}"><input id="invalid" type="checkbox" disabled checked></c:if></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>