<%@ page import="com.coherent.api.dao.enums.general.StatusEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<% pageContext.setAttribute("statuses", StatusEnum.values()); %>

<tiles:insertDefinition name="baseTemplate">
    <tiles:putAttribute name="styles">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery.dataTables.min.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery.resizableColumns.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery-ui.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/customDataTable.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/Master.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/GlobalSearch.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dialogs.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="scripts">
    	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-1.11.1.min.js"></script>
		<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/date.format.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.inputmask.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/Master.js"></script>

        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/person-cards.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/person-dialog.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/getDataForSmallTables.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/ckeditor/ckeditor.js"></script>

        <!-- Document Dialog -->
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/autocomplete.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/intellisence.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/ckeditor.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/name.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/template-functions.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/template-generator.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/doc-dialog/doc_dialog.js"></script>


    </tiles:putAttribute>
    <tiles:putAttribute name="title" value="Persons"/>
    <tiles:putAttribute name="body">

    <div class="toolbar-border-top">
        <div class="container">

            <div class="container-search">
                <div>
                    <spring:message var="placeholder" code="search"/>
                    <div class="global-search-container">
                        <input id="search" onblur="if(this.value=='') this.value='${placeholder}'"
                               onfocus="if (this.value=='${placeholder}') this.value = ''"
                               class="global-search" name="SearchQuery"
                               value="${placeholder}" type="text"/>
                        <button id="clearSearch" type="button" class="clear-button"
                                title="<spring:message code="search"/>">
                        </button>
                    </div>
                </div>

                <div class="container-search">
                    <button id="preferenceButton" class="button columns_button">Колонки</button>
                    <div><label style="color: #0975A6;"><spring:message code="status"/>: </label></div>
                    <div class="filter-selector">
                        <div class="select_div popup-select">
                            <select id="statusFilter">
                                <option value=""></option>
                                <c:forEach var="status" items="${statuses}">
                                    <spring:message var="statusLabel" code="${status.value}"/>
                                    <option value="${statusLabel}">${statusLabel}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="new-item-buttons">

                    <div class="chooseFile">
                        <form id="formChooseExcelCards" action="${pageContext.request.contextPath}/file/import/excel/cards" enctype="multipart/form-data" method="post">
                            <input id="chooseExcelCards" type="file" name="fileXls" style="display: none;" />
                        </form>
                    </div>
                    <button id="import-personCards-excel"  type="button" class="button">Экспорт из Excel</button>
                    <button id="export-personCards-excel"  type="button" class="button">Импорт в Excel</button>
                    <button id="newCard" data-id="0" type="button" class="button">Добавить карту</button>
                    <button id="newDocument" data-url="document" data-person-id="0" type="button" class="button">Добавить приказ</button>
                </div>
            </div>
        </div>
    </div>

    <div class="curtain">
        <%--<div class="preference-background" style="display: none"></div>--%>
        <div class="container">
            <div id="preferenceBlock" class="preference-container">
                <form id="preferenceForm">
                <div style="height: 80px; font-size: 20px; line-height: 48px;padding: 0 10px">
                    <div>
                        Колонки
                        <button class="close-icon close-icon-position" title="Close"></button>
                    </div>
                    <div>
                        <input type="text" name="preferenceSearch" id="preferenceSearch" class="preferenceSearch" placeholder="Поиск"/>
                    </div>
                </div>

                <div class="preference-scroller">
                        <div class="table">
                            <div>
                                <div>поиск</div>
                                <div>показать</div>
                                <div></div>
                            </div>

                            <c:forEach items="${preferences}" var="preference">
                            <div class="preference" data-column="${preference.column}">
                                <div>
                                    <input type="checkbox" class="toggle-column-filter" id="preference_full_name" name="fullName" <c:if test="${preference.searchable}">checked</c:if> <c:if test="${preference.name eq 'status'}">disabled</c:if>>
                                </div>
                                <div>
                                    <input type="checkbox" class="toggle-column-vis" <c:if test="${preference.visible}">checked</c:if> >
                                </div>
                                <div>
                                    <spring:message code="${preference.name}"/>
                                </div>
                            </div>
                            </c:forEach>
                        </div>
                    </div>
                <div style="text-align: center;padding: 20px 10px; height: 28px">
                    <button id="applyPreference" form="preferenceForm" class="button"><spring:message code="save"/></button>
                    <button id="cancelPreference" type="button" class="button"><spring:message code="cancel"/></button>
                </div>

                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div id="scroll_container">
        <table id="personsTable">
            <thead>
            <tr>
                <th class="column-full_name">
                    <div class="fixed_header" title="<spring:message code="full_name"/>">
                        <div><span><spring:message code="full_name"/></span></div>
                    </div>
                </th>
                <th class="column-rate">
                    <div class="fixed_header" title="<spring:message code="rate"/>">
                        <div><span><spring:message code="rate"/></span></div>
                    </div>
                </th>
                <th class="column-gender">
                    <div class="fixed_header" title="<spring:message code="gender"/>">
                        <div><span><spring:message code="gender"/></span></div>
                    </div>
                </th>
                <th class="column-position">
                    <div class="fixed_header" title="<spring:message code="position"/>">
                        <div><span><spring:message code="position"/></span></div>
                    </div>
                </th>
                <th class="column-start_date">
                    <div class="fixed_header" title="<spring:message code="start_date"/>">
                        <div><span><spring:message code="start_date"/></span></div>
                    </div>
                </th>
                <th class="column-transfer">
                    <div class="fixed_header" title="<spring:message code="transfer"/>">
                        <div><span><spring:message code="transfer"/></span></div>
                    </div>
                </th>
                <th class="column-order_number">
                    <div class="fixed_header" title="<spring:message code="order_number"/>">
                        <div><span><spring:message code="order_number"/></span></div>
                    </div>
                </th>
                <th class="column-order_date">
                    <div class="fixed_header" title="<spring:message code="order_date"/>">
                        <div><span><spring:message code="order_date"/></span></div>
                    </div>
                </th>
                <th class="column-contract_number">
                    <div class="fixed_header" title="<spring:message code="contract_number"/>">
                        <div><span><spring:message code="contract_number"/></span></div>
                    </div>
                </th>
                <th class="column-education">
                    <div class="fixed_header" title="<spring:message code="education"/>">
                        <div><span><spring:message code="education"/></span></div>
                    </div>
                </th>
                <th class="column-finish_date">
                    <div class="fixed_header" title="<spring:message code="finish_date"/>">
                        <div><span><spring:message code="finish_date"/></span></div>
                    </div>
                </th>
                <th class="column-birthday">
                    <div class="fixed_header" title="<spring:message code="birthday"/>">
                        <div><span><spring:message code="birthday"/></span></div>
                    </div>
                </th>
                <th class="column-allocation">
                    <div class="fixed_header" title="<spring:message code="assignment"/>">
                        <div><span><spring:message code="assignment"/></span></div>
                    </div>
                </th>
                <th class="column-phone">
                    <div class="fixed_header" title="<spring:message code="phone"/>">
                        <div><span><spring:message code="phone"/></span></div>
                    </div>
                </th>
                <th class="column-children_info">
                    <div class="fixed_header" title="<spring:message code="children_info"/>">
                        <div><span><spring:message code="children_info"/></span></div>
                    </div>
                </th>
                <th class="column-full_name_latin">
                    <div class="fixed_header" title="<spring:message code="full_name_latin"/>">
                        <div><span><spring:message code="full_name_latin"/></span></div>
                    </div>
                </th>
                <th class="column-passport_number">
                    <div class="fixed_header" title="<spring:message code="passport_number"/>">
                        <div><span><spring:message code="passport_number"/></span></div>
                    </div>
                </th>
                <th class="column-passport_individual_number">
                    <div class="fixed_header" title="<spring:message code="passport_individual_number"/>">
                        <div><span><spring:message code="passport_individual_number"/></span></div>
                    </div>
                </th>
                <th class="column-passport_date_issuance">
                    <div class="fixed_header" title="<spring:message code="passport_date_issuance"/>">
                        <div><span><spring:message code="passport_date_issuance"/></span></div>
                    </div>
                </th>
                <th class="column-passport_validity_period">
                    <div class="fixed_header" title="<spring:message code="passport_validity_period"/>">
                        <div><span><spring:message code="passport_validity_period"/></span></div>
                    </div>
                </th>
                <th class="column-passport_office">
                    <div class="fixed_header" title="<spring:message code="passport_office"/>">
                        <div><span><spring:message code="passport_office"/></span></div>
                    </div>
                </th>
                <th class="column-registration">
                    <div class="fixed_header" title="<spring:message code="registration"/>">
                        <div><span><spring:message code="registration"/></span></div>
                    </div>
                </th>
                <th class="column-form_of_lease">
                    <div class="fixed_header" title="<spring:message code="form_of_lease"/>">
                        <div><span><spring:message code="form_of_lease"/></span></div>
                    </div>
                </th>
                <th class="column-status">
                    <div class="fixed_header" title="<spring:message code="status"/>">
                        <div><span><spring:message code="status"/></span></div>
                    </div>
                </th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${persons}" var="person" varStatus="iter">
                <tr data-id="${person.id}">
                    <c:set var="assignment" value="${person.lastAssignment}" />
                    <td>
                        <a href="#${person.id}"
                           class="edit_person">${person.lastName} ${person.firstName} ${person.patronymic}</a>
                    </td>
                    <td>
                        <c:if test="${assignment !=  null}">
                            ${assignment.rate}
                        </c:if>
                    </td>
                    <td><spring:message code="${person.gender.value}" text=" "/></td>
                    <td>
                        <c:if test="${assignment != null}">
                            <spring:message code="${assignment.position.value}" text=" "/>
                        </c:if>
                    </td>
                    <td>
                        <%--//start contract date--%>
                    </td>
                    <td>нет данных</td>
                    <td>
                        <c:if test="${assignment != null}">
                            ${assignment.orderNumber}
                        </c:if>
                    </td>
                    <td>
                        <c:if test="${assignment != null}">
                            <spring:eval expression="assignment.orderDate"/>
                        </c:if>
                    </td>
                    <td>${iter.count}</td>
                    <td>
                        <c:if test="${person.educationDegree != null}">
                            <spring:message code="${person.educationDegree.value}"/>
                        </c:if>
                    </td>
                    <td>
                        <%--//finish contract date--%>
                    </td>
                    <td><spring:eval expression="person.birthday"/></td>
                    <td>что писать сюда ${person.allocation}</td>
                    <td>${person.residentialAddress.phone}</td>
                    <td>
                        <div>
                            ${person.familyMembersAsString}
                        </div>
                    </td>
                    <td>${iter.count}</td>
                    <td>${person.documentSerialNumber}</td>
                    <td>${person.documentIndividualNumber}</td>
                    <td><spring:eval expression="person.documentDateIssuance"/></td>
                    <td><spring:eval expression="person.documentValidityPeriod"/></td>
                    <td>${person.documentIssuedBy}</td>
                    <td>${person.registrationAddress.street} ${person.registrationAddress.house} / ${person.registrationAddress.apartment} </td>
                    <td>нет данных</td>
                    <td><spring:message code="${person.status.value}" text=" "/></td>
                </tr>
            </c:forEach>
            </tbody>
        </table>
            <div class="table-right-border"></div>
        </div>

    <div class="curtain"><!-- 1st level -->
        <div id="editDialog" class="reveal-modal xlarge"></div>
    </div>

    <div class="curtain">
        <div id="warning-dialog" class="reveal-modal small">
            <jsp:include page="dialogs/warning_message.jsp"/>
        </div>
    </div>

    </tiles:putAttribute>
</tiles:insertDefinition>