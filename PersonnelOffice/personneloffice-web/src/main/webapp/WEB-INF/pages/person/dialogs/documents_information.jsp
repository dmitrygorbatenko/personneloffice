<%@ page import="com.coherent.api.dao.enums.documents.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<% pageContext.setAttribute("paperTypes", PaperTypeEnum.values()); %>


<div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }">
    <!-- <a class="export-to-word" href="#"></a> -->
    <a class="add-row crud" href="#" data-url="document" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
    <!-- <a class="edit-row crud" href="#"></a> -->
    <a class="delete-row crud" href="#"></a>
</div>

<table class="orders-sort">
    <tr>
        <td><label for="doc-type-select" class="blue-label">Тип документа:</label></td>
        <td>
            <div id="doc-type-select" class="select_div">
                <select id="documentFilter">
                    <option value=""></option>
                        <c:forEach var="paperType" items="${paperTypes}">
                            <spring:message var="paperTypeLabel" code="${paperType.value}"/>
                        <option value="${paperType}">${paperTypeLabel}</option>
                    </c:forEach>
                </select>
            </div>
        </td>
    </tr>
</table>

<div class="info-tables-wrapper">

    <table id="documents_table" class="info-tables">
        <thead data-entity-attributes="data-id:id">
            <tr>
                <td data-entity-property="paperType">Тип Документа</td><!--  class="document-type"-->
                <td data-entity-property="paperNumber">№</td>
                <td data-entity-property="paperDate">Дата</td>
                <td data-entity-property="species">Вид</td>
                <td data-entity-property="reason">Основание</td>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${personCard.documents}" var="document">
                <tr data-url="document" data-person-id="${personCard.id}" data-id="${document.id}">
                    <td><%-- ${document.paperType} --%>
                        <c:if test="${document.paperType != null}">
                            <spring:message code="${document.paperType.value}"/>
                        </c:if>
                    </td><!--  class="document-type" -->
                    <td>
                        ${document.paperNumber}
                        <c:if test="${document.category != null }">
                            /
                            <spring:message code="${document.category.value}"/>
                        </c:if>
                    </td>
                    <td>
                        <spring:eval expression="document.paperDate"/>
                    </td>
                    <td>
                        <c:if test="${document.species != null }">
                            <spring:message code="${document.species.value}"/>
                        </c:if>
                        <c:if test="${document.subspecies != null }">
                            /
                            <spring:message code="${document.subspecies.value}"/>
                        </c:if>
                    </td>
                    <td>
                        ${document.reason}
                    </td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

</div>