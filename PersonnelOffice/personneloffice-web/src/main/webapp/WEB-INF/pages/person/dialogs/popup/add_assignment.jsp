<%@ page import="com.coherent.api.dao.enums.assignment.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% pageContext.setAttribute("positions", PositionEnum.values()); %>
<% pageContext.setAttribute("forms", FormEnum.values()); %>

<span class="popup-title"
      data-add="Создать назначение"
      data-edit="Редактировать назначение"
      data-delete="Удалить назначение">
</span>

<hr>

<form:form id="assignment_form" action="${pageContext.request.contextPath}/persons/assignment/save" commandName="assignment">
    <form:input path="personCardId" value="${personCard.id}" type="hidden"/>
    <form:input path="id" value="${ assignment.id }" type="hidden"/>

    <div class="divTable">
        <div class="divRow">
            <div>Должность<i class="red">*</i></div>
            <div>
                <div class="select_div">
                    <form:select path="position" cssClass="required">
                        <form:option value=""/>
                        <c:forEach items="${positions}" var="position">
                            <form:option value="${position}"><spring:message code="${position.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </div>
        <div class="divRow">
            <div>Кол-во ставок<i class="red">*</i></div>
            <div><form:input path="rate" class="required"/></div>
        </div>
        <div class="divRow">
            <div>Форма</div>
            <div>
                <div class="select_div popup-select">
                    <form:select path="form">
                        <form:option value=""/>
                        <c:forEach items="${forms}" var="form">
                            <form:option value="${form}"><spring:message code="${form.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </div>
        <div class="divRow">
            <div>№ контракта/договора</div>
            <div><form:input path="contractNumber" type="text"/></div>
        </div>
        <div class="divRow">
            <div>Дата контракта/договора</div>
            <div><form:input path="contractDate" class="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>Дата начала контракта/договора</div>
            <div><form:input path="contractStart" class="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>Дата окончания контракта/договора</div>
            <div><form:input path="contractEnd" class="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>№ приказа</div>
            <div><form:input path="orderNumber"  type="text"/></div>
        </div>
        <div class="divRow">
            <div>Дата приказа</div>
            <div><form:input path="orderDate"  class="date-picker document-number"/></div>
        </div>
        <div class="divRow">
            <div>Подразделение</div>
            <div><form:input path="department" type="text"/></div>
        </div>
    </div>
</form:form>
<br>