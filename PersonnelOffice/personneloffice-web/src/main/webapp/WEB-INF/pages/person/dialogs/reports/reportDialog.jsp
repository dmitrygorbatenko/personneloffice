<%@ page import="com.coherent.api.dao.enums.reports.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% pageContext.setAttribute("months", MonthEnum.values()); %>

<form:form id="report_form" action="${pageContext.request.contextPath}/report/getReportBody" commandName="report">
    <form:input path="id" type="hidden" value="${ report.id }" />
    <form:input path="reportType" type="hidden" value="${ report.reportType }" />

    <div>
        <span class="dialog_title report-title">${ report.reportType }</span>
        <a class="dialog_close close-X-button"></a>
        <hr>
    </div>

    <div class="report-block">
        <table>
            <tr>
                <td>Год</td>
                <td style="width: 130px">
                    <div class="select_div">
                        <form:select path="selectedYear" id="select-year">
                            <form:option value="2012"/>
                            <form:option value="2013"/>
                            <form:option value="2014"/>
                        </form:select>
                    </div>
                </td>
            </tr>
            <tr <c:if test="${report.reportType != 'Месячный' }"> style="display: none;" </c:if> >
                <td>Месяц</td>
                <td>
                    <div class="select_div">
                        <form:select path="selectedMonth" id="select-month">
                            <c:forEach items="${months}" var="month">
                                <form:option value="${month}"><spring:message code="${month.value}"/></form:option>
                            </c:forEach>
                        </form:select>
                    </div>
                </td>
            </tr>
        </table>
        <div>
            <button id="view-report-button" class="button" type="button">Просмотр</button>
            <button class="button" type="button"
                    onclick="location.href=createLink(${ report.id })">
                Экспорт</button>
        </div>
    </div>

    <div id="report-container" class="report-table-block"></div>

</form:form>