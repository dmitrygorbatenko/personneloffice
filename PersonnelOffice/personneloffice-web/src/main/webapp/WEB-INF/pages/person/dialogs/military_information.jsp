<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.coherent.api.dao.enums.military.*" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% pageContext.setAttribute("categoryStocks", CategoryStockEnum.values()); %>
<% pageContext.setAttribute("groupRecords", GroupRecordEnum.values()); %>
<% pageContext.setAttribute("militaryRanks", MilitaryRankEnum.values()); %>
<% pageContext.setAttribute("consists", ConsistEnum.values()); %>
<% pageContext.setAttribute("militaryAvailables", MilitaryAvailableEnum.values()); %>

<div id="military_tabs">
    <ul>
        <li><a href="#reservist">Военнообязанный</a></li>
        <li><a href="#conscript">Призывник</a></li>
    </ul>
        <div id="reservist">
            <form:hidden path="reservist.id"/>
            <div class="right-border disabled">
                <label for="is_conscript">Призывник</label>
                <input type="checkbox" id="is_conscript">
                <table>
                <tr>
                    <td style="width: 105px;">Дата постановки</td>
                    <td colspan="2"><form:input path="reservist.registrationDate" class="date-picker"/></td>
                </tr>
                <tr>
                    <td>№ билета</td>
                    <td colspan="2"><form:input path="reservist.ticketNumber"/></td>
                    <td>Категория запаса</td>
                    <td>
                        <div class="select_div">
                            <form:select path="reservist.categoryStock">
                                <form:option value=""/>
                                <c:forEach items="${categoryStocks}" var="categoryStock">
                                    <form:option value="${categoryStock}"><spring:message code="${categoryStock.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Группа учета<i class="red">*</i></td>
                    <td colspan="4">
                        <div class="select_div">
                            <form:select path="reservist.groupRecord">
                                <c:forEach items="${groupRecords}" var="groupRecord">
                                    <form:option value="${groupRecord}"><spring:message code="${groupRecord.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Воинское звание</td>
                    <td colspan="4">
                        <div class="select_div">
                            <form:select path="reservist.militaryRank">
                                <form:option value=""/>
                                <c:forEach items="${militaryRanks}" var="militaryRank">
                                    <form:option value="${militaryRank}"><spring:message code="${militaryRank.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Состав</td>
                    <td colspan="4">
                        <div class="select_div">
                            <form:select path="reservist.consist">
                                <form:option value=""/>
                                <c:forEach items="${consists}" var="consist">
                                    <form:option value="${consist}"><spring:message code="${consist.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>ВУС</td>
                    <td colspan="2"><form:input path="reservist.militarySpecialties"/></td>
                </tr>
                <tr>
                    <td>Годность к службе</td>
                    <td colspan="4">
                        <div class="select_div">
                            <form:select path="reservist.militaryAvailable">
                                <form:option value=""/>
                                <c:forEach items="${militaryAvailables}" var="militaryAvailable">
                                    <form:option value="${militaryAvailable}"><spring:message code="${militaryAvailable.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Военкомат</td>
                    <td colspan="4"><form:input path="reservist.recruitmentOffice" /></td>
                </tr>
                <tr>
                    <td>Стоит на учете</td>
                    <td colspan="4"><form:input path="reservist.beRegistered"/></td>
                </tr>
                <tr>
                    <td>Забронирован</td>
                    <td><form:checkbox path="reservist.reserved"/></td>
                    <td colspan="3">
                        <nobr>
                            <label for="has_prescription">Имееет мобилизационное предписание</label>
                            <form:checkbox path="reservist.mobilizationPrescription" id="has_prescription"/>
                        </nobr>
                    </td>
                </tr>
                <tr>
                    <td>Дата присяги</td>
                    <td colspan="2"><form:input path="reservist.swearDate" class="date-picker"/></td>
                    <td>№ части присяги</td>
                    <td><form:input path="reservist.unitSwearNumber"/></td>
                </tr>
                <tr>
                    <td>Категория для ф.6</td>
                    <td colspan="4"><form:input path="reservist.categoryForm"/></td>
                </tr>
            </table>
            </div>
            <fieldset class="disabled">
                <legend>Снятие с военного учета</legend>
                <table class="withdrawal">
                    <tr>
                        <td>Дата</td>
                        <td><form:input path="reservist.removeDate" class="date-picker"/></td>
                    </tr>
                    <tr>
                        <td>Причина</td>
                        <td><form:input path="reservist.reason"/></td>
                    </tr>
                    <tr>
                        <td>РВК по месту проживания</td>
                        <td><input type="text"></td>
                </table>
            </fieldset>
        </div>
        <div id="conscript">
            <form:hidden path="conscript.id"/>
            <table class="withdrawal fb_category">
                <tr>
                    <td>Категория для ф.6</td>
                    <td><form:input path="conscript.categoryForm"/></td>
                </tr>
            </table>
            <hr>
            <fieldset>
                <legend>Снятие с военного учета</legend>
                <table class="withdrawal">
                    <tr>
                        <td>Дата</td>
                        <td><form:input path="conscript.removeDate" class="date-picker"/></td>
                    </tr>
                    <tr>
                        <td>Причина</td>
                        <td><form:input path="conscript.reason"/></td>
                    </tr>
                    <tr>
                        <td>РВК по месту проживания</td>
                        <td><input type="text"></td>
                </table>
            </fieldset>
        </div>
    </div>
