<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<hr class="report-separator">

<table class="report-table-title">
    <tr>
        <td>
            <table class="report-table title">
                <tr><td></td></tr>
                <tr><td>Численность работников</td></tr>
                <tr><td></td></tr>
                <tr><td>женщины</td></tr>
                <tr><td></td></tr>
                <tr><td>приняли</td></tr>
                <tr><td>уволили</td></tr>
                <tr><td>совместили</td></tr>
                <tr><td>декр отпуск</td></tr>
                <tr><td></td></tr>
                <tr><td>списочная численность без декртн.отп.</td></tr>
                <tr><td>Среднесписочная численность работников</td></tr>
            </table>
        </td>
    </tr>
</table>

<div class="report-table-wrapper">
    <table class="report-table-title">
        <tr>
            <td>
                <table class="report-table rgh-border-none">
                    <tr><td>На<br>нач.<br>года</td></tr>
                    <tr><td>${reportBody[0].workersCount}</td></tr>
                    <tr><td></td></tr>
                    <tr><td>${reportBody[0].woman}</td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                    <tr><td></td></tr>
                </table>
            </td>
            <c:forEach var="i" begin="1" end="${fn:length(reportBody) -1 }">
                <c:set var="sumWorkers" value="${sumWorkers + reportBody[i].workersCount}" />
                <td>
                    <table class="report-table month">
                        <tr>
                            <td colspan="3" class="align-center">
                                <spring:message code="${reportBody[i].monthName.value}"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td>${reportBody[i].workersCount}</td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td colspan="2"></td>
                            <td>${reportBody[i].woman}</td>
                        </tr>
                        <tr>
                            <td class="align-center">Ж</td>
                            <td class="align-center">М</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>${reportBody[i].hiredW}</td>
                            <td>${reportBody[i].hiredM}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>${reportBody[i].dismissedW}</td>
                            <td>${reportBody[i].dismissedM}</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td>${reportBody[i].workersCount}</td>
                        </tr>

                    </table>
                </td>
            </c:forEach>
            <td>
                <div class="report-month-sum">${sumWorkers}</div>
                <div class="report-month-avg">
                    <fmt:formatNumber type="number"
                    groupingUsed="false"
                    value="${sumWorkers/(fn:length(reportBody) -1) }"/>
                </div>
            </td>
        </tr>
    </table>
</div>