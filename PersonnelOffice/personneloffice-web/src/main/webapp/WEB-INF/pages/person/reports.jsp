<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tiles:insertDefinition name="baseTemplate">
    <tiles:putAttribute name="styles">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery.dataTables.min.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery.resizableColumns.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery/jquery-ui.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/customDataTable.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/Master.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/GlobalSearch.css">
        <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dialogs.css">
    </tiles:putAttribute>
    <tiles:putAttribute name="scripts">
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery-ui.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/date.format.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/Master.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/init-main-table.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/reports.js"></script>
    </tiles:putAttribute>
    <tiles:putAttribute name="title" value="Reports"/>
    <tiles:putAttribute name="body">

        <div class="toolbar-border-top">
            <div class="container">

                <div class="container-search">
                    <div>
                        <spring:message var="placholder" code="search"/>
                        <div class="global-search-container">
                            <input id="search" onblur="if(this.value=='') this.value='<spring:message code="search"/>'"
                                   onfocus="if (this.value=='<spring:message code="search"/>') this.value = ''"
                                   class="global-search" name="SearchQuery"
                                   value="<spring:message code="search"/>" type="text"/>
                            <button id="clearSearch" type="button" class="clear-button"
                                    title="<spring:message code="search"/>">
                            </button>
                        </div>
                    </div>

                    <div class="new-item-buttons">
                        <div class="button-block">
                            <a class="add-row crud-report" data-report-id="0"></a>
                            <a class="edit-row crud-report" href="#"></a>
                            <a class="delete-row crud-report" href="#"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div id="scroll_container">
                <table id="reportsTable">
                    <thead>
                    <tr>
                        <th class="column-report-type">
                            <div class="fixed_header"><span>Название</span></div>
                        </th>
                        <th class="column-date">
                            <div class="fixed_header"><span>Дата последнего применения</span></div>
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${reports}" var="report" varStatus="iter">
                            <tr data-report-id="${report.id}">
                                <td class="column-report-type">${report.reportType}</td>
                                <td class="column-date"><spring:eval expression="report.lastUsingDate"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
                <div class="table-right-border"></div>
            </div>
        </div>

        <div class="curtain">
            <div id="editDialog" class="reveal-modal report"></div>
        </div>

    </tiles:putAttribute>
</tiles:insertDefinition>