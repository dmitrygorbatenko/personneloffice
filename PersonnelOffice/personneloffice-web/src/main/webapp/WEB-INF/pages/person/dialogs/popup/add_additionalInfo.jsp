<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<span class="popup-title"
      data-add="Создать дополнительные сведения"
      data-edit="Редактировать дополнительные сведения"
      data-delete="Удалить дополнительные сведения">
</span>

<hr>

<form:form id="additionalInfo_form" action="${pageContext.request.contextPath}/persons/additional_info/save" commandName="additionalInfo">
    <form:input path="personCardId" type="hidden" value="${ personCard.id }" />
    <form:input path="id" type="hidden" value="${ additionalInfo.id }" />
    <div class="divTable add-family-member">
        <div class="divRow">
            <div>Событие<i class="red">*</i></div>
            <div><form:input path="eventName" class="required" /></div>
        </div>
        <div class="divRow">
            <div>Дата события<i class="red">*</i></div>
            <div style="width: 32%">
                <form:input path="eventDate" class="date-picker required" />
            </div>
        </div>
        <div class="divRow">
            <div>Дата уведомления<i class="red">*</i></div>
            <div style="width: 32%">
                <form:input path="notificationDate" class="date-picker required" />
            </div>
        </div>
    </div>
</form:form>
<br>