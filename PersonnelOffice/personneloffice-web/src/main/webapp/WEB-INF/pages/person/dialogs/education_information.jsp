<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.coherent.api.dao.enums.education.*" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<% pageContext.setAttribute("educations", EducationEnum.values()); %>
<% pageContext.setAttribute("learningForms", LearningFormEnum.values()); %>

    <div id="education_tabs">
        <ul>
            <li><a href="#education_info">Образование</a></li>
            <li><a href="#training">Повышение квалификации</a></li>
            <li><a href="#retraining">Переподготовка</a></li>
            <li><a href="#attestation">Аттестация</a></li>
        </ul>
        <div id="education_info" data-url="education" data-buttonName="<spring:message code="button.education"/>">
            <div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }">
                <a class="add-row crud" href="#" data-url="education" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
                <a class="edit-row crud" href="#"></a>
                <a class="delete-row crud" href="#"></a>
            </div>
            <div  class="edu-block">
                <table>
                    <tr>
                        <td style="width: 120px;">Образование<i class="red">*</i></td>
                        <td>
                            <div class="select_div">
                                <form:select path="educationDegree" cssClass="required">
                                    <form:option value=""/>
                                    <c:forEach items="${educations}" var="education">
                                        <form:option value="${education}"><spring:message code="${education.value}"/></form:option>
                                    </c:forEach>
                                </form:select>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>Учебное заведение</td>
                        <td><form:input path="institution" /></td>
                    </tr>
                    <tr>
                        <td>Вид обучения<i class="red">*</i></td>
                        <td>
                            <div class="select_div">
                                <form:select path="learningForm" cssClass="required">
                                    <form:option value=""/>
                                    <c:forEach items="${learningForms}" var="learningForm">
                                        <form:option value="${learningForm}"><spring:message code="${learningForm.value}"/></form:option>
                                    </c:forEach>
                                </form:select>
                            </div>
                        </td>
                    </tr>
                </table>
                <table class="with-border">
                    <tr>
                        <td style="width: 90px">Учащийся</td>
                        <td><form:checkbox path="studying" id="studying"/></td>
                    </tr>
                    <tr>
                        <td>Бюджет</td>
                        <td><form:checkbox path="budget" id="budget"/></td>
                    </tr>
                    <tr>
                        <td>Дата окончания</td>
                        <td><form:input path="studyingEndDate" class="date-picker"/></td>
                    </tr>
                </table>
                <div class="edu_finish_date">
                </div>
            </div>
            <hr>

            <div class="info-tables-wrapper">

                <table id="education_table" class="info-tables">
                    <thead data-entity-attributes="data-id:id">
                        <tr>
                            <td data-entity-property="institution">Учебное заведение</td>
                            <td data-entity-property="faculty">Факультет</td>
                            <td data-entity-property="qualification">Квалификация</td>
                            <td data-entity-property="specialty">Специальность</td>
                            <td data-entity-property="budget">Бюджет</td>
                            <td data-entity-property="documentType">Тип документа</td>
                            <td data-entity-property="diplomaDate">Дата выдачи документа</td>
                            <td data-entity-property="diplomaNumber">№ документа</td>
                            <td data-entity-property="educationEndDate">Дата окончания</td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${personCard.education}" var="education">
                            <tr data-url="education" data-person-id="${personCard.id}" data-id="${education.id}">
                                <td>${education.institution}</td>
                                <td>${education.faculty}</td>
                                <td>${education.qualification}</td>
                                <td>${education.specialty}</td>
                                <td><c:if test="${education.budget}"><input id="invalid" type="checkbox" disabled checked></c:if></td>
                                <td>
                                    <c:if test="${education.documentType != null}">
                                        <spring:message code="${education.documentType.value}"/>
                                    </c:if>
                                </td>
                                <td><spring:eval expression="education.diplomaDate"/></td>
                                <td>${education.diplomaNumber}</td>
                                <td><spring:eval expression="education.educationEndDate"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>

            <hr>
            <fieldset id="missionFieldset">
                <legend><spring:message code="education.mission"/></legend>
                <table>
                    <tr>
                        <td>Дата начала</td>
                        <td><form:input path="allocationStartDate" type="text" class="date-picker" /></td>
                        <td>Дата окончания</td>
                        <td><form:input path="allocationEndDate" type="text" class="date-picker" /></td>
                    </tr>
                    <tr>
                        <td>№ свидетельства</td>
                        <td colspan="3"><form:input path="certificateNumber" type="text" /></td>
                    </tr>
                </table>
            </fieldset>
        </div>



        <div id="training" data-url="professional" data-buttonName="<spring:message code="button.increase"/>">
            <div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }" >
                <a class="add-row crud" href="#" data-url="training" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
                <a class="edit-row crud" href="#"></a>
                <a class="delete-row crud" href="#"></a>
            </div>

            <div class="info-tables-wrapper">

                <table id="training_table" class="info-tables">
                    <thead data-entity-attributes="data-id:id">
                        <tr>
                            <td data-entity-property="startDate">Дата начала</td>
                            <td data-entity-property="endDate">Дата окончания</td>
                            <td data-entity-property="institution">Учебное заведение</td>
                            <td data-entity-property="studyingTopic">Тематика обучения</td>
                            <td data-entity-property="documentType">Тип документа</td>
                            <td data-entity-property="documentNumber">№ документа</td>
                            <td data-entity-property="documentDate">Дата документа</td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${personCard.training}" var="training">
                            <tr data-url="training" data-person-id="${personCard.id}" data-id="${training.id}">
                                <td><f:formatDate value="${training.startDate}" pattern="dd.MM.yyyy"/></td>
                                <td><f:formatDate value="${training.endDate}" pattern="dd.MM.yyyy"/></td>
                                <td>${training.institution}</td>
                                <td>${training.studyingTopic}</td>
                                <td>
                                    <c:if test="${training.documentType != null}">
                                        <spring:message code="${training.documentType.value}"/>
                                    </c:if>
                                </td>
                                <td>${training.documentNumber}</td>
                                <td><f:formatDate value="${training.documentDate}" pattern="dd.MM.yyyy"/></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </div>
        </div>



        <div id="retraining" data-url="retraining" data-buttonName="<spring:message code="button.training"/>">
            <div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }" >
                <a class="add-row crud" href="#" href="#" data-url="retraining" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
                <a class="edit-row crud" href="#"></a>
                <a class="delete-row crud" href="#"></a>
            </div>

            <div class="info-tables-wrapper">

                <table id="retraining_table" class="info-tables">
                    <thead data-entity-attributes="data-id:id">
                        <tr>
                            <td data-entity-property="startDate">Дата начала</td>
                            <td data-entity-property="endDate">Дата окончания</td>
                            <td data-entity-property="institution">Учебное заведение</td>
                            <td data-entity-property="studyingTopic">Специальность</td>
                            <td data-entity-property="documentType">Тип приказа</td>
                            <td data-entity-property="documentDate">Приказ <br>дата</td>
                            <td data-entity-property="documentNumber">№ приказа</td>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach items="${personCard.retraining}" var="retraining">
                            <tr data-url="retraining" data-person-id="${personCard.id}" data-id="${retraining.id}">
                                <td><f:formatDate value="${retraining.startDate}" pattern="dd.MM.yyyy"/></td>
                                <td><f:formatDate value="${retraining.endDate}" pattern="dd.MM.yyyy"/></td>
                                <td>${retraining.institution}</td>
                                <td>${retraining.studyingTopic}</td>
                                <td>
                                    <c:if test="${retraining.documentType != null}">
                                        <spring:message code="${retraining.documentType.value}"/>
                                    </c:if>
                                </td>
                                <td><f:formatDate value="${retraining.documentDate}" pattern="dd.MM.yyyy"/></td>
                                <td>${retraining.documentNumber}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>



        <div id="attestation" data-url="attestation" data-buttonName="<spring:message code="button.attestation"/>">
            <div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }" >
                <a class="add-row crud" href="#" data-url="attestation" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
                <a class="edit-row crud" href="#"></a>
                <a class="delete-row crud" href="#"></a>
            </div>

            <div class="info-tables-wrapper">

                <table id="attestation_table" class="info-tables">
                    <thead data-entity-attributes="data-id:id">
                    <tr>
                        <td data-entity-property="date">Дата аттестации</td>
                        <td data-entity-property="decision">Решение комиссии</td>
                        <td data-entity-property="documentType">Тип документа</td>
                        <td data-entity-property="documentNumber">№ <br> документа</td>
                        <td data-entity-property="documentDate">Дата документа</td>
                        <td data-entity-property="nextAttestation">Следующая аттестация</td>
                        <td data-entity-property="reason">Основание</td>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${personCard.attestations}" var="attestation">
                        <tr data-url="attestation" data-person-id="${personCard.id}" data-id="${attestation.id}">
                            <td><f:formatDate value="${attestation.date}" pattern="dd.MM.yyyy"/></td>
                            <td>${attestation.decision}</td>
                            <td>
                                <c:if test="${attestation.documentType != null}">
                                    <spring:message code="${attestation.documentType.value}"/>
                                </c:if>
                            </td>
                            <td>${attestation.documentNumber}</td>
                            <td><f:formatDate value="${attestation.documentDate}" pattern="dd.MM.yyyy"/></td>
                            <td><f:formatDate value="${attestation.nextAttestation}" pattern="dd.MM.yyyy"/></td>
                            <td>${attestation.reason}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>