<%@ page import="com.coherent.api.dao.enums.documents.*" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% pageContext.setAttribute("categoryValues", CategoryEnum.values()); %>
<% pageContext.setAttribute("subspeciesValues", SubSpeciesEnum.values()); %>
<% pageContext.setAttribute("speciesValues", SpeciesEnum.values()); %>
<% pageContext.setAttribute("paperTypeValues", PaperTypeEnum.values()); %>

<span class="popup-title"
      data-add="Создать документ"
      data-edit="Редактировать документ"
      data-delete="Удалить документ">
</span>

<hr>
<div id="doc-main-wrap">
<div id="doc-sidebar">
    <form:form id="document_form" action="${pageContext.request.contextPath}/persons/document/save" commandName="document">
    <form:input path="id" type="hidden" value="${ document.id }" />

    <table>
        <tbody>
        <tr>
            <td>Тип документа<i class="red">*</i></td>
            <td>
                <div style="width:110px" class="select_div popup-select">
                    <form:select path="paperType" class="required">
                        <form:option value=""/>
                        <c:forEach items="${paperTypeValues}" var="paperType">
                            <form:option value="${paperType}"><spring:message code="${paperType.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </td>
        </tr>

        <tr>
            <td>Вид<i class="red">*</i></td>
            <td>
                <div style="width:110px" class="select_div popup-select">
                    <form:select path="species" class="required">
                        <form:option value=""/>
                        <c:forEach items="${speciesValues}" var="species">
                            <form:option value="${species}"><spring:message code="${species.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </td>
        </tr>
        <tr>
            <td>Подвид</td>
            <td>
                <div style="width:110px" class="select_div popup-select">
                    <form:select path="subspecies">
                        <form:option value=""/>
                        <c:forEach items="${subspeciesValues}" var="subspecies">
                            <form:option value="${subspecies}"><spring:message code="${subspecies.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </td>
        </tr>

        <tr>
            <td>Дата приказа<i class="red">*</i></td>
            <td style="text-align: left;">
                <form:input path="paperDate" class="date-picker required" />
            </td>
        </tr>
        <tr>
            <td>Номер приказа<i class="red">*</i></td>
            <td>
                <form:input path="paperNumber" class="required" />
            </td>
        </tr>

        <tr>
            <td>Категория<i class="red">*</i></td>
            <td>
                <div style="width:110px" class="select_div popup-select">
                    <form:select path="category" class="required">
                        <form:option value=""/>
                        <c:forEach items="${categoryValues}" var="category">
                            <form:option value="${category}"><spring:message code="${category.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </td>
        </tr>
        <tr><td></td><td></td></tr>
        <tr><td></td><td></td></tr>
        <tr>
            <td>ФИО<i class="red">*</i></td>
            <td>
                <div class="ui-widget">
                    <input id="doc-full-name-search-two">
                </div>
                <div id="doc-auto-complete-result"></div>
            </td>
        </tr>
        <tr style='display: none;'>
            <td>Котент</td>
            <td>
                <form:input id="doc-content" path="content" />
            </td>
        </tr>
        <tr style='display: none;'>
            <td>PersonCardIds</td>
            <td id="document-card-id-list">
                <c:forEach var="i" items="${ document.personCardIdList }">
                    <div><c:out value="${ i.person_card_id }"/></div>
                </c:forEach>
            </td>
        </tr>

        </tbody>
    </table>

    <div id="autocomplete-card-ids" class="autocomplete name-selector">
        <ul>
        <!--<li><span>Иванов Иван Иванович</span><a class="remove-name" href="#"></a></li>-->
        </ul>
    </div>
    <button id="document-generate-btn" class="button doc-editor-update-btn" type="button">Генерация</button>
    <button id="document-template-btn" class="button doc-editor-update-btn" type="button">Шаблон</button>
    </div>
    <div id="doc-content-wrap">
        <div class="name-selector" id="intelli-sense" style="position: absolute; background-color: white; width: auto; height: auto;">
                <ul>
                    <li data-value="-1"><span>@full_name</span>
                </ul>
        </div>
        <textarea name="document-editor" id="document-editor" rows="10" cols="80"></textarea>
    </div>
</div> 
</form:form>

