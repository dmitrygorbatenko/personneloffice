<%@ page import="com.coherent.api.dao.enums.general.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<% pageContext.setAttribute("documents", DocumentTypeEnum.values()); %>
<% pageContext.setAttribute("countries", CountryEnum.values()); %>
<% pageContext.setAttribute("nationalities", NationalityEnum.values()); %>

<spring:message code="placeholder.telephone.home" var="homeTelephone"/>
<spring:message code="placeholder.telephone.work" var="workTelephone"/>
<spring:message code="placeholder.telephone.mobile" var="mobileTelephone"/>

    <div>
        <fieldset>
            <legend>Место рождения</legend>
            <table class="birthPlace">
                <tr>
                    <td>страна<i class="red">*</i></td>
                    <td>
                        <div class="select_div">
                            <form:select path="birthplaceCountry" cssClass="required">
                                <form:option value=""/>
                                <c:forEach items="${countries}" var="country">
                                    <form:option value="${country}"><spring:message code="${country.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                    <td>область</td>
                    <td><form:input id="bpProvince" path="birthplaceProvince" /></td>
                </tr>
                <tr>
                    <td>район</td>
                    <td><form:input id="bpRegion" path="birthplaceRegion"/></td>
                    <td>нас.пункт</td>
                    <td><form:input id="bpLocality" path="birthplaceLocality"/></td>
                </tr>
            </table>
        </fieldset>
        <hr>
    </div>

    <div>
        <fieldset>
            <legend>Гражданство и документ</legend>
            <table class="documents">
                <tr>
                    <td>документ</td>
                    <td colspan="3">
                        <div class="select_div">
                            <form:select path="document">
                                <form:option value=""/>
                                <c:forEach items="${documents}" var="document">
                                    <form:option value="${document}"><spring:message code="${document.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>серия</td>
                    <td><form:input path="documentSeries"/></td>
                    <td style="width: 20px">№</td>
                    <td><form:input path="documentSerialNumber"/></td>
                </tr>
                <tr>
                    <td style="width: 65px;">личный №</td>
                    <td colspan="3"><form:input path="documentIndividualNumber" id="individualNumber"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="3"><a class="copy-fields" data-copy="insurance" href="#">копировать личный № в № страх.свидетельства</a></td>
                </tr>
                <tr>
                    <td>выдан</td>
                    <td colspan="3"><form:input path="documentIssuedBy"/></td>
                </tr>
            </table>
            <table class="citizenship">
                <tr>
                    <td>Гражданство</td>
                    <td colspan="3">
                        <div class="select_div">
                            <form:select path="nationality">
                                <form:option value=""/>
                                <c:forEach items="${nationalities}" var="nationality">
                                    <form:option value="${nationality}"><spring:message code="${nationality.value}"/></form:option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Латиница</td>
                    <td colspan="3"><form:input path="latin"/></td>
                </tr>
                <tr>
                    <td>№ страхового <br> свидетельства</td>
                    <td colspan="3"><form:input path="insuranceCertificateNumber" id="insuranceNumber"/></td>
                </tr>
                <tr>
                    <td>дата выдачи</td>
                    <td><form:input path="documentDateIssuance" class="date-picker" /></td>
                    <td><nobr>действителен до</nobr></td>
                    <td colspan="2"><form:input path="documentValidityPeriod" class="date-picker"/></td>
                </tr>
            </table>
        </fieldset>
    </div>
    <div id="address_tabs">
        <ul>
            <li><a href="#living_address">Адрес проживания</a></li>
            <li><a href="#registration_address">Адрес регистрации</a></li>
        </ul>
        <div id="living_address">
            <fieldset>
                <form:hidden path="residentialAddress.id"/>
                <table>
                    <tr>
                        <td style="width: 40px;">область</td>
                        <td style="width: 30%;"><form:input path="residentialAddress.province" id="residentialAddress_province"/></td>
                        <td>район</td>
                        <td colspan="2"><form:input path="residentialAddress.region" id="residentialAddress_region"/></td>
                        <td colspan="2">сельский исп.комитет</td>
                        <td colspan="2"><form:input path="residentialAddress.ruralCommittee" id="residentialAddress_ruralCommittee"/></td>
                    </tr>
                    <tr>
                        <td>населенный пункт</td>
                        <td><form:input path="residentialAddress.locality" id="residentialAddress_locality"/></td>
                        <td>индекс</td>
                        <td><form:input path="residentialAddress.postcode" id="residentialAddress_postcode"/></td>
                        <td></td>
                        <td colspan="2">регистрация до</td>
                        <td><form:input path="residentialAddress.registrationUntil" class="date-picker"/></td>
                    </tr>
                    <tr>
                        <td>улица</td>
                        <td><form:input path="residentialAddress.street" id="residentialAddress_street"/></td>
                        <td>дом</td>
                        <td><form:input path="residentialAddress.house" id="residentialAddress_house"/></td>
                        <td>корпус</td>
                        <td><form:input path="residentialAddress.building" id="residentialAddress_building" style="width: 70px;"/></td>
                        <td style="width: 20px">кв.</td>
                        <td><form:input path="residentialAddress.apartment" id="residentialAddress_apartment" style="width: 70px;"/></td>
                    </tr>
                </table>
                <a class="copy-fields" data-copy="registration" href="#">копировать адрес проживания в адрес регистрации</a>
            </fieldset>
            <hr>
            <fieldset style="margin-top: 0">
                <table>
                    <tr>
                        <td>дом.телефон</td>
                        <td><form:input id="homeTelephone" path="residentialAddress.phone" placeholder="${homeTelephone}" /></td>
                        <td colspan="3">раб.телефон</td>
                        <td><form:input id="workTelephone" path="residentialAddress.workPhone" placeholder="${workTelephone}" /></td>
                        <td>моб.телефон</td>
                        <td colspan="3"><form:input id="mobileTelephone" path="residentialAddress.mobilePhone" placeholder="${mobileTelephone}" /></td>
                    </tr>
                </table>
            </fieldset>
        </div>
        <div id="registration_address">
            <fieldset>
                <form:hidden path="registrationAddress.id"/>
                <table>
                    <tr>
                        <td style="width: 40px;">область</td>
                        <td style="width: 30%;"><form:input path="registrationAddress.province" id="registrationAddress_province"/></td>
                        <td>район</td>
                        <td colspan="2"><form:input path="registrationAddress.region" id="registrationAddress_region"/></td>
                        <td>сельский исп.комитет</td>
                        <td colspan="2"><form:input path="registrationAddress.ruralCommittee" id="registrationAddress_ruralCommittee"/></td>
                    </tr>
                    <tr>
                        <td>населенный пункт</td>
                        <td><form:input path="registrationAddress.locality" id="registrationAddress_locality"/></td>
                        <td>индекс</td>
                        <td><form:input path="registrationAddress.postcode" id="registrationAddress_postcode"/></td>
                    </tr>
                    <tr>
                        <td>улица</td>
                        <td><form:input path="registrationAddress.street" id="registrationAddress_street"/></td>
                        <td>дом</td>
                        <td><form:input path="registrationAddress.house" id="registrationAddress_house"/></td>
                        <td>корпус</td>
                        <td><form:input path="registrationAddress.building" id="registrationAddress_building" style="width: 70px;"/></td>
                        <td style="width: 20px">кв.</td>
                        <td><form:input path="registrationAddress.apartment" id="registrationAddress_apartment" style="width: 70px;"/></td>
                    </tr>
                </table>
            </fieldset>
        </div>
    </div>