<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

     <tiles:insertAttribute name="dialogHeader"/>
     <tiles:insertAttribute name="body"/>
     <tiles:insertAttribute name="dialogFooter"/>
