<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }">
    <a class="add-row crud" href="#" data-url="vacation" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
    <a class="edit-row crud" href="#"></a>
    <a class="delete-row crud" href="#"></a>
</div>

<div class="rest_counter">
    <label>Остаток отпуска на текущий период:</label>
    <span id="rest-of-vacation">7</span><br>
    <label>Всего дней отпуска в году:</label>
    <span id="total-vacation-year">28</span>
</div>

<div class="info-tables-wrapper">

    <table id="vacation_table" class="info-tables">
        <thead data-entity-attributes="data-id:id">
            <tr>
                <td colspan="2">Период</td>
                <td colspan="4">Отпуск</td>
                <td colspan="2">Приказ</td>
            </tr>
            <tr>
                <td data-entity-property="periodFrom">с</td>
                <td data-entity-property="periodTo">по</td>
                <td data-entity-property="vacationType">Вид отпуска</td>
                <td data-entity-property="vacationFrom">с</td>
                <td data-entity-property="vacationTo">по</td>
                <td data-entity-property="duration">Длит-ть/<br>комп.</td>
                <td data-entity-property="orderNumber">№</td>
                <td data-entity-property="orderDate">Дата</td>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="vacation" items="${personCard.vacations}">
                <tr data-url="vacation" data-person-id="${personCard.id}" data-id="${vacation.id}">
                    <td><f:formatDate value="${vacation.periodFrom}" pattern="dd.MM.yyyy"/></td>
                    <td><f:formatDate value="${vacation.periodTo}" pattern="dd.MM.yyyy"/></td>
                    <td>
                        <c:if test="${vacation.vacationType != null}">
                            <spring:message code="${vacation.vacationType.value}"/>
                        </c:if>
                    </td>
                    <td><f:formatDate value="${vacation.vacationFrom}" pattern="dd.MM.yyyy"/></td>
                    <td><f:formatDate value="${vacation.vacationTo}" pattern="dd.MM.yyyy"/></td>
                    <td>${vacation.duration}</td>
                    <td>${vacation.orderNumber}</td>
                    <td><f:formatDate value="${vacation.orderDate}" pattern="dd.MM.yyyy"/></td>
                </tr>
            </c:forEach>
        </tbody>
    </table>

</div>