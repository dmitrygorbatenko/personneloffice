<%@ page import="com.coherent.api.dao.enums.vacation.VacationTypeEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<% pageContext.setAttribute("vacationTypes", VacationTypeEnum.values()); %>

<span class="popup-title"
      data-add="Создать отпуск"
      data-edit="Редактировать отпуск"
      data-delete="Удалить потпуск">
</span>
<hr>

<form:form id="vacation_form" action="${pageContext.request.contextPath}/persons/vacation/save" commandName="vacation">
    <form:input path="personCardId" value="${personCard.id}" type="hidden"/>
    <form:input path="id" value="${ vacation.id }" type="hidden"/>

    <div class="divTable">

        <div class="divRow">
            <div>Период с</div>
            <div><form:input path="periodFrom" class="date-picker" /></div>
        </div>
        <div class="divRow">
            <div>Период по</div>
            <div><form:input path="periodTo" class="date-picker" /></div>
        </div>
        <div class="divRow">
            <div>Вид отпуска</div>
            <div>
                <div class="select_div popup-select">
                    <form:select path="vacationType">
                        <form:option value=""/>
                        <c:forEach items="${vacationTypes}" var="vacationType">
                            <form:option value="${vacationType}"><spring:message code="${vacationType.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </div>
        <div class="divRow">
            <div>Отпуск с</div>
            <div><form:input path="vacationFrom" class="date-picker" /></div>
        </div>
        <div class="divRow">
            <div>Отпуск по</div>
            <div><form:input path="vacationTo" class="date-picker" /></div>
        </div>
        <div class="divRow">
            <div>Продолжительность</div>
            <div><form:input path="duration"/></div>
        </div>
        <div class="divRow">
            <div>№ приказа</div>
            <div><form:input path="orderNumber" /></div>
        </div>
        <div class="divRow">
            <div>Дата приказа</div>
            <div><form:input path="orderDate" class="date-picker" /></div>
        </div>

    </div>

</form:form>