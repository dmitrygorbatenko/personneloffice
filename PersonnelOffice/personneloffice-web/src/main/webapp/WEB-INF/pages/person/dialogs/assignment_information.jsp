<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

    <div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }">
        <a href="#" class="add-row crud" data-url="assignment" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
        <a href="#" class="edit-row crud"></a>
        <a href="#" class="delete-row crud"></a>
    </div>
    <span class="table-name">Назначения/Перемещения</span>

<div class="info-tables-wrapper">

    <table id="assignment_table" class="info-tables">
        <thead data-entity-attributes="data-id:id">
            <tr>
                <td data-entity-property="position" class="assignment-head-td">Должность</td>
                <td data-entity-property="rate" class="assignment-head-td">Кол-во ставок</td>
                <td data-entity-property="form" class="assignment-head-td">Форма</td>
                <td data-entity-property="contractNumber" class="assignment-head-td">№ котракта/ договора</td>
                <td data-entity-property="contractDate" class="assignment-head-td">Дата контракта/ договора</td>
                <td data-entity-property="contractStart" class="assignment-head-td">Дата начала контракта/ договора</td>
                <td data-entity-property="contractEnd" class="assignment-head-td">Дата окончания контракта/ договора</td>
                <td data-entity-property="orderNumber" class="assignment-head-td">№ приказа</td>
                <td data-entity-property="orderDate" class="assignment-head-td">Дата приказа</td>
                <td data-entity-property="department" class="assignment-head-td">Подразделение (отдел)</td>
            </tr>
        </thead>
        <tbody>
            <c:forEach var="assignment" items="${personCard.assignments}">
                <tr data-url="assignment" data-person-id="${personCard.id}" data-id="${assignment.id}">
                    <td>
                        <c:if test="${assignment.position != null}">
                            <spring:message code="${assignment.position.value}"/>
                        </c:if>
                    </td>
                    <td>${assignment.rate}</td>
                    <td>
                        <c:if test="${assignment.form != null}">
                            <spring:message code="${assignment.form.value}"/>
                        </c:if>
                    </td>
                    <td>${assignment.contractNumber}</td>
                    <td><f:formatDate value="${assignment.contractDate}" pattern="dd.MM.yyyy"/></td>
                    <td><f:formatDate value="${assignment.contractStart}" pattern="dd.MM.yyyy"/></td>
                    <td><f:formatDate value="${assignment.contractEnd}" pattern="dd.MM.yyyy"/></td>
                    <td>${assignment.orderNumber}</td>
                    <td><f:formatDate value="${assignment.orderDate}" pattern="dd.MM.yyyy"/></td>
                    <td>${assignment.department}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</div>

<hr>
<span class="table-name">Увольнения</span>

<div class="info-tables-wrapper">

    <table id="dismissal_table" data-person-id="${personCard.id}" class="info-tables">

        <thead>
        <tr>
            <td data-entity-property="paperNumber">№ приказа</td>
            <td data-entity-property="paperDate">Дата приказа</td>
            <td data-entity-property="reason">Основание</td>
            <td data-entity-property="dismissalDate">Дата увольнения</td>
            <td data-entity-property="compensationDays">Кол-во компенс. дней</td>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
</div>