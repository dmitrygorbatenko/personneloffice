<%@ page import="com.coherent.api.dao.enums.education.DocumentTypeEnum" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<% pageContext.setAttribute("documentTypes", DocumentTypeEnum.values()); %>

<span class="popup-title"
      data-add="Создать атестацию"
      data-edit="Редактировать атестацию"
      data-delete="Удалить атестацию">
</span>

<hr>

<form:form id="training_form" action="${pageContext.request.contextPath}/persons/attestation/save" commandName="attestation">
    <form:input path="personCardId" type="hidden"  value="${ personCard.id }"/>
    <form:input path="id" type="hidden" value="${ attestation.id }" />

    <div class="divTable">
        <div class="divRow">
            <div>Дата аттестации<i class="red">*</i></div>
            <div><form:input path="date" class="date-picker" /></div>
        </div>
        <div class="divRow">
            <div>Решение комиссии</div>
            <div><form:input path="decision" /></div>
        </div>
        <div class="divRow">
            <div>Тип документа</div>
            <div>
                <div class="select_div popup-select">
                    <form:select path="documentType">
                        <form:option value=""/>
                        <c:forEach items="${documentTypes}" var="documentType">
                            <form:option value="${documentType}"><spring:message code="${documentType.value}"/></form:option>
                        </c:forEach>
                    </form:select>
                </div>
            </div>
        </div>
        <div class="divRow">
            <div>№ документа</div>
            <div><form:input path="documentNumber"  cssClass="document-number"/></div>
        </div>
        <div class="divRow">
            <div>Дата документа</div>
            <div><form:input path="documentDate" cssClass="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>Следующая аттестация</div>
            <div><form:input path="nextAttestation" cssClass="date-picker"/></div>
        </div>
        <div class="divRow">
            <div>Основание</div>
            <div><form:input path="reason" /></div>
        </div>
    </div>
</form:form>
<br>