<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="com.coherent.api.dao.enums.general.*" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="formID" value="generalInformationForm" scope="request"/>
<% pageContext.setAttribute("genders", GenderEnum.values()); %>
<% pageContext.setAttribute("familyStatuses", FamilyStatusEnum.values()); %>
<% pageContext.setAttribute("academicRanks", AcademicRankEnum.values()); %>
<% pageContext.setAttribute("statuses", StatusEnum.values()); %>
<% pageContext.setAttribute("documents", DocumentTypeEnum.values()); %>


<tiles:insertDefinition name="dialogTemplate">
    <tiles:putAttribute name="body">
        <form:form id="generalInformationForm" action="${pageContext.request.contextPath}/persons/save" commandName="personCard" >
            <form:hidden path="id" id="parent_id" value="${not empty personCard.id ? personCard.id : 0 }" />
            <div id="head">
                <div class="avatar">
                    <span class="ava_img"></span>
                    <a class="update_photo" href="#" title="Обновить фото"></a>
                </div>
                <div class="info_block">
                    <table>
                        <tr>
                            <td>Фамилия<i class="red">*</i></td>
                            <td><form:input path="lastName" class="required"/></td>
                            <td><a class="old_lastName" href="#"></a></td>
                        </tr>
                        <tr>
                            <td>Имя<i class="red">*</i></td>
                            <td><form:input path="firstName" class="required"/></td>
                        </tr>
                        <tr>
                            <td>Отчество<i class="red">*</i></td>
                            <td><form:input path="patronymic" class="required"/></td>
                        </tr>
                        <tr>
                            <td>№ карточки</td>
                            <td><form:input type="text" path="cardNumber"/></td>
                        </tr>
                        <tr>
                            <td>RMSys:</td>
                            <td>n/a</td>
                        </tr>
                    </table>
                </div>
                <div class="info_block">
                    <table>
                        <tr>
                            <td>Пол<i class="red">*</i></td>
                            <td>
                                <div style="width:110px" class="select_div">
                                    <form:select path="gender" id="gender"  cssClass="required">
                                        <form:option value=""/>
                                        <c:forEach items="${genders}" var="gender">
                                            <form:option value="${gender}"><spring:message code="${gender.value}"/></form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Дата рождения<i class="red">*</i></td>
                            <td><form:input id="card-birthday" path="birthday" class="date-picker required" /></td>
                        </tr>
                        <tr>
                            <td>Семейное положение</td>
                            <td>
                                <div class="select_div">
                                    <form:select path="familyStatus" id="familyStatus">
                                        <form:option value=""/>
                                        <c:forEach items="${familyStatuses}" var="familyStatus">
                                            <form:option value="${familyStatus}"><spring:message code="${familyStatus.value}"/></form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>Статус</td>
                            <td>
                                <div class="select_div">
                                    <form:select path="status" id="status">
                                        <c:forEach items="${statuses}" var="status">
                                            <form:option value="${status}"><spring:message code="${status.value}"/></form:option>
                                        </c:forEach>
                                    </form:select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <c:if test="${personCard.lastAssignment != null}">
                                    Должность: <spring:message code="${personCard.lastAssignment.position.value}"/>
                                </c:if>
                            </td>
                        </tr>
                    </table>
                </div>
                <c:choose>
                    <c:when test="${empty personCard.id}">
                        <div class="info_block third spacing">
                            <a id="export-personCard-excel" class="disabled-crud-icons" title="Экспорт в Excel"></a>
                            <i class="separator disabled-crud-icons">|</i>
                            <button id="parent-dialog-newDocument" data-person-id="0" class="button add_order disabled-crud-icons" disabled type="button">Добавить приказ</button>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="info_block third spacing">
                            <a id="export-personCard-excel" href="${pageContext.request.contextPath}/file/export/excel/T2/${personCard.id}" title="Экспорт в Excel"></a>
                            <i class="separator ${not empty personCard.id ? '' : 'disabled-crud-icons' }">|</i>
                            <button id="parent-dialog-newDocument" data-url="document" data-person-id="${personCard.id}" class="button add_order" type="button">Добавить приказ</button>
                        </div>
                    </c:otherwise>
                </c:choose>
                <div class="info_block third">
                    <table>
                        <tr>
                            <td>Таб №</td>
                            <td><form:input type="text" path="tabNumber"/></td>
                        </tr>
                        <tr>
                            <td>На испыт. сроке </td>
                            <td><form:checkbox path="testPeriod" id="test_period"/></td>
                        </tr>
                        <tr>
                            <td>Дата оконч. испыт. срока:</td>
                            <td><form:input path="testPeriodEndDate" class="date-picker" id="test_period_end_date"/></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="dialog_tabs">
                <ul id="tabs-titles">
                    <li><a href="#generalInformation"><span>Общие сведения</span></a></li>
                    <li><a href="#familyInformation"><span>Состав семьи</span></a></li>
                    <li><a href="#educationInformation"><span>Образование</span></a></li>
                    <li><a href="#militaryInformation"><span>Воинский учет</span></a></li>
                    <li><a href="#vacationInformation"><span>Отпуска</span></a></li>
                    <li><a href="#documentInformation"><span>Документы</span></a></li>
                    <li><a href="#assignmentInformation"><span>Назначения/Перемещения</span></a></li>
                    <li><a href="#additionalInformation"><span>Дополнительные сведения</span></a></li>
                </ul>
                <div id="tabs-contents">
                    <div id="generalInformation">
                        <jsp:include page="general_information.jsp"/>
                    </div>
                    <div id="familyInformation">
                        <jsp:include page="family_information.jsp"/>
                    </div>
                    <div id="educationInformation">
                        <jsp:include page="education_information.jsp"/>
                    </div>
                    <div id="militaryInformation">
                        <jsp:include page="military_information.jsp"/>
                    </div>
                    <div id="vacationInformation">
                        <jsp:include page="vacation_information.jsp"/>
                    </div>
                    <div id="documentInformation">
                        <jsp:include page="documents_information.jsp"/>
                    </div>
                    <div id="assignmentInformation">
                        <jsp:include page="assignment_information.jsp"/>
                    </div>
                    <div id="additionalInformation">
                        <jsp:include page="additional_information.jsp"/>
                    </div>
                </div>
            </div>
        </form:form>
    </tiles:putAttribute>
</tiles:insertDefinition>

<div class="curtain">
    <div id="draggable">
        <div id="addNote" class="reveal-modal medium">
            <tiles:insertDefinition name="addNote">
                <tiles:putAttribute name="body">
                    <div id="noteContent"></div>
                </tiles:putAttribute>
            </tiles:insertDefinition>
        </div>
    </div>

</div>
