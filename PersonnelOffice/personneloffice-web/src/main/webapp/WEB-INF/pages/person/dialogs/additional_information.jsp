<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="button-block ${not empty personCard.id ? '' : 'disabled-crud-icons' }">
    <a class="add-row crud" href="#" data-url="additional_info" data-person-id="${not empty personCard.id ? personCard.id : 0 }" data-id="0"></a>
    <a class="edit-row crud" href="#" data-person-id="${personCard.id}"></a>
    <a class="delete-row crud" href="#"></a>
</div>

<span class="table-name">Дополнительные сведения</span>

<div class="info-tables-wrapper">

    <table id="additional_info" class="info-tables">
        <thead data-entity-attributes="data-id:id">
        <tr>
            <td data-entity-property="eventName">Событие</td>
            <td data-entity-property="eventDate">Дата события</td>
            <td data-entity-property="notificationDate">Дата уведомления</td>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${personCard.additionals}" var="additionalInfo">
            <tr data-url="additional_info" data-person-id="${personCard.id}" data-id="${additionalInfo.id}">
                <td>${additionalInfo.eventName}</td>
                <td><spring:eval expression="additionalInfo.eventDate"/></td>
                <td><spring:eval expression="additionalInfo.notificationDate"/></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

</div>