<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<span class="warning-message"></span>

<script type="text/javascript">

    var record_not_selected = '<spring:message code="message.warning.record.not.selected" />';
    var record_successfully_saved = '<spring:message code="message.warning.record.successfully.saved" />';
    var record_successfully_deleted = '<spring:message code="message.warning.record.successfully.deleted" />';

</script>