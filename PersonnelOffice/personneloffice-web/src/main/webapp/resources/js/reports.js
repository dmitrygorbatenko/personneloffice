var reportsTableRow = null;
var reportsTable;

$(document).ready(function()
    {
        editDialog = $("#editDialog");
        reportsTable = $("#reportsTable");

//        $(".crud-report").click(getReportDialog);

        initMainTable(reportsTable, $("reportsTable_wrapper"));

        $(reportsTable).on("dblclick", "tr", getReportDialog);
        $(reportsTable).on("click", "tr", chooseTR);


    }
);

$( window ).resize(resizeTable);

function chooseTR(){
    $(reportsTable).find("tr").removeClass("chosen");
    $(this).addClass("chosen");
}

function getReportBody(){

    if ($(this).hasClass("button-pressed")) {
        return false;
    } else {
        $(this).addClass("button-pressed");
    }

    var form = $("#report_form");
    var data = $(form).serializeArray();
    var url = $(form).attr("action");

    $.ajax(
        {
            url: url,
            type: "POST",
            data: data,
            success: function (data) {
                $("#report-container").html(data);
            }
        });
}

function getReportDialog(){

    if (this.tagName == "TR" || $(this).hasClass("add-row")) {
        reportsTableRow = this;
    } else {
        reportsTableRow = $(reportsTable).find("TR.chosen").get(0);

        if (reportsTableRow == null) {
            showWarningMessage(record_not_selected);
            return false;
        }
    }

    var url = contextPath
        + "/report/get/"
        + $(reportsTableRow).attr("data-report-id");

    $.ajax(
        {
            url: url,
            type: "GET",
            success: function (data) {
                initReport(data);
            }
        });
}

function initReport(data) {

    $(editDialog).html(data);

    closeButton = $(".dialog_close");
    closeButton.on("click", function(){
        showOrHide($(this));
    });
    $("#select-year, #select-month").change(function(){
        $("#view-report-button").removeClass("button-pressed");
    });
    $("#view-report-button").click(getReportBody);
//    $("#export-report-button").click(exportReport);

    showOrHide(editDialog);
}

function resizeTable(){
    $("#scroll_container").height(window.innerHeight - 340);
    var table =  $("#reportsTable");
    table.width($("#reportsTable_wrapper").width() -24);
    $(".fixed-header-background").width(table.outerWidth()-2);
    $(".fixed_header").width(function(){
        return $(this).parent().width() - 20;
    });
}

function createLink(id){
    return contextPath
        + "/file/export/excel/report/"
        + id
        + "/"
        + $("#select-year").find(":selected").attr("value")
        + "/"
        + $("#select-month").find(":selected").attr("value");
}