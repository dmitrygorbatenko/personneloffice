function initMainTable(mainTable, wrapper){
    var documentTypeFilter = $("#documentTypeFilter");
    var dataTable =  mainTable.DataTable({
        "dom":'<"fixed-header-background">t',
        "autoWidth":true,
        "paging": false,
        "info": false,
        "destroy": true,
        "language": {
            "zeroRecords":    ""
        }
    });
    $('#search').keyup(function () {
        dataTable.search( this.value).draw();
    });
    mainTable.on("draw.dt",resizeTable(mainTable));
    documentTypeFilter.on("change", function(){
        dataTable.api().column(".column-status").search(this.value).draw();
    });
    $(wrapper).scroll(function(e){
        if(typeof InstallTrigger !== 'undefined'){  // Firefox 1.0+)
            $(".fixed_header").css("margin-right", this.scrollLeft);
        } else { //other browsers
            $(".fixed_header").css("margin-left", -this.scrollLeft);
        }
    });
    resizeTable(mainTable, wrapper);
    mainTable.find("td").resizable({
        handles: "e",
        resize: function(event, ui){
            var cellIndex = event.target.cellIndex;
            mainTable.find("td:nth-child("+(cellIndex+1)+")").width(ui.size.width);
            if((/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)) || /Opera Software ASA/.test(navigator.vendor)){ //fix for chrome and opera
                var fh = $(".fixed_header");
                fh.css("margin-left", 1);
                fh.css("margin-left",$(wrapper).scrollLeft());
            }
        }
    });
    $("#scroll_container").css("visibility", "visible");
}

//function resizeTable(mainTable, wrapper){
//    $("#scroll_container").height(window.innerHeight - 340);
//    $(mainTable).width($(wrapper).width() -24);
//    $(".fixed-header-background").width(table.outerWidth()-2);
//    $(".fixed_header").width(function(){
//        return $(this).parent().width() - 20;
//    });
//}