
$(document).ready(function () {

    var isAfterFirstCheck = false;
    sessionStorage.setItem("#tabID", window.location.hash) ;

    function isCookieSupports() {
        return navigator.cookieEnabled;
    }

    if (!isCookieSupports()) {
        $('#loginForm').hide();
        $('#cookieDisabledMessage').show();
    }

    $('.loginForm').validate({
        ignore: "",
        rules: {
            login: "required",
            password: "required"
        },
        messages: {
            login: "Введите имя пользователя",
            password: "Введите пароль"
        },
        showErrors: function (errorMap, errorList) {
            if (errorMap.login) {
                $('#error_message_username').text(errorMap.login);
                $('#usernameContainer').addClass("errorBorder");
            } else {
                $('#error_message_username').text("");
                $('#usernameContainer').removeClass("errorBorder");
            }
            if (errorMap.password) {
                $('#error_message_password').text(errorMap.password);
                $('#passwordContainer').addClass("errorBorder");
            } else {
                $('#error_message_password').text("");
                $('#passwordContainer').removeClass("errorBorder");
            }
            isAfterFirstCheck = true;
        },
        onkeyup: function (element, event) {
            if (isAfterFirstCheck) {
                var $input = $(element);
                if ($input.val() == "") {
                    if ($input.attr('id') == 'login') {
                        $('#error_message_username').text("Введите имя пользователя");
                        $('#usernameContainer').addClass("errorBorder");
                    } else if ($input.attr('id') == 'password') {
                        $('#error_message_password').text("Введите пароль");
                        $('#passwordContainer').addClass("errorBorder");
                    }
                } else {
                    $('#error_message_username, #error_message_password').text("");
                    $('#usernameContainer, #passwordContainer').removeClass("errorBorder");
                }
            } else{
                $('#error_message_username').text("");
            }
        },
        onfocusout: false
    });

    $("#login,#password").focus(function () {
        $(this).closest('.formInput').addClass("focused");
    });
    $("#login,#password").blur(function () {
        $(this).closest('.formInput').removeClass("focused");
    });

    $('#login,#password').placeholder();

});


$("#spanCheck").click(function(){
    if($('#remember_me').val()=='false'){
        $('#remember_me').val('true');
        $('#spanCheck').css('background','');
    }else{
        $('#remember_me').val('false');
        $('#spanCheck').css('background','none');
    }
});