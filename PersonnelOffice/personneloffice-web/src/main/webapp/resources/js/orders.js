$(document).ready(function()
    {
        initMainTable($("#ordersTable"), $("#ordersTable_wrapper"));
    }
);

$('#documentTypeFilter').on("change", function() {
    var oTable = $('#ordersTable').dataTable();
    oTable.fnFilter( this.value, 1 );
});

$( window ).resize(resizeTable);

function resizeTable(){
    $("#scroll_container").height(window.innerHeight - 340);
    var table =  $("#ordersTable");
    table.width($("#ordersTable_wrapper").width() -24);
    $(".fixed-header-background").width(table.outerWidth()-2);
    $(".fixed_header").width(function(){
        return $(this).parent().width() - 20;
    });
}
