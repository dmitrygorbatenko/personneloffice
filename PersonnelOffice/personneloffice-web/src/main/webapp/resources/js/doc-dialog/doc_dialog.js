function installDocumentDialogWidgets (contextPath) {
	installIntellisenceKeyPressEvent();
	installCKEditorSettings();
	installIntellisenceHint();
	initAutocompleteWidget(contextPath);
	installGenerateAndTemplateViewModeButtons();
	installFormTemplateGenerator();
}

//6
var datePicker = {
	init: function() {
		$(".date-picker").datepicker({
			dateFormat: 'dd.mm.yy',
			firstDay: 1,
			changeMonth: true,
			changeYear: true,
			yearRange: '-60:+40',
			onClose: this.removeValidationError
		})
	},
	removeValidationError : function() {
		$(this).removeClass("validation_error");
	}
};

//1
$('#newDocument').click(function() {
	orderButtonPress(this);
});

$('#newOrder').click(function() {
	console.log('new order');
	orderButtonPress(this);
});

//2
function orderButtonPress(obj) {
	var card_id = $(obj).attr('data-person-id');
	dialogTableRow = $('#parent-dialog-newDocument'); //why do we need global var dialogTableRow in initPopup function ?!
	var url = contextPath + "/persons/document/edit/" + card_id + "/0";
	link = null;

	$.ajax({
		url: url,
		type: "GET",
		success: function (data) {
			if(card_id != 0) {
				initPopup(data, contextPath);
			} else {
				initPopup_btn(data, contextPath);
			}
		},
		error: function (jqXHR) {
			if(jqXHR.status  == 401){
				location.reload(true);
			}
		}
	});
}

//3
/*
 * Displaying the doc dialog doesn't fit the main ShowHide Dialog Pattern.
 * Reason: according to the main template Doc dialog is a 2nd level dialog ('#editDialog' is 1st, '#addNote' 2nd).
 *         The wrong thing with the main template is that '#addNote' is not available util '#editDialog' is opened.
 *         As a temporary solution we can manually insert the Doc-dialog body into the '#editDialog' 1st level.
 *         That's an easy but not comprehensive quick fix. The main ShowHide Dialog Pattern needs to be changed.
 */
function initPopup_btn(data, contextPath) {

	bindEscapePressEvent();
	var popup = $("#editDialog");
	popup.html(data);

	//title + close button
	var title = $('#editDialog .popup-title').attr('data-add');
	$('#editDialog .popup-title').html(title + '<a class="dialog_close close-X-button"></a>');
	$('#editDialog .popup-title .close-X-button').click(function () {
		showOrHide($('#editDialog'));
	});


	//datepicker
	datePicker.init();

	//widgets
	installDocumentDialogWidgets(contextPath);

	//save and close btns
	$("#editDialog").append('<div class="dialog_footer">' +
		'<button class="button save_dialog_button" type="button" data-form-id="document_form">Сохранить</button>' +
		'<button class="button dialog_close" type="button">Отменить</button>' +
		'</div>');
	$('#editDialog button.dialog_close').click(function () {
		showOrHide($('#editDialog'));
	});
	$('#editDialog button.save_dialog_button').click(function() {
		var currentForm = $('#document_form');
		if (validateForm(currentForm)) {
			currentForm.from_btn_click = true;
			var content = CKEDITOR.instances['document-editor'].getData();
			$('#doc-content').val(content);
			submitForm(currentForm);
		} else {
			return true;
		}
	});

	//warning message
	$("#editDialog").append('<div class="curtain"><div id="warning-dialog" class="reveal-modal small"></div></div>');
	showOrHide(popup);
}

//4
function bindEscapePressEvent() {
	$('body:not(.escape-keyup)').addClass('escape-keyup').keyup(function (e) {
		if (e.which === 27) {
			showOrHide(getCurrentDialog());
		} // 27 is the keycode for the Escape key
	});
}

//5
function getCurrentDialog() {
	var addNote = $('#addNote'); //addNote must be first
	if(addNote.is(":visible")) {
		return addNote;
	}
	var editDialog = $('#editDialog');
	if(editDialog.is(":visible")) {
		return editDialog;
	}
	return null;
}

//7
function validateForm(form) {
	var errorCounter = 0;
	$('.validation_error').removeClass('validation_error');
	$(form).find(".required").each(function(){
		if($(this).val().length == 0) {
			if($(this).is("select")){
				$(this).parent().addClass("validation_error");
			} else {
				$(this).addClass("validation_error");
			}
			errorCounter++;
		}
	});
	if (errorCounter > 0){
		var errorTabIndex = $("#tabs-contents > div").has(".validation_error").index();
		if(errorTabIndex >= 0) {
			$("#dialog_tabs").tabs("option", "active", errorTabIndex);
		}
		return false;
	} else {
		return true;
	}
}

//8
function getChosenCardIds() {
	var li_elements = $('#autocomplete-card-ids li');
	var i = 0, n = li_elements.length;
	var arr = [];
	for(i = 0; i < n; i += 1) {
		arr.push($(li_elements[i]).attr('data-value'));
	}
	return arr.join(",");
}

//9
function submitForm(form){
	var data = $(form).serializeArray();
	var url = $(form).attr("action");
	if(url === contextPath + "/persons/document/save") {
		if(getChosenCardIds() === "") {
			showWarningMessage('Добавьте ФИО пользователя');
			return;
		}
		url += "/" + getChosenCardIds();
	}

	$.ajax(
		{
			url: url,
			type: "POST",
			data: data,
			success: function (data) {
				$("form").removeClass("changed");
				var personCardId;
				if($(form).attr("id") == "assignment_form") {
					personCardId = $("#assignment_form").find("#personCardId").val();
					getPersonById(personCardId);
				}
				if($(form).attr("id") == "generalInformationForm") {
					updatePersonsTable(data.personCard);

					if($(form).find('#parent_id').val() == 0) {
						editPerson(null, data.personCard.id);
						APP.parentDialog.editMode = CRUD_MODE.UPDATE;
						personCardTableRow = $('#personsTable tr[data-id="' + data.personCard.id + '"]');
						return;
					}
				} else if(form.from_btn_click !== true) { //&data-person-id="0" //$(form).attr("id") != "document_form"
					personCardId = $(dialogTableRow).attr("data-person-id");
					var dataUrlAttrName = $(dialogTableRow).attr("data-url");
					var ajaxUrl = contextPath + "/persons/" + dataUrlAttrName + "/get/" + personCardId;
					var skipDataProperties = ["personCardId", "id"];
					var parentAttrName = 'data-person-id';
					if(dataUrlAttrName === 'document') {
						skipDataProperties = ['category',
							'content',
							'id',
							'personCardIdList',
							'subspecies'];
					}
					var indexOfCurrentTableInDataTableArray = 1; // 0 is main table, 1 is the header of the 1st table, 3 is the header of the 2nd tbl;
					var javaReturnedParameterName = dataUrlAttrName; /* Important! Frontend to backend convention: javaReturnedParameterName array in Get action has the same name as dataUrlAttrName. */
					getTableRows(ajaxUrl, dataUrlAttrName, parentAttrName, personCardId, indexOfCurrentTableInDataTableArray)
					if(dataUrlAttrName == "document"){
						changeStatus(personCardId);
					}
					showOrHide(addNote);
					// this line is commented - may cause new issue - related to remove button click; //personCardTableRow = null;
				} else if ($(form).attr("id") === "document_form") {
					if( form.from_btn_click === true ) {
						showOrHide($('#editDialog'));
					} else {
						showOrHide($('#addNote'));
					}
					showWarningMessage("Документ успешно сохранён");
					return;
				}
				//createRow($("#generalInformationForm").serializeArray().personCard);
				showWarningMessage(record_successfully_saved);
			},
			error: function (jqXHR) {
				if (jqXHR.status == 401) {
					location.reload(true);
				}
			}
		});
}