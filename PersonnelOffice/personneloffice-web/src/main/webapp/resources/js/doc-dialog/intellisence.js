
var init_once = false;

function installIntellisenceHint() {
    $("#intelli-sense").hide();
    enableYScrollIntelliAdjustment();
    $("#intelli-sense li").unbind('click');
    $("#intelli-sense li").click(function (event) {
        var editor = CKEDITOR.instances['document-editor']; //remove markup characters
        editor.focus();

        var new_str = $(this).find('span').text()
        selectIntellisenseMark(new_str);
        $("#intelli-sense").hide();
    });
}

/*
 *  Intellisence - Key press event;
 */

function installIntellisenceKeyPressEvent() {
    if(init_once === false) {
        var EXCEPT_KEY = CKEDITOR.CTRL + 32;
        /*initKeyCombinationEvent(
         EXCEPT_KEY,
         function (coord) {

         $("#intelli-sense").show();
         $("#intelli-sense").offset({ top: coord.y + 5, left: coord.x + 5 });
         iy1 = getYScrollValue();
         }
         );*/
        initLetterCombinationEvent(
                CKEDITOR.SHIFT + 192, //Ё
                CKEDITOR.SHIFT + 49,  //!
            EXCEPT_KEY,
            function (coord) {

                $("#intelli-sense").show();
                $("#intelli-sense").offset({ top: coord.y + 5, left: coord.x + 5 });
                iy1 = getYScrollValue();
            },
            function () {
                $("#intelli-sense").hide();
            }
        );
        init_once = true;
    }
}

function initLetterCombinationEvent(key_one, key_two, except_key, combination_callback, typing_continuation_callback) {

    var that = this;
    that.prev_key = null;
    that.EN_SHIFT = CKEDITOR.SHIFT;
    that.RU_SHIFT = 2228240;
    that.KEY_ONE = key_one;
    that.KEY_TWO = key_two;
    that.display = false;
    that.bookmarks2 = null;
    that.coordinate = null;

    that.getCaretPosition = function () {
        var editor = CKEDITOR.instances['document-editor'];
        var ranges = editor.getSelection().getRanges();
        editor.getSelection().selectRanges( ranges );
        that.bookmarks2 = editor.getSelection().createBookmarks2();
    }
    that.setCaretPosition = function (shift) {
        var editor = CKEDITOR.instances['document-editor'];
        if(typeof shift !== 'undefined') {
            that.bookmarks2[0].startOffset += shift;
            editor.getSelection().selectBookmarks(that.bookmarks2);
            that.bookmarks2[0].startOffset -= shift;
        } else {
            editor.getSelection().selectBookmarks( that.bookmarks2 );
        }
        return that.bookmarks2[0].startOffset;
    }

    CKEDITOR.on('instanceCreated', function(e) {
        e.editor.on('key', function (event) {
            var key = event.data.keyCode;
            if(that.prev_key != null && key == that.KEY_TWO) { //if second symbol
                that.coordinate = getCaretCoordinate(e.editor);
                that.setCaretPosition(1); //solves caret ~! -> ~word! issue
                combination_callback(coordinate);
                that.prev_key = null;
            } else if(key != that.RU_SHIFT && key != except_key) { //if another key
                that.prev_key = null;
                typing_continuation_callback();
            }
            if(that.prev_key == null && key == that.KEY_ONE) { //if first symbol
                that.prev_key = that.KEY_ONE;
                that.getCaretPosition(); //save caret position
            }
        });
    });
}

/*function initKeyCombinationEvent(key_combination, combination_callback) {
    Duplicate

    var that = this;
    that.key_combination = key_combination;
    that.display = false;
    that.bookmarks2 = null;
    that.coordinate = null;

    that.getCaretPosition = function () {
        var editor = CKEDITOR.instances['document-editor'];
        var ranges = editor.getSelection().getRanges();
        editor.getSelection().selectRanges(ranges);
        that.bookmarks2 = editor.getSelection().createBookmarks2();
    }
    that.setCaretPosition = function (shift) {
        var editor = CKEDITOR.instances['document-editor'];
        if (typeof shift !== 'undefined') {
            that.bookmarks2[0].startOffset += shift;
            editor.getSelection().selectBookmarks(that.bookmarks2);
            that.bookmarks2[0].startOffset -= shift;
        } else {
            editor.getSelection().selectBookmarks(that.bookmarks2);
        }
        return that.bookmarks2[0].startOffset;
    }

    CKEDITOR.on('instanceCreated', function (e) {
        e.editor.on('key', function (event) {
            //e.preventDefault();
            var key = event.data.keyCode;
            if (key == that.key_combination) {
                that.coordinate = getCaretCoordinate(e.editor);
                combination_callback(that.coordinate);
            }
        });
    });
}*/


/*
 *  Intellisence - Y-scroll adjustment;
 */

var y_global = null;
var iy1 = null;
var iy2 = null;

function getYScrollValue() {
    var scroll_top = $(CKEDITOR.instances['document-editor'].document.getBody().$).scrollTop;
    return scroll_top();
}

function enableYScrollIntelliAdjustment() {
    CKEDITOR.instances['document-editor'].on( 'contentDom', function(){
        this.document.on('scroll', function(event) {
            //if($("#intelli-sense").css('display') === "block") {

            var intelli = $("#intelli-sense");
            var top = intelli.position().top;
            var x = getYScrollValue();

            iy2 = getYScrollValue();
            //var new_style = { top: (y_global + (st_global - x) - 50) + 'px' };
            var top_val = (y_global + (iy1 - iy2) - 50);
            var new_style = { top: top_val + 'px' };

            console.log(top_val);
            if(top_val < 0 || top_val > 800) {
                console.log('hide');
                $("#intelli-sense").hide();
            } else {
                console.log('show');
                $("#intelli-sense").show();
            }

            if (typeof y_global !== 'undefined' && typeof x  !== 'undefined' ) {
                intelli.css(new_style);
            }
            //}
        })
    });
}

function adjustIntellisenceYPosition(editor, y) {
    var element = editor.document.$.documentElement;
    var sh = element.scrollHeight;
    var ch = element.clientHeight;
    var st = element.children[1].scrollTop;

    var delta = sh - ch;
    if(delta > 0) {
        y -= st;
        y_global = y;
    }
    return y;
}

function selectIntellisenseMark(new_str) {
    var editor = CKEDITOR.instances['document-editor'];
    var sel = editor.getSelection();
    var element = sel.getStartElement();
    sel.selectElement(element);
    element.$.lastChild.length += -2 + new_str.length;
    setCaretPosition();
    $("#document-editor").insertAtCaret(new_str);
    removeAllIntelliTextMarks(editor);
}

function removeAllIntelliTextMarks(editor) {
    var editor = CKEDITOR.instances['document-editor'];
    var element = editor.elementPath();
    var content = element.block.$.textContent;
    element.block.$.textContent = content.replace(/~!/g, '');
    //element.block.$.textContent = content.replace(/Ё!/g, '');
}

$.fn.insertAtCaret = function (myValue) { //insert text
    myValue = myValue.trim();
    CKEDITOR.instances['document-editor'].insertText(myValue);
};