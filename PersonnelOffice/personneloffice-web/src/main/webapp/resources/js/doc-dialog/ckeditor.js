function installCKEditorSettings() {
    CKEDITOR.replace('document-editor', //default configuration is in ckeditor/config.js
        {
            extraPlugins : 'timestamp',
            toolbar : [
                { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ /*'Timestamp', '-',*/ 'Source', '-', /*'Save',*/ 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
                { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
                { name: 'editing', groups: [ 'find', 'selection'/*, 'spellchecker' */ ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-'/*, 'Scayt'*/ ] },
                /*{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },*/
                '/',
                { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
                { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', /* 'Blockquote','CreateDiv',*/ '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'/*, 'Language'*/ ] },
                /*{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },*/
                { name: 'insert', items: [ 'Image', /*'Flash',*/ 'Table', 'HorizontalRule', /*'Smiley', 'SpecialChar',*/ 'PageBreak' /*'Iframe'*/ ] },
                '/',
                { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
                { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
                { name: 'others', items: [ '-' ] },
                { name: 'about', items: [ /*'About'*/ ] }
            ]
        });
}

function selectText(startIndex, offset) {
    var editor = CKEDITOR.instances['document-editor'];
    var sel = editor.getSelection();
    var element = sel.getStartElement();
    sel.selectElement(element);
    var ranges = editor.getSelection().getRanges();
    if (startIndex != -1) {
        ranges[0].setStart(element.getFirst(), startIndex);
        ranges[0].setEnd(element.getFirst(), startIndex + offset);
        sel.selectRanges([ranges[0]]);
    }
}

function getCaretCoordinate(editor) {
    var dummyElement = editor.document.createElement( 'img', {
        attributes : {
            src : '#',
            width : 0,
            height : 0
        }
    });
    editor.insertElement( dummyElement );
    var x = 0;
    var y  = 0;
    var obj = dummyElement.$;
    var el  = parent.frames[0].frameElement;
    while (el){ // Get offSetPos from IFrame-->Up
        x += el.offsetLeft;
        y += el.offsetTop;
        el = el.offsetParent;
    }
    while (obj.offsetParent){ // Get offSetPos from IFrame-->Down[/b]
        x += obj.offsetLeft;
        y  += obj.offsetTop;
        obj = obj.offsetParent;
    }
    var scrollTop = editor.document.$.documentElement.scrollTop; // Account for left/right scrolling
    var scrollLeft = editor.document.$.documentElement.scrollLeft;
    x += obj.offsetLeft-scrollLeft;
    y += obj.offsetTop-scrollTop ;
    y = adjustIntellisenceYPosition(editor, y);
    dummyElement.remove();
    return { x: x, y: y, scrollLeft: scrollLeft, scrollTop: scrollTop };
}


/*
 * CKEditor - Disable menu and body;

(function() { // Temporary workaround for providing editor 'read-only' toggling functionality.
    var cancelEvent = function( evt ) {
        evt.cancel();
    };

    CKEDITOR.editor.prototype.readOnly = function( isReadOnly ) {

        this.document.$.body.disabled = isReadOnly; // Turn off contentEditable.
        CKEDITOR.env.ie ? this.document.$.body.contentEditable = !isReadOnly
            : this.document.$.designMode = isReadOnly ? "off" : "on";

        this[ isReadOnly ? 'on' : 'removeListener' ]( 'key', cancelEvent, null, null, 0 ); // Prevent key handling.
        this[ isReadOnly ? 'on' : 'removeListener' ]( 'selectionChange', cancelEvent, null, null, 0 );

        var command, // Disable all commands in wysiwyg mode.
            commands = this._.commands,
            mode = this.mode;

        for ( var name in commands ) {
            command = commands[ name ];
            isReadOnly ? command.disable() : command[ command.modes[ mode ] ? 'enable' : 'disable' ]();
            this[ isReadOnly ? 'on' : 'removeListener' ]( 'state', cancelEvent, null, null, 0 );
        }
    }
} )();*/
//CKEDITOR.instances['document-editor'].setReadOnly(true); //disable
//CKEDITOR.instances['document-editor'].setReadOnly(false); //enable