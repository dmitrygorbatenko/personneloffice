function moveCKEditorContentToTheFormsInput() {
    $('#doc-content').val(CKEDITOR.instances['document-editor'].getData());
}

function getFullNamesFromAutocompleteForm() {
    var els = $('#autocomplete-card-ids li');
    var i = 0, n = els.length;
    var persons = [];
    for(i = 0; i < n; i += 1) {
        /*persons.push({
            card_id: $(els[i]).attr('data-value'),
            full_name: $(els[i]).find('span').text()
        });*/
        persons.push(
            $(els[i]).find('span').text()
        );
    }
    return persons;
}

function updateDocumentBlocks() { //update on: person add/delete, doc dialog load;
    var selected_persons = getFullNamesFromAutocompleteForm();
    var selected_val = $('#species').val();
    updateConfirmation(html_body, selected_persons);
    if(selected_persons.length !== 0) {
        updateSpeciesAndSpeciesBodyBlocks(html_body, html_body.blocks[3], html_body.blocks[4], selected_val, selected_persons);
    } else {
        html_body.blocks[3].setBody('');
        html_body.blocks[4].setBody('');
    }
    html_body.apply();
}

var html_body;
function installFormTemplateGenerator() {

    var that = this;
    that.paperType = undefined;
    that.category = undefined;
    that.date = undefined;
    that.paperNumber = undefined;
    that.species = undefined;
    that.subspecies = undefined;
    that.order_number = undefined;
    that.ids = [];

    html_body = new HtmlBody(); //GLOBAL!
    var content = $('#doc-content').val();

    var header_block = new HtmlBlock(HtmlBody.prototype.ORG_HEADER, 'ORG_HEADER', 0); //0. issoft header (depends on category);
    var paperTypeBlock = new HtmlBlock('', 'paperType', 1); //1. paper type;
    var categoryBlock = new HtmlBlock('', 'category', 2); //2. category;
    var speciesBlock = new HtmlBlock('', 'species', 3); //3. species;
    var speciesBodyBlock = new HtmlBlock('', 'speciesBodyBlock', 4); //4. species body;
    var directorBlock = new HtmlBlock('', 'director', 5); //5. director;
    var individualConfirmationBlock = new HtmlBlock('', 'individ_confirm', 6); //6. individual confirmation (autoupdate-personlist);
    html_body.apply();

    html_body.addBlock(header_block);
    html_body.addBlock(paperTypeBlock);
    html_body.addBlock(categoryBlock);
    html_body.addBlock(speciesBlock);
    html_body.addBlock(speciesBodyBlock);
    html_body.addBlock(directorBlock);
    html_body.addBlock(individualConfirmationBlock);

    if(content === '') {
        //var abbreviated_full_names = getFullNamesFromAutocompleteForm(); //['А.А. Чеплюков', 'В.В. Харитонов']; //individual confirmation;
        //individualConfirmationBlock.setBody(html_body.INDIVIDUAL_CONFIRMATION(abbreviated_full_names));
    } else { //apply content
        CKEDITOR.instances['document-editor'].setData( content );
    }

    // Block #0: ORG_HEADER becomes visible after CATEGORY block has all values set;

    $('#paperType').change(function () { // Block #1: paperType;
        var selection = this.value;
        that.paperType = this.value;
        directorBlock.setBody(html_body.DIRECTOR_BLOCK);
        switch(selection) {
            case "ORDER":
                paperTypeBlock.setBody(html_body.PAPER_TYPE('ПРИКАЗ'));
                break;
            case "NOTICE":
                paperTypeBlock.setBody(html_body.PAPER_TYPE('УВЕДОМЛЕНИЕ'));
                break;
            case "CONTRACT":
                paperTypeBlock.setBody(html_body.PAPER_TYPE('КОНТРАКТ'));
                break;
            case "TREATY":
                paperTypeBlock.setBody(html_body.PAPER_TYPE('ДОГОВОР'));
                break;
            case "REFERENCE":
                paperTypeBlock.setBody(html_body.PAPER_TYPE('СПРАВКА'));
                break;
            default:
                directorBlock.setBody('');
                paperTypeBlock.setBody('');
        }
        html_body.updateBlockBody(directorBlock);
        html_body.updateBlockBody(paperTypeBlock);
        html_body.apply();
    });


    $('#category').change(function () { // Block #2.1: Category;
        switch(this.value) {
            case "K":
                that.category = "к";
                break;
            case "L":
                that.category = "л";
                break;
            default:
                that.category = "";
        }
        categoryBlock.setBody(html_body.CATEGORY_BLOCK(that.category, that.date, that.paperNumber, show_header_callback));
        html_body.updateBlockBody(categoryBlock);
        html_body.apply();
    });

    $("#paperDate").change(function () { // Block #2.2: Date;
        var selection = this.value;
        that.date = selection;
        var block = html_body.CATEGORY_BLOCK(that.category, that.date, that.paperNumber, show_header_callback);
        categoryBlock.setBody(block);
        html_body.updateBlockBody(categoryBlock);
        html_body.apply();
    });

    $("#paperNumber").change(function () { // Block #2.3: Paper Number;
        that.paperNumber = this.value;
        var block = html_body.CATEGORY_BLOCK(that.category, that.date, that.paperNumber, show_header_callback);
        categoryBlock.setBody(block);
        html_body.updateBlockBody(categoryBlock);
        html_body.apply();
    });


    $("#species").change(function () { // Blocks #3,4: Species & Species Body;
        var selection = this.value;
        var selected_persons = getFullNamesFromAutocompleteForm();
        if(selected_persons.length !== 0) {
            updateSpeciesAndSpeciesBodyBlocks(html_body, speciesBlock, speciesBodyBlock, selection, selected_persons);
            html_body.updateBlockBody(speciesBlock);
            html_body.apply();
        } else {
                html_body.blocks[3].setBody('');
                html_body.blocks[4].setBody('');
        }
    });

    // Block #5: Director. Depends on paper type;
    // Block #6: Confirmation. Event driven initialization (depends on person list, see updateDocumentBlocks);
}

function updateConfirmation(html_body, selected_persons) {
    var formatted_names = [], i, n = selected_persons.length;
    for(i = 0; i < n; i += 1) {
        formatted_names.push(fullNameFormat(selected_persons[i], RussianNameProcessor.gcaseIm, false, true, true));
    }
    html_body.blocks[6].setBody(html_body.INDIVIDUAL_CONFIRMATION(formatted_names));
}

function updateSpeciesAndSpeciesBodyBlocks(html_body, speciesBlock, speciesBodyBlock, selection, selected_persons) {
    this.selected_persons = selected_persons;
    var args = [];
    var i, n = this.selected_persons.length;
    switch(selection) {
        case "WORKER":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('О приёме на работу', 'ПРИНЯТЬ:'));
            speciesBodyBlock.setBody('');
            break;
        case "TRANSFER":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('О продлении контракта', 'ПРОДЛИТЬ:'));
            for(i = 0; i < n; i += 1) {
                var full_name_dat = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseDat, true);
                args.push(new ContinueModel(full_name_dat, '[НомерКонтракта]', '[ДатаКонтракта]')); //'ХАРИТОНОВУ Владиславу Владимировичу', 'ЧЕПЛЮКОВУ Антону Анатольевичу'
            }
            speciesBodyBlock.setBody(html_body.BODY_BLOCK_CONTINUE(args));
            break;
        case "DISMISSAL":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('Об увольнении', 'УВОЛИТЬ:'));
            for(i = 0; i < n; i += 1) {
                var full_name_vin = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseVin, true);
                var full_name_rod_short = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseRod, true, true);
                args.push(new DismissalModel(full_name_vin, full_name_rod_short, '[ОтпускныхДней]')); //'ЯНУКОВА Станислава Владимировича', 'Янукова С.В.'
            }
            speciesBodyBlock.setBody(html_body.DISMISSAL(args));
            break;
        case "PRACTICE":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('О приёме на работу', 'ПРИНЯТЬ:'));
            for(i = 0; i < n; i += 1) {
                var full_name_vin = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseVin, true);
                var full_name_rod_short = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseRod, true, true);
                args.push(new PracticeModel(full_name_vin, full_name_rod_short, undefined, undefined)); //'ФРОЛОВА Владимира Васильевича', 'Фролова В.В.'; 'ПРОКОФЬЕВА Николая Валентиновича', 'Прокофьева Н.В.'
            }
            speciesBodyBlock.setBody(html_body.PRACTICE(args));
            break;
        case "CHANGE_OF_SURNAMES":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('Об изменении фамилии'));
            for(i = 0; i < n; i += 1) {
                var full_name_dat = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseDat);
                var full_name_im_CAPITAL = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseIm, true);
                args.push(new LastNameChangeModel(full_name_dat, full_name_im_CAPITAL)); //'Чеплюкову Антону Анатольевичу', 'ЧЕПЛЮКОВ Антон Анатольевич'; 'Харитонову Владиславу Владимировичу', 'ХАРИТОНОВ Владислав Владимирович'
            }
            speciesBodyBlock.setBody(html_body.LAST_NAME_CHANGE(args));
            break;
        case "CHANGE_WORKING_HOURS":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('Об изменении продолжительности рабочего дня'));
            speciesBodyBlock.setBody('');
            break;
        case "LABOR":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('Об изменении существенных</br> условий труда'));
            for(i = 0; i < n; i += 1) {
                var full_name_tvor_CAPITAL = fullNameFormat(this.selected_persons[i], RussianNameProcessor.gcaseTvor, true);
                args.push(new LaborModel(full_name_tvor_CAPITAL, '2 года', undefined, '[НомерУведомления]')); //'ЯКИМЕНКО Анной Игоревной'; 'ЯНКОВСКИМ Арсением Михайловичем'
            }
            speciesBodyBlock.setBody(html_body.LABOR(args));
            break;
        case "SOCIAL":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('SOCIAL'));
            speciesBodyBlock.setBody('');
            break;
        case "LABOR_COMPENSATION":
            speciesBlock.setBody(html_body.SPECIES_BLOCK('LABOR_COMPENSATION'));
            speciesBodyBlock.setBody('');
            break;
        default:
            speciesBlock.setBody('');
            speciesBodyBlock.setBody('');
    }
}

function show_header_callback (visible) {
    if (visible === true) {
        html_body.blocks[0].setBody(html_body.ORG_HEADER);
    } else {
        html_body.blocks[0].setBody('');
    }
    //html_body.apply(); //causes issues;
}


function SubTemplate(subTemplate) {
    this.card_ids = [];
    this.subTemaplate = subTemplate;
    this.getSpecificValue = function () {
        //get specific value
    }
    this.getSubTemplate = function () {
        return this.subTemaplate;
    }
    this.getGeneratedSubTemplate = function () {
        //generate
    }
}

function HtmlBlock(body, name, priority) {
    this.body = body;
    this.name = name;
    this.priority = priority;
    this.setBody = function (value) {
        this.body = value;
    }
}