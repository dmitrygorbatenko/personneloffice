function HtmlBody() {
    this.editor = CKEDITOR.instances['document-editor'];
    this.isInitialized = false;
    this.blocks = [];
    this.addBlock = function (block) {
        this.blocks.push(block);
    }
    this.updateBlockBody = function (block) {
        this.blocks[block.priority].body = block.body;
    }
    this.getDocument = function () {
        var i = 0, n = this.blocks.length, result_doc = '';
        for(i = 0; i < n; i += 1) {
            if(typeof this.blocks[i] !== 'undefined') {
                result_doc += this.blocks[i].body;
            }
        }
        return result_doc;
    }
    this.apply = function () {
        var doc_html = this.getDocument();
        if(this.isInitialized === true) {
            CKEDITOR.instances['document-editor'].setData(doc_html);
        }
        this.isInitialized = true;
    }
}

HtmlBody.prototype.EMPTY_ROW = '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr>' +
    '<td style="vertical-align:top">&nbsp;</td></tr></tbody></table>';


//--------------------------------------
//--- Block #0: Organisation header; ---
//--------------------------------------

HtmlBody.prototype.ORG_HEADER = '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
        <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">ИНОСТРАННОЕ ПРОИЗВОДСТВЕННОЕ УНИТАРНОЕ ПРЕДПРИЯТИЕ<br /> \
        &laquo;ИССОФТ СОЛЮШЕНЗ&raquo;</span></span></td></tr></tbody></table>' +
    HtmlBody.prototype.EMPTY_ROW +
    HtmlBody.prototype.EMPTY_ROW;


//-----------------------------
//--- Block #1: Paper type; ---
//-----------------------------

HtmlBody.prototype.PAPER_TYPE = function (value) {
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"> \
        <tbody> \
        <tr> \
            <td style="vertical-align:top"><span style="font-family:times new roman,times,serif; font-size:16px">' + value + '</span><br /> \
            &nbsp;</td> \
        </tr> \
        </tbody> \
    </table>';
}


//--------------------------------
//--- Block #2: Category; ---
//--------------------------------

HtmlBody.prototype.CATEGORY_BLOCK = function (category, date, paperNumber, show_header_callback) {
    var i = 0;
    if(typeof category === 'undefined' || category === '' || category === ' ') {
        var category = '[Категория]';
        i += 1;
    }
    if(typeof date === 'undefined' || date === '') {
        var date = '[Дата]';
        i += 1;
    }
    if(typeof paperNumber === 'undefined' || paperNumber === '' || paperNumber === ' ') {
        var paperNumber = '[НомерПриказа]';
        i += 1;
    }
    if(i === 3) { //all parameters are not defined
        if(typeof show_header_callback !== 'undefined') { show_header_callback(false); }
        return '';
    } else if(i === 0) {
        if(typeof show_header_callback !== 'undefined') { show_header_callback(true); }
    }
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr>' +
        '<td style="vertical-align:top"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">' +
        date +' №' + paperNumber + '-' + category + '<br /><br />г. Минск</span></span><br />&nbsp;</td></tr></tbody></table>';
}


//----------------------------------------------
//--- Block #3: Species & Species templates; ---
//----------------------------------------------

HtmlBody.prototype.SPECIES_BLOCK = function (val, val2) {
    var conditional_block = ""
    if(typeof val2 === 'undefined') {
        var val2 = "ПРИКАЗЫВАЮ:";
    }
    conditional_block =
        '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
        <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">' + val2 + '</span></span></td> \
            </tr></tbody></table>';
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
            <td style="vertical-align:top"><span style="font-family:times new roman,times,serif; font-size:16px">' + val + '</span> \
            <br />&nbsp;</td></tr></tbody></table>' + conditional_block;

}

//Block #3.1 - Continue contract
function ContinueModel(full_name_datelnyi, contract_numbers, contract_dates) {
    this.full_name_datelnyi = full_name_datelnyi;
    this.contract_numbers = contract_numbers;
    this.contract_dates = contract_dates;
}

HtmlBody.prototype.BODY_BLOCK_CONTINUE = function (continuation) {
    var result = "";
    var i= 0, n = continuation.length;
    if(n > 1) {
        for (i = 0; i < n; i += 1) {
            result += continuation_item(continuation[i].full_name_datelnyi, continuation[i].contract_numbers, continuation[i].contract_dates, i+1);
        }
    } else if(n === 1) {
        result += continuation_item(continuation[0].full_name_datelnyi, continuation[0].contract_numbers, continuation[0].contract_dates);
    }
    return result;
}

function continuation_item (full_name_datelnyi, contract_number, contract_date, index) {
    if(typeof full_name_datelnyi === 'undefined') {
        var full_name_datelnyi = '[ФАМИЛИЯимяотч_Дат]';
    }
    if(typeof contract_number === 'undefined') {
        var contract_number = '[НомерКонтракта]';
    }
    if(typeof contract_date === 'undefined') {
        var contract_date = '[ДатаКонтракта]';
    }
    if(typeof index === 'undefined') {
        var index = "";
    } else {
        index = index.toString() + ". ";
    }
    var paragraph = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
                <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">' + paragraph + index + full_name_datelnyi + ', инженеру-программисту контракт №' + contract_number + ' от ' + contract_date + ', сроком на два года на прежних условиях с [Дата1] по </span></span><span style="font-family:times new roman,times,serif; font-size:16px">[Дата2] г.</span></td> \
            </tr></tbody></table> \
            <table style="border-collapse:collapse; border:0px; line-height:20px; width:640px"><tbody><tr> \
                <td style="vertical-align:top"><span style="font-family:times new roman,times,serif"><span style="font-size:16px">' + paragraph + 'Основание: Согласие с уведомлением о продлении трудовых отношений №153/14.</span></span></td> \
            </tr></tbody></table>';
}


//Block #3.2 - Change last name
function LastNameChangeModel(prev_full_name_datelnyi, new_full_name) {
    this.prev_full_name_datelnyi = prev_full_name_datelnyi;
    this.new_full_name = new_full_name;
}

HtmlBody.prototype.LAST_NAME_CHANGE = function (lastNameChangeList) {
    var result = "";
    var i= 0, n = lastNameChangeList.length;
    if(n > 1) {
        for (i = 0; i < n; i += 1) {
            result += last_name_change_item(lastNameChangeList[i].prev_full_name_datelnyi, lastNameChangeList[i].new_full_name, i+1);
        }
    } else if(n === 1) {
        result += last_name_change_item(lastNameChangeList[0].prev_full_name_datelnyi, lastNameChangeList[0].new_full_name);
    }
    return result;
}

function last_name_change_item (prev_last_name_datelnyi, new_last_name) {
    if(typeof prev_last_name_datelnyi === 'undefined') {
        var prev_last_name_datelnyi = '[фамилияимяотч_Дат]';
    }
    if(typeof new_last_name === 'undefined') {
        var new_last_name = '[НомерКонтракта]';
    }
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
                <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif"> \
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Изменить в учётных и иных официальных документах ИУП &quot;ИСсофт Солюшенз&quot; фамилию инженера-программиста ' + prev_last_name_datelnyi + ', на фамилию </span></span><span style="font-family:times new roman,times,serif; font-size:16px">' +
        new_last_name  + '</span><span style="font-size:16px"><span style="font-family:times new roman,times,serif">&nbsp;с </span></span><span style="font-family:times new roman,times,serif; font-size:16px">[Дата].</span></td> \
            </tr></tbody></table>' +
        '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
         <td style="vertical-align:top">&nbsp;</td></tr></tbody></table>' +
        '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px; font-family:times new roman,times,serif; font-size:16px"><tbody><tr> \
             <td style="width:120px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="font-family:times new roman,times,serif"><span style="font-size:16px">Основание:</span></span></td> \
             <td>1.&nbsp;<span style="font-family:times new roman,times,serif; font-size:16px">копия паспорта</span></td> \
         </tr><tr> \
             <td style="width:120px">&nbsp;</td> \
             <td>2.&nbsp;<span style="font-family:times new roman,times,serif; font-size:16px">свидетельство о заключении брака</span></td> \
         </tr></tbody></table>' + HtmlBody.prototype.EMPTY_ROW;
}

//Block #3.3 - Dismissal
function DismissalModel(LAST_first_patr, last_FP, vacation_cnt) {
    this.LAST_first_patr = LAST_first_patr;
    this.last_FP = last_FP;
    this.vacation_cnt = vacation_cnt;
}

HtmlBody.prototype.DISMISSAL = function (dismissal_lst) {
    var result = "";
    var i= 0, n = dismissal_lst.length;
    if(n > 1) {
        for (i = 0; i < n; i += 1) {
            result += dismissal_helper(dismissal_lst[i].LAST_first_patr, dismissal_lst[i].last_FP, dismissal_lst[i].vacation_cnt, i+1);
        }
    } else if(n === 1) {
        result += dismissal_helper(dismissal_lst[0].LAST_first_patr, dismissal_lst[0].last_FP, dismissal_lst[0].vacation_cnt);
    }
    return result;
}

function dismissal_helper(LAST_first_patr, last_FP, vacation_cnt, index) {
    if(typeof LAST_first_patr === 'undefined') {
        var LAST_first_patr = '[ФАМИЛИЯ_имя_отч_Род]';
    }
    if(typeof last_FP === 'undefined') {
        var last_FP = '[ФАМИЛИЯ_И_О_Род]';
    }
    if(typeof vacation_cnt === 'undefined') {
        var vacation_cnt = '[ЧслОтпускныхДней]';
    }
    if(typeof index === 'undefined') {
        var index = "";
    } else {
        index = index.toString() + ". ";
    }
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
            <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ' + index + LAST_first_patr +', инженера-программиста [Дата] г., по соглашению сторон, согласно п.1 ст.35 Трудового Кодекса Республики Беларусь.</span></span></td> \
            </tr></tbody></table>' +
        '<table style="border-collapse:collapse; border:0px; line-height:20px; width:640px"> \
        <tbody><tr><td style="vertical-align:top"><span style="font-family:times new roman,times,serif"><span style="font-size:16px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Бухгалтерии выплатить компенсацию за неиспользованный трудовой отпуск в количестве ' + vacation_cnt + ' календарных дней.</span></span></td></tr></tbody></table>' +
        '<table style="border-collapse:collapse; border:0px; line-height:20px; width:640px"><tbody><tr> \
        <td style="vertical-align:top"><span style="font-family:times new roman,times,serif"><span style="font-size:16px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Основание: заявление ' + last_FP +'</span></span></td> \
            </tr></tbody></table>' + HtmlBody.prototype.EMPTY_ROW;
}

//Block #3.4 - Practice
function PracticeModel(LAST_first_patr_Rod, LAST_fp_Rod, post, date) {
    this.LAST_first_patr_Rod = LAST_first_patr_Rod;
    this.LAST_fp_Rod = LAST_fp_Rod;
    this.post = post;
    this.date = date;
}

HtmlBody.prototype.PRACTICE = function (practice_lst) {
    var result = "";
    var i= 0, n = practice_lst.length;
    if(n > 1) {
        for (i = 0; i < n; i += 1) {
            result += practice_helper(practice_lst[i].LAST_first_patr_Rod, practice_lst[i].LAST_fp_Rod, practice_lst[i].post, practice_lst[i].date, i+1);
        }
    } else if(n === 1) {
        result += practice_helper(practice_lst[0].LAST_first_patr_Rod, practice_lst[i].LAST_fp_Rod, practice_lst[0].post, practice_lst[0].date);
    }
    return result;
}

function practice_helper(LAST_first_patr_Rod, LAST_fp_Rod, post, date, index) {
    if(typeof LAST_first_patr_Rod === 'undefined') {
        var LAST_first_patr_Rod = '[ФАМИЛИЯ_имя_отч_Род]';
    }
    if(typeof LAST_fp_Rod === 'undefined') {
        var LAST_fp_Rod = '[ФамилияИО]';
    }
    if(typeof post === 'undefined') {
        var post = '[Должность]';
    }
    if(typeof date === 'undefined') {
        var date = '[Дата]';
    }
    if(typeof index === 'undefined') {
        var index = "";
    } else {
        index = index.toString() + ". ";
    }
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
            <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ' + index + LAST_first_patr_Rod +', на должность ' + post + ' ' + date + ' г., и оплатой согласно штатному расписанию.&nbsp;<br /> \
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Срок предварительного расписания 2 месяца.</span></span></td> \
            </tr></tbody></table>' +
        '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; font-family:times new roman,times,serif; font-size:16px; line-height:20px; width:640px"><tbody><tr> \
        <td style="width:120px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="font-family:times new roman,times,serif"><span style="font-size:16px">Основание:</span></span></td> \
        <td>1.&nbsp;<span style="font-family:times new roman,times,serif; font-size:16px">заявление ' + LAST_fp_Rod + '</span></td></tr><tr><td style="width:120px">&nbsp;</td> \
        <td>2.&nbsp;<span style="font-family:times new roman,times,serif; font-size:16px">трудовой договор от [Дата]</span></td></tr></tbody></table>' +
        HtmlBody.prototype.EMPTY_ROW;
}

//Block #3.5 - Labor
function LaborModel(LAST_first_patr_Tvor, time_period, date, notification_number) {
    this.LAST_first_patr_Tvor = LAST_first_patr_Tvor;
    this.time_period = time_period;
    this.date = date;
    this.notification_number = notification_number;
}

HtmlBody.prototype.LABOR = function (labor_lst) {
    var result = "";
    var i= 0, n = labor_lst.length;
    if(n > 1) {
        for (i = 0; i < n; i += 1) {
            result += labor_helper(labor_lst[i].LAST_first_patr_Tvor, labor_lst[i].time_period, labor_lst[i].date, labor_lst[i].notification_number, i+1);
        }
    } else if(n === 1) {
        result += labor_helper(labor_lst[0].LAST_first_patr_Tvor, labor_lst[i].time_period, labor_lst[0].date, labor_lst[i].notification_number);
    }
    return result;
}

function labor_helper(LAST_first_patr_Tvor, time_period, date, notification_number, index) {
    if(typeof LAST_first_patr_Tvor === 'undefined') {
        var LAST_first_patr_Tvor = '[ФАМИЛИЯ_имя_отч_Твор]';
    }
    if(typeof time_period === 'undefined') {
        var time_period = '[Период]';
    }
    if(typeof date === 'undefined') {
        var date = '[Дата]';
    }
    if(typeof notification_number === 'undefined') {
        var notification_number = '[НомерУведимления]';
    }
    if(typeof index === 'undefined') {
        var index = "";
    } else {
        index = index.toString() + ". ";
    }
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; line-height:20px; width:640px"><tbody><tr> \
            <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;' + index + 'C ' + LAST_first_patr_Tvor + ', инженером-прораммистом заключить контракт сроком на '+ time_period + ' с [Дата1] по&nbsp;</span></span><span style="font-family:times new roman,times,serif; font-size:16px">[Дата2] с окладом согласно штатному расписанию</span><span style="font-family:times new roman,times,serif; font-size:16px">.</span></td> \
            </tr></tbody></table>' +
        '<table style="border-collapse:collapse; border:0px; line-height:20px; width:640px"><tbody><tr> \
         <td style="vertical-align:top; width:277px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Изменить продолдительность рабочего времени с 0,5 ставки на полную с [Дата3] г. с уменьшением тарифного разряда</span></span><span style="font-family:times new roman,times,serif; font-size:16px">.</span></td> \
         </tr></tbody></table>' +
        '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; font-family:times new roman,times,serif; font-size:16px; line-height:20px; width:640px"><tbody><tr> \
         <td style="width:120px">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Основание:</td><td>1.&nbsp;согласие с уведомлением №' + notification_number +'</td></tr><tr><td style="width:120px">&nbsp;</td><td>2. контракт от [Дата4]</td></tr> \
            </tbody></table>' +
        HtmlBody.prototype.EMPTY_ROW;
}


//---------------------------------
//--- Block #5: Director block; ---
//---------------------------------

HtmlBody.prototype.DIRECTOR_BLOCK =
    HtmlBody.prototype.EMPTY_ROW +
    HtmlBody.prototype.EMPTY_ROW +
    '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; width:640px"><tbody><tr>' +
    '<td style="width:250px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">Генеральный директор</span></span></td>' +
    '<td style="width:200px">&nbsp;</td>' +
    '<td><span style="font-size:16px"><span style="font-family:times new roman,times,serif">А.В. Шнеерсон</span></span></td>' +
    '</tr></tbody></table>' +
    HtmlBody.prototype.EMPTY_ROW +
    HtmlBody.prototype.EMPTY_ROW;


//------------------------------------------
//--- Block #6: Individual Confirmation; ---
//------------------------------------------

HtmlBody.prototype.INDIVIDUAL_CONFIRMATION = function (abbreviated_full_names) {
    var result = "";
    if(abbreviated_full_names.length > 1) {
        result += individual_confirmation_block("С приказом ознакомлены:", abbreviated_full_names[0]);
        var i = 1, n = abbreviated_full_names.length;
        for(i = 1; i < n; i += 1) {
            result += individual_confirmation_block("", abbreviated_full_names[i]);
        }
    } else if(abbreviated_full_names.length === 1) {
        result = individual_confirmation_block("С приказом ознакомлен(а):", abbreviated_full_names[0]);
    } else {
        return '';
    }
    return result
}

function individual_confirmation_block(val1, val2) {
    return '<table style="border-collapse:collapse; border:0; cellpadding:0; cellspacing:0; width:640px"><tbody><tr> \
        <td style="width:250px"><span style="font-size:16px"><span style="font-family:times new roman,times,serif">' + val1 +  '</span></span></td> \
        <td style="width:200px">&nbsp;</td> \
        <td><span style="font-size:16px"><span style="font-family:times new roman,times,serif">' + val2 + '</span></span></td> \
        </tr></tbody></table>' +
        HtmlBody.prototype.EMPTY_ROW;
}