var selected_full_names;

function initAutocompleteWidget(contextPath) {
    var availableTags = [];
    var get_full_names_url = contextPath + "/persons/document/getFullNames";
    $.ajax({
        url: get_full_names_url,
        type: "GET",
        success: function (data) {
            cardIdListInitializationOnPageOpen(data.fullname_list);
            full_name_list = data.fullname_list;
            for (var i = 0; i < data.fullname_list.length; i++) {
                var id = data.fullname_list[i][0];
                var full_name = data.fullname_list[i][1];
                availableTags.push({ value: id, label: full_name }); //get key/value
            }

            $('#doc-full-name-search-two').autocomplete({ //list position
                appendTo: "#doc-auto-complete-result",
                open: function () {
                    var position = $("#doc-auto-complete-result").position(),
                        left = position.left - 20, top = position.top;

                    $("#doc-auto-complete-result > ul").css({left: left + 20 + "px",
                        top: top + 4 + "px" });

                },
                source: availableTags,
                select: function (event, ui) { //clean Autocomplete widged input on selected event
                    AddNewAutocompleteListItem(ui.item.value, ui.item.label);
                    $(this).val("");
                    return false;
                }
            });
        }
    });
}

function cardIdListInitializationOnPageOpen(full_name_list) {
    var id_list = [];
    var items = $('#document-card-id-list div');
    var i = 0, n = items.length;
    for (i = 0; i < n; i++) {
        for (j = 0; j < full_name_list.length; j++) {
            if ($(items[i]).text() == full_name_list[j][0]) {
                AddNewAutocompleteListItem(full_name_list[j][0], full_name_list[j][1]); //push the key & value (all available cards)
            }
        }
    }
}

function AddNewAutocompleteListItem(value, label) {
    $('div.autocomplete.name-selector ul').append("<li data-value='" + value + "'><span>" + label + "</span><a class='remove-name' href='#'></a></li>");
    $('#intelli-sense ul').append("<li data-value='" + value + "'><span>" + label + "</span></li>");
    $("#intelli-sense li").unbind('click');
    $('#intelli-sense li').click(function (event) {
        var editor = CKEDITOR.instances['document-editor']; //remove markup characters
        editor.focus();
        var new_str = $(this).find('span').text()
        selectIntellisenseMark(new_str);
        $("#intelli-sense").hide();
    });
    $('div.name-selector a').click(function(){
        var autocomlete_el = $(this).parent();
        var user_id = autocomlete_el.attr( 'data-value' );
        autocomlete_el.remove();
        var el =  $('#intelli-sense').find('li[data-value="' + user_id + '"]');
        el.remove();
        updateDocumentBlocks(); //update template parameters
    });
    updateDocumentBlocks(); //update template parameters
}

/*
 *  Autocomplete extended widget - Generate And Template Buttons;
 */

function installGenerateAndTemplateViewModeButtons() {
    var template_html = "";
    enableButton($('#document-template-btn'), false);
    $('#document-generate-btn').click(function () {
        var li_list =  $('div.autocomplete.name-selector li'); //check if list has values
        var generated_html = "";
        if(li_list.length > 0) {
            enableButton($('#document-template-btn'), true);
            enableButton($(this), false);
            var editor = CKEDITOR.instances['document-editor']; //get and save current markup;
            template_html = editor.getData();
            var i= 0, n=li_list.length;
            for(i=0; i<n; i+=1) {
                var user_id = $(li_list[i]).attr('data-value');
                var replace_value = getDbValueByUserId(user_id, 'default');
                generated_html += replaceMarksWithDbValuesOnce(template_html, '@full_name', replace_value); //substitute the marks with db values;
                if(i < n-1) {
                    generated_html += '<div style="page-break-after: always"><span style="display:none">&nbsp;</span></div>'
                }
            }
            editor.setData( generated_html );
        } else {
            showWarningMessage('Добавьте ФИО пользователя и индикаторы имён в документ');
        }
    });
    $('#document-template-btn').click(function () {
        enableButton($('#document-generate-btn'), true);
        enableButton($(this), false);
        CKEDITOR.instances['document-editor'].setData( template_html );
    });
}

function enableButton(jq_button, enable_fl) {
    if(!enable_fl) {
        jq_button.prop("disabled",true);
        jq_button.css('opacity', '0.8');
    } else {
        jq_button.prop("disabled",false);
        jq_button.css('opacity', '1.0');
    }
}

function getDbValueByUserId(user_id, value_type) {
    var element = $('#intelli-sense').find('li[data-value="' + user_id + '"]');
    return element.text();
}

function replaceMarksWithDbValuesOnce(html, mark_name, replace_value) {
    return html.split(mark_name).join(replace_value);
}