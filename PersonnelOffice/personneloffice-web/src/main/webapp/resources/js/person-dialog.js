var dialogTableRow = null;
var addNote;
var link = null;



function initDialog() {

    $('#parent-dialog-newDocument').click(function() {
        orderButtonPress(this);
    });

    $('#dialog_tabs li').click(function() {
        if($(this).attr('aria-controls') === 'assignmentInformation') {
            if($('#parent_id').val() == 0) { return; }
            var personCardId = $('#dismissal_table').attr('data-person-id');
            var dataUrlAttrName = 'dismissal';
            var parentAttrName = 'data-person-id';
            var ajaxUrl = contextPath + "/persons/dismissal/get/" + personCardId;
            var indexOfCurrentTableInDataTableArray = 2; //2nd table in assignment tab
            var oTable = $('#dismissal_table').dataTable();
            getTableRows(ajaxUrl, dataUrlAttrName, parentAttrName, personCardId, oTable)
        }
    });

    editDialog = $("#editDialog");
    saveParentButton = $("#editDialog .save_dialog_button");
    saveChildButton = $("#addNote .save_dialog_button");
    deleteChildButton = $("#addNote .delete-dialog-button");
    closeButton = $(".dialog_close");
    addNote = $("#addNote");
    datePicker.init();

    var infoTables = $( ".info-tables" ).dataTable( {
        bSort: false,
        paging: false,
        ordering:  true,
        sScrollY: "160px",
        bScrollCollapse: true,
        language: {
            zeroRecords: " "
        }
    });

    var infoTableRow = infoTables.find( "tbody tr" );

    initChanged();
    requiredFieldInit();

    $(infoTables).on("click", "tr", function(){
        $( ".info-tables tr").removeClass("chosen");
        $(this).addClass("chosen");
    });

    infoTableRow.on("dblclick",editTableRow);
    $(".crud").click(editTableRow);
    deleteChildButton.click(deleteTableRow);
    closeButton.click(closeCurrentDialog);
    $(".copy-fields").click(copyAddressFields);
    bindEscapePressEvent();


    $("#birthday, #pass_give_date").datepicker( "option", "yearRange", '-90:+0' );

    $("#dialog_tabs, #education_tabs").tabs({
        beforeActivate: function() {
            $(infoTables).find("tr").removeClass("chosen");
        },
        activate: function() {
            reDrawDataTables();
        }
    });

    $("#address_tabs, #military_tabs").tabs();

    var disabledFields = $(".disabled table input, .disabled select");

    $("#is_conscript").change(function(){
        if($(this).is(":checked")) {
            $(disabledFields).prop('disabled', true);
            $("#military_tabs").tabs("option", "active", 1);
        } else {
            $(disabledFields).prop('disabled', false);
        }
        $(".disabled table").toggleClass("disabled_fields");
    });

//    EDUCATION TAB
    var studying = $("#studying");
    var fields = $("#budget, #studyingEndDate");
    studyingFieldsDisable(studying, fields);

    studying.change(function() {studyingFieldsDisable(this, fields)});
//    END EDUCATION
//
//    Document TAB
    $("#documentFilter").change(function() {
        var text_value = $(this).find('[value="' + this.value + '"]').text();
        var documentTable = $("#documents_table").dataTable();
        documentTable.fnFilter(text_value, 0); //Doesn't work: documentTable.api().column(".document-type").search(x).draw();
    });
//    End Document tab

    saveParentButton.click(function(e){

        e.preventDefault();
        if($(this).attr("data-form-id") === 'document_form') {
            moveCKEditorContentToTheFormsInput();
        }
        var currentForm = editDialog.find("#" + $(this).attr("data-form-id"));
        if (validateForm(currentForm)) {
            submitForm(currentForm);
        } else {
            return true;
        }
    });
}

function reDrawDataTables () {
    var table = $.fn.dataTable.fnTables(true);
    $(table).each(function(){
        if($(this).attr("id") != "personsTable") {
            $(this).dataTable().fnAdjustColumnSizing();
        }
    });
}

function requiredFieldInit () {
    $(".required").keyup(function(){
        $(this).removeClass("validation_error");
    });
    $("select").change(function(){
        $(this).parent().removeClass("validation_error");
    });
}

function closeCurrentDialog() {
    var currentDialog = $(this);
    var currentForm = $(currentDialog).find("form");

    if ($(this).hasClass("close-X-button") && $(currentForm).hasClass("changed")){
        if(confirm("Сохранить изменения?")){
            submitForm(currentForm);
            $(currentForm).removeClass("changed");
        }
//        Вставить нормальное окошко
    }
    showOrHide(currentDialog);
}





function showWarningMessage(message, time) {
    /*
     * Messages takes in warning_message.jsp.
     * Without parameter time it take default time 1500ms (1.5 sec)
     */
    $(".warning-message").text(message);
    showOrHide(warningDialog);
    setTimeout("showOrHide(warningDialog)", time || 1500);
}


function getPersonById(personCardId) {
    var url = contextPath + "/persons/get/" + personCardId;
    $.ajax({
        url : url,
        type: "GET",
        success: function(data) {
            updatePersonsTable(data.personCard);
        }
    });
}

function updatePersonsTable(personCard){
    var oTable = $("#personsTable").DataTable();
    var row = createRow(personCard);
    if (APP.parentDialog.editMode === CRUD_MODE.UPDATE) {
        oTable.row(personCardTableRow).data(row);
    } else if(APP.parentDialog.editMode === CRUD_MODE.CREATE) {
        var node = oTable.row.add(row).draw().node();
        $(node).attr("data-id", personCard.id).on("dblclick", editPerson);
    }
}

function createRow(personCard){
    var regAddress = personCard.registrationAddress;
    var residentialAddress = personCard.residentialAddress;
    var education = personCard.education;
    var assignment = personCard.lastAssignment;
    var rowData = [];
    rowData[0] =  "<a href='#"+personCard.id+"' class='edit_person'>"+personCard.lastName+" "+ personCard.firstName + " "+ personCard.patronymic +"</a>";
    rowData[1] = assignment? assignment.rate : "";
    rowData[2] = personCard.gender? personCard.gender.label : "";
    rowData[3] = assignment? assignment.position.label : "";
    rowData[4] = "";
    rowData[5] = "";
    rowData[6] = assignment? assignment.orderNumber : "";
    rowData[7] = assignment? assignment.orderDate : "";
    rowData[8] = "";
    rowData[9] = ""; //education ? education.label:"";
    rowData[10] = "";
    rowData[11] = personCard.birthday;
    rowData[12] = personCard.allocation;
    rowData[13] = residentialAddress? residentialAddress.mobilePhone : "";
    rowData[14] = personCard.familyMembersAsString;
    rowData[15] = personCard.latin;
    rowData[16] = personCard.documentSeries + personCard.documentSerialNumber;
    rowData[17] = personCard.documentIndividualNumber;
    rowData[18] = personCard.documentDateIssuance;
    rowData[19] = personCard.documentValidityPeriod;
    rowData[20] = personCard.documentIssuedBy;
    rowData[21] = regAddress? regAddress.locality + " " + regAddress.street : "";
    rowData[22] = "";
    rowData[23] = typeof personCard.status !== 'undefined' && personCard.status != null ? personCard.status.label : "";
    return rowData;

}

function editTableRow(e){
    e.preventDefault();
    link = this;
    if (this.tagName == "TR" || $(this).hasClass("add-row")) {
        dialogTableRow = link;
    } else {
        dialogTableRow = $(".info-tables").find("TR.chosen").get(0);
        //if (dialogTableRow == null) {
        //    showWarningMessage(record_not_selected);
        //    return false;
        //}
    }
    var parent_id = $("#parent_id").val();
    if(parent_id == 0) {
        showWarningMessage('Для добавления записей в таблицу сохраните карточку');
        return;
    }
    var selected_row_id = $(dialogTableRow).attr("data-person-id");
    if(typeof selected_row_id === 'undefined') {
        showWarningMessage(record_not_selected);
        return;
    }
    var url = contextPath
        + "/persons/"
        + $(dialogTableRow).attr("data-url")
        + "/edit/"
        + $(dialogTableRow).attr("data-person-id")
        + "/"
        + $(dialogTableRow).attr("data-id");

    $.ajax(
        {
            url: url,
            type: "GET",
            success: function (data) {
                initPopup(data, contextPath);
            },
            error: function (jqXHR) {
                if(jqXHR.status  == 401){
                    location.reload(true);
                }
            }
        });
}

function deleteTableRow(e){
    e.preventDefault();
    dialogTableRow = $(".info-tables").find("TR.chosen").get(0);
    if (dialogTableRow == null) {
        alert("Строка не выделена");
        return false;
    }
    var url = contextPath
        + "/persons/"
        + $(dialogTableRow).attr("data-url")
        + "/delete/"
        + $(dialogTableRow).attr("data-id");
    $.ajax(
        {
            url: url,
            type: "POST",
            success: function () {
                if($(dialogTableRow).attr("data-url")=="document"){
                    changeStatus(getChosenCardIds());
                }
                var table_id = '#' + $(dialogTableRow).closest('table').attr('id');  //fix: $(dialogTableRow).remove();
                $(table_id).dataTable().api().row('.chosen').remove().draw();
                showOrHide(addNote);
                showWarningMessage(record_successfully_deleted);

            },
            error: function (jqXHR) {
                if(jqXHR.status  == 401){
                    location.reload(true);
                }
            }
        });
}



function initPopup(data, contextPath){
    var popup_body = $("#noteContent");
    var popup = popup_body.parent();
    popup_body.html(data);
    popup.find(".save_dialog_button").attr("data-form-id", popup_body.find("form").attr("id"));

    datePicker.init();

    if ($(link).hasClass("delete-row")){
        saveChildButton.hide();
        deleteChildButton.show();
        popup_body.find("input, select, textarea, button").prop('disabled', true);
    } else {
        saveChildButton.show();
        deleteChildButton.hide();
        initChanged();
    }

    if($(dialogTableRow).attr("data-url") === "document" || contextPath === "document") {
        addNote.removeClass("medium").addClass("xlarge");
        installDocumentDialogWidgets(contextPath);
    } else {
        addNote.addClass("medium").removeClass("xlarge");
    }
    setPopupTitle();
    showOrHide(popup_body.closest(".curtain"));
}

function setPopupTitle() {

    var dataAttr;

    if ($(link).hasClass("add-row")){
        dataAttr = "data-add";
    } else if ($(link).hasClass("delete-row")){
        dataAttr = "data-delete";
    } else {
        dataAttr = "data-edit";
    }

    var title = $(".popup-title");
    var text = $(title).attr(dataAttr);
    $(title).text(text);
}

function initChanged(){
    $("input, select, textarea").change(function(){
        $(this).closest("form").addClass("changed");
    });
}

function studyingFieldsDisable(studying, fields) {
    var checked = $(studying).prop("checked");
    (checked)
        ? $(fields).prop('disabled', false)
        : $(fields).prop('disabled', true);
}

function copyAddressFields(){

    switch ($(this).attr("data-copy")) {

        case "insurance" :
            $("#insuranceNumber").val($("#individualNumber").val());
            break;

        case "registration" :
            $("#registrationAddress_province").val($("#residentialAddress_province").val());
            $("#registrationAddress_region").val($("#residentialAddress_region").val());
            $("#registrationAddress_ruralCommittee").val($("#residentialAddress_ruralCommittee").val());
            $("#registrationAddress_locality").val($("#residentialAddress_locality").val());
            $("#registrationAddress_postcode").val($("#residentialAddress_postcode").val());
            $("#registrationAddress_street").val($("#residentialAddress_street").val());
            $("#registrationAddress_house").val($("#residentialAddress_house").val());
            $("#registrationAddress_building").val($("#residentialAddress_building").val());
            $("#registrationAddress_apartment").val($("#residentialAddress_apartment").val());
            break;

        case "residential" :
            $("#residentialAddress_province").val($("#registrationAddress_province").val());
            $("#residentialAddress_region").val($("#registrationAddress_region").val());
            $("#residentialAddress_ruralCommittee").val($("#registrationAddress_ruralCommittee").val());
            $("#residentialAddress_locality").val($("#registrationAddress_locality").val());
            $("#residentialAddress_postcode").val($("#registrationAddress_postcode").val());
            $("#residentialAddress_street").val($("#registrationAddress_street").val());
            $("#residentialAddress_house").val($("#registrationAddress_house").val());
            $("#residentialAddress_building").val($("#registrationAddress_building").val());
            $("#residentialAddress_apartment").val($("#registrationAddress_apartment").val());
            break;

    }
}

function changeStatus(personCardId){
    var url = contextPath + "/persons/status/" + personCardId;
    $.ajax(
        {
            url: url,
            type: "POST",
            success: function (data) {
                $('#status').val(data.status);
            },
            error: function (jqXHR) {
                if(jqXHR.status  == 401){
                    location.reload(true);
                }
            }
        });
}
