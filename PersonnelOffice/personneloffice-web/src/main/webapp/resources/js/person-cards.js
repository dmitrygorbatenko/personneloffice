var preferencesInitialState;
var personCardTableRow = null;
$(document).ready(function()
    {
        preferencesInitialState = initPreferences();
        initCardsDataTable();
        var rows = $("#personsTable tbody tr");
        rows.on("dblclick",editPerson);
        $("form").bind("keyup keypress", function (e) {
            var code = e.keyCode || e.which;
            if (code == 13) {
                e.preventDefault();
                return false;
            }
        });
    }
);

$( window ).resize(resizeTable);

function resizeTable(){
    $("#scroll_container").height(window.innerHeight - 340);
    var personsTable =  $("#personsTable");
    personsTable.width($("#personsTable_wrapper").width() -24);
    $(".fixed-header-background").width(personsTable.outerWidth()-2);
    $(".fixed_header").width(function(){
        return $(this).parent().width() - 20;
    });
}

function initCardsDataTable(){
    preferencesInitialState = initPreferences();
    var personCardsTable =  $("#personsTable");
    var dataTable = personCardsTable.DataTable({
        "dom":'<"fixed-header-background">t',
        "autoWidth":false,
        "paging": false,
        "info": false,
        "destroy": true,
        "language": {
            "zeroRecords":    ""
        },
        "aoColumns":getAOColumns(preferencesInitialState)
    });
    $('#search').keyup(function () {
        $('#personsTable').dataTable().fnFilter($(this).val()); //dataTable.fnFilter($(this).val());
    });
    personCardsTable.on("draw.dt",resizeTable);
    var statusFilter = $("#statusFilter");
    statusFilter.on("change", function(){
        dataTable.column(".column-status").search( this.value).draw();
    });
    dataTable.column(".column-status").search( statusFilter.val()).draw();
    $("#personsTable_wrapper").scroll(function(e){
        if(typeof InstallTrigger !== 'undefined'){  // Firefox 1.0+)
            $(".fixed_header").css("margin-right", this.scrollLeft);
        } else { //other browsers
            $(".fixed_header").css("margin-left", -this.scrollLeft);
        }
    });
    resizeTable();
    personCardsTable.find("td").resizable({
        handles: "e",
        resize: function(event, ui){
            var cellIndex = event.target.cellIndex;
            personCardsTable.find("td:nth-child("+(cellIndex+1)+")").width(ui.size.width);
            if((/Chrome/.test(navigator.userAgent) && /Google Inc/.test(navigator.vendor)) || /Opera Software ASA/.test(navigator.vendor)){ //fix for chrome and opera
                var fh = $(".fixed_header");
                fh.css("margin-left", 1);
                fh.css("margin-left",$("#personsTable_wrapper").scrollLeft());
            }
        }
    });
    $("#scroll_container").css("visibility", "visible");
}

function initPreferences(){
    var prs = $(".table .preference");
    var preferences = [];
    prs.each(function(){
        var preference = $(this);
        var i  = preference.attr("data-column");
        var columnStyleClass  = preference.attr("data-styleClass");
        var searchable = preference.find("input.toggle-column-filter").get(0).checked;
        var visible =   preference.find("input.toggle-column-vis").get(0).checked;
        preferences[i] =  {"searchable": searchable,  "visible": visible, "sClass":columnStyleClass};
    });
    return preferences;
}

function getAOColumns( preferences){
    var aoColumns= [];
    var length = preferences.length;
    for(var i = 0; i < length; i++){
        var preference = preferences[i];
        if(preference.visible){
            aoColumns[i] = {"bSearchable": preference.searchable, "bVisible": true};
        } else {
            aoColumns[i] = {"bSearchable": false, "bVisible": false};
        }
    }
    aoColumns[length-1] =  {"bSearchable": true, "bVisible": preferences[length-1].visible}// status always searchable
    aoColumns[0].sClass = "column-full_name";
    aoColumns[1].sClass = "column-rate";
    aoColumns[2].sClass = "column-gender";
    aoColumns[3].sClass = "column-position";
    aoColumns[4].sClass = "column-start_date";
    aoColumns[5].sClass = "column-transfer";
    aoColumns[6].sClass = "column-order_number";
    aoColumns[7].sClass = "column-order_date";
    aoColumns[8].sClass = "column-contract_number";
    aoColumns[9].sClass = "column-education";
    aoColumns[10].sClass = "column-finish_date";
    aoColumns[11].sClass = "column-birthday";
    aoColumns[12].sClass = "column-allocation";
    aoColumns[13].sClass = "column-phone";
    aoColumns[14].sClass = "column-children_info";
    aoColumns[15].sClass = "column-full_name_latin";
    aoColumns[16].sClass = "column-passport_number";
    aoColumns[17].sClass = "column-passport_individual_number";
    aoColumns[18].sClass = "column-passport_date_issuance";
    aoColumns[19].sClass = "column-passport_validity_period";
    aoColumns[20].sClass = "column-passport_office";
    aoColumns[21].sClass = "column-registration";
    aoColumns[22].sClass = "column-form_of_lease";
    aoColumns[23].sClass = "column-status";
    return aoColumns;
}
function editPerson(event, parent_id){

    personCardTableRow = this.tagName == "TR" ? this : null;
    var isCreatePersonCardDialog = (typeof parent_id === 'undefined');
    var person_card_id = isCreatePersonCardDialog === true ? $(this).attr("data-id") : parent_id;
    var url = contextPath + "/persons/edit/" + person_card_id;
    if(this.nodeName === 'BUTTON') {
        APP.parentDialog.editMode = CRUD_MODE.CREATE;
    } else if(this.nodeName === 'TR') {
        APP.parentDialog.editMode = CRUD_MODE.UPDATE;
    }
    $.ajax(
        {
            url:url,
            type: "GET",
            success: function (data, textStatus, jqXHR) {
                var dialog = $("#editDialog");
                dialog.html(data);
                initDialog();
                if(isCreatePersonCardDialog == true) {
                    showOrHide($(dialog).closest(".curtain"));
                    phoneMask();
                };

            },
            error: function (jqXHR, textStatus, errorThrown) {
                if(jqXHR.status  == 401){
                    location.reload(true);
                }
            }
        });
}

function phoneMask(){
    $("#homeTelephone").inputmask("(999) 999-999[9]");
    $("#workTelephone").inputmask("(999) 999-999[9]");
    $("#mobileTelephone").inputmask("(99) 999-9999");
    $("#homeTelephone").on("keypress keydown change",function(){validation($(this))});
    $("#workTelephone").on("keypress keydown change",function(){validation($(this))});
    $("#mobileTelephone").on("keypress keydown change",function(){validation($(this))});
}
function validation(inputPhone){
    if(inputPhone.inputmask("isComplete") || inputPhone.val()=="")
        inputPhone.removeClass("validation_error");
    else
        inputPhone.addClass("validation_error");
}

$("#preferenceButton").click(function(){
    var  preferences= $("div.preference").clone();
    var newTable =  $('#preferenceForm .table').empty();
    preferences.sort(comparatorPreferences);
    var flagUnchecked = false;
    newTable.append('<div><div>поиск</div><div>показать</div><div></div></div>');
    preferences.find('div').removeClass('lastSelectedRow');
    preferences.removeClass('selected');
    preferences.removeClass('unselected');
    for(var i=0; i<preferences.size(); i++){
        if(!$(preferences[i]).find(".toggle-column-filter").prop("checked") &&
            !$(preferences[i]).find(".toggle-column-vis").prop("checked") &&
            !flagUnchecked){
            flagUnchecked = true;
            $(preferences[i-1]).find('div').addClass('lastSelectedRow');
        }
        if(flagUnchecked == true){
            $(preferences[i]).addClass('unselected');
        }
        else{
            $(preferences[i]).addClass('selected');
        }
        newTable.append(preferences[i]);
    }

    $("#preferenceButton").addClass("columns_button-pressed");
    showOrHide($("#preferenceBlock"));
});
function comparatorPreferences(a,b){
    var weightA = 0;
    var weightB = 0;
    if($(a).find(".toggle-column-filter").prop("checked")){
        weightA+=2;
    }
    if($(b).find(".toggle-column-filter").prop("checked")){
        weightB+=2;
    }
    if($(a).find(".toggle-column-vis").prop("checked")){
        weightA++;
    }
    if($(b).find(".toggle-column-vis").prop("checked")){
        weightB++;
    }
    return weightB - weightA;
}
$("#preferenceSearch").on("keyup change",function(){
    var searchVal = $(this).val().toLowerCase();
    var preferences = $('.unselected');
    for(var i=0; i<preferences.size(); i++){
        var temp = $($(preferences[i]).find('div')[2]).text().trim();
        if($($(preferences[i]).find('div')[2]).text().toLowerCase().indexOf(searchVal)==-1){
            $(preferences[i]).css('display','none');
        }
        else{
            $(preferences[i]).show();
        }
    }

})
$("#preferenceForm").submit(function(e){
    e.preventDefault();
    initCardsDataTable();
    $("#preferenceButton").removeClass("columns_button-pressed");
    showOrHide($("#preferenceBlock"));
});


function revertPreferences(){
    for(var i = 0; i < preferencesInitialState.length; i++ ){
        var preference = preferencesInitialState[i];
        $("#preferenceForm input[data-column='"+i+"'].toggle-column-filter").prop( "checked", preference.searchable);
        $("#preferenceForm input[data-column='"+i+"'].toggle-column-vis").prop( "checked", preference.visible);
    }
    $("#preferenceButton").removeClass("columns_button-pressed");
    showOrHide($("#preferenceBlock"));
}
$("#cancelPreference").click(revertPreferences);
//$(".curtain").click(revertPreferences);

$("#newCard").click(editPerson);

$("#export-personCards-excel").click(function(){
    location.href = contextPath + "/file/export/excel/cards/0";
})
$('#chooseExcelCards').on("change", function(event) {
    $('#formChooseExcelCards').submit();
});
$('#import-personCards-excel').click(function() {
    $('#chooseExcelCards').trigger('click');


});