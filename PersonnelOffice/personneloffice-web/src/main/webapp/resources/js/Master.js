//region NotDistributed

var warningDialog = $("#warning-dialog");
//showWarningMessage(message, time);

var saveParentButton;
var saveChildButton;
var deleteChildButton;
var closeButton;
var editDialog;

//endregion

//region #commonObjectsAndFunctions

var CRUD_MODE = {
    CREATE: "CREATE",
    READ: "READ",
    UPDATE: "UPDATE",
    DELETE: "DELETE"
};

function showOrHide(popup) {
    if(popup) {
        var jCurtain = popup.closest(".curtain");
        var curtainOpacity = jCurtain.css('opacity');
        var isCompletelyVisibleOrHidden = curtainOpacity == 0 || curtainOpacity == 1;
        if (isCompletelyVisibleOrHidden == true) {
            jCurtain.toggle("fade");
        }
    }
}

//endregion

//region #Namespaces

var APP = {};

//endregion


//region MainTable

APP.mainTable = $('#personsTable').dataTable();

//endregion

//region #ParentDialog
APP.parentDialog = new ParentDialog();

function ParentDialog() {}
ParentDialog.prototype.oTable = null;
ParentDialog.prototype.editMode = null;
ParentDialog.prototype.personCardTableRow = null;
ParentDialog.prototype.dialogRow = null;

//endregion

//region #WarningDialog

//endregion
function showWarningMessage(message, time) {
    /*
     * Messages takes in warning_message.jsp.
     * Without parameter time it take default time 1500ms (1.5 sec)
     */
    $(".warning-message").text(message);
    showOrHide(warningDialog);
    setTimeout("showOrHide(warningDialog)", time || 1500);
}
$(document).ready(function(){

    $(".clear-button").on("click", function(){
        $("#search").val("").trigger("keyup").val("поиск");
    });
    $(".tab-active").on("click", "a", function(){
        return false;
    });
});