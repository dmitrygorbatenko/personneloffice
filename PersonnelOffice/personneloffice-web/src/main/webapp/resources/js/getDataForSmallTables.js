/*
 ------------------------------------------
 getAll plugin - version 1.4
 ------------------------------------------
 #Call Example:
 var personCardId = $(dialogTableRow).attr("data-person-id");
 var dataUrlAttrName = $(dialogTableRow).attr("data-url");
 var ajaxUrl = contextPath + "/persons/" + dataUrlAttrName + "/get/" + personCardId;
 var skipDataProperties = ["personCardId", "id"];
 var indexOfCurrentTableInDataTableArray = 1; // 0 is main table, 1 is the header of the 1st table, 3 is the header of the 2nd tbl;
 getTableRows(ajaxUrl, dataUrlAttrName, indexOfCurrentTableInDataTableArray, skipDataProperties);

 ajaxUrl - ajax url to get new data;
 dataUrlAttrName - the form specific name;
 indexOfCurrentTableInDataTableArray - the index of the dataTable. It helps to choose the right tables if there are several dataTables on the page;
 //skipDataProperties - can be removed

 ------------------------------------------
 Important! Frontend to backend convention:
 the field names of the received ajax object must match the corresponding the names in 'data-entity-property'.

 For example 'FamilyMember':
 <table>
 <thead data-entity-attributes="data-url,data-id,data-person-id">
 <tr>
 <td data-entity-property="kinship">Родство</td>
 <td data-entity-property="name">Дата Рождения</td>
 <td data-entity-property="invalid">Инвалид</td>
 </tr>
 </thead>
 </table>

 callback(data) {
 // Here the 'data' object is an array of the following objects:
 // { 'form_specific_name' : [
 //      { "name": "val12", "kinship": "val11", "invalid", "val13" },
 //      { "name": "val22", "kinship": "val21", "invalid", "val23" },
 //      { "name": "val32", "kinship": "val31", "invalid", "val33" }
 // ]}
 }
 Note: the 'data' object field names are equal to the FamilyMember java class names, but usually come in a different order.
 ------------------------------------------ */

//region GetDataInCertainOrder.

/*
 * getTdPropertyNames(oTable): saves the table > tr > td "data-entity-property" attributes;
 * oTable - is a jQuery table object or dataTable object;
 *
 * Property example:
 * <thead>
 *     <tr>
 *     <td data-entity-property="kinship">Родство</td>
 *     </tr>
 * </thead>
 * Improved;
 */
function getTdPropertyNames(oTable) {
    var tds = $(oTable).find('thead tr').children();
    var property_names = [];
    var i, n = tds.length, attr_val;
    for(i = 0; i < n; i += 1) {
        attr_val = $(tds[i]).attr('data-entity-property');
        if (typeof attr_val != 'undefined') {
            property_names.push(attr_val);
        }
    }
    if(property_names.length == 0) {
        throw new Error('getDataForSmallTables.js plugin: the table > tr > td "data-entity-property" attributes not found;');
    }
    return property_names;
}

/*
 * Example:
 * <thead data-entity-attributes="data-url,data-id,data-person-id">
 *     <tr> <!-- comma separated
 *         <td data-entity-property="kinship">Родство</td>
 *     </tr>
 * </thead>
 * Improved;
 */
function getTrPropertyNames(oTable) {
    var trs_commaSeparatedString = $(oTable).find('thead').attr('data-entity-attributes');
    if(typeof trs_commaSeparatedString === 'undefined') { /*console.log('Tr doesn\'t contain \'data-entity-attributes\' attribute;');*/ return []; }
    trs_commaSeparatedString = trs_commaSeparatedString.replace(/[\s,]+/g, ','); //trim spaces
    var tr_property_names = trs_commaSeparatedString.split(','); //split string names
    var i, n = tr_property_names.length;
    for(i = 0; i < n; i += 1) {
        tr_property_names[i] = tr_property_names[i].split(':');
    }
    if(tr_property_names.length == 0) {
        throw new Error('getDataForSmallTables.js plugin: the table > tr "data-entity-attributes" attribute not found;');
    }
    return tr_property_names;
}

/*
 * getTableRows: makes get request for the tabs' top (1st) table;
 * Fills DataTable with tr elements:
 * 1. The "data-url", "data-person-id", "data-id" are stored as <tr> attributes.
 * 2. The properties from getTdPropertyNames() function are inserted as <td> elements;
 */
function getTableRows(ajaxUrl, dataUrlAttrName, parentAttrName, personCardId, indexOfCurrentTableInDataTableArray)
{
    //indexOfCurrentTableInDataTableArray needs to be substituted with oTable object (it requires adding an attribute with table id value $(form))
    //$(form).find('data-refresh-table-id').val() -> '#family_member';
    var self = this, ix = indexOfCurrentTableInDataTableArray;
    var tables = $.fn.dataTable.fnTables(true);
    if(typeof ix === 'undefined') {
        ix = 1; //default: 0 is main table, 1 is the header of the 1st dialog tbl, 3 is the header of the 2nd dialog tbl;
        self.oTable = $(tables[ix]).dataTable();
    } else if (typeof ix === 'number') {
        self.oTable = $(tables[ix]).dataTable();
    } else if(typeof ix === "object") {
        self.oTable = ix; //supposed to be a dataTable object;
    }

    $.ajax({
        url: ajaxUrl,
        type: "GET",
        success: function (data) {
            var arr = data[dataUrlAttrName];
            var td_property_names = getTdPropertyNames(self.oTable);
            var tr_property_names = getTrPropertyNames(self.oTable);
            self.oTable.fnClearTable();
            var i, n = arr.length;
            for(i = 0; i < n; i += 1) {
                var tr = addTds(arr[i], td_property_names);
                addTrAttributes(tr, tr_property_names, arr[i], dataUrlAttrName, parentAttrName, personCardId);
                $(tr).on("dblclick", editTableRow);
            }
        }
    });
}


function addTds(obj, property_names) {
    var fnAddData_input = buildArrayOfCellValuesForDataTable(obj, property_names);
    var ai = self.oTable.fnAddData(fnAddData_input);
    return self.oTable.fnSettings().aoData[ ai[0] ].nTr;
}

function addTrAttributes(tr, tr_property_names, data, dataUrlAttrName, parentAttrName, personCardId) {
    var i = 0, n = tr_property_names.length, attrs = [];
    attrs.push({ attr_name: 'data-url', attr_val: dataUrlAttrName });
    attrs.push({ attr_name: parentAttrName, attr_val: personCardId });
    for(i = 0; i < n; i += 1) {
        attrs.push({
            attr_name: tr_property_names[i][0],
            attr_val: data[tr_property_names[i][1]]
        })
    }
    bindAttributesToTrElement(tr, attrs);
}

/*
 * buildArrayOfCellValuesForDataTable: converts row values into cell html depending on property type.
 * Example: boolean property type returns checkbox;
 *          drop down list is an object with property label: { label: "value" };
 * Improved;
 */
function buildArrayOfCellValuesForDataTable(row, property_names) {
    var fnAddData_input = [], propertyName;
    var i, n = property_names.length;
    for(i = 0; i < n; i += 1) {
        propertyName = property_names[i];
        if( Object.prototype.toString.call( row[propertyName] ) === '[object Array]' ) {
            throw new Error('getDataForSmallTables.js plugin: automatic array to cell conversion is not implemented yet.');
        }
        else {
            if(row[propertyName] == null) {
                fnAddData_input.push("");
                continue;
            }
            switch (typeof row[propertyName]) {
                case "number":
                case "string":
                    fnAddData_input.push(row[propertyName]);
                    break;
                case "object":
                    if((row[propertyName])["label"] === undefined){
                        throw new Error('getDataForSmallTables.js plugin: drop down list object { label: "value" } is the only implemented object type by now;');
                    }
                    fnAddData_input.push(row[propertyName].label);
                    break;
                case "boolean":
                    fnAddData_input.push(row[propertyName] ? '<input type="checkbox" checked disabled />' : ''); //do not display unchecked values <input type="checkbox" disabled />
            }
        }
    }
    return fnAddData_input;
}

function bindAttributesToTrElement(tr, attrs) {
    var d = document;
    var i, n = attrs.length;
    for(i = 0; i < n; i += 1) {
        var data_url = d.createAttribute(attrs[i].attr_name);
        data_url.value = attrs[i].attr_val;
        tr.setAttributeNode(data_url);
    }
    return tr;
}

//endregion