CKEDITOR.plugins.add( 'timestamp',
{
	init: function( editor )
	{
		editor.addCommand( 'insertTimestamp',
			{
                clicked: false,
                template: '',
                command_list: [ //CKEDITOR.config.plugins;
                    'source', 'newpage', 'templates', 'cut', 'copy',
                    'paste', 'pastefromword', 'pastetext', 'undo', 'redo',
                    'find', 'replace', 'bold', 'italic', 'selectAll',
                    'underline', 'strike', 'subscript', /*'removeformat',*/ 'numberedlist',
                    'bulletedlist', 'outdent', 'indent', 'justifyleft', 'justifycenter',
                    'justifyright', 'justifyblock', 'table', 'horizontalrule', 'pagebreak',
                    'bidiltr', 'bidirtl', 'image', /*'Superscript', /*'format',
                    'font', 'fontsize','font', 'basicstyles','showblocks',*/
                    'superscript', 'showblocks', 'removeFormat' /*, "textcolor", "bgcolor"*/
                ],
                disableMenuButtonList: function (disable_fl) {
                    var editor = CKEDITOR.instances['document-editor']; // CKEDITOR.instances['document-editor'].getCommand( 'save' ).disable();
                    var i, n = this.command_list.length;
                    for(i = 0; i < n; i += 1) {
                        console.log(this.command_list[i]);
                        if(disable_fl) {
                            editor.getCommand( this.command_list[i] ).disable();
                        } else {
                            editor.getCommand( this.command_list[i] ).enable();
                        }
                    }
                },
                exec : function( editor )
				{
                    var that = this;
                    $('#cke_14').removeClass('cke_button_on cke_button_off');
                    if(that.clicked) {
                        $('#cke_14').addClass('cke_button_off');
                        that.disableMenuButtonList(false);
                    } else {
                        $('#cke_14').addClass('cke_button_on');
                        that.disableMenuButtonList(true);
                    }
                    that.clicked = !that.clicked;

                    CKEDITOR.instances['document-editor'].on('focus', function () {
                        var is_selected = $('#cke_14').hasClass('cke_button_on');
                        if (is_selected) {
                            that.disableMenuButtonList(true);
                        } else {
                            that.disableMenuButtonList(false);
                        }
                    });

					//var timestamp = new Date();
					//editor.insertHtml( 'The current date and time is: <em>' + timestamp.toString() + '</em>' );
				}
			});
		editor.ui.addButton( 'Timestamp',
		{
			label: 'Generate',
			command: 'insertTimestamp',
			icon: this.path + 'images/timestamp.png',
            title: 'Generate'
		} );
	}
} );


/*

 disableMenuButtons: function () {
 $('.cke_button__source').addClass('cke_button_disabled');
 $('.cke_button__templates').addClass('cke_button_disabled');
 },
 enableMenuButtons: function () {
 $('.cke_button__source').removeClass('cke_button_disabled');
 $('.cke_button__templates').removeClass('cke_button_disabled');
 },

 */