/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {

    //1. TURN OFF PAGE CACHING;
    config.timestamp='ABCD';

    //2. TEMPLATES - TURN OFF 'CLEAR PAGE' TICK;
    config.templates_replaceContent = false;

    //3. SPACES ON TAB CLICK - not solved;
    //config.tabSpaces = 10;

    //4. SKIN and EDITOR HEIGHT DEPENDING ON THE SKIN;
	config.skin = 'office2013';
    config.width = 800-117;
    var EDITOR_HEIGHT = 706-8;
    var TITLE_HEIGHT = 35;
    if(config.skin === 'office2013') {
        var THEME_HEIGHT_DELTA = 18;
        var FOOTER_HEIGHT = 23;
        config.height = (EDITOR_HEIGHT - TITLE_HEIGHT + FOOTER_HEIGHT + THEME_HEIGHT_DELTA) + 'px';
    } else {
        config.height = (EDITOR_HEIGHT - TITLE_HEIGHT) + 'px';
    }
    config.resize_enabled = false;

    //5. KEEP THE TEXT FORMAT WHEN COPY FROM WORD or EXCEL;
    config.pasteFromWordRemoveFontStyles = false;
    config.pasteFromWordRemoveStyles = false;

    //6. CONFIGURE THE EDITOR'S MENU;
    config.toolbar = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', /*'Save',*/ 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'editing', groups: [ 'find', 'selection', /* 'spellchecker' */ ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-'/*, 'Scayt'*/ ] },
        /*{ name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },*/
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', /* 'Blockquote','CreateDiv',*/ '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl'/*, 'Language'*/ ] },
        /*{ name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },*/
        { name: 'insert', items: [ 'Image', /*'Flash',*/ 'Table', 'HorizontalRule', /*'Smiley', 'SpecialChar',*/ 'PageBreak', /*'Iframe'*/ ] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        { name: 'others', items: [ '-' ] },
        { name: 'about', items: [ /*'About'*/ ] }
    ];

    //7. REMOVE FOOTER;
    config.removePlugins = 'elementspath';

    //8. SAVE BUTTON EVENT HANDLER;
    /*CKEDITOR.plugins.registered['save'] = {
        init: function (editor) {
            var command = editor.addCommand('save',
                {
                    modes: { wysiwyg: 1, source: 1 },
                    exec: function (editor) { // Add here custom function for the save button
                        //var editor_data = CKEDITOR.instances['document-editor'].getData(); //get value
                        //var data = $( '#document-editor' ).val('set value'); //set value

                        $( '#document-editor' ).val( 'my new content' );
                        var url = contextPath + "/file/doc/save/";

                        $.ajax({
                            url: url,
                            type: "POST",
                            data: { name: 'test', extension: 'doc', content: '<p>test</p>' },
                            success: function (data) {
                                alert('The document has been saved.');
                            },
                            error: function (jqXHR) {
                                if(jqXHR.status  == 401){
                                    location.reload(true);
                                }
                            }
                        });
                    }
                });
            editor.ui.addButton('Save', { label: 'Save', command: 'save' });
        }
    }*/
};
