package com.coherent.api.dao.enums.documents;


import com.coherent.api.dao.enums.BaseEnum;

public enum SpeciesEnum implements BaseEnum {

    WORKER("documents.species.k.worker", "k"),
    TRANSFER("documents.species.k.transfer", "k"),
    DISMISSAL("documents.species.k.dismissal", "k"),
    PRACTICE("documents.species.k.practice", "k"),
    CHANGE_OF_SURNAMES("documents.species.k.change_of_surnames", "k"),
    CHANGE_WORKING_HOURS("documents.species.k.change_working_hours", "k"),
    LABOR("documents.species.l.labor", "l"),
    SOCIAL("documents.species.l.social", "l"),
    LABOR_COMPENSATION("documents.species.l.labor_compensation", "l");

    private final String value;
    private final String type;

    SpeciesEnum(String value, String type){
        this.value = value;
        this.type = type;

    }

    public String getValue() {
        return value;
    }

    public String getType() {
        return type;
    }
}
