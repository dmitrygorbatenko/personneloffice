package com.coherent.api.dao.enums.general;


import com.coherent.api.dao.enums.BaseEnum;

public enum DocumentTypeEnum implements BaseEnum {

    PASSPORT("general.document.passport"), RESIDENCE_PERMIT("general.document.residence_permit"), REFUGEE("general.document.refugee"),  FOREIGN_PASSPORT("general.document.foreign_passport");

    private final String value;

    DocumentTypeEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
