package com.coherent.api.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.*;


@Service
public class Test {

    @Autowired
    private DataSource dataSource;

    private  Connection getDBConnection() throws SQLException {
        return dataSource.getConnection();
    }

    public  Integer get_report_active_card_count_up_to_date(int year, int month) {
        int res_count = -1;
        try {
            res_count = exec_get_report_active_card_count_up_to_date(year, month);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return res_count;
    }

    private  Integer exec_get_report_active_card_count_up_to_date(int year, int month) throws SQLException {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String res_count = "-1";
        String getDBUSERByUserIdSql = "{CALL get_report_active_card_count_up_to_date(?, ?, ?)}";
        try {
            dbConnection = getDBConnection();
            callableStatement = dbConnection.prepareCall(getDBUSERByUserIdSql);
            callableStatement.registerOutParameter(1, java.sql.Types.INTEGER);
            callableStatement.setInt(2, year);
            callableStatement.setInt(3, month);
            callableStatement.executeUpdate();
            res_count = callableStatement.getString(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return Integer.parseInt(res_count);
    }

    //endregion

    //region 2# get_report_active_card_count_up_to_date_and_by_gender

    public  Integer get_report_active_card_count_up_to_date_and_by_gender(String gender, int year, int month) {
        int res_count = -1;
        try {
            res_count = exec_get_report_active_card_count_up_to_date_and_by_gender(gender, year, month);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return res_count;
    }

    private  Integer exec_get_report_active_card_count_up_to_date_and_by_gender(String gender, int year, int month) throws SQLException {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String res_count = "-1";
        String getDBUSERByUserIdSql = "{CALL get_report_active_card_count_up_to_date_and_by_gender(?, ?, ?, ?)}";
        try {
            dbConnection = getDBConnection();
            callableStatement = dbConnection.prepareCall(getDBUSERByUserIdSql);
            callableStatement.registerOutParameter(1, java.sql.Types.INTEGER);
            callableStatement.setString(2, gender);
            callableStatement.setInt(3, year);
            callableStatement.setInt(4, month);
            callableStatement.executeUpdate();
            res_count = callableStatement.getString(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return Integer.parseInt(res_count);
    }

    //endregion

    //region 3# get_active_count_for_specific_gender_year_month

    public  Integer get_active_count_for_specific_gender_year_month(String gender, int year, int month) {
        int res_count = -1;
        try {
            res_count = exec_get_active_count_for_specific_gender_year_month(gender, year, month);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return res_count;
    }

    private  Integer exec_get_active_count_for_specific_gender_year_month(String gender, int year, int month) throws SQLException {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String res_count = "-1";
        String getDBUSERByUserIdSql = "{CALL get_active_count_for_specific_gender_year_month(?, ?, ?, ?)}";
        try {
            dbConnection = getDBConnection();
            callableStatement = dbConnection.prepareCall(getDBUSERByUserIdSql);
            callableStatement.registerOutParameter(1, java.sql.Types.INTEGER);
            callableStatement.setString(2, gender);
            callableStatement.setInt(3, year);
            callableStatement.setInt(4, month);
            callableStatement.executeUpdate();
            res_count = callableStatement.getString(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return Integer.parseInt(res_count);
    }

    //endregion

    //region 4# get_active_count_for_specific_gender_year_month

    public  Integer get_dismissed_count_for_specific_gender_year_month(String gender, int year, int month) {
        int res_count = -1;
        try {
            res_count = exec_get_dismissed_count_for_specific_gender_year_month(gender, year, month);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return res_count;
    }

    private  Integer exec_get_dismissed_count_for_specific_gender_year_month(String gender, int year, int month) throws SQLException {
        Connection dbConnection = null;
        CallableStatement callableStatement = null;
        String res_count = "-1";
        String getDBUSERByUserIdSql = "{CALL get_dismissed_count_for_specific_gender_year_month(?, ?, ?, ?)}";
        try {
            dbConnection = getDBConnection();
            callableStatement = dbConnection.prepareCall(getDBUSERByUserIdSql);
            callableStatement.registerOutParameter(1, java.sql.Types.INTEGER);
            callableStatement.setString(2, gender);
            callableStatement.setInt(3, year);
            callableStatement.setInt(4, month);
            callableStatement.executeUpdate();
            res_count = callableStatement.getString(1);
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            if (callableStatement != null) {
                callableStatement.close();
            }
            if (dbConnection != null) {
                dbConnection.close();
            }
        }
        return Integer.parseInt(res_count);
    }

    //endregion
}