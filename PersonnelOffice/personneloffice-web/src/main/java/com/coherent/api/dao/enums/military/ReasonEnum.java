package com.coherent.api.dao.enums.military;

import com.coherent.api.dao.enums.BaseEnum;

public enum ReasonEnum implements BaseEnum {
    SERVICE_COMPLETE("military.reason.complete"),
    SERVICE_DELAY("military.reason.delay"),
    SERVICE_INVALID("military.reason.invalid");
    private String value;

    private ReasonEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
