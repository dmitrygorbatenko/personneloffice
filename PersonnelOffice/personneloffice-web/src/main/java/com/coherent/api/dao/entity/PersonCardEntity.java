package com.coherent.api.dao.entity;


import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.education.EducationEnum;
import com.coherent.api.dao.enums.education.LearningFormEnum;
import com.coherent.api.dao.enums.general.*;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Entity(name = "person_card")
public class PersonCardEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person_card_id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "patronymic")
    private String patronymic;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "tab_number")
    private String tabNumber;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private StatusEnum status;

    @Column(name = "trade_unionist")
    private Boolean tradeUnionist;

    @Column(name = "nationality")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private NationalityEnum nationality;

    @Column(name = "document")
    private String document;

    @Column(name = "document_series")
    private String documentSeries;

    @Column(name = "document_serial_number")
    private String documentSerialNumber;

    @Column(name = "document_individual_number")
    private String documentIndividualNumber;

    @Column(name = "document_date_issuance")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date documentDateIssuance;

    @Column(name = "document_validity_period")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date documentValidityPeriod;

    @Column(name = "document_issued_by")
    private String documentIssuedBy;

    @Column(name = "latin")
    private String latin;

    @Column(name = "insurance_certificate_number")
    private String insuranceCertificateNumber;

    @Column(name = "birthday")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date birthday;

    @Column(name = "allocation")
    private String allocation;

    @Column(name = "birthplace_country")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private CountryEnum birthplaceCountry;

    @Column(name = "birthplace_province")
    private String birthplaceProvince;

    @Column(name = "birthplace_region")
    private String birthplaceRegion;

    @Column(name = "birthplace_locality")
    private String birthplaceLocality;

    @Column(name = "large_family")
    private String largeFamily;

    @Column(name = "gender")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private GenderEnum gender;

    @Column(name = "family_status")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private FamilyStatusEnum familyStatus;

    @Column(name = "learning_form")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private LearningFormEnum learningForm;

    @Column(name = "education_degree")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private EducationEnum educationDegree;

    @Column(name = "institution")
    private String institution;

    @Column(name = "studying")
    private Boolean studying;

    @Column(name = "budget")
    private Boolean budget;

    @Column(name = "test_period")
    private Boolean testPeriod;

    @Column(name = "test_period_end_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date testPeriodEndDate;

    @Column(name = "studying_end_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date studyingEndDate;

    @Column(name = "allocation_start_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date allocationStartDate;

    @Column(name = "allocation_end_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date allocationEndDate;

    @Column(name = "certificate_number")
    private String certificateNumber;

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn( name = "registration_address_id", referencedColumnName = "address_id", updatable = true, insertable = true)
    @Fetch(FetchMode.SELECT)
    private AddressEntity registrationAddress = new AddressEntity();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn( name = "residential_address_id", referencedColumnName = "address_id", updatable = true, insertable = true)
    @Fetch(FetchMode.SELECT)
    private AddressEntity residentialAddress = new AddressEntity();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn( name = "reservist_id", referencedColumnName = "military_id", updatable = true, insertable = true)
    @Fetch(FetchMode.SELECT)
    private MilitaryEntity reservist = new MilitaryEntity();

    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE}, fetch = FetchType.EAGER)
    @JoinColumn( name = "conscript_id", referencedColumnName = "military_id", updatable = true, insertable = true)
    @Fetch(FetchMode.SELECT)
    private MilitaryEntity conscript = new MilitaryEntity();

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<FamilyMemberEntity> familyMembers;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<EducationEntity> education;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<TrainingEntity> training;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<RetrainingEntity> retraining;


    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<AdditionalInfoEntity> additionals;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<AssignmentEntity> assignments;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<ContractEntity> contracts;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<AttestationEntity> attestations;

    @OneToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinColumn(name = "person_card_id", referencedColumnName = "person_card_id", updatable = false, insertable = true)
    @Fetch(FetchMode.SELECT)
    private List<VacationEntity> vacations;

    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    @JoinTable(
            name="card_document_xref",
            joinColumns={@JoinColumn(name="person_card_id", referencedColumnName="person_card_id")},
            inverseJoinColumns={@JoinColumn(name="document_id", referencedColumnName="document_id")})
    @Fetch(FetchMode.SELECT)
    private List<DocumentEntity> documents;

    public PersonCardEntity(){
        registrationAddress = null;
        residentialAddress = null;
        reservist = null;
        conscript = null;
    }

    public List<VacationEntity> getVacations() {
        return vacations;
    }

    public void setVacations(List<VacationEntity> vacation) {
        this.vacations = vacation;
    }

    public String getShortName(){
        return lastName + " " + firstName.charAt(0) + ". " + patronymic.charAt(0) + ".";
    }

    public AssignmentEntity getLastAssignment(){
        if(assignments!= null && !assignments.isEmpty()){
            return assignments.get(assignments.size()-1);
        }
        return null;
    }

    public String getFamilyMembersAsString(){
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.DATE_FORMAT);
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        for(FamilyMemberEntity entity: familyMembers){
            sb.append(prefix);
            prefix = ", ";
            sb.append(entity.getName()).append(" ").append(sdf.format(entity.getBirthday()));
        }
        return sb.toString();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public String getLargeFamily() {
        return largeFamily;
    }

    public void setLargeFamily(String largeFamily) {
        this.largeFamily = largeFamily;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public CountryEnum getBirthplaceCountry() {
        return birthplaceCountry;
    }

    public void setBirthplaceCountry(CountryEnum birthplaceCountry) {
        this.birthplaceCountry = birthplaceCountry;
    }

    public String getBirthplaceProvince() {
        return birthplaceProvince;
    }

    public void setBirthplaceProvince(String birthplaceProvince) {
        this.birthplaceProvince = birthplaceProvince;
    }

    public String getBirthplaceRegion() {
        return birthplaceRegion;
    }

    public void setBirthplaceRegion(String birthplaceRegion) {
        this.birthplaceRegion = birthplaceRegion;
    }

    public String getBirthplaceLocality() {
        return birthplaceLocality;
    }

    public void setBirthplaceLocality(String birthplaceLocality) {
        this.birthplaceLocality = birthplaceLocality;
    }

    public FamilyStatusEnum getFamilyStatus() {
        return familyStatus;
    }

    public void setFamilyStatus(FamilyStatusEnum familyStatus) {
        this.familyStatus = familyStatus;
    }

    public GenderEnum getGender() {
        return gender;
    }

    public void setGender(GenderEnum gender) {
        this.gender = gender;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getTabNumber() {
        return tabNumber;
    }

    public void setTabNumber(String tabNumber) {
        this.tabNumber = tabNumber;
    }

    public Boolean getTradeUnionist() {
        return tradeUnionist;
    }

    public void setTradeUnionist(Boolean tradeUnionist) {
        this.tradeUnionist = tradeUnionist;
    }

    public NationalityEnum getNationality() {
        return nationality;
    }

    public void setNationality(NationalityEnum nationality) {
        this.nationality = nationality;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getDocumentSerialNumber() {
        return documentSerialNumber;
    }

    public void setDocumentSerialNumber(String documentSerialNumber) {
        this.documentSerialNumber = documentSerialNumber;
    }

    public List<AdditionalInfoEntity> getAdditionals() {
        return additionals;
    }

    public void setAdditionals(List<AdditionalInfoEntity> additionals) {
        this.additionals = additionals;
    }

    public String getDocumentIndividualNumber() {
        return documentIndividualNumber;
    }

    public void setDocumentIndividualNumber(String documentIndividualNumber) {
        this.documentIndividualNumber = documentIndividualNumber;
    }

    public Date getDocumentDateIssuance() {
        return documentDateIssuance;
    }

    public void setDocumentDateIssuance(Date documentDateIssuance) {
        this.documentDateIssuance = documentDateIssuance;
    }

    public Date getDocumentValidityPeriod() {
        return documentValidityPeriod;
    }

    public void setDocumentValidityPeriod(Date documentValidityPeriod) {
        this.documentValidityPeriod = documentValidityPeriod;
    }

    public String getDocumentIssuedBy() {
        return documentIssuedBy;
    }

    public void setDocumentIssuedBy(String documentIssuedBy) {
        this.documentIssuedBy = documentIssuedBy;
    }

    public String getInsuranceCertificateNumber() {
        return insuranceCertificateNumber;
    }

    public void setInsuranceCertificateNumber(String insuranceCertificateNumber) {
        this.insuranceCertificateNumber = insuranceCertificateNumber;
    }

    public String getLatin() {
        return latin;
    }

    public void setLatin(String latin) {
        this.latin = latin;
    }

    public String getDocumentSeries() {
        return documentSeries;
    }

    public void setDocumentSeries(String documentSeries) {
        this.documentSeries = documentSeries;
    }

    public StatusEnum getStatus() {
        return status;
    }

    public void setStatus(StatusEnum status) {
        this.status = status;
    }

    public AddressEntity getRegistrationAddress() {
        return registrationAddress;
    }

    public void setRegistrationAddress(AddressEntity registrationAddress) {
        this.registrationAddress = registrationAddress;
    }

    public AddressEntity getResidentialAddress() {
        return residentialAddress;
    }

    public void setResidentialAddress(AddressEntity residentialAddress) {
        this.residentialAddress = residentialAddress;
    }

    public MilitaryEntity getReservist() {
        return reservist;
    }

    public void setReservist(MilitaryEntity reservist) {
        this.reservist = reservist;
    }

    public MilitaryEntity getConscript() {
        return conscript;
    }

    public void setConscript(MilitaryEntity conscript) {
        this.conscript = conscript;
    }

    public List<FamilyMemberEntity> getFamilyMembers() {
        return familyMembers;
    }

    public void setFamilyMembers(List<FamilyMemberEntity> familyMembers) {
        this.familyMembers = familyMembers;
    }

    public List<AssignmentEntity> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<AssignmentEntity> assignments) {
        this.assignments = assignments;
    }

    public String getAllocation() {
        return allocation;
    }

    public void setAllocation(String allocation) {
        this.allocation = allocation;
    }

    public List<ContractEntity> getContracts() {
        return contracts;
    }

    public void setContracts(List<ContractEntity> contracts) {
        this.contracts = contracts;
    }

    public List<AttestationEntity> getAttestations() {
        return attestations;
    }

    public void setAttestations(List<AttestationEntity> attestations) {
        this.attestations = attestations;
    }

    public void setPersonCardNull(){
        for(DocumentEntity entity: documents){
            entity.setPersonCards(null);
        }
    }

    public List<DocumentEntity> getDocuments() {
        return documents;
    }

    public void setDocuments(List<DocumentEntity> documents) {
        this.documents = documents;
    }

    public Boolean getStudying() {
        return studying;
    }

    public void setStudying(Boolean studying) {
        this.studying = studying;
    }

    public Boolean getBudget() {
        return budget;
    }

    public void setBudget(Boolean budget) {
        this.budget = budget;
    }

    public Boolean getTestPeriod() {
        return testPeriod;
    }

    public void setTestPeriod(Boolean testPeriod) {
        this.testPeriod = testPeriod;
    }

    public Date getTestPeriodEndDate() {
        return testPeriodEndDate;
    }

    public void setTestPeriodEndDate(Date testPeriodEndDate) {
        this.testPeriodEndDate = testPeriodEndDate;
    }

    public Date getStudyingEndDate() {
        return studyingEndDate;
    }

    public void setStudyingEndDate(Date studyingEndDate) {
        this.studyingEndDate = studyingEndDate;
    }

    public Date getAllocationStartDate() {
        return allocationStartDate;
    }

    public void setAllocationStartDate(Date allocationStartDate) {
        this.allocationStartDate = allocationStartDate;
    }

    public Date getAllocationEndDate() {
        return allocationEndDate;
    }

    public void setAllocationEndDate(Date allocationEndDate) {
        this.allocationEndDate = allocationEndDate;
    }

    public String getCertificateNumber() {
        return certificateNumber;
    }

    public void setCertificateNumber(String certificateNumber) {
        this.certificateNumber = certificateNumber;
    }

    public List<EducationEntity> getEducation() {
        return education;
    }

    public void setEducation(List<EducationEntity> education) {
        this.education = education;
    }

    public List<TrainingEntity> getTraining() {
        return training;
    }

    public void setTraining(List<TrainingEntity> training) {
        this.training = training;
    }

    public List<RetrainingEntity> getRetraining() {
        return retraining;
    }

    public void setRetraining(List<RetrainingEntity> retraining) {
        this.retraining = retraining;
    }

    public LearningFormEnum getLearningForm() {
        return learningForm;
    }

    public void setLearningForm(LearningFormEnum learningForm) {
        this.learningForm = learningForm;
    }

    public EducationEnum getEducationDegree() {
        return educationDegree;
    }

    public void setEducationDegree(EducationEnum educationDegree) {
        this.educationDegree = educationDegree;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }
}
