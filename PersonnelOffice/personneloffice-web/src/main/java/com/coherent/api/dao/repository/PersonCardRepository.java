package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.*;
import com.coherent.api.dao.enums.general.StatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PersonCardRepository extends JpaRepository<PersonCardEntity, Long> {

    //Document dialog - Autocomplete Widget
    @Query("SELECT pc.id, CONCAT(pc.lastName, ' ', pc.firstName, ' ', pc.patronymic) FROM person_card AS pc")
    List<PersonCardEntity> getFullNames();

    //region #GetAll

    @Query("SELECT NEW com.coherent.api.dao.entity.FamilyMemberEntity(fm.id, fm.personCardId, fm.birthday, fm.kinship, fm.name, fm.invalid) FROM family_member AS fm WHERE fm.personCardId = :person_card_id")
    public List<FamilyMemberEntity> getAllFamilyMembers(@Param("person_card_id") Long person_card_id);

    //region #Documents

    @Query("SELECT NEW com.coherent.api.dao.entity.DocumentEntity(d.id, d.paperNumber, d.paperType, d.paperDate, d.species, d.subspecies, d.reason, d.category, d.content)" +
           "FROM document AS d " +
           "INNER JOIN d.personCardIdList AS cd " +
           "WHERE cd.person_card_id = :person_card_id")
    public List<DocumentEntity> getAllDocuments(@Param("person_card_id") Long person_card_id);

    @Query("SELECT NEW com.coherent.api.dao.entity.DocumentEntity(d.id, d.paperNumber, d.paperType, d.paperDate, d.species, d.subspecies, d.reason, d.category, d.content)" +
           "FROM document AS d " +
           "INNER JOIN d.personCardIdList AS cd " +
           "WHERE cd.person_card_id = :person_card_id and d.species = 'DISMISSAL'")
    public List<DocumentEntity> getAllDismissals(@Param("person_card_id") Long person_card_id);

    //region #GetAll

    @Query("SELECT NEW com.coherent.api.dao.entity.AdditionalInfoEntity(ai.id, ai.personCardId, ai.eventName, ai.eventDate, ai.notificationDate) FROM additional_info AS ai WHERE ai.personCardId = :person_card_id")
    List<AdditionalInfoEntity> getAllAdditionalInfoObjects(@Param("person_card_id") Long person_card_id);

    //region #Education

    @Query("SELECT NEW com.coherent.api.dao.entity.EducationEntity(id, personCardId, institution, faculty, qualification, " +
            "specialty, budget, diplomaDate, documentType, educationEndDate, " +
            "diplomaNumber) FROM education WHERE personCardId = :person_card_id")
    public List<EducationEntity> getAllEducationObjects(@Param("person_card_id") Long person_card_id);

    @Query("SELECT NEW com.coherent.api.dao.entity.TrainingEntity(id, personCardId, startDate, endDate, institution, " +
            "studyingTopic, documentType, documentNumber, documentDate) FROM training WHERE personCardId = :person_card_id")
    public List<TrainingEntity> getAllTrainings(@Param("person_card_id") Long person_card_id);

    @Query("SELECT NEW com.coherent.api.dao.entity.AttestationEntity(id, personCardId, date, decision, documentType, " +
            "documentNumber, documentDate, nextAttestation, reason) FROM attestation WHERE personCardId = :person_card_id")
    public List<AttestationEntity> getAllAttestations(@Param("person_card_id") Long person_card_id);

    @Query("SELECT NEW com.coherent.api.dao.entity.RetrainingEntity(id, personCardId, startDate, endDate, institution, " +
            "studyingTopic, documentType, documentNumber, documentDate) FROM retraining WHERE personCardId = :person_card_id")
    public List<RetrainingEntity> getAllRetrainings(@Param("person_card_id") Long person_card_id);

    //endregion

    @Query("SELECT NEW com.coherent.api.dao.entity.VacationEntity(id, personCardId, periodFrom, periodTo, vacationType, " +
            "vacationFrom, vacationTo, duration, orderNumber, orderDate) FROM vacation WHERE personCardId = :person_card_id")
    List<VacationEntity> getAllVacations(@Param("person_card_id") Long person_card_id);

    @Query("SELECT NEW com.coherent.api.dao.entity.AssignmentEntity(id, personCardId, department, position, rate, " +
            "orderNumber, orderDate, form, contractNumber, contractDate, " +
            "contractStart, contractEnd) FROM assignment WHERE personCardId = :person_card_id")
    List<AssignmentEntity> getAllAssignments(@Param("person_card_id") Long person_card_id);

    @Query("SELECT pc.status FROM person_card AS pc WHERE pc.id = :person_card_id")
    List<StatusEnum> getStatus(@Param("person_card_id") Long person_card_id);
    //endregion

}
