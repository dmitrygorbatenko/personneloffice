package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.DocumentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface DocumentRepository extends JpaRepository<DocumentEntity, Long> {

    @Query("SELECT cd.person_card_id FROM card_document_xref AS cd WHERE cd.document_id = (:document_id)")
    Long[] getCardIdList(@Param("document_id") Long document_id);
}
