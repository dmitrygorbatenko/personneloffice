package com.coherent.api.dao.repository;

import com.coherent.api.dao.entity.AdditionalInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AdditionalInfoRepository extends JpaRepository<AdditionalInfoEntity, Long>{

}