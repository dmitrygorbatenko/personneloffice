package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.family.KinshipEnum;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "family_member")
public class FamilyMemberEntity {

    public FamilyMemberEntity() {}
    public FamilyMemberEntity(Long id, Long personCardId, Date birthday, KinshipEnum kinship, String name, Boolean invalid)
    {
        this.id = id;
        this.personCardId = personCardId;
        this.birthday = birthday;
        this.kinship = kinship;
        this.name = name;
        this.invalid = invalid;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "family_member_id")
    private Long id;

    @Column(name = "person_card_id")
    private Long personCardId;

    @Column(name = "birthday")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date birthday;

    @Column(name = "kinship")
    @JsonSerialize(using = EnumSerializer.class)
    @Enumerated(EnumType.STRING)
    private KinshipEnum kinship;

    @Column(name = "name")
    private String name;

    @Column(name = "invalid")
    private Boolean invalid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonCardId() {
        return personCardId;
    }

    public void setPersonCardId(Long personCardId) {
        this.personCardId = personCardId;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public KinshipEnum getKinship() {
        return kinship;
    }

    public void setKinship(KinshipEnum kinship) {
        this.kinship = kinship;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getInvalid() {
        return invalid;
    }

    public void setInvalid(Boolean invalid) {
        this.invalid = invalid;
    }
}
