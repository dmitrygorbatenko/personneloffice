package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.education.DocumentTypeEnum;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "education")
public class EducationEntity {

    public EducationEntity() {}
    public EducationEntity(Long id, Long personCardId, String institution, String faculty, String qualification,
                           String specialty, Boolean budget, Date diplomaDate, DocumentTypeEnum documentType, Date educationEndDate,
                           String diplomaNumber)
    {
        this.id = id;
        this.personCardId = personCardId;
        this.institution = institution;
        this.faculty = faculty;
        this.qualification = qualification;
        this.specialty = specialty;
        this.budget = budget;
        this.diplomaDate = diplomaDate;
        this.documentType = documentType;
        this.educationEndDate = educationEndDate;
        this.diplomaNumber = diplomaNumber;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "education_id")
    private Long id;

    @Column(name = "person_card_id")
    private Long personCardId;

    @Column(name = "institution")
    private String institution;

    @Column(name = "faculty")
    private String faculty;

    @Column(name = "qualification")
    private String qualification;

    @Column(name = "specialty")
    private String specialty;

    @Column(name = "budget")
    private Boolean budget;

    @Column(name = "diploma_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date diplomaDate;

    @Column(name = "document_type")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private DocumentTypeEnum documentType;

    @Column(name = "education_end_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date educationEndDate;

    @Column(name = "diploma_number")
    private String diplomaNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonCardId() {
        return personCardId;
    }

    public void setPersonCardId(Long personCardId) {
        this.personCardId = personCardId;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public Boolean getBudget() {
        return budget;
    }

    public void setBudget(Boolean budget) {
        this.budget = budget;
    }

    public Date getDiplomaDate() {
        return diplomaDate;
    }

    public void setDiplomaDate(Date diplomaDate) {
        this.diplomaDate = diplomaDate;
    }

    public Date getEducationEndDate() {
        return educationEndDate;
    }

    public void setEducationEndDate(Date educationEndDate) {
        this.educationEndDate = educationEndDate;
    }

    public String getDiplomaNumber() {
        return diplomaNumber;
    }

    public void setDiplomaNumber(String diplomaNumber) {
        this.diplomaNumber = diplomaNumber;
    }

    public DocumentTypeEnum getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeEnum documentType) {
        this.documentType = documentType;
    }
}
