package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.AssignmentEntity;
import com.coherent.api.dao.repository.AssignmentRepository;
import com.coherent.api.service.AssignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

@Service("assignmentService")
public class AssignmentServiceImpl implements AssignmentService {

    @Autowired
    AssignmentRepository assignmentRepository;

    @Override
    @Modifying
    public AssignmentEntity save(AssignmentEntity entity) {
        return assignmentRepository.save(entity);
    }
    @Override
    public AssignmentEntity get(Long id) {
        return assignmentRepository.findOne(id);
    }
    @Override
    public void delete(Long id) {
        assignmentRepository.delete(id);
    }
}
