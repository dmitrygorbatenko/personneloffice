package com.coherent.api.service;

import com.coherent.api.dao.entity.VacationEntity;

public interface VacationService {

    public VacationEntity save(VacationEntity entity);

    public VacationEntity get(Long id);

    void delete(Long id);

}
