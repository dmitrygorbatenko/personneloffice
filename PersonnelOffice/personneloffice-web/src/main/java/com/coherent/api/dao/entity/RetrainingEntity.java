package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.education.DocumentTypeEnum;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "retraining")
public class RetrainingEntity {

    public RetrainingEntity() {}
    public RetrainingEntity(Long id, Long personCardId, Date startDate, Date endDate, String institution,
                            String studyingTopic, DocumentTypeEnum documentType, String documentNumber, Date documentDate)
    {
        this.id = id;
        this.personCardId = personCardId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.institution = institution;
        this.studyingTopic = studyingTopic;
        this.documentType = documentType;
        this.documentNumber = documentNumber;
        this.documentDate = documentDate;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "retraining_id")
    private Long id;

    @Column(name = "person_card_id")
    private Long personCardId;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date startDate;

    @Column(name = "end_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date endDate;

    @Column(name = "institution")
    private String institution;

    @Column(name = "studying_topic")
    private String studyingTopic;

    @Column(name = "document_type")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private DocumentTypeEnum documentType;

    @Column(name = "document_number")
    private String documentNumber;

    @Column(name = "document_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date documentDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonCardId() {
        return personCardId;
    }

    public void setPersonCardId(Long personCardId) {
        this.personCardId = personCardId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getStudyingTopic() {
        return studyingTopic;
    }

    public void setStudyingTopic(String studyingTopic) {
        this.studyingTopic = studyingTopic;
    }

    public DocumentTypeEnum getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentTypeEnum documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getDocumentDate() {
        return documentDate;
    }

    public void setDocumentDate(Date documentDate) {
        this.documentDate = documentDate;
    }
}
