package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.MilitaryEntity;
import com.coherent.api.dao.repository.MilitaryRepository;
import com.coherent.api.dao.repository.PersonCardRepository;
import com.coherent.api.service.MilitaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

@Service("militaryService")
public class MilitaryServiceImpl implements MilitaryService {

    @Autowired
    MilitaryRepository militaryRepository;

    @Autowired
    PersonCardRepository personCardRepository;

    @Override
    @Modifying
    public MilitaryEntity save(MilitaryEntity entity) {
        return militaryRepository.save(entity);
    }
}