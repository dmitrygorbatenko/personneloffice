package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.documents.*;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity(name = "document")
public class DocumentEntity {

    public DocumentEntity() {}
    public DocumentEntity(Long id, String paperNumber, PaperTypeEnum paperType, Date paperDate, SpeciesEnum species, SubSpeciesEnum subspecies, String reason, CategoryEnum category, String content)
    {
        this.id = id;
        this.paperNumber = paperNumber;
        this.paperType = paperType;
        this.paperDate = paperDate;
        this.species = species;
        this.subspecies = subspecies;
        this.reason = reason;
        this.category = category;
        this.content = content;
        this.personCardIdList = null;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "document_id")
    private Long id;

    @Column(name = "paper_number")
    private String paperNumber;

    @Column(name = "paper_type")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private PaperTypeEnum paperType;

    @Column(name= "paper_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date paperDate;

    @Column(name = "species")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private SpeciesEnum species;

    @Column(name = "subspecies")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private SubSpeciesEnum subspecies;

    @Column(name = "reason")
    private String reason;

    @Column(name = "category")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private CategoryEnum category;

    @Column(name = "content")
    private String content;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "document_id", referencedColumnName = "document_id", updatable = false, insertable = false)
    @Fetch(FetchMode.SELECT)
    private List<PersonCardDocumentEntity> personCardIdList;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="card_document_xref",
            joinColumns={@JoinColumn(name="document_id", referencedColumnName="document_id")},
            inverseJoinColumns={@JoinColumn(name="person_card_id", referencedColumnName="person_card_id")})
    @Fetch(FetchMode.SELECT)
    private List<PersonCardEntity> personCards;


    public List<PersonCardEntity> getPersonCards() {
        return personCards;
    }

    public void setPersonCards(List<PersonCardEntity> personCards) {
        this.personCards = personCards;
    }

    public List<PersonCardDocumentEntity> getPersonCardIdList() {
        return personCardIdList;
    }

    public void setPersonCardIdList(List<PersonCardDocumentEntity> personCardIdList) {
        this.personCardIdList = personCardIdList;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPaperNumber() {
        return paperNumber;
    }

    public void setPaperNumber(String paperNumber) {
        this.paperNumber = paperNumber;
    }

    public PaperTypeEnum getPaperType() {
        return paperType;
    }

    public void setPaperType(PaperTypeEnum paperType) {
        this.paperType = paperType;
    }

    public Date getPaperDate() {
        return paperDate;
    }

    public void setPaperDate(Date paperDate) {
        this.paperDate = paperDate;
    }

    public SpeciesEnum getSpecies() {
        return species;
    }

    public void setSpecies(SpeciesEnum species) {
        this.species = species;
    }

    public SubSpeciesEnum getSubspecies() {
        return subspecies;
    }

    public void setSubspecies(SubSpeciesEnum subspecies) {
        this.subspecies = subspecies;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public CategoryEnum getCategory() {
        return category;
    }

    public void setCategory(CategoryEnum category) {
        this.category = category;
    }
}
