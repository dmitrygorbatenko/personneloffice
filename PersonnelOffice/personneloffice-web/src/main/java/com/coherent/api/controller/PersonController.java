package com.coherent.api.controller;

import com.coherent.api.dao.entity.*;
import com.coherent.api.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.*;


@Controller
@RequestMapping(value = "/persons")
public class PersonController {


    @Autowired
    private PersonCardService personCardService;

    @Autowired
    private AssignmentService assignmentService;

    @Autowired
    private FamilyMemberService familyMemberService;

    @Autowired
    private VacationService vacationService;

    @Autowired
    private AdditionalInfoService additionalInfoService;

    @Autowired
    private EducationService educationService;

    @Autowired
    private TrainingService trainingService;

    @Autowired
    private RetrainingService retrainingService;

    @Autowired
    private AttestationService attestationService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private PersonCardDocumentService personCardDocumentService;

    private PreferenceEntity [] preferences = {
            new PreferenceEntity(0,"full_name",true,true),
            new PreferenceEntity(1,"rate",false,true),
            new PreferenceEntity(2,"gender",false,false),
            new PreferenceEntity(3,"position",true,true),
            new PreferenceEntity(4,"start_date",false,true),
            new PreferenceEntity(5,"transfer",false,false),
            new PreferenceEntity(6,"order_number",false,false),
            new PreferenceEntity(7,"order_date",false,false),
            new PreferenceEntity(8,"contract_number",false,false),
            new PreferenceEntity(9,"education",false,false),
            new PreferenceEntity(10,"finish_date",false,false),
            new PreferenceEntity(11,"birthday",false,false),
            new PreferenceEntity(12,"assignment",false,false),
            new PreferenceEntity(13,"phone",false,false),
            new PreferenceEntity(14,"children_info",false,false),
            new PreferenceEntity(15,"full_name_latin",false,false),
            new PreferenceEntity(16,"passport_number",false,false),
            new PreferenceEntity(17,"passport_individual_number",false,false),
            new PreferenceEntity(18,"passport_date_issuance",false,false),
            new PreferenceEntity(19,"passport_validity_period",false,false),
            new PreferenceEntity(20,"passport_office",false,false),
            new PreferenceEntity(21,"registration",false,false),
            new PreferenceEntity(22,"form_of_lease",false,true),
            new PreferenceEntity(23,"status",true,false),
    };

    //region Person

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getAllPersons(ModelMap modelMap) {
        modelMap.addAttribute("persons", personCardService.getAll());
        modelMap.addAttribute("preferences",preferences);
        modelMap.addAttribute("activeTab", "cards");
        return "person/cards";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String editPerson(@PathVariable Long id, ModelMap modelMap) {
        if(id == 0 ){
            modelMap.addAttribute("personCard", new PersonCardEntity());
        } else {
            PersonCardEntity personCard =  personCardService.get(id);
            modelMap.addAttribute("personCard",personCard);
        }

        return "person/dialogs/card_editor";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> savePerson(@ModelAttribute("personCard")PersonCardEntity personCard) {
        Map<String, Object> map = new HashMap<>();

        map.put("personCard", personCardService.save(personCard));
        map.put("message", "success");
        return map;
    }

    @RequestMapping(value = "/status/{personCardId}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> savePerson(@PathVariable("personCardId") Long personCardId) {
        Map<String, Object> map = new HashMap<>();

        map.put("status", personCardService.getStatus(personCardId));
        map.put("message", "success");
        return map;
    }
    //endregion

    //region FamilyMember

    @RequestMapping(value = "/family_member/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editFamilyMember(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            FamilyMemberEntity familyMember = new FamilyMemberEntity();
            familyMember.setPersonCardId(personCardId);
            modelMap.addAttribute("familyMember", familyMember);
        } else {
            modelMap.addAttribute("familyMember", familyMemberService.get(id));
        }
        return "person/dialogs/popup/add_familyMember";
    }

    @RequestMapping(value = "/family_member/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteFamilyMember(@PathVariable Long id){
        familyMemberService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/family_member/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveFamilyMember(@ModelAttribute("familyMember") FamilyMemberEntity familyMember){
        Map<String, Object> response = new HashMap<>();
        familyMember = familyMemberService.save(familyMember);
        response.put("familyMember", familyMember);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/family_member/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getFamilyMembers(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        List<FamilyMemberEntity> lst = personCardService.getAllFamilyMembers(personCardId);
        response.put("family_member", lst);
        response.put("status", "success");
        return response;
    }

    //endregion

    //region AdditionalInfo

    @RequestMapping(value = "/additional_info/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editAdditionalInfo(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            AdditionalInfoEntity additionalInfo = new AdditionalInfoEntity();
            additionalInfo.setPersonCardId(personCardId);
            modelMap.addAttribute("additionalInfo", additionalInfo);
        } else {
            modelMap.addAttribute("additionalInfo", additionalInfoService.get(id));
        }
        return "person/dialogs/popup/add_additionalInfo";
    }

    @RequestMapping(value = "/additional_info/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteAdditionalInfo(@PathVariable Long id){
        additionalInfoService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/additional_info/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveAdditionalInfo(@ModelAttribute("additionalInfo") AdditionalInfoEntity additionalInfo){
        Map<String, Object> response = new HashMap<>();
        additionalInfo = additionalInfoService.save(additionalInfo);
        response.put("additionalInfo", additionalInfo);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/additional_info/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAdditionalPiecesOfInfo(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("additional_info", personCardService.getAllAdditionalInfoObjects(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

    //region Assignment

    @RequestMapping(value = "/assignment/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveAssignment(@ModelAttribute("assignment") AssignmentEntity assignment){
        Map<String, Object> response = new HashMap<>();
        assignment = assignmentService.save(assignment);
        response.put("assignment", assignment);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/assignment/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editAssignment(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            AssignmentEntity assignment = new AssignmentEntity();
            assignment.setPersonCardId(personCardId);
            modelMap.addAttribute("assignment", assignment);
        } else {
            modelMap.addAttribute("assignment", assignmentService.get(id));
        }
        return "person/dialogs/popup/add_assignment";
    }

    @RequestMapping(value = "/assignment/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteAssignment(@PathVariable Long id){
        assignmentService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/assignment/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAssignments(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        List<AssignmentEntity> lst = personCardService.getAllAssignments(personCardId);
        response.put("assignment", lst);
        response.put("status", "success");
        return response;
    }

    //endregion

    //region Document (many-to-many)

    @RequestMapping(value = "/document/save/{card_ids}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveDocument(@ModelAttribute("document") DocumentEntity document, @PathVariable String card_ids){
        Map<String, Object> response = new HashMap<>();
        document = documentService.save(document);
        MapUIToDbConnectionRows(parseCommaDelimitedNumbers(card_ids), document.getId());
        response.put("document", document);
        response.put("status", "success");
        return response;
    }
    private Long[] parseCommaDelimitedNumbers(String ids) {
        String[] ids_str = ids.split("[,]");
        Long[] result = new Long[ids_str.length];
        for(int i=0; i<ids_str.length; i++) {
            result[i] = Long.parseLong(ids_str[i]);
        }
        return result;
    }
    private void MapUIToDbConnectionRows(Long[] card_ids, Long document_id) {
        if(card_ids.length == 0) { return; }
        Arrays.sort(card_ids);
        Long[] complete_card_ids = personCardDocumentService.getAllCardIdsByDocumentId(document_id);
        int i=0, j=0;
        for(; i<complete_card_ids.length && j<card_ids.length;) {
            if(complete_card_ids[i] < card_ids[j]) {
                personCardDocumentService.delete(new PersonCardDocumentEntity(complete_card_ids[i], document_id));
                i++;
            } else if (complete_card_ids[i] > card_ids[j]) {
                personCardDocumentService.save(new PersonCardDocumentEntity(card_ids[j], document_id));
                j++;
            } else {
                i++;
                j++;
            }
        }
        while(i<complete_card_ids.length) {
            personCardDocumentService.delete(new PersonCardDocumentEntity(complete_card_ids[i], document_id));
            i++;
        }
        while(j<card_ids.length) {
            personCardDocumentService.save(new PersonCardDocumentEntity(card_ids[j], document_id));
            j++;
        }
    }

    @RequestMapping(value = "/document/getFullNames", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public Map<String, Object> getFullNames(){
        Map<String, Object> response = new HashMap<>();
        List<PersonCardEntity> fullname_list = personCardService.getFullNames();
        response.put("fullname_list", fullname_list);
        response.put("status", "success");
        return response;
    }
    @RequestMapping(value = "/document/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editDocument(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            DocumentEntity document = new DocumentEntity();
            document.setId(id);
            List<PersonCardDocumentEntity> lst = new ArrayList<PersonCardDocumentEntity>();
            if(personCardId != 0) {
                lst.add(new PersonCardDocumentEntity(personCardId, id));
                document.setPersonCardIdList(lst);
            }
            modelMap.addAttribute("document", document);
        } else {
            DocumentEntity document = documentService.get(id);

            Long[] id_lst = documentService.getCardIdList(id);
            List<PersonCardDocumentEntity> en_lst = new ArrayList<PersonCardDocumentEntity>();
            for(int i=0; i<id_lst.length; i++) {
                en_lst.add(new PersonCardDocumentEntity(id_lst[i], id));
            }
            document.setPersonCardIdList(en_lst);
            modelMap.addAttribute("document", document);
        }
        return "person/dialogs/popup/add_document";
    }

    @RequestMapping(value = "/document/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteDocument(@PathVariable Long id){
        documentService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/document/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getDocument(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("document", personCardService.getAllDocuments(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

    //region Attestation

    @RequestMapping(value = "/attestation/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveAttestation(@ModelAttribute("attestation") AttestationEntity attestation){
        Map<String, Object> response = new HashMap<>();
        attestation = attestationService.save(attestation);
        response.put("attestation", attestation);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/attestation/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editAttestation(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            AttestationEntity attestation = new AttestationEntity();
            attestation.setPersonCardId(personCardId);
            modelMap.addAttribute("attestation", attestation);
        } else {
            modelMap.addAttribute("attestation", attestationService.get(id));
        }
        return "person/dialogs/popup/add_attestation";
    }

    @RequestMapping(value = "/attestation/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteAttestation(@PathVariable Long id){
        attestationService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/attestation/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAttestation(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("attestation", personCardService.getAllAttestations(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

    // region Education

    @RequestMapping(value = "/education/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveEducation(@ModelAttribute("education") EducationEntity education){
        Map<String, Object> response = new HashMap<>();
        education = educationService.save(education);
        response.put("education", education);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/education/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editEducation(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            EducationEntity education = new EducationEntity();
            education.setPersonCardId(personCardId);
            modelMap.addAttribute("education", education);
        } else {
            modelMap.addAttribute("education", educationService.get(id));
        }
        return "person/dialogs/popup/add_education";
    }

    @RequestMapping(value = "/education/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteEducation(@PathVariable Long id){
        educationService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/education/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getEducation(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("education", personCardService.getAllEducationObjects(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

    // region Training

    @RequestMapping(value = "/training/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveTraining(@ModelAttribute("training") TrainingEntity training){
        Map<String, Object> response = new HashMap<>();
        training = trainingService.save(training);
        response.put("training", training);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/training/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editTraining(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            TrainingEntity training = new TrainingEntity();
            training.setPersonCardId(personCardId);
            modelMap.addAttribute("training", training);
        } else {
            modelMap.addAttribute("training", trainingService.get(id));
        }
        return "person/dialogs/popup/add_training";
    }

    @RequestMapping(value = "/training/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteTraining(@PathVariable Long id){
        trainingService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/training/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getTraining(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("training", personCardService.getAllTrainings(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

    // region Retraining

    @RequestMapping(value = "/retraining/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveRetraining(@ModelAttribute("retraining") RetrainingEntity retraining){
        Map<String, Object> response = new HashMap<>();
        retraining = retrainingService.save(retraining);
        response.put("retraining", retraining);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/retraining/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editRetraining(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            RetrainingEntity retraining = new RetrainingEntity();
            retraining.setPersonCardId(personCardId);
            modelMap.addAttribute("retraining", retraining);
        } else {
            modelMap.addAttribute("retraining", retrainingService.get(id));
        }
        return "person/dialogs/popup/add_retraining";
    }

    @RequestMapping(value = "/retraining/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteRetraining(@PathVariable Long id){
        retrainingService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/retraining/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllRetrainings(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("retraining", personCardService.getAllRetrainings(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

    // region Vacation

    @RequestMapping(value = "/vacation/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveVacation(@ModelAttribute("vacation") VacationEntity vacation){
        Map<String, Object> response = new HashMap<>();
        vacation = vacationService.save(vacation);
        response.put("vacation", vacation);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/vacation/edit/{personCardId}/{id}", method = RequestMethod.GET)
    public String editVacation(@PathVariable Long id, @PathVariable Long personCardId, ModelMap modelMap){
        if(id == 0){
            VacationEntity vacation = new VacationEntity();
            vacation.setPersonCardId(personCardId);
            modelMap.addAttribute("vacation", vacation);
        } else {
            modelMap.addAttribute("vacation", vacationService.get(id));
        }
        return "person/dialogs/popup/add_vacation";
    }

    @RequestMapping(value = "/vacation/delete/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> deleteVacation(@PathVariable Long id){
        vacationService.delete(id);
        Map<String, Object> map = new HashMap<>();
        map.put("status", "success");
        return map;
    }

    @RequestMapping(value = "/vacation/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllVacations(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("vacation", personCardService.getAllVacations(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

    // region dismissal

    @RequestMapping(value = "/dismissal/get/{personCardId}", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Object> getAllDismissals(@PathVariable Long personCardId){
        Map<String, Object> response = new HashMap<>();
        response.put("dismissal", personCardService.getAllDismissals(personCardId));
        response.put("status", "success");
        return response;
    }

    //endregion

}
