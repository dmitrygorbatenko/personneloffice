package com.coherent.api.dao.enums.education;


import com.coherent.api.dao.enums.BaseEnum;

public enum LearningFormEnum  implements BaseEnum {
    FULL_TIME("learning_form.full_time"), EVENING("learning_form.evening"), EXTRAMURAL("learning_form.extramural");

    private String value;

    private LearningFormEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
