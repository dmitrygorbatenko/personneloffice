package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.vacation.VacationTypeEnum;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "vacation")
public class VacationEntity {

    public VacationEntity() {}
    public VacationEntity(Long id, Long personCardId, Date periodFrom, Date periodTo, VacationTypeEnum vacationType,
                          Date vacationFrom, Date vacationTo, Integer duration, String orderNumber, Date orderDate)
    {
        this.id = id;
        this.personCardId = personCardId;
        this.periodFrom = periodFrom;
        this.periodTo = periodTo;
        this.vacationType = vacationType;
        this.vacationFrom = vacationFrom;
        this.vacationTo = vacationTo;
        this.duration = duration;
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "vacation_id")
    private Long id;

    @Column(name = "person_card_id")
    private Long personCardId;

    @Column(name= "period_from")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date periodFrom;

    @Column(name= "period_to")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date periodTo;

    @Column(name = "vacation_type")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private VacationTypeEnum vacationType;

    @Column(name= "vacation_from")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date vacationFrom;

    @Column(name= "vacation_to")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date vacationTo;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name= "order_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date orderDate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonCardId() {
        return personCardId;
    }

    public void setPersonCardId(Long personCardId) {
        this.personCardId = personCardId;
    }

    public Date getPeriodFrom() {
        return periodFrom;
    }

    public void setPeriodFrom(Date periodFrom) {
        this.periodFrom = periodFrom;
    }

    public Date getPeriodTo() {
        return periodTo;
    }

    public void setPeriodTo(Date periodTo) {
        this.periodTo = periodTo;
    }

    public VacationTypeEnum getVacationType() {
        return vacationType;
    }

    public void setVacationType(VacationTypeEnum vacationType) {
        this.vacationType = vacationType;
    }

    public Date getVacationFrom() {
        return vacationFrom;
    }

    public void setVacationFrom(Date vacationFrom) {
        this.vacationFrom = vacationFrom;
    }

    public Date getVacationTo() {
        return vacationTo;
    }

    public void setVacationTo(Date vacationTo) {
        this.vacationTo = vacationTo;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }
}
