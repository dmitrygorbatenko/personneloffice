package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.TrainingEntity;
import com.coherent.api.dao.repository.TrainingRepository;
import com.coherent.api.service.TrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("trainingService")
public class TrainingServiceImpl implements TrainingService {

    @Autowired
    private TrainingRepository trainingRepository;

    @Override
    public TrainingEntity get(Long id) {
        return trainingRepository.findOne(id);
    }

    @Override
    public TrainingEntity save(TrainingEntity entity) {
        return trainingRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        trainingRepository.delete(id);
    }
}
