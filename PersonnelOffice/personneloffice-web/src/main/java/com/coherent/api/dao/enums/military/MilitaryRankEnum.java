package com.coherent.api.dao.enums.military;

import com.coherent.api.dao.enums.BaseEnum;

public enum MilitaryRankEnum implements BaseEnum {

    PRIVATE("military.rank.private"),
    CORPORAL("military.rank.corporal"),
    LANCE_SERGEANT("military.rank.lance_sergeant"),
    SERGEANT("military.rank.sergeant"),
    STAFF_SERGEANT("military.rank.staff_sergeant"),
    FOREMAN("military.rank.foreman"),
    ENSIGN("military.rank.ensign"),
    SENIOR_ENSIGN("military.rank.senior_ensign"),
    LANCE_LIEUTENANT("military.rank.lance_lieutenant"),
    LIEUTENANT("military.rank.lieutenant"),
    STAFF_LIEUTENANT("military.rank.staff_lieutenant"),
    CAPTAIN("military.rank.captain"),
    MAJOR("military.rank.major"),
    LIEUTENANT_COLONEL("military.rank.lieutenant_colonel"),
    COLONEL("military.rank.colonel"),
    MAJOR_GENERAL("military.rank.major_general"),
    LIEUTENANT_GENERAL("military.rank.lieutenant_general"),
    COLONEL_GENERAL("military.rank.colonel_general"),
    SAILOR("military.rank.sailor"),
    SEAMAN("military.rank.seaman"),
    PETTY_OFFICER_2ND_CLASS("military.rank.petty_officer_2nd_class"),
    PETTY_OFFICER_1ST_CLASS("military.rank.petty_officer_1st_class"),
    CHIEF_PETTY_OFFICER("military.rank.chief_petty_officer"),
    CHIEF_PETTY_SHEEP_OFFICER("military.rank.chief_petty_sheep_officer"),
    MIDSHIPMAN("military.rank.midshipman"),
    SENIOR_WARRANT_OFFICER("military.rank.senior_warrant_officer"),
    LIEUTENANT_COMMANDER("military.rank.lieutenant_commander"),
    CAPTAIN_3RD_RANK("military.rank.captain_3rd_rank"),
    CAPTAIN_2ND_RANK("military.rank.captain_2nd_rank"),
    CAPTAIN_1ST_RANK("military.rank.captain_1st_rank");

    private String value;

    private MilitaryRankEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
