package com.coherent.api.dao.entity;

import com.coherent.api.dao.enums.reports.MonthEnum;

public class MonthReportEntity {

    private MonthEnum monthName;
    private int workersCount;
    private int woman;
    private int hiredW;
    private int hiredM;
    private int dismissedW;
    private int dismissedM;

    public MonthEnum getMonthName() {
        return monthName;
    }

    public void setMonthName(MonthEnum monthName) {
        this.monthName = monthName;
    }

    public int getWorkersCount() {
        return workersCount;
    }

    public void setWorkersCount(int workersCount) {
        this.workersCount = workersCount;
    }

    public int getWoman() {
        return woman;
    }

    public void setWoman(int woman) {
        this.woman = woman;
    }

    public int getHiredW() {
        return hiredW;
    }

    public void setHiredW(int hiredW) {
        this.hiredW = hiredW;
    }

    public int getHiredM() {
        return hiredM;
    }

    public void setHiredM(int hiredM) {
        this.hiredM = hiredM;
    }

    public int getDismissedW() {
        return dismissedW;
    }

    public void setDismissedW(int dismissedW) {
        this.dismissedW = dismissedW;
    }

    public int getDismissedM() {
        return dismissedM;
    }

    public void setDismissedM(int dismissedM) {
        this.dismissedM = dismissedM;
    }
}
