package com.coherent.api.dao.enums.general;


import com.coherent.api.dao.enums.BaseEnum;

public enum AcademicRankEnum implements BaseEnum {
    PhD("academic_rank.phd"), MAGISTER("academic_rank.magister"), BACHELOR("academic_rank.bachelor");

    private String value;

    private AcademicRankEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


}
