package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.AttestationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttestationRepository extends JpaRepository<AttestationEntity, Long> {
}
