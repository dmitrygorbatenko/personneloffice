package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.*;
import com.coherent.api.dao.enums.BaseEnum;
import com.coherent.api.dao.enums.reports.MonthEnum;
import com.coherent.api.service.ExportService;
import com.coherent.api.util.Translit;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

@Service("exportService")
public class ExportServiceImpl implements ExportService {

    private static final int T2_GENDER_CELL_ROW = 9;
    private static final int T2_GENDER_CELL_COLUMN = 23;
    private static final int T2_ALPHABET_CELL_ROW = 9;
    private static final int T2_ALPHABET_CELL_COLUMN = 30;
    private static final int T2_CARD_NUMBER_CELL_ROW = 11;
    private static final int T2_CARD_NUMBER_CELL_COLUMN = 21;
    private static final int T2_LAST_NAME_CELL_ROW = 14;
    private static final int T2_LAST_NAME_CELL_COLUMN = 6;
    private static final int T2_FIRST_NAME_CELL_ROW = 15;
    private static final int T2_FIRST_NAME_CELL_COLUMN = 4;
    private static final int T2_PATRONYMIC_CELL_ROW = 15;
    private static final int T2_PATRONYMIC_CELL_COLUMN = 11;
    private static final int T2_BIRTH_YEAR_CELL_ROW = 16;
    private static final int T2_BIRTH_YEAR_CELL_COLUMN = 7;
    private static final int T2_BIRTH_DAY_CELL_ROW = 16;
    private static final int T2_BIRTH_DAY_CELL_COLUMN = 12;
    private static final int T2_BIRTH_MONTH_CELL_ROW = 16;
    private static final int T2_BIRTH_MONTH_CELL_COLUMN = 16;
    private static final int T2_BIRTH_PLACE_CELL_ROW = 17;
    private static final int T2_BIRTH_PLACE_CELL_COLUMN = 8;
    private static final int T2_NATIONALITY_CELL_ROW = 21;
    private static final int T2_NATIONALITY_CELL_COLUMN = 7;
    private static final int T2_EDUCATION_DEGREE_CELL_ROW = 26;
    private static final int T2_EDUCATION_DEGREE_CELL_COLUMN = 3;
    private static final int T2_INSTITUTION_FIRST_LINE_CELL_ROW = 28;
    private static final int T2_INSTITUTION_FIRST_LINE_CELL_COLUMN = 3;
    private static final int T2_INSTITUTION_SECOND_LINE_CELL_ROW = 30;
    private static final int T2_INSTITUTION_SECOND_LINE_CELL_COLUMN = 2;
    private static final int T2_LEARNING_FORM_CELL_ROW = 40;
    private static final int T2_LEARNING_FORM_CELL_COLUMN = 10;
    private static final int T2_FAMILY_STATUS_CELL_ROW = 20;
    private static final int T2_FAMILY_STATUS_CELL_COLUMN = 25;
    private static final int T2_PASSPORT_CELL_ROW = 38;
    private static final int T2_PASSPORT_CELL_COLUMN = 22;
    private static final int T2_DOCUMENT_ISSUED_BY_CELL_ROW = 40;
    private static final int T2_DOCUMENT_ISSUED_BY_CELL_COLUMN = 22;
    private static final int T2_DOCUMENT_DATE_ISSUANCE_CELL_ROW = 42;
    private static final int T2_DOCUMENT_DATE_ISSUANCE_CELL_COLUMN = 22;
    private static final int T2_DOCUMENT_VALIDITY_PERIOD_CELL_ROW = 43;
    private static final int T2_DOCUMENT_VALIDITY_PERIOD_CELL_COLUMN = 23;
    private static final int T2_DOCUMENT_INDIVIDUAL_NUMBER_CELL_ROW = 45;
    private static final int T2_DOCUMENT_INDIVIDUAL_NUMBER_CELL_COLUMN = 22;
    private static final int T2_INSURANCE_CERTIFICATE_NUMBER_CELL_ROW = 47;
    private static final int T2_INSURANCE_CERTIFICATE_NUMBER_CELL_COLUMN = 27;
    private static final int T2_ADDRESS_FIRST_CELL_ROW = 48;
    private static final int T2_ADDRESS_FIRST_CELL_COLUMN = 24;
    private static final int T2_ADDRESS_SECOND_CELL_ROW = 49;
    private static final int T2_ADDRESS_SECOND_CELL_COLUMN = 18;
    private static final int T2_PHONE_NUMBER_CELL_ROW = 53;
    private static final int T2_PHONE_NUMBER_CELL_COLUMN = 21;
    private static final int T2_GROUP_RECORD_CELL_ROW = 62;
    private static final int T2_GROUP_RECORD_CELL_COLUMN = 6;
    private static final int T2_CATEGORY_STOCK_CELL_ROW = 63;
    private static final int T2_CATEGORY_STOCK_CELL_COLUMN = 7;
    private static final int T2_CONSIST_CELL_ROW = 64;
    private static final int T2_CONSIST_CELL_COLUMN = 5;
    private static final int T2_MILITARY_RANK_CELL_ROW = 65;
    private static final int T2_MILITARY_RANK_CELL_COLUMN = 7;
    private static final int T2_TICKET_NUMBER_CELL_ROW = 63;
    private static final int T2_TICKET_NUMBER_CELL_COLUMN = 19;
    private static final int T2_MILITARY_AVAILABLE_CELL_ROW = 64;
    private static final int T2_MILITARY_AVAILABLE_CELL_COLUMN = 26;
    private static final int T2_RECRUITMENT_OFFICE_CELL_ROW = 66;
    private static final int T2_RECRUITMENT_OFFICE_CELL_COLUMN  = 24;
    private static final int T2_BE_REGISTERED_CELL_ROW = 67;
    private static final int T2_BE_REGISTERED_CELL_COLUMN  = 28;
    private static final int T2_DIPLOMA_SPECIALITY_NUMBER_CELL_ROW  = 45;
    private static final int T2_DIPLOMA_SPECIALITY_NUMBER_CELL_COLUMN = 2;
    private static final int T2_DIPLOMA_QUALIFICATION_FIRST_LINE_NUMBER_CELL_ROW = 52;
    private static final int T2_DIPLOMA_QUALIFICATION_FIRST_LINE_NUMBER_CELL_COLUMN = 7;
    private static final int T2_DIPLOMA_QUALIFICATION_SECOND_LINE_NUMBER_CELL_ROW = 52;
    private static final int T2_DIPLOMA_QUALIFICATION_SECOND_LINE_NUMBER_CELL_COLUMN = 7;
    private static final int T2_DIPLOMA_NUMBER_NUMBER_CELL_ROW  = 55;
    private static final int T2_DIPLOMA_NUMBER_NUMBER_CELL_COLUMN  = 7;
    private static final int T2_DIPLOMA_DATE_NUMBER_CELL_ROW  = 55;
    private static final int T2_DIPLOMA_DATE_NUMBER_CELL_COLUMN  = 11;
    private static final int T2_DIPLOMA_YEAR_NUMBER_CELL_ROW  = 55;
    private static final int T2_DIPLOMA_YEAR_NUMBER_CELL_COLUMN  = 14;
    private static final int T2_ASSIGNMENT_NUMBER_CELL_ROW  = 15;
    private static final int T2_ASSIGNMENT_NUMBER_CELL_COLUMN  = 18;
    private static final int T2_FAMILY_INFO_NUMBER_CELL_ROW  = 22;
    private static final int T2_FAMILY_INFO_NUMBER_CELL_COLUMN  = 18;

    private static final int T2_ASSIGNMENT_TABLE_ROW = 75;
    private static final int T2_ASSIGNMENT_CONTRACT_DATE_CELL_COLUMN = 2;
    private static final int T2_ASSIGNMENT_POSITION_CELL_COLUMN = 12;
    private static final int T2_ASSIGNMENT_RATE_CELL_COLUMN = 18;
    private static final int T2_ASSIGNMENT_ORDER_CELL_COLUMN = 24;

    private static final int T2_VACATIONS_TABLE_ROW = 144;
    private static final int T2_VACATION_TYPE_CELL_COLUMN = 2;
    private static final int T2_VACATION_PERIOD_CELL_COLUMN = 9;
    private static final int T2_VACATION_START_CELL_COLUMN = 15;
    private static final int T2_VACATION_END_CELL_COLUMN = 21;
    private static final int T2_VACATION_ORDER_CELL_COLUMN = 27;

    private static final int T2_TRAINING_TABLE_ROW = 214;
    private static final int T2_TRAINING_DATE_CELL_COLUMN = 2;
    private static final int T2_TRAINING_INSTITUTION_CELL_COLUMN = 6;
    private static final int T2_TRAINING_ORDER_CELL_COLUMN = 12;

    private static final int T2_RETRAINING_DATE_CELL_COLUMN = 17;
    private static final int T2_RETRAINING_STUDYING_TOPIC_CELL_COLUMN = 21;
    private static final int T2_RETRAINING_DIPLOMA_DATE_CELL_COLUMN = 27;
    private static final int T2_RETRAINING_DIPLOMA_NUMBER_CELL_COLUMN = 31;

    private static final int T2_ATTESTATION_TABLE_ROW = 242;
    private static final int T2_ATTESTATION_DATE_CELL_COLUMN = 2;
    private static final int T2_ATTESTATION_DECISION_CELL_COLUMN = 6;

    private static final int T2_ADDITIONAL_INFO_TABLE_ROW = 256;
    private static final int T2_ADDITIONAL_INFO_CELL_COLUMN = 2;

    private static final int T2_CONTRACT_TABLE_ROW = 119;
    private static final int T2_CONTRACT_START_DATE_CELL_COLUMN = 2;
    private static final int T2_CONTRACT_DOC_NUMBER_CELL_COLUMN = 22;

    private SimpleDateFormat simpleDateFormat;
    private Locale locale;
    private String path;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private Test test;

    public File exportT2(PersonCardEntity personCardEntity, Locale locale, ServletContext servletContext){

        this.locale = locale;
        simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        File personT2 = null;
        path = servletContext.getRealPath("WEB-INF/classes/documents") + "/";
        int lastRecord;
        int lastSpace;

        try {

            FileInputStream file = new FileInputStream(new File(path + "T-2.xls"));

            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheetAt(0);
            HSSFRow row;
            HSSFCellStyle style = workbook.createCellStyle();
            HSSFFont font = workbook.createFont();
            font.setFontHeightInPoints((short) 7);
            font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

            HSSFCellStyle styleBorder = workbook.createCellStyle();
            styleBorder.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleBorder.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleBorder.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleBorder.setBorderLeft(HSSFCellStyle.BORDER_THIN);
            styleBorder.setAlignment(CellStyle.ALIGN_CENTER);
            styleBorder.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
            styleBorder.setWrapText(true);
            styleBorder.setFont(font);
            styleBorder.setLocked(false);

            style.setAlignment(CellStyle.ALIGN_LEFT);
            style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            style.setFont(font);
            style.setLocked(false);

            HSSFCellStyle styleWithoutBorder = workbook.createCellStyle();
            styleWithoutBorder.setBorderBottom(HSSFCellStyle.BORDER_NONE);
            styleWithoutBorder.setFont(font);
            styleWithoutBorder.setLocked(false);

            Cell gender = sheet.getRow(T2_GENDER_CELL_ROW).getCell(T2_GENDER_CELL_COLUMN);
            gender.setCellStyle(styleBorder);
            gender.setCellValue(getMessage(personCardEntity.getGender()));

            Cell alphabet = sheet.getRow(T2_ALPHABET_CELL_ROW).getCell(T2_ALPHABET_CELL_COLUMN);
            alphabet.setCellStyle(styleBorder);
            alphabet.setCellValue(checkValue(personCardEntity.getLastName().charAt(0)).toUpperCase());

            Cell cardNumber = sheet.getRow(T2_CARD_NUMBER_CELL_ROW).getCell(T2_CARD_NUMBER_CELL_COLUMN);
            cardNumber.setCellStyle(style);
            cardNumber.setCellValue(checkValue(personCardEntity.getCardNumber()));

            Cell lastName = sheet.getRow(T2_LAST_NAME_CELL_ROW).getCell(T2_LAST_NAME_CELL_COLUMN);
            lastName.setCellStyle(style);
            lastName.setCellValue(checkValue(personCardEntity.getLastName()));

            Cell firstName = sheet.getRow(T2_FIRST_NAME_CELL_ROW).getCell(T2_FIRST_NAME_CELL_COLUMN);
            firstName.setCellStyle(style);
            firstName.setCellValue(checkValue(personCardEntity.getFirstName()));

            Cell patronymic = sheet.getRow(T2_PATRONYMIC_CELL_ROW).getCell(T2_PATRONYMIC_CELL_COLUMN);
            patronymic.setCellStyle(style);
            patronymic.setCellValue(checkValue(personCardEntity.getPatronymic()));

            Date birthday = personCardEntity.getBirthday();
            if (birthday != null) {
                Cell birthYear = sheet.getRow(T2_BIRTH_YEAR_CELL_ROW).getCell(T2_BIRTH_YEAR_CELL_COLUMN);
                birthYear.setCellStyle(style);
                birthYear.setCellValue(new SimpleDateFormat("yyyy").format(birthday));

                Cell birthDay = sheet.getRow(T2_BIRTH_DAY_CELL_ROW).getCell(T2_BIRTH_DAY_CELL_COLUMN);
                birthDay.setCellStyle(style);
                birthDay.setCellValue(new SimpleDateFormat("dd").format(birthday));

                Cell birthMonth = sheet.getRow(T2_BIRTH_MONTH_CELL_ROW).getCell(T2_BIRTH_MONTH_CELL_COLUMN);
                birthMonth.setCellStyle(style);
                birthMonth.setCellValue(new SimpleDateFormat("MM").format(birthday));
            }

            Cell birthPlace = sheet.getRow(T2_BIRTH_PLACE_CELL_ROW).getCell(T2_BIRTH_PLACE_CELL_COLUMN);
            birthPlace.setCellStyle(style);
            birthPlace.setCellValue(getMessage(personCardEntity.getBirthplaceCountry()));

            Cell nationality = sheet.getRow(T2_NATIONALITY_CELL_ROW).getCell(T2_NATIONALITY_CELL_COLUMN);
            nationality.setCellStyle(style);
            nationality.setCellValue(getMessage(personCardEntity.getNationality()));

            Cell educationDegree = sheet.getRow(T2_EDUCATION_DEGREE_CELL_ROW).getCell(T2_EDUCATION_DEGREE_CELL_COLUMN);
            educationDegree.setCellStyle(style);
            educationDegree.setCellValue(getMessage(personCardEntity.getEducationDegree()));

            Cell institutionFirstLine = sheet.getRow(T2_INSTITUTION_FIRST_LINE_CELL_ROW).getCell(T2_INSTITUTION_FIRST_LINE_CELL_COLUMN);
            institutionFirstLine.setCellStyle(style);

            Cell institutionSecondLine = sheet.getRow(T2_INSTITUTION_SECOND_LINE_CELL_ROW).getCell(T2_INSTITUTION_SECOND_LINE_CELL_COLUMN);
            institutionSecondLine.setCellStyle(style);

            String institutionSplitter = checkValue(personCardEntity.getInstitution()) + " " + getDate(personCardEntity.getStudyingEndDate());
            if (institutionSplitter.length() < 50) {
                institutionFirstLine.setCellValue(institutionSplitter);
            } else {
                lastSpace = institutionSplitter.substring(0, 50).lastIndexOf(" ");
                institutionFirstLine.setCellValue(institutionSplitter.substring(0, lastSpace));
                institutionSecondLine.setCellValue(institutionSplitter.substring(lastSpace, institutionSplitter.length()));

            }

            Cell learningForm = sheet.getRow(T2_LEARNING_FORM_CELL_ROW).getCell(T2_LEARNING_FORM_CELL_COLUMN);
            learningForm.setCellStyle(style);
            learningForm.setCellValue(getMessage(personCardEntity.getLearningForm()));

            Cell familyStatus = sheet.getRow(T2_FAMILY_STATUS_CELL_ROW).getCell(T2_FAMILY_STATUS_CELL_COLUMN);
            familyStatus.setCellStyle(style);
            familyStatus.setCellValue(getMessage(personCardEntity.getFamilyStatus()));

            Cell passport = sheet.getRow(T2_PASSPORT_CELL_ROW).getCell(T2_PASSPORT_CELL_COLUMN);
            passport.setCellStyle(style);
            passport.setCellValue(checkValue(personCardEntity.getDocumentSeries()) + " " + checkValue(personCardEntity.getDocumentSerialNumber()));

            Cell documentIssuedBy = sheet.getRow(T2_DOCUMENT_ISSUED_BY_CELL_ROW).getCell(T2_DOCUMENT_ISSUED_BY_CELL_COLUMN);
            documentIssuedBy.setCellStyle(style);
            documentIssuedBy.setCellValue(checkValue(personCardEntity.getDocumentIssuedBy()));

            Cell documentDateIssuance = sheet.getRow(T2_DOCUMENT_DATE_ISSUANCE_CELL_ROW).getCell(T2_DOCUMENT_DATE_ISSUANCE_CELL_COLUMN);
            documentDateIssuance.setCellStyle(style);
            documentDateIssuance.setCellValue(getDate(personCardEntity.getDocumentDateIssuance()));

            Cell documentValidityPeriod = sheet.getRow(T2_DOCUMENT_VALIDITY_PERIOD_CELL_ROW).getCell(T2_DOCUMENT_VALIDITY_PERIOD_CELL_COLUMN);
            documentValidityPeriod.setCellStyle(style);
            documentValidityPeriod.setCellValue(getDate(personCardEntity.getDocumentValidityPeriod()));

            Cell documentIndividualNumber = sheet.getRow(T2_DOCUMENT_INDIVIDUAL_NUMBER_CELL_ROW).getCell(T2_DOCUMENT_INDIVIDUAL_NUMBER_CELL_COLUMN);
            documentIndividualNumber.setCellStyle(style);
            documentIndividualNumber.setCellValue(checkValue(personCardEntity.getDocumentIndividualNumber()));

            Cell insuranceCertificateNumber = sheet.getRow(T2_INSURANCE_CERTIFICATE_NUMBER_CELL_ROW).getCell(T2_INSURANCE_CERTIFICATE_NUMBER_CELL_COLUMN);
            insuranceCertificateNumber.setCellStyle(style);
            insuranceCertificateNumber.setCellValue(checkValue(personCardEntity.getInsuranceCertificateNumber()));

            lastRecord = personCardEntity.getEducation().size();
            if (lastRecord > 0) {
                EducationEntity education = personCardEntity.getEducation().get(lastRecord - 1); // GET LAST EDUCATION
                if (education != null) {

                    Cell diplomaSpecialityFirstLine = sheet.getRow(T2_DIPLOMA_SPECIALITY_NUMBER_CELL_ROW).getCell(T2_DIPLOMA_SPECIALITY_NUMBER_CELL_COLUMN);
                    diplomaSpecialityFirstLine.setCellStyle(style);

                    Cell diplomaSpecialitySecondLine = sheet.getRow(T2_DIPLOMA_SPECIALITY_NUMBER_CELL_ROW + 2).getCell(T2_DIPLOMA_SPECIALITY_NUMBER_CELL_COLUMN);
                    diplomaSpecialitySecondLine.setCellStyle(style);

                    String specialitySplitter = checkValue(education.getSpecialty());
                    if (specialitySplitter.length() < 50) {
                        diplomaSpecialityFirstLine.setCellValue(specialitySplitter);
                    } else {
                        lastSpace = specialitySplitter.substring(0, 50).lastIndexOf(" ");
                        diplomaSpecialityFirstLine.setCellValue(specialitySplitter.substring(0, lastSpace));
                        diplomaSpecialitySecondLine.setCellValue(specialitySplitter.substring(lastSpace, specialitySplitter.length()));

                    }

                    Cell diplomaQualificationFirstLine = sheet.getRow(T2_DIPLOMA_QUALIFICATION_FIRST_LINE_NUMBER_CELL_ROW).getCell(T2_DIPLOMA_QUALIFICATION_FIRST_LINE_NUMBER_CELL_COLUMN);
                    diplomaQualificationFirstLine.setCellStyle(style);

                    Cell diplomaQualificationSecondLine = sheet.getRow(T2_DIPLOMA_QUALIFICATION_SECOND_LINE_NUMBER_CELL_ROW).getCell(T2_DIPLOMA_QUALIFICATION_SECOND_LINE_NUMBER_CELL_COLUMN);
                    diplomaQualificationSecondLine.setCellStyle(style);

                    String qualificationSplitter = checkValue(education.getQualification());
                    if (qualificationSplitter.length() < 35) {
                        diplomaQualificationFirstLine.setCellValue(qualificationSplitter);
                    } else {
                        lastSpace = qualificationSplitter.substring(0, 35).lastIndexOf(" ");
                        diplomaQualificationFirstLine.setCellValue(qualificationSplitter.substring(0, lastSpace));
                        diplomaQualificationSecondLine.setCellValue(qualificationSplitter.substring(lastSpace, specialitySplitter.length()));

                    }

                    Cell diplomaNumber = sheet.getRow(T2_DIPLOMA_NUMBER_NUMBER_CELL_ROW).getCell(T2_DIPLOMA_NUMBER_NUMBER_CELL_COLUMN);
                    diplomaNumber.setCellStyle(style);
                    diplomaNumber.setCellValue(checkValue(education.getDiplomaNumber()));

                    Date dipDate = education.getDiplomaDate();
                    if (dipDate != null) {

                        Cell diplomaDate = sheet.getRow(T2_DIPLOMA_DATE_NUMBER_CELL_ROW).getCell(T2_DIPLOMA_DATE_NUMBER_CELL_COLUMN);
                        diplomaDate.setCellStyle(style);
                        diplomaDate.setCellValue(new SimpleDateFormat("dd.MM").format(dipDate));

                        Cell diplomaDateYear = sheet.getRow(T2_DIPLOMA_YEAR_NUMBER_CELL_ROW).getCell(T2_DIPLOMA_YEAR_NUMBER_CELL_COLUMN);
                        diplomaDateYear.setCellStyle(style);
                        diplomaDateYear.setCellValue(new SimpleDateFormat("yyyy").format(dipDate));
                    }
                }
            }

            List<FamilyMemberEntity> familyMemberEntities = personCardEntity.getFamilyMembers();
            if (familyMemberEntities != null){
                int column = 0;
                for (int i = 0; i < familyMemberEntities.size(); i++){
                    FamilyMemberEntity familyMember = familyMemberEntities.get(i);
                    if (i > 2) {
                        column = 1;
                    }
                    Cell familyInfo = sheet.getRow(T2_FAMILY_INFO_NUMBER_CELL_ROW + i*2).getCell(T2_FAMILY_INFO_NUMBER_CELL_COLUMN + column);
                    familyInfo.setCellStyle(style);
                    familyInfo.setCellValue(getMessage(familyMember.getKinship()) + " " + checkValue(familyMember.getName()) + " " + getDate(familyMember.getBirthday()));
                }
            }

            Cell lastAssignmentFirstLine = sheet.getRow(T2_ASSIGNMENT_NUMBER_CELL_ROW).getCell(T2_ASSIGNMENT_NUMBER_CELL_COLUMN);
            lastAssignmentFirstLine.setCellStyle(style);

            Cell lastAssignmentSecondLine = sheet.getRow(T2_ASSIGNMENT_NUMBER_CELL_ROW + 1).getCell(T2_ASSIGNMENT_NUMBER_CELL_COLUMN);
            lastAssignmentSecondLine.setCellStyle(style);

            if (personCardEntity.getLastAssignment() != null) {
                String lastAssignmentSplitter = getMessage(personCardEntity.getLastAssignment().getPosition());
                if (lastAssignmentSplitter.length() < 50) {
                    lastAssignmentFirstLine.setCellValue(lastAssignmentSplitter);
                } else {
                    lastSpace = lastAssignmentSplitter.substring(0, 50).lastIndexOf(" ");
                    lastAssignmentFirstLine.setCellValue(lastAssignmentSplitter.substring(0, lastSpace));
                    lastAssignmentSecondLine.setCellValue(lastAssignmentSplitter.substring(lastSpace, lastAssignmentSplitter.length()));

                }

            }
            String addressSplitter = "";


            AddressEntity residentialAddress = personCardEntity.getResidentialAddress();
            if (residentialAddress != null) {

                addressSplitter = getAddress(residentialAddress, "");
            }

            AddressEntity registrationAddress = personCardEntity.getRegistrationAddress();
            if (registrationAddress != null) {
                addressSplitter +=  " " + getAddress(registrationAddress, messageSource.getMessage("short.registration", null, locale));
            }

            Cell homeAddressFirst = sheet.getRow(T2_ADDRESS_FIRST_CELL_ROW).getCell(T2_ADDRESS_FIRST_CELL_COLUMN);
            homeAddressFirst.setCellStyle(style);

            if (addressSplitter.length() < 40) {
                homeAddressFirst.setCellValue(addressSplitter);
            } else {
                lastSpace = addressSplitter.substring(0, 40).lastIndexOf(" ");
                homeAddressFirst.setCellValue(addressSplitter.substring(0, lastSpace));

                int rowCount = ((addressSplitter.length()%60)>0) ? (addressSplitter.length()/60)+1 : (addressSplitter.length()/60);
                int lastSpaceSecond;

                for (int i = 0; i < rowCount; i++) {
                    Cell homeAddressSecond = sheet.getRow(T2_ADDRESS_SECOND_CELL_ROW + i).getCell(T2_ADDRESS_SECOND_CELL_COLUMN);
                    if((i%2) > 0) {
                        homeAddressSecond.setCellStyle(style);
                    } else {
                        homeAddressSecond.setCellStyle(styleWithoutBorder);
                    }

                    if (i == rowCount-1) {
                        homeAddressSecond.setCellValue(addressSplitter.substring(lastSpace, addressSplitter.length()));
                    } else {
                        lastSpaceSecond = addressSplitter.substring(lastSpace, lastSpace + 60).lastIndexOf(" ");
                        homeAddressSecond.setCellValue(addressSplitter.substring(lastSpace, lastSpace + lastSpaceSecond));
                        lastSpace += lastSpaceSecond;
                    }
                }
            }

            Cell phoneNumber = sheet.getRow(T2_PHONE_NUMBER_CELL_ROW).getCell(T2_PHONE_NUMBER_CELL_COLUMN);
            phoneNumber.setCellStyle(style);
            phoneNumber.setCellValue(getPhones(residentialAddress));

            MilitaryEntity reservist = personCardEntity.getReservist();
            if (reservist != null) {

                Cell groupRecord = sheet.getRow(T2_GROUP_RECORD_CELL_ROW).getCell(T2_GROUP_RECORD_CELL_COLUMN);
                groupRecord.setCellStyle(style);
                groupRecord.setCellValue(getMessage(reservist.getGroupRecord()));

                Cell categoryStock = sheet.getRow(T2_CATEGORY_STOCK_CELL_ROW).getCell(T2_CATEGORY_STOCK_CELL_COLUMN);
                categoryStock.setCellStyle(style);
                categoryStock.setCellValue(getMessage(reservist.getCategoryStock()));

                Cell consist = sheet.getRow(T2_CONSIST_CELL_ROW).getCell(T2_CONSIST_CELL_COLUMN);
                consist.setCellStyle(style);
                consist.setCellValue(getMessage(reservist.getConsist()));

                Cell militaryRank = sheet.getRow(T2_MILITARY_RANK_CELL_ROW).getCell(T2_MILITARY_RANK_CELL_COLUMN);
                militaryRank.setCellStyle(style);
                militaryRank.setCellValue(getMessage(reservist.getMilitaryRank()));

                Cell ticketNumber = sheet.getRow(T2_TICKET_NUMBER_CELL_ROW).getCell(T2_TICKET_NUMBER_CELL_COLUMN);
                ticketNumber.setCellStyle(style);
                ticketNumber.setCellValue(checkValue(reservist.getTicketNumber()));

                Cell militaryAvailable = sheet.getRow(T2_MILITARY_AVAILABLE_CELL_ROW).getCell(T2_MILITARY_AVAILABLE_CELL_COLUMN);
                militaryAvailable.setCellStyle(style);
                militaryAvailable.setCellValue(getMessage(reservist.getMilitaryAvailable()));

                Cell recruitmentOffice = sheet.getRow(T2_RECRUITMENT_OFFICE_CELL_ROW).getCell(T2_RECRUITMENT_OFFICE_CELL_COLUMN);
                recruitmentOffice.setCellStyle(style);
                recruitmentOffice.setCellValue(checkValue(reservist.getRecruitmentOffice()));

                Cell beRegistered = sheet.getRow(T2_BE_REGISTERED_CELL_ROW).getCell(T2_BE_REGISTERED_CELL_COLUMN);
                beRegistered.setCellStyle(style);
                beRegistered.setCellValue(checkValue(reservist.getBeRegistered()));

            }

// ASSIGNMENT TABLE
            List<AssignmentEntity> assignments = personCardEntity.getAssignments();
            if (assignments != null) {
                for (int i = 0; i < assignments.size(); i++) {

                    AssignmentEntity assignment = assignments.get(i);

                    row = sheet.getRow(T2_ASSIGNMENT_TABLE_ROW + i * 2);

                    Cell contractDate = row.getCell(T2_ASSIGNMENT_CONTRACT_DATE_CELL_COLUMN);
                    contractDate.setCellStyle(styleBorder);
                    contractDate.setCellValue(getDate(assignment.getContractDate()));

                    Cell position = row.getCell(T2_ASSIGNMENT_POSITION_CELL_COLUMN);
                    position.setCellStyle(styleBorder);
                    position.setCellValue(getMessage(assignment.getPosition()));

                    Cell rate = row.getCell(T2_ASSIGNMENT_RATE_CELL_COLUMN);
                    rate.setCellStyle(styleBorder);
                    rate.setCellValue(checkValue(assignment.getRate()));

                    Cell orderNumber = row.getCell(T2_ASSIGNMENT_ORDER_CELL_COLUMN);
                    orderNumber.setCellStyle(styleBorder);
                    orderNumber.setCellValue(checkValue(assignment. getOrderNumber()));

                    row.setHeight((short)500);

                }
            }
// END ASSIGNMENT
// VACATION TABLE
            List<VacationEntity> vacations = personCardEntity.getVacations();
            if (vacations != null) {
                for (int i = 0; i < vacations.size(); i++) {

                    VacationEntity vacation = vacations.get(i);

                    row = sheet.getRow(T2_VACATIONS_TABLE_ROW + i * 2);

                    Cell position = row.getCell(T2_VACATION_TYPE_CELL_COLUMN);
                    position.setCellStyle(styleBorder);
                    position.setCellValue(getMessage(vacation.getVacationType()));

                    Cell period = row.getCell(T2_VACATION_PERIOD_CELL_COLUMN);
                    period.setCellStyle(styleBorder);
                    period.setCellValue(getDate(vacation.getPeriodFrom()) + "-" + getDate(vacation.getPeriodTo()));

                    Cell vacationStartDate = row.getCell(T2_VACATION_START_CELL_COLUMN);
                    vacationStartDate.setCellStyle(styleBorder);
                    vacationStartDate.setCellValue(getDate(vacation.getVacationFrom()));

                    Cell vacationEndDate = row.getCell(T2_VACATION_END_CELL_COLUMN);
                    vacationEndDate.setCellStyle(styleBorder);
                    vacationEndDate.setCellValue(getDate(vacation.getVacationTo()));

                    Cell orderNumber = row.getCell(T2_VACATION_ORDER_CELL_COLUMN);
                    orderNumber.setCellStyle(styleBorder);
                    orderNumber.setCellValue(checkValue(vacation.getOrderNumber()));

                }
            }
// END VACATION
// TRAINING TABLE
            List<TrainingEntity> trainings = personCardEntity.getTraining();
            if (trainings != null) {
                for (int i = 0; i < trainings.size(); i++) {

                    TrainingEntity training = trainings.get(i);

                    row = sheet.getRow(T2_TRAINING_TABLE_ROW + i * 2);
                    row.setHeight((short)500);

                    Cell trainingDate = row.getCell(T2_TRAINING_DATE_CELL_COLUMN);
                    trainingDate.setCellStyle(styleBorder);
                    trainingDate.setCellValue(getDate(training.getEndDate()));

                    Cell trainingInstitution = row.getCell(T2_TRAINING_INSTITUTION_CELL_COLUMN);
                    trainingInstitution.setCellStyle(styleBorder);
                    trainingInstitution.setCellValue(checkValue(training.getInstitution()));

                    Cell orderNumber = row.getCell(T2_TRAINING_ORDER_CELL_COLUMN);
                    orderNumber.setCellStyle(styleBorder);
                    orderNumber.setCellValue(checkValue(training.getDocumentNumber()));

                }
            }
// END TRAINING
// RETRAINING TABLE
            List<RetrainingEntity> reTrainings = personCardEntity.getRetraining();
            if (reTrainings != null) {
                for (int i = 0; i < reTrainings.size(); i++) {

                    RetrainingEntity reTraining = reTrainings.get(i);

                    row = sheet.getRow(T2_TRAINING_TABLE_ROW + i * 2);
                    row.setHeight((short)500);

                    Cell reTrainingDate = row.getCell(T2_RETRAINING_DATE_CELL_COLUMN);
                    reTrainingDate.setCellStyle(styleBorder);
                    reTrainingDate.setCellValue(getDate(reTraining.getEndDate()));

                    Cell reTrainingStudyingTopic = row.getCell(T2_RETRAINING_STUDYING_TOPIC_CELL_COLUMN);
                    reTrainingStudyingTopic.setCellStyle(styleBorder);
                    reTrainingStudyingTopic.setCellValue(checkValue(reTraining.getStudyingTopic()));

                    Cell reTrainingDiplomaDate = row.getCell(T2_RETRAINING_DIPLOMA_DATE_CELL_COLUMN);
                    reTrainingDiplomaDate.setCellStyle(styleBorder);
                    reTrainingDiplomaDate.setCellValue(getDate(reTraining.getDocumentDate()));

                    Cell reTrainingDiplomaNumber = row.getCell(T2_RETRAINING_DIPLOMA_NUMBER_CELL_COLUMN);
                    reTrainingDiplomaNumber.setCellStyle(styleBorder);
                    reTrainingDiplomaNumber.setCellValue(checkValue(reTraining.getDocumentNumber()));

                }
            }
// END RETRAINING
// ATTESTATION TABLE
            List<AttestationEntity> attestations = personCardEntity.getAttestations();
            if (attestations != null) {
                for (int i = 0; i < attestations.size(); i++) {

                    AttestationEntity attestation = attestations.get(i);

                    Cell attestationDate = sheet.getRow(T2_ATTESTATION_TABLE_ROW + i * 2).getCell(T2_ATTESTATION_DATE_CELL_COLUMN);
                    attestationDate.setCellStyle(styleBorder);
                    attestationDate.setCellValue(getDate(attestation.getDate()));

                    Cell attestationDecision = sheet.getRow(T2_ATTESTATION_TABLE_ROW + i * 2).getCell(T2_ATTESTATION_DECISION_CELL_COLUMN);
                    attestationDecision.setCellStyle(styleBorder);
                    attestationDecision.setCellValue(checkValue(attestation.getDecision()));

                }
            }
// END ATTESTATION
// ADDITIONAL_INFO TABLE
            List<AdditionalInfoEntity> additionals = personCardEntity.getAdditionals();
            if (additionals != null) {
                for (int i = 0; i < additionals.size(); i++) {

                    AdditionalInfoEntity additional = additionals.get(i);

                    Cell additionalInfo = sheet.getRow(T2_ADDITIONAL_INFO_TABLE_ROW + i * 2).getCell(T2_ADDITIONAL_INFO_CELL_COLUMN);
                    additionalInfo.setCellStyle(styleBorder);
                    additionalInfo.setCellValue(getDate(additional.getEventDate()) + " - " + checkValue(additional.getEventName()));

                }
            }
// END ADDITIONAL_INFO
// CONTRACTS TABLE
            List<DocumentEntity> documents = personCardEntity.getDocuments();
            if (documents != null) {
                int contractCounter = 0;
                for (int i = 0; i < documents.size(); i++) {

                    DocumentEntity document = documents.get(i);

                    if (("documents.paperType.contract").equals(document.getPaperType().getValue())) {

                        Cell documentStart = sheet.getRow(T2_CONTRACT_TABLE_ROW + contractCounter * 2).getCell(T2_CONTRACT_START_DATE_CELL_COLUMN);
                        documentStart.setCellStyle(styleBorder);
                        documentStart.setCellValue(getDate(document.getPaperDate()));

                        Cell documentNumber = sheet.getRow(T2_CONTRACT_TABLE_ROW + contractCounter * 2).getCell(T2_CONTRACT_DOC_NUMBER_CELL_COLUMN);
                        documentNumber.setCellStyle(styleBorder);
                        documentNumber.setCellValue(checkValue(document.getPaperNumber()));

                        contractCounter++;
                    }
                }
            }
// END CONTRACTS

            file.close();

            String fileName;
            if (personCardEntity.getLatin() != null && personCardEntity.getLatin().length() > 0) {
                fileName = personCardEntity.getLatin();
            } else {
                fileName = Translit.toTranslit(personCardEntity.getShortName());
            }
            personT2 = new File(path + fileName + ".xls");
            FileOutputStream outFile = new FileOutputStream(personT2);
            workbook.write(outFile);
            outFile.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return personT2;
    }

    private String getPhones(AddressEntity address) { //test
        String phoneCell = "";
        String phone = checkValue(address.getPhone());
        String mobilePhone = checkValue(address.getMobilePhone());

        phoneCell += (phone.length() > 0) ?
                messageSource.getMessage("short.phone", null, locale) + phone
                : phone;
        phoneCell += (mobilePhone.length() > 0) ?
                "  " + messageSource.getMessage("short.phone.mobile", null, locale) + mobilePhone
                : mobilePhone;

        return phoneCell;
    }


    private String getAddress(AddressEntity addressEntity, String registration) {

        String address = (registration.length() > 0) ? registration + ": " : "";

        address += (addressEntity.getPostcode().length() > 0) ? addressEntity.getPostcode() + " " : "";
        address += (addressEntity.getProvince().length() > 0) ? messageSource.getMessage("short.province", null, locale) + addressEntity.getProvince() + " " : "";
        address += (addressEntity.getRegion().length() > 0) ? messageSource.getMessage("short.region", null, locale) + addressEntity.getRegion() + " " : "";
        address += (addressEntity.getLocality().length() > 0) ? messageSource.getMessage("short.city", null, locale) + addressEntity.getLocality() + " " : "";
        address += (addressEntity.getRuralCommittee().length() > 0) ? messageSource.getMessage("short.committee", null, locale) + addressEntity.getRuralCommittee() + " " : "";
        address += (addressEntity.getStreet().length() > 0) ? messageSource.getMessage("short.street", null, locale) + addressEntity.getStreet() + " " : "";
        address += (addressEntity.getHouse() != null) ? messageSource.getMessage("short.house", null, locale)  + addressEntity.getHouse() + " " : "";
        address += (addressEntity.getBuilding().length() > 0) ? messageSource.getMessage("short.building", null, locale) + addressEntity.getBuilding() + " " : "";
        address += (addressEntity.getApartment() != null) ? messageSource.getMessage("short.apartment", null, locale) + addressEntity.getApartment() : "";

        return address;
    }

    private String getDate(Date date) {
        return (date == null) ? "" : simpleDateFormat.format(date) + messageSource.getMessage("short.year", null, locale);
    }

    private String getMessage(BaseEnum e) {
        return (e == null) ? "" : messageSource.getMessage(e.getValue(), null, locale);
    }

    private String checkValue(Object value){
        return (value == null) ? "" : value.toString();
    }

    @Override
    public File exportReportMonth(ReportEntity reportEntity, ServletContext servletContext, Locale locale) {

        this.locale = locale;
        simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        path = servletContext.getRealPath("WEB-INF/classes/documents") + "/";

        File personT2 = new File(path + "month.xls");

        int year = Integer.parseInt(reportEntity.getSelectedYear());
        int month = reportEntity.getSelectedMonth().ordinal();

        try {

            FileInputStream file = new FileInputStream(personT2);

            HSSFWorkbook workbook = new HSSFWorkbook(file);
            workbook.setSheetName(0, String.valueOf(year));
            HSSFSheet sheet = workbook.getSheetAt(0);

            HSSFCellStyle styleCenter = workbook.createCellStyle();
            HSSFCellStyle styleRight = workbook.createCellStyle();
            HSSFCellStyle styleLeft = workbook.createCellStyle();
            HSSFCellStyle topStyle = workbook.createCellStyle();
            HSSFCellStyle bottomRightStyle = workbook.createCellStyle();
            HSSFCellStyle bottomLeftStyle = workbook.createCellStyle();
            HSSFCellStyle bottomCenterStyle = workbook.createCellStyle();

            HSSFFont boldFont = workbook.createFont();
            boldFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

            styleCenter.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleCenter.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleCenter.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleCenter.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            styleRight.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleRight.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleRight.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            styleRight.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            styleLeft.setBorderBottom(HSSFCellStyle.BORDER_THIN);
            styleLeft.setBorderTop(HSSFCellStyle.BORDER_THIN);
            styleLeft.setBorderRight(HSSFCellStyle.BORDER_THIN);
            styleLeft.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);

            bottomRightStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            bottomRightStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            bottomRightStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            bottomRightStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            bottomLeftStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            bottomLeftStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            bottomLeftStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            bottomLeftStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);

            bottomCenterStyle.setBorderBottom(HSSFCellStyle.BORDER_MEDIUM);
            bottomCenterStyle.setBorderTop(HSSFCellStyle.BORDER_THIN);
            bottomCenterStyle.setBorderRight(HSSFCellStyle.BORDER_THIN);
            bottomCenterStyle.setBorderLeft(HSSFCellStyle.BORDER_THIN);

            topStyle.setBorderTop(HSSFCellStyle.BORDER_MEDIUM);
            topStyle.setBorderRight(HSSFCellStyle.BORDER_MEDIUM);
            topStyle.setBorderLeft(HSSFCellStyle.BORDER_MEDIUM);
            topStyle.setAlignment(CellStyle.ALIGN_CENTER);
            topStyle.setFont(boldFont);

            int startYearWorkers = test.get_report_active_card_count_up_to_date(year, 1);
            int startYearWoman = test.get_report_active_card_count_up_to_date_and_by_gender("FEMALE", year, 1);

            Cell startYear = sheet.getRow(1).createCell(1);
            startYear.setCellValue(year);

            Cell startYearWorkersCount = sheet.getRow(3).createCell(1);
            startYearWorkersCount.setCellStyle(styleCenter);
            startYearWorkersCount.setCellValue(startYearWorkers);

            Cell startYearWomanCount = sheet.getRow(5).createCell(1);
            startYearWomanCount.setCellStyle(styleCenter);
            startYearWomanCount.setCellValue(startYearWoman);

            int total = 0;
            int cellCount = 1;

            for (int i = 1; i <= month+1; i++) {

                for(int x = 1; x <= 3; x++) {
                    for (int y = 2; y <= 14; y++) {
                        Cell cell = sheet.getRow(y).createCell(x+cellCount);
                        if(y == 14) {
                            if (x == 1) {
                                cell.setCellStyle(bottomLeftStyle);
                            } else if (x == 2) {
                                cell.setCellStyle(bottomCenterStyle);
                            } else {
                                cell.setCellStyle(bottomRightStyle);
                            }
                        } else {
                            if (x == 1) {
                                cell.setCellStyle(styleLeft);
                            } else if (x == 2) {
                                cell.setCellStyle(styleCenter);
                            } else {
                                cell.setCellStyle(styleRight);
                            }
                        }
                    }
                }

                int activeCardCount = test.get_report_active_card_count_up_to_date(year, i + 1);
                total += activeCardCount;

                Cell monthName = sheet.getRow(2).getCell(i * 3 - 1);
                monthName.setCellStyle(topStyle);
                sheet.getRow(2).getCell(i * 3).setCellStyle(topStyle);
                sheet.getRow(2).getCell(i*3+1).setCellStyle(topStyle);
                monthName.setCellValue(messageSource.getMessage(MonthEnum.get(i - 1).getValue(), null, locale));

                Cell workersCount = sheet.getRow(3).getCell(1 + i * 3);
                workersCount.setCellValue(activeCardCount);

                Cell woman = sheet.getRow(5).getCell(1 + i * 3);
                woman.setCellValue(test.get_report_active_card_count_up_to_date_and_by_gender("FEMALE", year, i + 1));

                Cell womanLabel = sheet.getRow(6).getCell(i * 3 - 1);
                womanLabel.setCellValue(messageSource.getMessage("short.gender.woman", null, locale));

                Cell manLabel = sheet.getRow(6).getCell(i * 3);
                manLabel.setCellValue(messageSource.getMessage("short.gender.man", null, locale));

                Cell womanHire = sheet.getRow(7).getCell(i * 3 - 1);
                womanHire.setCellValue(test.get_active_count_for_specific_gender_year_month("FEMALE", year, i));

                Cell manHire = sheet.getRow(7).getCell(i * 3);
                manHire.setCellValue(test.get_active_count_for_specific_gender_year_month("MALE", year, i));

                Cell womanDismissed = sheet.getRow(8).getCell(i * 3 - 1);
                womanDismissed.setCellValue(test.get_dismissed_count_for_specific_gender_year_month("FEMALE", year, i));

                Cell manDismissed = sheet.getRow(8).getCell(i * 3);
                manDismissed.setCellValue(test.get_dismissed_count_for_specific_gender_year_month("MALE", year, i));

                Cell avgWorkersCount = sheet.getRow(14).getCell(1 + i * 3);
                avgWorkersCount.setCellValue(activeCardCount); // need to add formula

//                merge Cells
                sheet.addMergedRegion(new CellRangeAddress(2, 2, i*3-1, i*3+1)); // month
                sheet.addMergedRegion(new CellRangeAddress(3, 3, i*3-1, i*3)); // 2 cells before workers count
                sheet.addMergedRegion(new CellRangeAddress(4, 4, i*3-1, i*3)); // 2 cells between workers and woman row
                sheet.addMergedRegion(new CellRangeAddress(5, 5, i*3-1, i*3)); // 2 cells before woman count

                if (i == month+1){

                    Cell totalWorkers = sheet.getRow(14).createCell(2 + i*3);
                    totalWorkers.setCellValue(total);

                    Cell avgWorkers = sheet.createRow(15).createCell(2 + i*3);
                    avgWorkers.setCellValue((double)total/(double)(month+1));
                }

                cellCount += 3;

            }


            file.close();

            personT2 = new File(path + reportEntity.getSelectedYear() + ".xls");
            FileOutputStream outFile = new FileOutputStream(personT2);
            workbook.write(outFile);
            outFile.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return personT2;
    }
}
