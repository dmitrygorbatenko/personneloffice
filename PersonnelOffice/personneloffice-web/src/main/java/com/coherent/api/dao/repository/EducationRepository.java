package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.EducationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EducationRepository extends JpaRepository<EducationEntity, Long> {
}
