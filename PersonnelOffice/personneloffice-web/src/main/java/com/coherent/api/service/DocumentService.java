package com.coherent.api.service;


import com.coherent.api.dao.entity.DocumentEntity;

import java.util.List;

public interface DocumentService {

    public DocumentEntity save(DocumentEntity entity);

    public DocumentEntity get(Long id);

    void delete(Long id);

    Long[] getCardIdList(Long document_id);

    List<DocumentEntity> getAll();
}
