package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.FileEntity;
import com.coherent.api.dao.repository.FileRepository;
import com.coherent.api.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("fileService")
public class FileServiceImpl implements FileService {
    @Autowired
    private FileRepository fileRepository;

    @Override
    public FileEntity get(Long id) {
        return fileRepository.findOne(id);
    }

    @Override
    public FileEntity save(FileEntity entity) {
        return fileRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        fileRepository.delete(id);
    }
}
