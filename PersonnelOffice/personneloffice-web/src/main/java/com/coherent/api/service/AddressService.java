package com.coherent.api.service;

import com.coherent.api.dao.entity.AddressEntity;

public interface AddressService {

    public AddressEntity save(AddressEntity entity);
}
