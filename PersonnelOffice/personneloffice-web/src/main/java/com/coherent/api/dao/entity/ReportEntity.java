package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.reports.MonthEnum;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "report")
public class ReportEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "report_id")
    private Long Id;

    @Column(name = "report_type")
    private String reportType;

    @Column(name = "last_using_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date lastUsingDate;

    @Column(name = "selected_year")
    private String selectedYear;

    @Column(name = "selected_month")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private MonthEnum selectedMonth;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public Date getLastUsingDate() {
        return lastUsingDate;
    }

    public void setLastUsingDate(Date lastUsingDate) {
        this.lastUsingDate = lastUsingDate;
    }

    public MonthEnum getSelectedMonth() {
        return selectedMonth;
    }

    public void setSelectedMonth(MonthEnum selectedMonth) {
        this.selectedMonth = selectedMonth;
    }

    public String getSelectedYear() {
        return selectedYear;
    }

    public void setSelectedYear(String selectedYear) {
        this.selectedYear = selectedYear;
    }
}
