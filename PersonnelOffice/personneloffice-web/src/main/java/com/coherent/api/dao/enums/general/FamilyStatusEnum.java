package com.coherent.api.dao.enums.general;


import com.coherent.api.dao.enums.BaseEnum;

public enum FamilyStatusEnum  implements BaseEnum {
    SINGLE_HE("family_status.single_he"),
    SINGLE_SHE("family_status.single_she"),
    MARRIED_HE("family_status.married_she"),
    MARRIED_SHE("family_status.married_he");

    private String value;

    private FamilyStatusEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
