package com.coherent.api.dao.enums.documents;


import com.coherent.api.dao.enums.BaseEnum;

public enum PaperTypeEnum implements BaseEnum {

    ORDER("documents.paperType.order"),
    NOTICE("documents.paperType.notice"),
    CONTRACT("documents.paperType.contract"),
    TREATY("documents.paperType.treaty"),
    REFERENCE("documents.paperType.reference");

    private final String value;

    PaperTypeEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
