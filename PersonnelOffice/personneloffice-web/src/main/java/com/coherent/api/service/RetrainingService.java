package com.coherent.api.service;

import com.coherent.api.dao.entity.RetrainingEntity;

public interface RetrainingService {

    public RetrainingEntity save(RetrainingEntity entity);

    public RetrainingEntity get(Long id);

    void delete(Long id);

}
