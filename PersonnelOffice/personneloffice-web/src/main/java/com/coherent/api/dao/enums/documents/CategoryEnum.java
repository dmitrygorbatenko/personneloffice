package com.coherent.api.dao.enums.documents;


import com.coherent.api.dao.enums.BaseEnum;

public enum CategoryEnum implements BaseEnum {

    K("documents.category.k"),
    L("documents.category.l");

    private final String value;

    CategoryEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
