package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.PersonCardDocumentEntity;
import com.coherent.api.dao.repository.PersonCardDocumentRepository;
import com.coherent.api.service.PersonCardDocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("personCardDocumentService")
public class PersonCardDocumentServiceImpl implements PersonCardDocumentService {

    @Autowired
    private PersonCardDocumentRepository personCardDocumentRepository;

    @Override
    public Long[] getAllCardIdsByDocumentId(Long document_id) {
        return personCardDocumentRepository.getAllCardIdsByDocumentId(document_id);
    }

    @Override
    public PersonCardDocumentEntity save(PersonCardDocumentEntity entity) {
        return personCardDocumentRepository.save(entity);
    }

    @Override
    public void delete(PersonCardDocumentEntity entity) {
        personCardDocumentRepository.delete(entity);
    }
}
