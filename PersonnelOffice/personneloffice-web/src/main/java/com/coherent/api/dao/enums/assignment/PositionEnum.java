package com.coherent.api.dao.enums.assignment;


import com.coherent.api.dao.enums.BaseEnum;

public enum PositionEnum implements BaseEnum {
    ADMINISTRATOR("assignment.position.administrator"),
    DATABASE_ADMINISTRATOR("assignment.position.database_administrator"),
    THE_SERVER_ADMINISTRATOR("assignment.position.the_server_administrator"),
    SYSTEM_ADMINISTRATOR("assignment.position.system_administrator"),
    LEAD_SYSTEM_ADMINISTRATOR("assignment.position.lead_system_administrator"),
    BUSINESS_ANALYST("assignment.position.business_analyst"),
    SENIOR_SOFTWARE_ENGINEER("assignment.position.senior_software_engineer"),
    LEAD_MARKETING_SPECIALIST("assignment.position.lead_marketing_specialist"),
    LEAD_HR_MANAGER("assignment.position.lead_hr_manager"),
    HEAD_TRAIN_CENTER("assignment.position.head_train_center"),
    LEADING_EXPERT_SOFTWARE_TESTING("assignment.position.a_leading_expert_on_software_testing"),
    CHIEF_ACCOUNTANT("assignment.position.chief_accountant"),
    SUB_CHIEF_ACCOUNTANT("assignment.position.sub_chief_accountant"),
    HEAD_OF_HOUSEHOLD("assignment.position.head_of_household"),
    SOFTWARE_ENGINEER("assignment.position.software_engineer"),
    INFORMATION_TECHNOLOGY_MANAGER("assignment.position.information_technology_manager"),
    HR_MANAGER("assignment.position.hr_manager"),
    HEAD_OF_THE_GENERAL_DEPARTMENT("assignment.position.head_of_the_general_department"),
    INTERPRETER("assignment.position.interpreter"),
    HR_SPECIALIST("assignment.position.hr_specialist"),
    MARKETING_SPECIALIST("assignment.position.marketing_specialist"),
    SPECIALIST_IN_PROMOTION_WEB_SITE("assignment.position.specialist_in_search_engine_promotion_web_site"),
    TESTER_SOFTWARE("assignment.position.tester_software"),
    SENIOR_TESTER_SOFTWARE("assignment.position.senior_tester_software"),
    CLEANER_FACILITIES("assignment.position.cleaner_facilities"),
    CLEANER("assignment.position.cleaner"),
    SYSTEM_ARCHITECT("assignment.position.system_architect"),
    BRAND_MANAGER("assignment.position.brand_manager"),
    SUB_GENERAL_DIRECTOR_PUBLIC_QUESTIONS("assignment.position.sub_general_director_public_questions"),
    HEAD_OF_MARKETING_DEPARTMENT("assignment.position.head_of_marketing.department"),
    SPECIALIST_IN_VED("assignment.position.specialist_in_ved"),
    SPECIALIST_IN_TESTING("assignment.position.specialist_in_testing"),
    SPECIALIST_IN_COMPUTER_GRAPHICS("assignment.position.specialist_in_computer_graphics"),
    GENERAL_DIRECTOR("assignment.position.general_director"),
    DIRECTOR_EXTERNAL_ECONOMIC("assignment.position.director_external_economic"),
    DIRECTOR_QUALITY("assignment.position.director_quality"),
    SUB_GENERAL_DIRECTOR("assignment.position.sub_general_director"),
    SUB_GENERAL_DIRECTOR_FINANCES_QUESTIONS("assignment.position.sub_general_director_finances_questions"),
    HEAD_HR_MANAGER("assignment.position.head_hr_manager"),
    REDACTOR_INTERNET_RESOURCES("assignment.position.redactor_internet_resources"),
    HEAD_TECHNICAL_TRAINING("assignment.position.head_technical_training"),
    SUB_GENERAL_DIRECTOR_INFORMATION_TECHNOLOGY("assignment.position.sub_general_director_information_technology"),
    TECHNICAL("assignment.position.technician"),
    ECONOMIST("assignment.position.economist");


    private String value;

    private PositionEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
