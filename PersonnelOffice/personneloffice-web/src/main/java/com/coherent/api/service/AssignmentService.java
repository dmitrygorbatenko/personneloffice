package com.coherent.api.service;

import com.coherent.api.dao.entity.AssignmentEntity;

public interface AssignmentService {

    public AssignmentEntity save(AssignmentEntity entity);

    public AssignmentEntity get(Long id);

    void delete(Long id);

}
