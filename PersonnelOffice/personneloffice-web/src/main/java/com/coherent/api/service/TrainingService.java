package com.coherent.api.service;


import com.coherent.api.dao.entity.TrainingEntity;

public interface TrainingService {

    public TrainingEntity save(TrainingEntity entity);

    public TrainingEntity get(Long id);

    void delete(Long id);

}
