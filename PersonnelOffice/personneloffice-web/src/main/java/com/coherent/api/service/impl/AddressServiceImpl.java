package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.AddressEntity;
import com.coherent.api.dao.repository.AddressRepository;
import com.coherent.api.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

@Service("addressService")
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressRepository addressRepository;

    @Override
    @Modifying
    public AddressEntity save(AddressEntity entity) {
        return addressRepository.save(entity);
    }
}
