package com.coherent.api.dao.enums.military;

import com.coherent.api.dao.enums.BaseEnum;

public enum CategoryStockEnum implements BaseEnum {
    CATEGORY_STOCK_FIRST("military.category.stock.first"), CATEGORY_STOCK_SECOND("military.category.stock.second");
    private String value;

    private CategoryStockEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
