package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.controller.jsonSerializer.EnumSerializer;
import com.coherent.api.dao.enums.assignment.FormEnum;
import com.coherent.api.dao.enums.assignment.PositionEnum;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "assignment")
public class AssignmentEntity {

    public AssignmentEntity() {}
    public AssignmentEntity(Long id, Long personCardId, String department, PositionEnum position, String rate,
                            String orderNumber, Date orderDate, FormEnum form, String contractNumber, Date contractDate,
                            Date contractStart, Date contractEnd)
    {
        this.id = id;
        this.personCardId = personCardId;
        this.department = department;
        this.position = position;
        this.rate = rate;
        this.orderNumber = orderNumber;
        this.orderDate = orderDate;
        this.form = form;
        this.contractNumber = contractNumber;
        this.contractDate = contractDate;
        this.contractStart = contractStart;
        this.contractEnd = contractEnd;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "assignment_id")
    private Long id;

    @Column(name = "person_card_id")
    private Long personCardId;
    
    @Column(name = "department")
    private String department;

    @Column(name = "position")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private PositionEnum position;

    @Column(name = "rate")
    private String rate;

    @Column(name = "order_number")
    private String orderNumber;

    @Column(name= "order_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date orderDate;

    @Column(name = "form")
    @Enumerated(EnumType.STRING)
    @JsonSerialize(using = EnumSerializer.class)
    private FormEnum form;

    @Column(name = "contract_number")
    private String contractNumber;

    @Column(name= "contract_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date contractDate;

    @Column(name= "contract_start")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date contractStart;

    @Column(name= "contract_end")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date contractEnd;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPersonCardId() {
        return personCardId;
    }

    public void setPersonCardId(Long personCardId) {
        this.personCardId = personCardId;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public PositionEnum getPosition() {
        return position;
    }

    public void setPosition(PositionEnum position) {
        this.position = position;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getRate() {
        return rate;
    }

    public FormEnum getForm() {
        return form;
    }

    public void setForm(FormEnum form) {
        this.form = form;
    }

    public Date getContractDate() {
        return contractDate;
    }

    public void setContractDate(Date contractDate) {
        this.contractDate = contractDate;
    }

    public Date getContractStart() {
        return contractStart;
    }

    public void setContractStart(Date contractStart) {
        this.contractStart = contractStart;
    }

    public Date getContractEnd() {
        return contractEnd;
    }

    public void setContractEnd(Date contractEnd) {
        this.contractEnd = contractEnd;
    }

    public String getContractNumber() {
        return contractNumber;
    }

    public void setContractNumber(String contractNumber) {
        this.contractNumber = contractNumber;
    }

}
