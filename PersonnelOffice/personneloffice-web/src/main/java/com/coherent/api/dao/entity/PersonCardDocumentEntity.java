package com.coherent.api.dao.entity;

import javax.persistence.*;

@IdClass(CardDocPK.class)
@Entity(name = "card_document_xref")
public class PersonCardDocumentEntity {

    public PersonCardDocumentEntity() {}
    public PersonCardDocumentEntity(Long person_card_id, Long document_id) {
        this.person_card_id = person_card_id;
        this.document_id = document_id;
    }

    @Id
    @Column(name = "person_card_id")
    private Long person_card_id;

    @Id
    @Column(name = "document_id")
    private Long document_id;

    public Long getPerson_card_id() {
        return person_card_id;
    }

    public void setPerson_card_id(Long person_card_id) {
        this.person_card_id = person_card_id;
    }

    public Long getDocument_id() {
        return document_id;
    }

    public void setDocument_id(Long document_id) {
        this.document_id = document_id;
    }
}