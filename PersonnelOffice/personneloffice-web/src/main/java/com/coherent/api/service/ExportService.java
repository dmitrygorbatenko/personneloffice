package com.coherent.api.service;


import com.coherent.api.dao.entity.PersonCardEntity;
import com.coherent.api.dao.entity.ReportEntity;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.Locale;

public interface ExportService {

    public File exportT2(PersonCardEntity personCardEntity, Locale locale,  ServletContext servletContext);

    public File exportReportMonth(ReportEntity reportEntity, ServletContext servletContext, Locale locale);
}