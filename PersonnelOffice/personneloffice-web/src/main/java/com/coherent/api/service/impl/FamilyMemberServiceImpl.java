package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.FamilyMemberEntity;
import com.coherent.api.dao.repository.FamilyMemberRepository;
import com.coherent.api.service.FamilyMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("familyMemberService")
public class FamilyMemberServiceImpl implements FamilyMemberService {

    @Autowired
    private FamilyMemberRepository familyMemberRepository;

    @Override
    public FamilyMemberEntity get(Long id) {
        return familyMemberRepository.findOne(id);
    }

    @Override
    public FamilyMemberEntity save(FamilyMemberEntity entity) {
        return familyMemberRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        familyMemberRepository.delete(id);
    }
}
