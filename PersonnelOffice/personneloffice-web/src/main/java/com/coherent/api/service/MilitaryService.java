package com.coherent.api.service;

import com.coherent.api.dao.entity.MilitaryEntity;

public interface MilitaryService {

    public MilitaryEntity save(MilitaryEntity entity);

}
