package com.coherent.api.controller;

import com.coherent.api.dao.entity.ReportEntity;
import com.coherent.api.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@Controller
@RequestMapping(value = "/report")
public class ReportController {

    @Autowired
    ReportService reportService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getAll(ModelMap modelMap) {
        modelMap.addAttribute("reports", reportService.getAll());
        modelMap.addAttribute("activeTab", "reports");
        return "person/reports";
    }

    @RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
    public String get(@PathVariable Long id, ModelMap modelMap) {
        if(id == 0 ){
            modelMap.addAttribute("report", new ReportEntity());
        } else {
            ReportEntity reportEntity =  reportService.get(id);
            modelMap.addAttribute("report", reportEntity);
        }
        return "person/dialogs/reports/reportDialog";
    }

    @RequestMapping(value = "/getReportBody", method = RequestMethod.POST)
    public String getReportBody(@ModelAttribute("report")ReportEntity reportEntity, ModelMap modelMap) {

    //save current report with current date and return current reportBody
        reportEntity.setLastUsingDate(new Date());

        modelMap.addAttribute("report", reportService.save(reportEntity));
        modelMap.addAttribute("reportBody", reportService.getMonthReport(reportEntity));
        modelMap.addAttribute("message", "success");

        return "person/dialogs/reports/reportBody";
    }

}
