package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.*;
import com.coherent.api.dao.enums.general.StatusEnum;
import com.coherent.api.dao.repository.AddressRepository;
import com.coherent.api.dao.repository.MilitaryRepository;
import com.coherent.api.dao.repository.PersonCardRepository;
import com.coherent.api.service.PersonCardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ExecutionException;

@Service("personCardService")
public class PersonCardServiceImpl implements PersonCardService {

    @Autowired
    private PersonCardRepository personRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private MilitaryRepository militaryRepository;

    @Override
    public PersonCardEntity get(Long id) {
        return personRepository.findOne(id);
    }

    @Override
    public List<PersonCardEntity> getAll() {
        return personRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        personRepository.delete(id);
    }

    @Override
    @Modifying
    public PersonCardEntity save(PersonCardEntity entity) {

        //now we can't
        if(entity.getId()!=null){
            PersonCardEntity oldEntity = personRepository.findOne(entity.getId());
            entity.setDocuments(oldEntity.getDocuments());
        }
        long id = personRepository.save(entity).getId();
        entity = personRepository.findOne(id);
        entity.setPersonCardNull();
        return entity;
    }

    @Override
    public List<PersonCardEntity> getFullNames() {
        return personRepository.getFullNames();
    }

    @Override
    public String getStatus(Long person_card_id) {
        return personRepository.getStatus(person_card_id).get(0).name();
    }

    //region #GetAll

    @Override
    public List<FamilyMemberEntity> getAllFamilyMembers(Long person_card_id) { return personRepository.getAllFamilyMembers(person_card_id); }

    //region #Documents

    @Override
    public List<DocumentEntity> getAllDocuments(Long person_card_id) { return personRepository.getAllDocuments(person_card_id); }

    @Override
    public List<DocumentEntity> getAllDismissals(Long person_card_id) { return personRepository.getAllDismissals(person_card_id); }

    //endregion

    @Override
    public List<AdditionalInfoEntity> getAllAdditionalInfoObjects(Long person_card_id) { return personRepository.getAllAdditionalInfoObjects(person_card_id); }

    //region EducationTabs

    @Override
    public List<EducationEntity> getAllEducationObjects(Long person_card_id) { return personRepository.getAllEducationObjects(person_card_id); }

    @Override
    public List<TrainingEntity> getAllTrainings(Long person_card_id) { return personRepository.getAllTrainings(person_card_id); }

    @Override
    public List<AttestationEntity> getAllAttestations(Long person_card_id) { return personRepository.getAllAttestations(person_card_id); }

    @Override
    public List<RetrainingEntity> getAllRetrainings(Long person_card_id) { return personRepository.getAllRetrainings(person_card_id); }

    //endregion
    @Override
    public List<VacationEntity> getAllVacations(Long person_card_id) { return personRepository.getAllVacations(person_card_id); }

    @Override
    public List<AssignmentEntity> getAllAssignments(Long person_card_id) { return personRepository.getAllAssignments(person_card_id);  }

    //endregion


}
