package com.coherent.api.util;

import com.coherent.api.dao.entity.*;
import com.coherent.api.dao.enums.assignment.*;
import com.coherent.api.dao.enums.documents.PaperTypeEnum;
import com.coherent.api.dao.enums.documents.SpeciesEnum;
import com.coherent.api.dao.enums.education.EducationEnum;
import com.coherent.api.dao.enums.family.KinshipEnum;
import com.coherent.api.dao.enums.general.GenderEnum;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Excel {
    private File fileXls;

    private final static int NAME = 0;
    private final static int NAME_TRANSlATE = 1;
    private final static int DAY = 2;
    private final static int MONTH = 3;
    private final static int YEAR = 4;
    private final static int TWO_YEAR_END = 5;
    private final static int CONTRACT_END = 6;
    private final static int RATE = 7;
    private final static int SEX = 8;
    private final static int ASSIGNMENT = 9;
    private final static int APPOINTMENT_DAY = 10;
    private final static int APPOINTMENT_MONTH = 11;
    private final static int APPOINTMENT_YEAR = 12;
    private final static int TRANSFER = 13;
    private final static int ORDER_APPOINTMENT = 14;
    private final static int CONTRACT = 15;
    private final static int EDUCATION_DEGREE = 16;
    private final static int BIRTH_DAY = 17;
    private final static int BIRTH_MONTH = 18;
    private final static int BIRTH_YEAR = 19;
    private final static int PHONE = 20;
    private final static int FAMILY = 21;
    private final static int NAME_LATIN = 22;
    private final static int PASSPORT_SERIES = 23;
    private final static int PASSPORT_ID_NUMBER = 24;
    private final static int PASSPORT_DATE_ISSUES = 25;
    private final static int PASSPORT_VALIDITY_PERIOD = 26;
    private final static int PASSPORT_ISSUED_BY = 27;
    private final static int REGISTRATION = 28;
    private final static int LAST_MONTH = 29;
    private final static int LAST_DAY = 30;
    private final static int LAST_YEAR = 31;
    private final static int NEXT_MONTH= 32;
    private final static int NEXT_DAY = 33;
    private final static int NEXT_YEAR = 34;

    private final static String delimiter = ",|//";

    private  final static String EDUCATION_HIGHER = "высшее";
    private  final static String EDUCATION_SECONDARY  =  "средне";
    private  final static String EDUCATION_SECONDARY_SPECIAL = "среднеспециальное";
    private  final static String EDUCATION_BASIC = "базовое";

    private String path;

    public File importExcel(PersonCardEntity personCardEntity, ServletContext servletContext) {
        List<PersonCardEntity> entityList = new ArrayList<>();
        entityList.add(personCardEntity);
        return importExcel(entityList, servletContext);
    }

    public File importExcel(List<PersonCardEntity> personCards, ServletContext servletContext){

        path = servletContext.getRealPath("WEB-INF/classes/documents") + "/";
        File cards = null;
        try(FileInputStream fileInputStream = new FileInputStream(new File(path + "template.xls"))){

            HSSFWorkbook workbook = new HSSFWorkbook(fileInputStream);
            HSSFSheet sheet = workbook.getSheetAt(0);
            int rownum = 2;
            for (PersonCardEntity personCard : personCards) {
               Row row = sheet.createRow(rownum++);
               setRow(row, personCard);
            }

            cards = new File(path + "cards " + getDayMonthYearString(Calendar.getInstance().getTime()) + ".xls");
            try(FileOutputStream fileOutputStream =new FileOutputStream(cards)) {
                workbook.write(fileOutputStream);
                fileOutputStream.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }finally {
            return cards;
        }
    }

    private void setRow(Row row, PersonCardEntity personCard) {

        row.createCell(NAME).setCellValue(personCard.getLastName() + " " + personCard.getFirstName() +
                " " + personCard.getPatronymic());
        Date dateContractEnd = getContractEnd(personCard);
        if(dateContractEnd != null)
            row.createCell(CONTRACT_END).setCellValue(dateContractEnd);
        else
            row.createCell(CONTRACT_END).setCellValue("");
        row.createCell(CONTRACT_END).setCellValue("");
        row.createCell(RATE).setCellValue(getRate(personCard));
        row.createCell(SEX).setCellValue(personCard.getGender() == GenderEnum.MALE ? "м" : "ж");
        row.createCell(ASSIGNMENT).setCellValue(getAssignments(personCard));
        String[] dayMonthYear = getDayMonthYearContractStart(personCard);
        if(dayMonthYear != null){
            row.createCell(APPOINTMENT_DAY).setCellValue(dayMonthYear[0]);
            row.createCell(APPOINTMENT_MONTH).setCellValue(dayMonthYear[1]);
            row.createCell(APPOINTMENT_YEAR).setCellValue(dayMonthYear[2]);
        }
        row.createCell(TRANSFER).setCellValue(getTransfer(personCard));
        row.createCell(ORDER_APPOINTMENT).setCellValue(getAppointment(personCard));
        row.createCell(CONTRACT).setCellValue(getContract(personCard));
        row.createCell(EDUCATION_DEGREE).setCellValue(getEducationDegree(personCard));
        dayMonthYear = getDayMonthYearBirthDay(personCard);
        if(dayMonthYear != null){
            row.createCell(BIRTH_DAY).setCellValue(dayMonthYear[0]);
            row.createCell(BIRTH_MONTH).setCellValue(dayMonthYear[1]);
            row.createCell(BIRTH_YEAR).setCellValue(dayMonthYear[2]);
        }
        row.createCell(PHONE).setCellValue(getPhones(personCard));
        row.createCell(FAMILY).setCellValue(getFamilyMembers(personCard));
        row.createCell(NAME_LATIN).setCellValue(personCard.getLatin());
        row.createCell(PASSPORT_SERIES).setCellValue(getPassportNumber(personCard));
        row.createCell(PASSPORT_ID_NUMBER).setCellValue(personCard.getDocumentIndividualNumber());
        row.createCell(PASSPORT_DATE_ISSUES).setCellValue(getDayMonthYearString(personCard.getDocumentDateIssuance()));
        row.createCell(PASSPORT_VALIDITY_PERIOD).setCellValue(getDayMonthYearString(personCard.getDocumentValidityPeriod()));
        row.createCell(PASSPORT_ISSUED_BY).setCellValue(personCard.getDocumentIssuedBy());
        row.createCell(REGISTRATION).setCellValue(getRegistrationAddress(personCard));

    }

    private String getRate(PersonCardEntity personCard) {

        return  personCard.getLastAssignment() != null ?
                personCard.getLastAssignment().getRate() : null;
    }

    private Date getContractEnd(PersonCardEntity personCard) {
        if(personCard.getLastAssignment() != null &&  personCard.getLastAssignment().getContractEnd() != null){
            return personCard.getLastAssignment().getContractEnd();
        }
        return null;
//        return (personCard.getLastAssignment() != null && personCard.getLastAssignment().getContractEnd() != null ?
//                personCard.getLastAssignment().getContractEnd() : null);
    }

    private String getRegistrationAddress(PersonCardEntity personCard) {
        StringBuilder registrationString = new StringBuilder("РБ, ");
        if (personCard.getRegistrationAddress() != null)
            registrationString.append(personCard.getRegistrationAddress().toString());
        return registrationString.deleteCharAt(registrationString.length()- 2).toString();
    }

    private String getPassportNumber(PersonCardEntity personCard) {
        return personCard.getDocumentSerialNumber() != null && personCard.getDocumentSeries() != null ?
                personCard.getDocumentSeries() + personCard.getDocumentSerialNumber()  : null;
    }


    private String getFamilyMembers(PersonCardEntity personCard) {
        StringBuilder familyString = new StringBuilder(" ");
        List<FamilyMemberEntity> familyMembers = personCard.getFamilyMembers();
        if(familyMembers != null){
            for(FamilyMemberEntity familyMember: familyMembers){
                if(familyMember.getKinship() == KinshipEnum.DAUGHTER || familyMember.getKinship() == KinshipEnum.SON) {
                    familyString.append(familyMember.getName()).append(" ")
                            .append(getDayMonthYearString(familyMember.getBirthday()))
                            .append(",");
                }
            }
        }
        return familyString.deleteCharAt(familyString.length()-1).toString();
    }

    private String getPhones(PersonCardEntity personCard) {
        AddressEntity address = personCard.getResidentialAddress();
        if(address != null)
            return address.getMobilePhone();
        return null;
    }

    private String getEducationDegree(PersonCardEntity personCard) {
        if(personCard.getEducationDegree()!=null) {
            switch (personCard.getEducationDegree()) {
                case HIGHER:
                    return EDUCATION_HIGHER;
                case BASIC:
                    return EDUCATION_BASIC;
                case SECONDARY:
                    return EDUCATION_SECONDARY;
                case SECONDARY_SPECIAL:
                    return EDUCATION_SECONDARY_SPECIAL;
            }
        }
        return null;
    }

    private String getContract(PersonCardEntity personCard) {
        AssignmentEntity assignment = personCard.getLastAssignment();
        if(assignment != null){
            FormEnum form = assignment.getForm();
            if(form == FormEnum.D)
                return "д";
            if(form == FormEnum.K)
                return "к";
        }
        return null;
    }

    private String getAppointment(PersonCardEntity personCard) {

        StringBuilder transfer =  new StringBuilder("");;
            List<AssignmentEntity> assignments = personCard.getAssignments();
        if(assignments != null && !assignments.isEmpty()){
            AssignmentEntity assignment = assignments.get(0);
            if(assignment.getOrderDate()!=null && assignment.getOrderNumber()!=null)
               transfer.append(getDayMonthYearString(assignment.getOrderDate())).append(" ").
                       append(assignment.getOrderNumber());
        }
        return transfer.toString();
    }

    private String getDayMonthYearString(Date date){
        if(date != null){
            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
            return sdf.format(date);
        }
        return null;
    }

    private String getTransfer(PersonCardEntity personCard) {

        List<AssignmentEntity> assignments = personCard.getAssignments();
        StringBuilder transfer = new StringBuilder(" ");
        if(assignments != null){
            int i=0;
            for(AssignmentEntity assignment: assignments){
                if(i!=0){
                    if(assignment.getOrderDate() != null && assignment.getOrderNumber()!=null && !assignment.getOrderNumber().isEmpty()) {
                        transfer.append(assignment.getOrderDate()).append(" ").
                                append(assignment.getOrderNumber()).append(",");
                    }
                }
                i++;
            }
            transfer.deleteCharAt(transfer.length()-1);
        }

        return transfer.toString();
    }

    private String[] getDayMonthYearBirthDay(PersonCardEntity personCard) {
        String[] dayMonthYear = null;
        Date birthday = personCard.getBirthday();
        if(birthday != null){
            dayMonthYear = getDayMonthYearString(birthday).split("\\.");
        }
        return dayMonthYear;
    }

    private String[] getDayMonthYearContractStart(PersonCardEntity personCard) {
        String[] dayMonthYear = null;
        Date contractStart = null;
        AssignmentEntity assignment = personCard.getLastAssignment();
        if(assignment != null){
            contractStart = assignment.getContractStart();
            if(contractStart != null) {
                dayMonthYear = getDayMonthYearString(contractStart).split("\\.");
            }
        }
        return dayMonthYear;
    }

    private String getAssignments(PersonCardEntity personCard) {
        StringBuilder assignments = new StringBuilder("  ");
        for(AssignmentEntity assignment: personCard.getAssignments()){
            if(assignment.getPosition()!=null)
                assignments.append(positionFromEnumToString(assignment.getPosition())).append(",").append(" ");
        }
        assignments.deleteCharAt(assignments.length()-2);
        return assignments.toString();
    }

    private String positionFromEnumToString(PositionEnum position) {
        switch (position){
            case SENIOR_SOFTWARE_ENGINEER: return "вед. инж-прогр";
            case SOFTWARE_ENGINEER: return "инж-прогр";
            case INFORMATION_TECHNOLOGY_MANAGER: return "мен. по ИТ";
            case HR_MANAGER: return "мен. по кадрам";
            case LEAD_HR_MANAGER: return "вед. мен. по перс";
            case HEAD_HR_MANAGER: return "нач. отдела управл. перс.";
            case TESTER_SOFTWARE: return "тест. по";
            case SYSTEM_ARCHITECT: return "систем арх-р";
            case SUB_CHIEF_ACCOUNTANT: return "зам. глав. бух.";
            case CHIEF_ACCOUNTANT: return "глав. бух.";
            case LEAD_MARKETING_SPECIALIST: return "вед. спец. по марк.";
            case HEAD_OF_MARKETING_DEPARTMENT: return "нач. отд. марк.";
            case HEAD_TRAIN_CENTER: return "рук. тренинг центра";
            case INTERPRETER: return "референт-пер";
            case BRAND_MANAGER: return "брэнд-мен";
            case DATABASE_ADMINISTRATOR: return "админ. баз данных";
            case THE_SERVER_ADMINISTRATOR: return "админ. cерв.";
            case ADMINISTRATOR: return "администратор";
            case BUSINESS_ANALYST: return "бизнес-аналитик";
            case SUB_GENERAL_DIRECTOR_PUBLIC_QUESTIONS: return "зам.ген дир. по общ. вопр.";
            case SENIOR_TESTER_SOFTWARE: return "вед. спец. по тест ПО";
            case SPECIALIST_IN_VED: return "спец. по вэд";
            case GENERAL_DIRECTOR: return "ген. дир.";
            case DIRECTOR_EXTERNAL_ECONOMIC: return "дир. по внешнеэк деят-ти";
            case DIRECTOR_QUALITY: return "дир. по качеству";
            case SUB_GENERAL_DIRECTOR: return "зам. ген. дир.";
            case SUB_GENERAL_DIRECTOR_FINANCES_QUESTIONS: return "зам. ген. дир. по фин. вопр";
            case REDACTOR_INTERNET_RESOURCES: return "редактр интернет-ресурса";
            case HEAD_OF_THE_GENERAL_DEPARTMENT: return "нач. общего отд.";
            case SYSTEM_ADMINISTRATOR: return "сис. админ";
            case LEAD_SYSTEM_ADMINISTRATOR: return "вед. сис. админ";
            case HEAD_TECHNICAL_TRAINING: return "нач. отдела тех. под-ки";
            case SUB_GENERAL_DIRECTOR_INFORMATION_TECHNOLOGY: return "зам. ген. дир. по ит";
            case MARKETING_SPECIALIST: return "спец. по марк.";
            case SPECIALIST_IN_TESTING: return "спец. по тестир";
            case SPECIALIST_IN_COMPUTER_GRAPHICS: return "спец. по компьют. график.";
            case HR_SPECIALIST: return "спец по кадр";
            case TECHNICAL: return "техник";
            case CLEANER_FACILITIES: return "уборщик помещений";
            case CLEANER: return "уборщица";
            default: return null;
        }
    }

    private Date getContractDate(AssignmentEntity assignmentEntity) {
        return assignmentEntity != null ? assignmentEntity.getContractEnd() : null;
    }

    public List<PersonCardEntity> exportExcel(MultipartFile fileXls){
        List<PersonCardEntity> personCardEntities = new ArrayList<>();
        try{
            POIFSFileSystem fileSystem = new POIFSFileSystem(fileXls.getInputStream());
            HSSFWorkbook wb = new HSSFWorkbook(fileSystem);
            HSSFSheet sheet = wb.getSheetAt(0);
            Row row;
            int rows = sheet.getPhysicalNumberOfRows();
            for(int i = 2; i < rows; i++){
                row = sheet.getRow(i);
                if(row != null) {
                    personCardEntities.add(RowToPersonCardEntity(row));
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return personCardEntities;
    }


    private PersonCardEntity RowToPersonCardEntity(Row row) {
        PersonCardEntity person = new PersonCardEntity();
        String[] lastFirstMiddleName = getLastFirstMiddleName(row.getCell(NAME));
        person.setLastName(lastFirstMiddleName[0]);
        person.setFirstName(lastFirstMiddleName[1]);
        person.setPatronymic(lastFirstMiddleName[2]);
        person.setBirthday(getBirthday(row));
        person.setLatin(getLatin(row));
        person.setRegistrationAddress(getRegistrationAddress(row));
        person.setResidentialAddress(getResidentialAddress(row));
        person.setGender(getGender(row));
        person.setEducationDegree(getEducationDegree(row));
        person.setFamilyMembers(getFamilyMembers(row));
        person.setAssignments(getAssignments(row));
        person.setDocuments(getDocuments(person));
        person.setDocument("PASSPORT");
        String[] passportSeries = getPassportSeries(row);
        person.setDocumentSeries(passportSeries[0]);
        person.setDocumentSerialNumber(passportSeries[1]);
        person.setDocumentIndividualNumber(getPassportIndividualNumber(row));
        person.setDocumentDateIssuance(getPassportDateIssuance(row));
        person.setDocumentValidityPeriod(getPassportValidityPeriod(row));
        person.setDocumentIssuedBy(getIssuedBy(row));
        return person;
    }

    private GenderEnum getGender(Row row) {
        return row.getCell(SEX).getStringCellValue().trim().compareToIgnoreCase("м") == 0 ? GenderEnum.MALE : GenderEnum.FEMALE;
    }

    private EducationEnum getEducationDegree(Row row) {
        Cell educationCell = row.getCell(EDUCATION_DEGREE);

        if(educationCell!=null && educationCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !educationCell.getStringCellValue().trim().isEmpty()){
            String education = educationCell.getStringCellValue();
            Pattern patternHigher = Pattern.compile("высшее");
            //Pattern patternHigh = Pattern.compile("среднеспец");
            if(patternHigher.matcher(education).find()) {
                return EducationEnum.HIGHER;
            }
        }
        return null;
    }

    private String getIssuedBy(Row row) {
        Cell issuedByCell = row.getCell(PASSPORT_ISSUED_BY);
        return issuedByCell!=null && issuedByCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !issuedByCell.getStringCellValue().trim().isEmpty() ?
                issuedByCell.getStringCellValue().trim() : null;

    }

    private Date getPassportValidityPeriod(Row row) {
        Cell dateCell = row.getCell(PASSPORT_VALIDITY_PERIOD);
        Date date = null;
        if(dateCell!=null && dateCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !dateCell.getStringCellValue().trim().isEmpty()){
            date = getDate(dateCell.getStringCellValue().trim());
        }
        return date;
    }

    private Date getPassportDateIssuance(Row row) {
        Cell dateCell = row.getCell(PASSPORT_DATE_ISSUES);
        Date date = null;
        if(dateCell!=null && dateCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !dateCell.getStringCellValue().trim().isEmpty()){
            date = getDate(dateCell.getStringCellValue().trim());
        }
        return date;
    }

    private String getPassportIndividualNumber(Row row) {
        Cell idCell = row.getCell(PASSPORT_ID_NUMBER);
        return idCell!=null && idCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !idCell.getStringCellValue().trim().isEmpty() ?
                idCell.getStringCellValue().trim() : null;
    }

    private String[] getPassportSeries(Row row) {
        String[] series = new String[2];
        Cell seriesCell = row.getCell(PASSPORT_SERIES);
        if(seriesCell!=null && seriesCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !seriesCell.getStringCellValue().trim().isEmpty()){
            series = seriesCell.getStringCellValue().split("((?<=\\d{7})|(?=\\d{7}))");
        }
        return series;
    }

    private List<DocumentEntity> getDocuments(PersonCardEntity person) {
        List<DocumentEntity> documents = new ArrayList<>();
//        Cell cellTransfer = row.getCell(TRANSFER);
//        Cell cellAssignment = row.getCell(ASSIGNMENT);
//        String[] transfer = cellTransfer != null && cellTransfer.getCellType() == HSSFCell.CELL_TYPE_STRING && !cellTransfer.getStringCellValue().isEmpty() ?
//                cellTransfer.getStringCellValue().split(delimiter) : new String[]{};
//        String[] positions = cellAssignment != null && cellAssignment.getCellType() == HSSFCell.CELL_TYPE_STRING && !cellAssignment.getStringCellValue().isEmpty() ?
//                cellAssignment.getStringCellValue().split(delimiter) : new String[]{};
        documents.addAll(getTransferDocuments(person));
        documents.add(getContractDocuments(person));
        return documents;
    }

    private List<DocumentEntity> getTransferDocuments(PersonCardEntity person) {
        List<DocumentEntity> documents =  new ArrayList<>();
        for(int i=0; i < person.getAssignments().size()-1; i++){
            DocumentEntity document = new DocumentEntity();
            document.setPaperType(PaperTypeEnum.ORDER);
            document.setPaperDate(person.getAssignments().get(i).getOrderDate());
            document.setPaperNumber(person.getAssignments().get(i).getOrderNumber());
            document.setSpecies(SpeciesEnum.TRANSFER);
            List<PersonCardEntity> persons = new ArrayList<>();
            persons.add(person);
            //document.setPersonCards(persons);
        }
        return documents;
    }

    private DocumentEntity getContractDocuments(PersonCardEntity person) {
        AssignmentEntity assignment = person.getAssignments().get(0);
        List<DocumentEntity> documents =  new ArrayList<>();
        DocumentEntity document = new DocumentEntity();
        document.setPaperType(PaperTypeEnum.ORDER);
        document.setPaperDate(assignment.getOrderDate());
        document.setPaperNumber(assignment.getOrderNumber());
        document.setSpecies(SpeciesEnum.WORKER);
        List<PersonCardEntity> persons = new ArrayList<>();
        persons.add(person);
        //document.setPersonCards(persons);
        return document;
    }

    private AddressEntity getResidentialAddress(Row row) {
        AddressEntity address = null;
        List<String> phones = getPhones(row);
        if(phones!=null){
            address = new AddressEntity();
            for (String phone: phones){
                if(phone.matches("^((29)|(44)|(33)|(25))\\d+"))
                    address.setMobilePhone(phone);
                else
                    address.setPhone(phone);
            }
        }
        return address;
    }

    private List<String> getPhones(Row row){
        Cell phoneCell = row.getCell(PHONE);
        List<String> phones = null;
        if(phoneCell != null && phoneCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !phoneCell.getStringCellValue().trim().isEmpty()){
            phones = new ArrayList<>();
            String[] arrayPhones = phoneCell.getStringCellValue().split(delimiter);
            for(String phone: arrayPhones){
                String phoneForAddToList=phone.replaceAll("\\s|-","").trim();
                phoneForAddToList = phoneForAddToList.matches("^(\\+?375)\\d+") ? phoneForAddToList.replaceFirst("375","") : phoneForAddToList;
                phones.add(phoneForAddToList);
            }
        }
        return phones;
    }


    private AddressEntity getRegistrationAddress(Row row) {
        Cell registrationCell = row.getCell(REGISTRATION);
        AddressEntity address = null;
        if(registrationCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !registrationCell.getStringCellValue().isEmpty()){

            String[] elementsAddress = registrationCell.getStringCellValue().split(delimiter);
            //address.setLocality(elementsAddress[0].trim());
            if(elementsAddress.length > 1)
                address = new AddressEntity();
            for(int i=1; i<elementsAddress.length; i++){
                if(elementsAddress[i].matches("\\s*"+stringToRegex(AddressEntity.POINTER_PROVINCE)+".*")){
                    address.setProvince(elementsAddress[i].replaceFirst(stringToRegex(AddressEntity.POINTER_PROVINCE),"").trim());
                }
                else{
                    if(elementsAddress[i].matches("\\s*"+stringToRegex(AddressEntity.POINTER_REGION)+".*")){
                        address.setRegion(elementsAddress[i].replaceFirst(stringToRegex(AddressEntity.POINTER_REGION), "").trim());
                    }
                    else{
                        if(elementsAddress[i].matches("\\s*"+stringToRegex(AddressEntity.POINTER_STREET)+".*")){
                            address.setStreet(elementsAddress[i].replaceFirst(stringToRegex(AddressEntity.POINTER_STREET), "").trim());
                        }
                        else{
                            if(elementsAddress[i].matches("\\s*"+stringToRegex(AddressEntity.POINTER_HOUSE)+".*")){
                                address.setHouse(new Integer(elementsAddress[i].replaceFirst(stringToRegex(AddressEntity.POINTER_HOUSE), "").trim()));
                            }
                            else{
                                if(elementsAddress[i].matches("\\s*"+stringToRegex(AddressEntity.POINTER_APARTMENT)+".*")){
                                    address.setApartment(new Integer(elementsAddress[i].replaceFirst(stringToRegex(AddressEntity.POINTER_APARTMENT), "").trim()));
                                }
                                else{
                                    if(elementsAddress[i].matches("\\s*"+stringToRegex(AddressEntity.POINTER_BUILDING)+".*")){
                                        address.setBuilding(elementsAddress[i].replaceFirst(stringToRegex(AddressEntity.POINTER_BUILDING), "").trim());
                                    }
                                    else{
                                        address.setLocality(elementsAddress[i].trim());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return address;
    }

    private Date getBirthday(Row row) {
        Cell dayCell = row.getCell(BIRTH_DAY);
        Cell monthCell = row.getCell(BIRTH_MONTH);
        Cell yearCell = row.getCell(BIRTH_YEAR);
        if(dayCell != null && monthCell != null && yearCell !=null &&
                dayCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC &&
                monthCell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC &&
                yearCell.getCellType()== HSSFCell.CELL_TYPE_NUMERIC){
            int day = ((int) dayCell.getNumericCellValue());
            int month = ((int) monthCell.getNumericCellValue());
            int year = ((int) yearCell.getNumericCellValue());
            return getDate(day,month,year,1900);
        }
        return null;
    }


    private String getLatin(Row row) {
        if(row.getCell(NAME_LATIN)!=null && row.getCell(NAME_LATIN).getCellType() == HSSFCell.CELL_TYPE_STRING ){
            return row.getCell(NAME_LATIN).getStringCellValue();
        }
//        else {
//            if(row.getCell(NAME_TRANSlATE).getCellType() == HSSFCell.CELL_TYPE_STRING && !row.getCell(NAME_TRANSlATE).getStringCellValue().trim().isEmpty()){
//                return row.getCell(NAME_TRANSlATE).getStringCellValue();
//            }
//        }
        return null;
    }

    private List<AssignmentEntity> getAssignments(Row row) {
        List<AssignmentEntity> assignments = null;
        Cell cellAssignment = row.getCell(ASSIGNMENT);
        Cell cellTransfer = row.getCell(TRANSFER);
        if(cellAssignment.getCellType() == HSSFCell.CELL_TYPE_STRING){
            assignments = new ArrayList<AssignmentEntity>();
            String[] transfer = getTransfer(cellTransfer);

            String[] positions = cellAssignment.getStringCellValue().trim().split(delimiter);
            for(int i=0; i < transfer.length+1; i++){
                AssignmentEntity assignment = new AssignmentEntity();
                assignment = new AssignmentEntity();
                if(positions.length-1-i>=0)
                    assignment.setPosition(positionFromStringToEnum(positions[positions.length-1-i]));
                if(i==transfer.length){
                    String order = row.getCell(ORDER_APPOINTMENT).getStringCellValue();
                    assignment.setOrderDate(getDate(order));
                    assignment.setOrderNumber(getOrderNumber(order));
                    Cell contractCell = row.getCell(CONTRACT);
                    if(contractCell.getCellType() == HSSFCell.CELL_TYPE_STRING && !contractCell.getStringCellValue().isEmpty()){
                        switch (contractCell.getStringCellValue().trim()){
                            case "к": assignment.setForm(FormEnum.K); break;
                            case "д": assignment.setForm(FormEnum.D); break;
                        }
                    }
                    assignment.setContractEnd(getContractEnd(row));
                    assignment.setContractStart(getContractStartDate(row));
                }
                else{
                    assignment.setOrderDate(getDate(transfer[transfer.length-1-i]));
                    assignment.setOrderNumber(getOrderNumber(transfer[transfer.length - 1 - i]));
                }
                if(i==0 && row.getCell(RATE)!=null){
                    assignment.setRate(getRate(row));
                }
                assignments.add(assignment);
            }
        }
        return assignments;
    }

    private String getRate(Row row) {
        Cell cell = row.getCell(RATE);
        if(cell.getCellType() == HSSFCell.CELL_TYPE_NUMERIC)
            return new Double(cell.getNumericCellValue()).toString();
        return cell.getStringCellValue();
    }

    private Date getContractEnd(Row row) {
        Date date = null;
        try {
            Cell cell = row.getCell(CONTRACT_END);
            if (cell != null && cell.getCellType() == HSSFCell.CELL_TYPE_STRING && !cell.getStringCellValue().trim().isEmpty())
                return cell.getDateCellValue();
        }
        finally {
            return date;
        }

    }

    private String[] getTransfer(Cell cell) {
        String[] transfer = new String[]{};
        if(cell.getCellType() == HSSFCell.CELL_TYPE_STRING && !cell.getStringCellValue().trim().isEmpty()){
            transfer = cell.getStringCellValue().split(delimiter);
        }
        return  transfer;
    }

    private String getOrderNumber(String s) {
        return s.replaceAll("от","").replaceAll("\\d{1,2}\\.\\d{1,2}\\.\\d{2,4}","").trim();
    }

    private PositionEnum positionFromStringToEnum(String position) {
        switch (position.trim().toLowerCase().replaceAll("\\.","")){
            case "вед инж-прогр": return PositionEnum.SENIOR_SOFTWARE_ENGINEER;
            case "ведущий инж-прогр": return PositionEnum.SENIOR_SOFTWARE_ENGINEER;
            case "вединж-прогр": return PositionEnum.SENIOR_SOFTWARE_ENGINEER;
            case "вед инжпрогр": return PositionEnum.SENIOR_SOFTWARE_ENGINEER;
            case "инж-прогр": return PositionEnum.SOFTWARE_ENGINEER;
            case "инжпрогр": return PositionEnum.SOFTWARE_ENGINEER;
            case "мен по ит": return PositionEnum.INFORMATION_TECHNOLOGY_MANAGER;
            case "мен по инф технологиям": return PositionEnum.INFORMATION_TECHNOLOGY_MANAGER;
            case "мен по кадрам": return PositionEnum.HR_MANAGER;
            case "мен по персон": return PositionEnum.HR_MANAGER;
            case "мен по персоналу": return PositionEnum.HR_MANAGER;
            case "вед мен по перс": return PositionEnum.LEAD_HR_MANAGER;
            case "нач отдела управл перс": return PositionEnum.HEAD_HR_MANAGER;
            case "тест по": return PositionEnum.TESTER_SOFTWARE;
            case "тестир по": return PositionEnum.TESTER_SOFTWARE;
            case "систем арх-р": return PositionEnum.SYSTEM_ARCHITECT;
            case "зам глав бух": return PositionEnum.SUB_CHIEF_ACCOUNTANT;
            case "глав бух": return PositionEnum.CHIEF_ACCOUNTANT;
            case "вед спец по марк": return PositionEnum.LEAD_MARKETING_SPECIALIST;
            case "нач отд маркетинга": return PositionEnum.HEAD_OF_MARKETING_DEPARTMENT;
            case "рук тренинг центра": return PositionEnum.HEAD_TRAIN_CENTER;
            case "референт-пер": return PositionEnum.INTERPRETER;
            case "референт-переводчик": return PositionEnum.INTERPRETER;
            case "брэнд-мен": return PositionEnum.BRAND_MANAGER;
            case "админ баз данных": return PositionEnum.DATABASE_ADMINISTRATOR;
            case "администр баз данных": return PositionEnum.DATABASE_ADMINISTRATOR;
            case "администр cерв": return PositionEnum.THE_SERVER_ADMINISTRATOR;
            case "админ cерв": return PositionEnum.THE_SERVER_ADMINISTRATOR;
            case "администратор": return  PositionEnum.ADMINISTRATOR;
            case "бизнес аналитик": return  PositionEnum.BUSINESS_ANALYST;
            case "бизнес-аналитик": return  PositionEnum.BUSINESS_ANALYST;
            case "замген дир по общ вопр": return PositionEnum.SUB_GENERAL_DIRECTOR_PUBLIC_QUESTIONS;
            case "зам ген дир по общ вопр": return PositionEnum.SUB_GENERAL_DIRECTOR_PUBLIC_QUESTIONS;
            case "вед спец по тест по": return  PositionEnum.SENIOR_TESTER_SOFTWARE;
            case "спец по вэд": return PositionEnum.SPECIALIST_IN_VED;
            case "ген дир": return PositionEnum.GENERAL_DIRECTOR;
            case "дир по внешнеэк деят-ти": return PositionEnum.DIRECTOR_EXTERNAL_ECONOMIC;
            case "дир по качеству": return PositionEnum.DIRECTOR_QUALITY;
            case "зам ген дир": return PositionEnum.SUB_GENERAL_DIRECTOR;
            case "зам ген дир по фин вопр": return PositionEnum.SUB_GENERAL_DIRECTOR_FINANCES_QUESTIONS;
            case "редакт интернет-ресурса": return PositionEnum.REDACTOR_INTERNET_RESOURCES;
            case "нач общего отд": return PositionEnum.HEAD_OF_THE_GENERAL_DEPARTMENT;
            case "сис админ": return PositionEnum.SYSTEM_ADMINISTRATOR;
            case "вед сис админ": return PositionEnum.LEAD_SYSTEM_ADMINISTRATOR;
            case "начотдела тех под-ки": return PositionEnum.HEAD_TECHNICAL_TRAINING;
            case "нач отдела тех под-ки": return PositionEnum.HEAD_TECHNICAL_TRAINING;
            case "замгендир по ит": return PositionEnum.SUB_GENERAL_DIRECTOR_INFORMATION_TECHNOLOGY;
            case "зам ген дир по ит": return PositionEnum.SUB_GENERAL_DIRECTOR_INFORMATION_TECHNOLOGY;
            case "спец по маркетингу": return PositionEnum.MARKETING_SPECIALIST;
            case "спец по марк": return PositionEnum.MARKETING_SPECIALIST;
            case "спец по маркет": return PositionEnum.MARKETING_SPECIALIST;
            case "спец по тестир": return PositionEnum.SPECIALIST_IN_TESTING;
            case "спец по компьют график": return PositionEnum.SPECIALIST_IN_COMPUTER_GRAPHICS;
            case "специалист по кадр": return PositionEnum.HR_SPECIALIST;
            case "спец по кадр": return PositionEnum.HR_SPECIALIST;
            case "техник": return PositionEnum.TECHNICAL;
            case "уборщик помещений": return PositionEnum.CLEANER_FACILITIES;
            case "уборщица": return PositionEnum.CLEANER;
            default: return null;
        }
    }


    private Date getContractStartDate(Row row) {
        Date date = null;
        try {
            int year = row.getCell(APPOINTMENT_YEAR).getNumericCellValue() == 0.0 ? -1
                    : (int) row.getCell(APPOINTMENT_YEAR).getNumericCellValue();
            int month = row.getCell(APPOINTMENT_MONTH).getNumericCellValue() == 0.0 ? -1
                    : (int) row.getCell(APPOINTMENT_MONTH).getNumericCellValue();
            int day = row.getCell(APPOINTMENT_DAY).getNumericCellValue() == 0.0 ? -1
                    : (int) row.getCell(APPOINTMENT_DAY).getNumericCellValue();
            if (year != -1 && month != -1 && day != -1) {
                date = getDate(day, month, year, 2000);
            }
        }finally {
            return date;
        }
    }

    private String[] getLastFirstMiddleName(Cell cell) {
        String[] parseName = cell.getStringCellValue().split(" ");
        if(parseName.length==3){
            return parseName;
        }
        else{
            String[] lastFirstMiddleName = new String[3];
            StringBuilder lastName = new StringBuilder("");
            for(int i = 0; i<parseName.length-2; i++){
                lastName.append(parseName[i]).append(" ");
            }
            lastFirstMiddleName[0] = lastName.toString().trim();
            lastFirstMiddleName[1] = parseName[parseName.length-2];
            lastFirstMiddleName[2] = parseName[parseName.length-1];
            return  lastFirstMiddleName;
        }
    }

    public List<FamilyMemberEntity> getFamilyMembers(Row row) {
        List<FamilyMemberEntity> familyMembers = null;

        if(row.getCell(FAMILY).getCellType() == HSSFCell.CELL_TYPE_STRING){
            String allFamilyString = row.getCell(FAMILY).getStringCellValue().trim();
            familyMembers = new LinkedList<FamilyMemberEntity>();
            if(!allFamilyString.trim().isEmpty()) {
                String[] delimiterFamily = allFamilyString.trim().split("(?<=\\d{2}\\.\\d{2}\\.\\d{4})");
                for (String personString : delimiterFamily) {
                    FamilyMemberEntity personEntity = new FamilyMemberEntity();
                    personEntity.setBirthday(getDate(personString));
                    personEntity.setName(personString.split("\\d{2}\\.\\d{2}\\.\\d{4}")[0].trim());
                    personEntity.setKinship(getKinship(personEntity.getName()));
                    familyMembers.add(personEntity);
                }
            }
        }
        return familyMembers;
    }

    private KinshipEnum getKinship(String personString) {

        if(personString.matches("(.+(овна|евна|ична|инична)$)|(дочь)"))
            return KinshipEnum.DAUGHTER;
        if(personString.matches("(.+(ович|евич|ич)$)|(сын)"))
            return KinshipEnum.SON;
        return null;

    }

    private Date getDate(String personString) {
        Pattern pattern = Pattern.compile("\\d{1,2}\\.\\d{1,2}\\.\\d{2,4}");
        Matcher matcher = pattern.matcher(personString);
        if(matcher.find()){
            String str =  personString.substring(matcher.start(),matcher.end());
            String []dayMonthYear = personString.substring(matcher.start(),matcher.end()).split("\\.");
            return getDate(new Integer(dayMonthYear[0]),new Integer(dayMonthYear[1]),new Integer(dayMonthYear[2]),2000);
        }
        //String []dayMonthYear = personString.split(personString.split("\\d{2}\\.\\d{2}\\.\\d{4}")[0])[1].split("\\.");
        return null;
    }

    private  Date getDate(int day, int month, int year, int century){
        year = year > century ? year : year+century;
        return new GregorianCalendar(year,month,day).getTime();
    }

    private String stringToRegex(String str){
        return str.replaceAll("\\.","\\\\.");
    }


}
