package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.FamilyMemberEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FamilyMemberRepository extends JpaRepository<FamilyMemberEntity, Long> {
}
