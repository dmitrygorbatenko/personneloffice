package com.coherent.api.dao.enums.general;

import com.coherent.api.dao.enums.BaseEnum;

public enum GenderEnum implements BaseEnum {
    MALE("gender.man"), FEMALE("gender.woman");

    private String value;

    private GenderEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
