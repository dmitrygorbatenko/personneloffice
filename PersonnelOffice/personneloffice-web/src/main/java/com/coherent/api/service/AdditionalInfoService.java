package com.coherent.api.service;

import com.coherent.api.dao.entity.AdditionalInfoEntity;

public interface AdditionalInfoService {

    AdditionalInfoEntity get(Long id);

    AdditionalInfoEntity save(AdditionalInfoEntity entity);

    void delete(Long id);
}