package com.coherent.api.dao.entity;

import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;

/**
 * Created by Maksimpilipeyko on 11/17/2014.
 */
@Entity(name = "authority")
public class AuthorityEntity implements GrantedAuthority {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "authority_id")
    private  int authorityId;

    @Column(name = "authority")
    private String authority;

    public String getAuthority() {
        return authority;
    }
    public void setAuthority(String authority) {
        this.authority=authority;
    }

    public int getAuthorityId() {
        return authorityId;
    }
    public void setAuthorityId(int authorityId) {
        this.authorityId=authorityId;
    }
}
