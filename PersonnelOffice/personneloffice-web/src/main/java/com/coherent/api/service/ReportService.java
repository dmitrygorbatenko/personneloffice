package com.coherent.api.service;

import com.coherent.api.dao.entity.MonthReportEntity;
import com.coherent.api.dao.entity.ReportEntity;

import java.util.List;
import java.util.Map;

public interface ReportService {

    public List<ReportEntity> getAll();

    public ReportEntity get(Long id);

    public ReportEntity save(ReportEntity reportEntity);

    List<MonthReportEntity> getMonthReport(ReportEntity reportEntity);
}

