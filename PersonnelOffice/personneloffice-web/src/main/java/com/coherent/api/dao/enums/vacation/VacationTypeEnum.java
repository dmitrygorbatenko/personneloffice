package com.coherent.api.dao.enums.vacation;

import com.coherent.api.dao.enums.BaseEnum;

public enum VacationTypeEnum implements BaseEnum {

    LABOR_COMPENSATION("vacation.type.labor_compensation"),
    CHILD_CARING("vacation.type.child_caring"),
    LABOR("vacation.type.labor"),
    PREGNANCY_AND_CHILDBIRTH("vacation.type.pregnancy_and_childbirth"),
    WITHOUT_PAY("vacation.type.without_pay"),
    MILITARY_TRAINING("vacation.type.military_training"),
    THREE_YEARS("vacation.type.three_years");

    private String value;

    private VacationTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
