package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.VacationEntity;
import com.coherent.api.dao.repository.VacationRepository;
import com.coherent.api.service.VacationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Service;

@Service("vacationService")
public class VacationServiceImpl implements VacationService {

    @Autowired
    VacationRepository vacationRepository;

    @Override
    @Modifying
    public VacationEntity save(VacationEntity entity) {
        return vacationRepository.save(entity);
    }

    @Override
    public VacationEntity get(Long id) {
        return vacationRepository.findOne(id);
    }

    @Override
    public void delete(Long id) {
        vacationRepository.delete(id);
    }
}
