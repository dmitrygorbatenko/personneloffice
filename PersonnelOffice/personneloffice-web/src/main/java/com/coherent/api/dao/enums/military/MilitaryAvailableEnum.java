package com.coherent.api.dao.enums.military;

import com.coherent.api.dao.enums.BaseEnum;

public enum MilitaryAvailableEnum implements BaseEnum {
    G("military.available.g"), NGM("military.available.ngm"), OG("military.available.og");
    private String value;

    private MilitaryAvailableEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}