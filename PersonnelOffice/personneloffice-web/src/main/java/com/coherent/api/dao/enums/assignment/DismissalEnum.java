package com.coherent.api.dao.enums.assignment;


import com.coherent.api.dao.enums.BaseEnum;

public enum DismissalEnum implements BaseEnum {

    AGREED_BY_THE_PARTIES("assignment.dismissal.agreed_by_the_parties"),
    EMPLOYEES_REQUEST("assignment.dismissal.employees_request"),
    ABSENTEEISM("assignment.dismissal.absenteeism"),
    CONTRACT_TERMINATION("assignment.dismissal.contract_termination");

    private String value;

    private DismissalEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
