package com.coherent.api.dao.entity;


import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "address")
public class AddressEntity {

    public final static String POINTER_PROVINCE="обл.";
    public final static String POINTER_REGION="р-н";
    public final static String POINTER_RURAL_COMMITTEE="сел.ком.";
    public final static String POINTER_LOCALITY="";
    public final static String POINTER_POSTCODE="индекс";
    public final static String POINTER_STREET="ул.";
    public final static String POINTER_HOUSE="д.";
    public final static String POINTER_BUILDING="корп.";
    public final static String POINTER_APARTMENT="кв.";

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Long id;
    @Column(name = "province")
    private String province;
    @Column(name = "region")
    private String region;
    @Column(name = "locality")
    private String locality;
    @Column(name = "rural_committee")
    private String ruralCommittee;
    @Column(name = "postcode")
    private String postcode;
    @Column(name = "street")
    private String street;
    @Column(name = "house")
    private Integer house;
    @Column(name = "building")
    private String building;
    @Column(name = "apartment")
    private Integer apartment;
    @Column(name = "phone")
    private String phone;
    @Column(name = "work_phone")
    private String workPhone;
    @Column(name = "mobile_phone")
    private String mobilePhone;
    @Column(name = "registration_until")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date registrationUntil;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getRuralCommittee() {
        return ruralCommittee;
    }

    public void setRuralCommittee(String ruralCommittee) {
        this.ruralCommittee = ruralCommittee;
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouse() {
        return house;
    }

    public void setHouse(Integer house) {
        this.house = house;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer apartment) {
        this.apartment = apartment;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getWorkPhone() {
        return workPhone;
    }

    public void setWorkPhone(String workPhone) {
        this.workPhone = workPhone;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public Date getRegistrationUntil() {
        return registrationUntil;
    }

    public void setRegistrationUntil(Date registrationUntil) {
        this.registrationUntil = registrationUntil;
    }

    @Override
    public String toString(){
        StringBuilder address = new StringBuilder(" ");
        if(province != null && !province.trim().isEmpty())
            address.append(POINTER_PROVINCE).append(" ").append(province).append(", ");
        if(region != null && !region.trim().isEmpty())
            address.append(POINTER_REGION).append(" ").append(region).append(", ");
        if(locality != null && !locality.trim().isEmpty())
            address.append(POINTER_LOCALITY).append(" ").append(locality).append(", ");
        if(street != null && !street.trim().isEmpty())
            address.append(POINTER_STREET).append(" ").append(street).append(", ");
        if(building != null && !building.trim().isEmpty())
            address.append(POINTER_BUILDING).append(" ").append(building).append(", ");
        if(house != null)
            address.append(POINTER_HOUSE).append(" ").append(house).append(", ");
        return address.toString();
    }



}
