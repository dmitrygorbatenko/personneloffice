package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Entity(name = "contract")
public class ContractEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "contract_id")
    private Long id;

    @Column(name= "type")
    private String type;

    @Column(name = "document_type" )
    private String documentType;

    @Column(name = "document_number")
    private String documentNumber;

    @Column(name = "start_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date startDate;

    @Column(name = "duration")
    private Integer duration;

    @Column(name = "probation")
    private Integer probation;

    @Column(name = "reason")
    private String reason;

    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    public Date getFinishDate(){
        Calendar calendar = new GregorianCalendar();
        calendar.setTime(startDate);
        calendar.add(Calendar.YEAR, 2);
        return calendar.getTime();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getProbation() {
        return probation;
    }

    public void setProbation(Integer probation) {
        this.probation = probation;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
