package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.ReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;

import javax.persistence.*;

public interface ReportRepository extends JpaRepository<ReportEntity, Long> {

    @Procedure(procedureName = "get_report_active_card_count_up_to_date", outputParameterName = "count")
    Integer getReportActiveCardCountUpToDate(@Param("year") int year, @Param("month") int month);

//    @Query("select * from TABLE(get_report_active_card_count_up_to_date (?, ?)")
//    Integer getReportActiveCardCountUpToDate(int year, int month);


}
