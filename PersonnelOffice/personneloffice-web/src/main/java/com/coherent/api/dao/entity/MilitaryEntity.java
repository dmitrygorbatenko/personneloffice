package com.coherent.api.dao.entity;

import com.coherent.api.controller.jsonSerializer.DateSerializer;
import com.coherent.api.dao.enums.military.*;
import com.coherent.api.util.Constants;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "military")
public class MilitaryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "military_id")
    private Long id;

    @Column(name = "registration_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date registrationDate;

    @Column(name = "ticket_number")
    private Integer ticketNumber;

    @Column(name = "category_stock")
    @Enumerated(EnumType.STRING)
    private CategoryStockEnum categoryStock;

    @Column(name = "group_record")
    @Enumerated(EnumType.STRING)
    private GroupRecordEnum groupRecord;

    @Column(name = "military_rank")
    @Enumerated(EnumType.STRING)
    private MilitaryRankEnum militaryRank;

    @Column(name = "consist")
    @Enumerated(EnumType.STRING)
    private ConsistEnum consist;

    @Column(name = "military_specialties")
    private String militarySpecialties;

    @Column(name = "military_service_available")
    @Enumerated(EnumType.STRING)
    private MilitaryAvailableEnum militaryAvailable;

    @Column(name = "recruitment_office")
    private String recruitmentOffice;

    @Column(name = "be_registered")
    private String beRegistered;

    @Column(name = "reserved")
    private Boolean reserved;

     @Column(name = "mobilization_prescription")
    private Boolean mobilizationPrescription;

    @Column(name = "swear_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date swearDate;

    @Column(name = "unit_swear_number")
    private Integer unitSwearNumber;

    @Column(name = "category_form")
    private String categoryForm;

    @Column(name = "remove_military_registration_date")
    @DateTimeFormat(pattern = Constants.DATE_FORMAT)
    @JsonSerialize(using = DateSerializer.class)
    private Date removeDate;

    @Column(name = "reason")
    @Enumerated(EnumType.STRING)
    private ReasonEnum reason;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public Integer getTicketNumber() {
        return ticketNumber;
    }

    public void setTicketNumber(Integer ticketNumber) {
        this.ticketNumber = ticketNumber;
    }

    public CategoryStockEnum getCategoryStock() {
        return categoryStock;
    }

    public void setCategoryStock(CategoryStockEnum categoryStock) {
        this.categoryStock = categoryStock;
    }

    public GroupRecordEnum getGroupRecord() {
        return groupRecord;
    }

    public void setGroupRecord(GroupRecordEnum groupRecord) {
        this.groupRecord = groupRecord;
    }

    public MilitaryRankEnum getMilitaryRank() {
        return militaryRank;
    }

    public void setMilitaryRank(MilitaryRankEnum militaryRank) {
        this.militaryRank = militaryRank;
    }

    public ConsistEnum getConsist() {
        return consist;
    }

    public void setConsist(ConsistEnum consist) {
        this.consist = consist;
    }

    public String getMilitarySpecialties() {
        return militarySpecialties;
    }

    public void setMilitarySpecialties(String militarySpecialties) {
        this.militarySpecialties = militarySpecialties;
    }

    public MilitaryAvailableEnum getMilitaryAvailable() {
        return militaryAvailable;
    }

    public void setMilitaryAvailable(MilitaryAvailableEnum militaryAvailable) {
        this.militaryAvailable = militaryAvailable;
    }

    public String getRecruitmentOffice() {
        return recruitmentOffice;
    }

    public void setRecruitmentOffice(String recruitmentOffice) {
        this.recruitmentOffice = recruitmentOffice;
    }

    public Boolean getReserved() {
        return reserved;
    }

    public void setReserved(Boolean reserved) {
        this.reserved = reserved;
    }

    public Boolean getMobilizationPrescription() {
        return mobilizationPrescription;
    }

    public void setMobilizationPrescription(Boolean mobilizationPrescription) {
        this.mobilizationPrescription = mobilizationPrescription;
    }

    public Date getSwearDate() {
        return swearDate;
    }

    public void setSwearDate(Date swearDate) {
        this.swearDate = swearDate;
    }

    public Integer getUnitSwearNumber() {
        return unitSwearNumber;
    }

    public void setUnitSwearNumber(Integer unitSwearNumber) {
        this.unitSwearNumber = unitSwearNumber;
    }

    public String getCategoryForm() {
        return categoryForm;
    }

    public void setCategoryForm(String categoryForm) {
        this.categoryForm = categoryForm;
    }

    public Date getRemoveDate() {
        return removeDate;
    }

    public void setRemoveDate(Date removeDate) {
        this.removeDate = removeDate;
    }

    public ReasonEnum getReason() {
        return reason;
    }

    public void setReason(ReasonEnum reason) {
        this.reason = reason;
    }

    public String getBeRegistered() {
        return beRegistered;
    }

    public void setBeRegistered(String beRegistered) {
        this.beRegistered = beRegistered;
    }
}