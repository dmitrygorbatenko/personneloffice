package com.coherent.api.dao.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "role")
public class RoleEntity {


    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "role_id")
    private Long id;

    @Column(name = "role")
    private String name;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name="role_authority",
            joinColumns={@JoinColumn(name="role_id", referencedColumnName="role_id")},
            inverseJoinColumns={@JoinColumn(name="authority_id", referencedColumnName="authority_id")})
    @Fetch(FetchMode.SELECT)
    private Set<AuthorityEntity> authorities;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<AuthorityEntity> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<AuthorityEntity> authorities) {
        this.authorities = authorities;
    }
}
