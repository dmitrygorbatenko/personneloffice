package com.coherent.api.dao.repository;

import com.coherent.api.dao.entity.PersonCardDocumentEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface PersonCardDocumentRepository extends JpaRepository<PersonCardDocumentEntity, Long> {

    @Query("SELECT cd.person_card_id FROM card_document_xref AS cd WHERE cd.document_id = (:document_id) ORDER BY cd.person_card_id")
    Long[] getAllCardIdsByDocumentId(@Param("document_id") Long document_id);
}