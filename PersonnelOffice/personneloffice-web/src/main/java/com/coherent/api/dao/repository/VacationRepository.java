package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.VacationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VacationRepository extends JpaRepository<VacationEntity, Long>{
}
