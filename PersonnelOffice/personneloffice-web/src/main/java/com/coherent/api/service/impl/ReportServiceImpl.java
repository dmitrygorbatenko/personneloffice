package com.coherent.api.service.impl;


import com.coherent.api.dao.entity.MonthReportEntity;
import com.coherent.api.dao.entity.ReportEntity;
import com.coherent.api.dao.enums.reports.MonthEnum;
import com.coherent.api.dao.repository.ReportRepository;
import com.coherent.api.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManagerFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("reportService")
public class ReportServiceImpl implements ReportService {

    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private Test test;

    @Override
    public List<ReportEntity> getAll() {
        return reportRepository.findAll();
    }

    @Override
    public ReportEntity get(Long id) {
        return reportRepository.findOne(id);
    }

    @Override
    public ReportEntity save(ReportEntity entity) {
        return reportRepository.save(entity);
    }

    @Override
    public List<MonthReportEntity> getMonthReport(ReportEntity reportEntity) {

        List<MonthReportEntity> monthReportEntities = new ArrayList<>();

        int year = Integer.parseInt(reportEntity.getSelectedYear());
        int month = reportEntity.getSelectedMonth().ordinal();

        MonthReportEntity monthReport = new MonthReportEntity();

        monthReport.setWorkersCount(test.get_report_active_card_count_up_to_date(year, 1));
        monthReport.setWoman(test.get_report_active_card_count_up_to_date_and_by_gender("FEMALE", year, 1));
        monthReportEntities.add(monthReport);

        for (int i = 1; i <= month+1; i++) {

            monthReport = new MonthReportEntity();
            monthReport.setMonthName(MonthEnum.get(i-1));

            monthReport.setWorkersCount(test.get_report_active_card_count_up_to_date(year, i+1));

            monthReport.setWoman(test.get_report_active_card_count_up_to_date_and_by_gender("FEMALE", year, i+1));

            monthReport.setHiredW(test.get_active_count_for_specific_gender_year_month("FEMALE", year, i));

            monthReport.setHiredM(test.get_active_count_for_specific_gender_year_month("MALE", year, i));

            monthReport.setDismissedW(test.get_dismissed_count_for_specific_gender_year_month("FEMALE", year, i));

            monthReport.setDismissedM(test.get_dismissed_count_for_specific_gender_year_month("MALE", year, i));

            monthReportEntities.add(monthReport);
        }

        return monthReportEntities;
    }


}
