package com.coherent.api.service;


import com.coherent.api.dao.entity.EducationEntity;

public interface EducationService {

    public EducationEntity save(EducationEntity entity);

    public EducationEntity get(Long id);

    void delete(Long id);

}
