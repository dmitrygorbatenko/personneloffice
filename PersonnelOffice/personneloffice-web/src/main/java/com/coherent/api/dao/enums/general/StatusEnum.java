package com.coherent.api.dao.enums.general;


import com.coherent.api.dao.enums.BaseEnum;

public enum StatusEnum  implements BaseEnum {

    CANDIDATE("status.candidate"), CONTRACTOR("status.contractor"),DISMISSED("status.dismissed"), WORKER("status.worker");

    private String value;
    StatusEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
