package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.DocumentEntity;
import com.coherent.api.dao.repository.DocumentRepository;
import com.coherent.api.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("documentService")
public class DocumentServiceImpl implements DocumentService {

    @Autowired
    private DocumentRepository documentRepository;

    @Override
    public DocumentEntity get(Long id) {
        return documentRepository.findOne(id);
    }

    @Override
    public DocumentEntity save(DocumentEntity entity) {
        return documentRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        documentRepository.delete(id);
    }

    @Override
    public Long[] getCardIdList(Long document_id) { return documentRepository.getCardIdList(document_id); }

    public List<DocumentEntity> getAll() { return documentRepository.findAll(); }
}
