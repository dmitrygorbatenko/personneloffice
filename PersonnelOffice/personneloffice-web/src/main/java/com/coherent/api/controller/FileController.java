package com.coherent.api.controller;

import com.coherent.api.dao.entity.FileEntity;
import com.coherent.api.dao.entity.PersonCardEntity;
import com.coherent.api.dao.entity.ReportEntity;
import com.coherent.api.dao.enums.reports.MonthEnum;
import com.coherent.api.service.*;
import com.coherent.api.util.Excel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;


@Controller
@RequestMapping(value = "/file")
public class FileController {

    @Autowired
    private ExportService exportService;

    @Autowired
    private PersonCardService personCardService;

    @Autowired
    private FileService fileService;

    @Autowired
    private ReportService reportService;


    @RequestMapping(value = "/export/excel/T2/{id}", method = RequestMethod.GET)
    @ResponseBody
    public FileSystemResource exportPersonToExcel(
            @PathVariable Long id, Locale locale,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException {

        File personT2 = exportService.exportT2(personCardService.get(id), locale, request.getServletContext());
        response.setContentType("application/vnd.sealed-xls");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + personT2.getName() + "\"");
        return new FileSystemResource(personT2);
    }

    @RequestMapping(value = "/export/excel/cards/{id}", method = RequestMethod.GET)
    @ResponseBody
    public FileSystemResource exportCardsToExcel(
            @PathVariable Long id,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException {
        Excel excel = new Excel();
        File cards = null;
        if(id == 0)
            cards = excel.importExcel(personCardService.getAll(), request.getServletContext());
        else{
            cards = excel.importExcel(personCardService.get(id), request.getServletContext());
        }
        response.setContentType("application/vnd.sealed-xls");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + cards.getName() + "\"");
        return new FileSystemResource(cards);
    }

    @RequestMapping(value = "/import/excel/cards", method = RequestMethod.POST)
    public String importExcel(@RequestParam("fileXls") MultipartFile fileXls) throws IOException {
        Excel excel = new Excel();
        for(PersonCardEntity entity: excel.exportExcel(fileXls)){
            personCardService.save(entity);
        }

        return "redirect:/persons/list";
    }

    @RequestMapping(value = "doc/save", method = RequestMethod.POST)
    @ResponseBody
    public Map<String, Object> saveFile(@ModelAttribute("file") FileEntity file) {

        Map<String, Object> response = new HashMap<>();
        file = fileService.save(file);
        response.put("document", file);
        response.put("status", "success");
        return response;
    }

    @RequestMapping(value = "/export/excel/report/{id}/{selectedYear}/{selectedMonth}", method = RequestMethod.GET)
    @ResponseBody
    public FileSystemResource exportReportMonth(
            @PathVariable Long id,
            @PathVariable String selectedYear,
            @PathVariable MonthEnum selectedMonth,
            Locale locale,
            HttpServletResponse response,
            HttpServletRequest request) throws IOException {

        ReportEntity reportEntity = reportService.get(id);
        reportEntity.setSelectedMonth(selectedMonth);
        reportEntity.setSelectedYear(selectedYear);
        reportEntity.setLastUsingDate(new Date());

        reportEntity =  reportService.save(reportEntity);

        File reportMonth = exportService.exportReportMonth(reportEntity, request.getServletContext(), locale);
        response.setContentType("application/vnd.sealed.xls");
        response.setHeader("Content-Disposition", "attachment; filename=\"" + reportMonth.getName() + "\"");
        return new FileSystemResource(reportMonth);
    }

}
