package com.coherent.api.dao.enums.documents;


import com.coherent.api.dao.enums.BaseEnum;

public enum SubSpeciesEnum implements BaseEnum {

    CARING_FOR_A_CHILD("vacation.type.child_caring"),
    PREGNANCY_AND_CHILDBIRTH("vacation.type.pregnancy_and_childbirth"),
    WITHOUT_PAY("vacation.type.without_pay"),
    MILITARY_TRAINING("vacation.type.military_training"),
    REACHING_THE_AGE_OF_THREE("vacation.type.three_years");

    private final String value;

    SubSpeciesEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
