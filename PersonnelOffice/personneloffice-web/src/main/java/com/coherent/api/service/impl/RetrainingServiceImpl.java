package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.RetrainingEntity;
import com.coherent.api.dao.repository.RetrainingRepository;
import com.coherent.api.service.RetrainingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("retrainingService")
public class RetrainingServiceImpl implements RetrainingService {

    @Autowired
    private RetrainingRepository retrainingRepository;

    @Override
    public RetrainingEntity get(Long id) {
        return retrainingRepository.findOne(id);
    }

    @Override
    public RetrainingEntity save(RetrainingEntity entity) {
        return retrainingRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        retrainingRepository.delete(id);
    }
}
