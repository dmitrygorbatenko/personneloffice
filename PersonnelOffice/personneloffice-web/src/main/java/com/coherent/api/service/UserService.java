package com.coherent.api.service;


import com.coherent.api.dao.entity.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService{

    UserEntity get(Long id);

    List<UserEntity> getAll();

    void delete(Long id);

    UserEntity save(UserEntity entity);

    UserEntity getByUsername(String username);
}
