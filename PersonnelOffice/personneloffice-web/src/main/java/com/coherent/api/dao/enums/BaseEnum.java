package com.coherent.api.dao.enums;


public interface BaseEnum {

    String getValue();
    String name();
}
