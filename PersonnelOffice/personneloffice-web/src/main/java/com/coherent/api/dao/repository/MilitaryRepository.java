package com.coherent.api.dao.repository;


import com.coherent.api.dao.entity.MilitaryEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MilitaryRepository extends JpaRepository<MilitaryEntity, Long>{
}
