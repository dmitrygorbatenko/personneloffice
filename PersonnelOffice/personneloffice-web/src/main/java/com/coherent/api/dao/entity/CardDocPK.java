package com.coherent.api.dao.entity;

import java.io.Serializable;

public class CardDocPK implements Serializable {
    protected Long person_card_id;
    protected Long document_id;

    public CardDocPK() {}

    public CardDocPK(Long person_card_id, Long document_id) {
        this.person_card_id = person_card_id;
        this.document_id = document_id;
    }
}
