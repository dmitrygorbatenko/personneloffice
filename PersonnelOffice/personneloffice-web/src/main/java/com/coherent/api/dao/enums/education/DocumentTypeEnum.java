package com.coherent.api.dao.enums.education;

import com.coherent.api.dao.enums.BaseEnum;

public enum DocumentTypeEnum implements BaseEnum {

    EVIDENCE("education.document.type.evidence"),
    DIPLOMA("education.document.type.diploma"),
    CERTIFICATE("education.document.type.certificate"),
    ATTESTATE("education.document.type.attestate");

    private String value;

    private DocumentTypeEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
