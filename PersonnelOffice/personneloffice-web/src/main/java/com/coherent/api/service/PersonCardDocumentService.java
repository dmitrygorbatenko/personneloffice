package com.coherent.api.service;

import com.coherent.api.dao.entity.PersonCardDocumentEntity;

public interface PersonCardDocumentService {

    public Long[] getAllCardIdsByDocumentId(Long document_id);
    public PersonCardDocumentEntity save(PersonCardDocumentEntity entity);
    public void delete(PersonCardDocumentEntity entity);
}
