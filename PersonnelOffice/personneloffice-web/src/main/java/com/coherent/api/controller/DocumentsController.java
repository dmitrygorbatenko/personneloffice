package com.coherent.api.controller;

import com.coherent.api.dao.entity.DocumentEntity;
import com.coherent.api.service.DocumentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import java.util.List;


@Controller
@RequestMapping(value = "/documents")
public class DocumentsController {

    @Autowired
    private DocumentService documentService;

    @RequestMapping(value = "/list")
    public String getAll(ModelMap modelMap){
        List<DocumentEntity> lst = documentService.getAll();
        modelMap.addAttribute("documents", lst);
        modelMap.addAttribute("activeTab", "documents");
        return "/person/orders";
    }
}