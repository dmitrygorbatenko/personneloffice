package com.coherent.api.service;


import com.coherent.api.dao.entity.*;

import java.util.List;

public interface PersonCardService {


    public PersonCardEntity get(Long id);

    public List<PersonCardEntity> getAll();

    public void delete(Long id);

    public PersonCardEntity save(PersonCardEntity entity);

    public List<PersonCardEntity> getFullNames();

    public String getStatus(Long person_card_id);

    //region #GetAll

    public List<FamilyMemberEntity> getAllFamilyMembers(Long person_card_id);

    //region #Documents

    public List<DocumentEntity> getAllDocuments(Long person_card_id);

    public List<DocumentEntity> getAllDismissals(Long person_card_id);

    //endregion

    public List<AdditionalInfoEntity> getAllAdditionalInfoObjects(Long person_card_id);

    //region EducationTabs

    public List<EducationEntity> getAllEducationObjects(Long person_card_id);

    public List<TrainingEntity> getAllTrainings(Long person_card_id);

    public List<AttestationEntity> getAllAttestations(Long person_card_id);

    public List<RetrainingEntity> getAllRetrainings(Long person_card_id);

    //endregion

    public List<VacationEntity> getAllVacations(Long person_card_id);

    public List<AssignmentEntity> getAllAssignments(Long person_card_id);

    //endregion

}
