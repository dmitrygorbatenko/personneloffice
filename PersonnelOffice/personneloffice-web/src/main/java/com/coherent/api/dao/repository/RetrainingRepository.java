package com.coherent.api.dao.repository;

import com.coherent.api.dao.entity.RetrainingEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RetrainingRepository extends JpaRepository<RetrainingEntity, Long> {
}

