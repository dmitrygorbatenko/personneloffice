package com.coherent.api.dao.entity;


public class PreferenceEntity {

    private Long id;

    private int column;
    private String name;
    private boolean visible;
    private boolean searchable;

    public PreferenceEntity(int column, String name, boolean searchable, boolean visible) {
        this.column = column;
        this.name = name;
        this.visible = visible;
        this.searchable = searchable;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isSearchable() {
        return searchable;
    }

    public void setSearchable(boolean searchable) {
        this.searchable = searchable;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }
}
