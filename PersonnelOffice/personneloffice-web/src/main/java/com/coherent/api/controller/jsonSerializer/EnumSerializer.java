package com.coherent.api.controller.jsonSerializer;

import com.coherent.api.dao.enums.BaseEnum;
import org.codehaus.jackson.JsonGenerator;
import org.codehaus.jackson.JsonProcessingException;
import org.codehaus.jackson.map.JsonSerializer;
import org.codehaus.jackson.map.SerializerProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Component
public class EnumSerializer extends JsonSerializer<BaseEnum> {


    private static MessageSource messageSource;

    public EnumSerializer(){}

    @Autowired
    EnumSerializer(MessageSource messageSource){
        EnumSerializer.messageSource = messageSource;
    }



    @Override
    public void serialize(BaseEnum value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonProcessingException {
        Map<String, String> map = new HashMap<>();
        map.put("value",value.name());
        map.put("label",messageSource.getMessage(value.getValue(), null, LocaleContextHolder.getLocale()));
        jgen.writeObject(map);
    }
}
