package com.coherent.api.dao.enums.assignment;


import com.coherent.api.dao.enums.BaseEnum;

public enum FormEnum implements BaseEnum {
    K("assignment.form.k"),
    D("assignment.form.d");

    private String value;

    private FormEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
