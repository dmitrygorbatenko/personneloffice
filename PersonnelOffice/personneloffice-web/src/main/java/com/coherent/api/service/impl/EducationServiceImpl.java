package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.EducationEntity;
import com.coherent.api.dao.repository.EducationRepository;
import com.coherent.api.service.EducationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("educationService")
public class EducationServiceImpl implements EducationService {

    @Autowired
    private EducationRepository educationRepository;

    @Override
    public EducationEntity get(Long id) {
        return educationRepository.findOne(id);
    }

    @Override
    public EducationEntity save(EducationEntity entity) {
        return educationRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        educationRepository.delete(id);
    }
}
