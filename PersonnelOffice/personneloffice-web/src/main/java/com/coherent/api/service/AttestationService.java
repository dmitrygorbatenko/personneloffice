package com.coherent.api.service;


import com.coherent.api.dao.entity.AttestationEntity;

public interface AttestationService {

    public AttestationEntity save(AttestationEntity entity);

    public AttestationEntity get(Long id);

    void delete(Long id);

}
