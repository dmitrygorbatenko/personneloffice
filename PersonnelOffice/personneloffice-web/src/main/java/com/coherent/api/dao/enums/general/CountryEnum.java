package com.coherent.api.dao.enums.general;


import com.coherent.api.dao.enums.BaseEnum;

public enum CountryEnum implements BaseEnum {
    RUSSIA("country.russia"), UKRAINE("country.ukraine"), BELARUS("country.belarus");

    private String value;

    private CountryEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
