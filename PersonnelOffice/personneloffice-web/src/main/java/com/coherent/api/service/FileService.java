package com.coherent.api.service;
import com.coherent.api.dao.entity.FileEntity;

public interface FileService {

    public FileEntity save(FileEntity entity);

    public FileEntity get(Long id);

    void delete(Long id);

}

