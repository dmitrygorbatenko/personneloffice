package com.coherent.api.dao.enums.education;

import com.coherent.api.dao.enums.BaseEnum;

public enum EducationEnum implements BaseEnum {
    SECONDARY("education.secondary"), SECONDARY_SPECIAL("education.secondary_special"),  HIGHER("education.higher"), BASIC("education.basic");

    private String value;

    private EducationEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
