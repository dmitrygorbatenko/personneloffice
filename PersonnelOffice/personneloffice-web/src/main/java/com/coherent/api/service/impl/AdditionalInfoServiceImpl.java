package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.AdditionalInfoEntity;
import com.coherent.api.dao.repository.AdditionalInfoRepository;
import com.coherent.api.service.AdditionalInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("additionalInfoService")
public class AdditionalInfoServiceImpl implements AdditionalInfoService {

    @Autowired
    AdditionalInfoRepository additionalInfoRepository;

    @Override
    public AdditionalInfoEntity get(Long id) {
        return additionalInfoRepository.findOne(id);
    }

    @Override
    public AdditionalInfoEntity save(AdditionalInfoEntity entity) {
        return additionalInfoRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        additionalInfoRepository.delete(id);
    }
}