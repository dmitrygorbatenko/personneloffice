package com.coherent.api.dao.enums.military;

import com.coherent.api.dao.enums.BaseEnum;

public enum ConsistEnum implements BaseEnum {

    SOLDIERS_AND_SAILORS("military.consist.soldiers_and_sailors"),
    SERGEANTS_AND_SERGEANT("military.consist.sergeant_and_sergeants"),
    AND_WARRANT_OFFICERS("military.consist.and_warrant_officers"),
    JUNIOR_OFFICERS("military.consist.junior_officers"),
    SENIOR_OFFICERS("military.consist.senior_officers"),
    HEIGHT_OFFICERS("military.consist.height_officers");

    private String value;

    private ConsistEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
