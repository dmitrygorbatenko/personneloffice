package com.coherent.api.dao.repository;

import com.coherent.api.dao.entity.FileEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FileRepository extends JpaRepository<FileEntity, Long> {
}
