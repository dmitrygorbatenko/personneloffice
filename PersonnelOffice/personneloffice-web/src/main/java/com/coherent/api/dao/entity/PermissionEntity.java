package com.coherent.api.dao.entity;

import javax.persistence.*;

@Entity(name = "permission")
public class PermissionEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "permission_id")
    private Long id;

    @Column(name = "role_id")
    private Long roleId;

    @Column(name = "value")
    private int value;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
