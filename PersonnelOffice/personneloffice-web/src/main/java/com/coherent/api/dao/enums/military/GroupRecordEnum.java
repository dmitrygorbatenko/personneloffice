package com.coherent.api.dao.enums.military;

import com.coherent.api.dao.enums.BaseEnum;

public enum GroupRecordEnum implements BaseEnum {

    VS("military.group.record.vs"),
    KGB("military.group.record.kgb"),
    MVD("military.group.record.mvd"),
    OPS("military.group.record.ops"),
    MChS("military.group.record.mchs");

    private String value;

    private GroupRecordEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }
}
