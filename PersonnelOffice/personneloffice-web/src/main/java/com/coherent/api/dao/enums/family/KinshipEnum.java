package com.coherent.api.dao.enums.family;


import com.coherent.api.dao.enums.BaseEnum;

public enum KinshipEnum implements BaseEnum {

    WIFE("kinship.wife"), HUSBAND("kinship.husband"), SON("kinship.son"), DAUGHTER("kinship.daughter"), SISTER("kinship.sister"), BROTHER("kinship.brother");

    private final String value;

    KinshipEnum(String value){
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
