package com.coherent.api.dao.enums.general;


import com.coherent.api.dao.enums.BaseEnum;

public enum NationalityEnum implements BaseEnum {
    RUSSIAN("nationality.russian"), UKRAINIAN("nationality.ukrainian"), BELORUSSIAN("nationality.belorussian");

    private String value;

    private NationalityEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
