package com.coherent.api.service.impl;

import com.coherent.api.dao.entity.AttestationEntity;
import com.coherent.api.dao.repository.AttestationRepository;
import com.coherent.api.service.AttestationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("attestationService")
public class AttestationServiceImpl implements AttestationService {

    @Autowired
    private AttestationRepository attestationRepository;

    @Override
    public AttestationEntity get(Long id) {
        return attestationRepository.findOne(id);
    }

    @Override
    public AttestationEntity save(AttestationEntity entity) {
        return attestationRepository.save(entity);
    }

    @Override
    public void delete(Long id) {
        attestationRepository.delete(id);
    }
}
