package com.coherent.api.dao.enums.reports;

import com.coherent.api.dao.enums.BaseEnum;

public enum MonthEnum implements BaseEnum {

    JANUARY("reports.month.january"),
    FEBRUARY("reports.month.february"),
    MARCH("reports.month.march"),
    APRIL("reports.month.april"),
    MAY("reports.month.may"),
    JUNE("reports.month.june"),
    JULY("reports.month.july"),
    AUGUST("reports.month.august"),
    SEPTEMBER("reports.month.september"),
    OCTOBER("reports.month.october"),
    NOVEMBER("reports.month.november"),
    DECEMBER("reports.month.december");

    private String value;

    private MonthEnum(String value) {
        this.value = value;
    }
    public String getValue() {
        return value;
    }

    public static MonthEnum get(int i){
        return values()[i];
    }
}
