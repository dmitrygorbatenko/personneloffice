package com.coherent.api.service;

import com.coherent.api.dao.entity.FamilyMemberEntity;

public interface FamilyMemberService{

    FamilyMemberEntity get(Long id);

    FamilyMemberEntity save(FamilyMemberEntity entity);

    void delete(Long id);
}
